/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef VELORECMONITORS_VELOMONITORBASE_H 
#define VELORECMONITORS_VELOMONITORBASE_H 1

// Include files
// -------------
// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"

// from VeloDet
#include "VeloDet/DeVelo.h"

/** @class VeloMonitorBase VeloMonitorBase.h Velo/VeloMonitorBase.h
 *  
 *  Header file for the VELO reconstruction monitoring
 *  algorithm base class
 *
 *  @author Eduardo Rodrigues
 *  @date   2008-08-15
 */

namespace Velo
{

  class VeloMonitorBase : public GaudiHistoAlg {

  public: 

    /// Standard constructor
    VeloMonitorBase( const std::string& name, ISvcLocator* pSvcLocator );
    
    virtual ~VeloMonitorBase( ); ///< Destructor
    
    StatusCode initialize() override;    ///< Algorithm initialization
    StatusCode execute() override;    ///< Algorithm execution
    StatusCode finalize() override;    ///< Algorithm finalization

  protected:

    // pointer to the VELO detector element
    DeVelo* m_veloDet;

    // flag for DEBUG level
    bool m_debugLevel;

  private:

  };

} // end of Velo namespace

#endif // VELORECMONITORS_VELOMONITORBASE_H
