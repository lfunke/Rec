/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/**
 *  Algorithm to generate ntuples for measuring PV resolutions.
 *  The input track container is split in various ways to produce pairs of vertices.
 *  Use the various flags to steer the ntuple production according to the track splittings.
 */

// Include files

// from std
#include <string>
#include <vector>
#include <iterator>

// from Gaudi
#include "GaudiKernel/ToolHandle.h"

// from TrackEvent
#include "Event/Track.h"
#include "Event/MCVertex.h"
#include "Event/RecVertex.h"
#include "Event/ODIN.h"
#include "Event/L0DUReport.h"
#include "TrackInterfaces/IPVOfflineTool.h"

// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/RndmGenerators.h"

namespace Al {
  class Equations;
  class Residuals ;
}

/** @class PVResolution
 *
 *
 *  @author Marco Gersabeck
 *  @date   2010-05-04
 */

struct ElementHistos
{
  IHistogram1D* resh1 ;
  IHistogram1D* resfineh1 ;
  IHistogram1D* respullh1 ;
} ;

class PVResolution : public GaudiTupleAlg
{

public:
  /// Standard constructor
  PVResolution( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode finalize() override;    ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  /// Fill vertex information
  void fill_ntuplePV(std::vector<LHCb::RecVertex> outvec, std::string name);

  /// Fill vertex positions with MC truth comparison
  void fill_ntuplePVMC(std::vector<LHCb::RecVertex> outvec,
                       std::vector<LHCb::MCVertex* > mcp,
                       std::string name);

  /// Fill vertex position differences
  void fill_ntupleDiffPV(std::vector<LHCb::RecVertex> outvec1,
                         std::vector<LHCb::RecVertex> outvec2,
                         std::string name);


private:
  std::string m_trackLocation ;
  std::string m_MCVertexLocation ;

  double m_numSigma = 0.;
  double m_hitSigma = 0.;
  double m_maxdchisq = 0.;
  unsigned long m_maxiteration = 0;

  int m_runodin = 0;
  unsigned long long int m_eventodin = 0;
  int m_bunchid = 0;
  int m_bxtype = 0;
  double m_odinTime = 0;
  unsigned long long m_odinTime0 = 0;
  unsigned long long m_odinEvtTime = 0;
  bool m_firstOdin = 0;
  bool m_MC = false;
  bool m_fillIndividualSplits = false;
  bool m_fillAll = false;
  bool m_fillLR = false;
  bool m_fillUD = false;
  bool m_fillFB = false;
  bool m_fillRand = false;
  IRndmGenSvc* m_rsvc = nullptr;
  Rndm::Numbers m_rand;

  IPVOfflineTool* m_pvtool = nullptr;
} ;

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PVResolution )
namespace{
  /**
   * Split track container randomly in two containers of equal size
   * (or different by 1 track)
   */
  void splitRandomTracks( std::vector<LHCb::Track*> input,
			  std::vector<const LHCb::Track*> *outputA,
			  std::vector<const LHCb::Track*> *outputB,
			  Rndm::Numbers rand ) {
    unsigned int min_size = (unsigned int) floor( input.size() / 2. );
    unsigned int max_size = (unsigned int) input.size() - min_size;
    std::vector<LHCb::Track*>::iterator it_in = input.begin();
    for( ; it_in != input.end(); it_in++ ) {
      if ( ( outputA->size() < max_size ) && ( outputB->size() < max_size ) ) {
	if ( rand() < 0.5 ) outputA->push_back( *it_in );
	else outputB->push_back( *it_in );
      }
      else if ( outputA->size() < max_size ) outputA->push_back( *it_in );
      else outputB->push_back( *it_in );
    }
  }

  /// count the number of forward and backward tracks
  void countTracksFwdBack( const SmartRefVector<LHCb::Track> &input,
			   int & nForwd, int & nBackw ){
    nForwd = 0;
    nBackw = 0;
    for( auto t : input ){
      if( t.data()->checkFlag(LHCb::Track::Flags::Backward) ){
	++nBackw;
      }else{
	++nForwd;
      }
    }
  }
}

PVResolution::PVResolution( const std::string& name,
                              ISvcLocator* pSvcLocator)
  : GaudiTupleAlg( name , pSvcLocator ) {
  declareProperty("TrackLocation",
		  m_trackLocation = LHCb::TrackLocation::Default  );
  declareProperty("VertexLocation",
		  m_MCVertexLocation = LHCb::MCVertexLocation::Default );
  declareProperty("NumSigma", m_numSigma = 3 ) ;
  declareProperty("HitSigma", m_hitSigma = 1.4 ) ;
  declareProperty("MaxChiSq",m_maxdchisq=0.01);
  declareProperty("MaxIteration",m_maxiteration=10) ;
  declareProperty("WithMC",m_MC=false) ;
  declareProperty("FillIndividualSplits",m_fillIndividualSplits=false) ;
  declareProperty("FillAll",m_fillAll=true) ;
  declareProperty("FillLR",m_fillLR=true) ;
  declareProperty("FillUD",m_fillUD=false) ;
  declareProperty("FillFB",m_fillFB=false) ;
  declareProperty("FillRand",m_fillRand=true) ;
}


//========================================================================
// Initialization
//========================================================================

StatusCode PVResolution::initialize() {
  if ( !GaudiTupleAlg::initialize().isSuccess() ) return StatusCode::FAILURE ;

  m_pvtool =tool<IPVOfflineTool>("PVOfflineTool", this);
  m_firstOdin = true;
  m_rsvc = svc<IRndmGenSvc>( "RndmGenSvc", true );
  m_rand.initialize( m_rsvc , Rndm::Flat(0.,1.0) ).ignore();

  return StatusCode::SUCCESS;
}
//========================================================================
// Finalize
//========================================================================

StatusCode PVResolution::finalize()
{
  return  GaudiTupleAlg::finalize();  // must be called after all other actions
}


//========================================================================
// Main execution
//========================================================================


StatusCode PVResolution::execute()
{
  LHCb::Tracks* tracks = get<LHCb::Tracks>(m_trackLocation);
  LHCb::MCVertices* mcvertices = nullptr;
  if ( m_MC ) {
    mcvertices = get<LHCb::MCVertices>( m_MCVertexLocation );
    if ( !mcvertices ) m_MC = false;
  }

  std::vector< LHCb::MCVertex* > mcprimaries;
  if ( m_MC ) {
    for ( auto vertex : *mcvertices ) {
      if ( vertex->isPrimary() ) mcprimaries.push_back( vertex );
    }
  }

  LHCb::ODIN* odin = getIfExists<LHCb::ODIN> ( LHCb::ODINLocation::Default );
  if( odin ) {
    if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
      debug() << "Run "    << odin->runNumber()
              << ", Event " << odin->eventNumber() << endmsg;
    m_runodin=odin->runNumber();
    m_eventodin= odin->eventNumber();
    m_bunchid= odin->bunchId();
    m_bxtype= odin->bunchCrossingType();
    m_odinEvtTime = (long unsigned int) (odin->eventTime().ns() / 1e9);
    m_odinEvtTime -= 1259622000; // time in seconds since 01-Dec-09 00:00 CET
    if ( m_firstOdin ) {
      m_firstOdin = false;
      m_odinTime = 0.;
      m_odinTime0 = odin->gpsTime();
    }
    else {
      unsigned long itime = (unsigned long) (odin->gpsTime() - m_odinTime0);
      m_odinTime = 1e-6 * itime;
    }


  } else {
    Warning("No ODIN Header").ignore();
    m_runodin=0;
    m_eventodin=0;
    m_bunchid= 0;
    m_odinTime= 0.;
    m_bxtype=-1;
  }

  // copy tracks with VELO part to new const vector
  std::vector<const LHCb::Track*> trackvectorConst;
  copy_if(tracks->begin(), tracks->end(), back_inserter(trackvectorConst),
	  [](const LHCb::Track* track) { return track->hasVelo() ;});

  // copy tracks with VELO part to new vector
  std::vector<LHCb::Track*> trackvector;
  copy_if(tracks->begin(), tracks->end(), back_inserter(trackvector),
	  [](const LHCb::Track* track) { return track->hasVelo() ;});

  // left is dxdz > 0  (reversed if a backward track)
  std::vector<const LHCb::Track*> lefttracks;
  copy_if(trackvector.begin(),trackvector.end(),back_inserter(lefttracks),
	  [] (const LHCb::Track* track){
	    return (track->firstState().tx()*
		    (track->checkFlag(LHCb::Track::Flags::Backward)? -1 : 1 ))>0 ;
	  });
  // right is dxdz < 0  (reversed if a backward track)
  std::vector<const LHCb::Track*> righttracks;
  copy_if(trackvector.begin(),trackvector.end(),back_inserter(righttracks),
	  [] (const LHCb::Track* track){
	    return (track->firstState().tx()*
		    (track->checkFlag(LHCb::Track::Flags::Backward)? -1 : 1 ))<0 ;
	  });
  // up is dydz > 0  (reversed if a backward track)
  std::vector<const LHCb::Track*> uptracks;
  copy_if(trackvector.begin(),trackvector.end(),back_inserter(uptracks),
	  [] (const LHCb::Track* track){
	    return (track->firstState().ty()*
		    (track->checkFlag(LHCb::Track::Flags::Backward)? -1 : 1 ))>0 ;
	  });
  // down is dydz < 0  (reversed if a backward track)
  std::vector<const LHCb::Track*> downtracks;
  copy_if(trackvector.begin(),trackvector.end(),back_inserter(downtracks),
	  [] (const LHCb::Track* track){
	    return (track->firstState().ty()*
		    (track->checkFlag(LHCb::Track::Flags::Backward)? -1 : 1 ))<0 ;
	  });
  // forward is set as a not backward flag
  std::vector<const LHCb::Track*> forwtracks;
  copy_if(trackvector.begin(),trackvector.end(),back_inserter(forwtracks),
	  [] (const LHCb::Track* track){
	    return !(track->checkFlag(LHCb::Track::Flags::Backward));
	  });
  // backward is set as a flag
  std::vector<const LHCb::Track*> backwtracks;
  copy_if(trackvector.begin(),trackvector.end(),back_inserter(backwtracks),
	  [] (const LHCb::Track* track){
	    return (track->checkFlag(LHCb::Track::Flags::Backward));
	  });

  std::vector<const LHCb::Track*> randAtracks;
  std::vector<const LHCb::Track*> randBtracks;
  splitRandomTracks( trackvector, &randAtracks, &randBtracks, m_rand );

  std::vector<LHCb::RecVertex> outvtxvec;
  std::vector<LHCb::RecVertex> leftoutvtxvec;
  std::vector<LHCb::RecVertex> rightoutvtxvec;
  std::vector<LHCb::RecVertex> upoutvtxvec;
  std::vector<LHCb::RecVertex> downoutvtxvec;
  std::vector<LHCb::RecVertex> forwoutvtxvec;
  std::vector<LHCb::RecVertex> backwoutvtxvec;
  std::vector<LHCb::RecVertex> a_outvtxvec;
  std::vector<LHCb::RecVertex> b_outvtxvec;

  StatusCode scfit;
  if (trackvector.size()>2){
    scfit = m_pvtool->reconstructMultiPVFromTracks(trackvectorConst, outvtxvec);
    if ( m_fillAll ) {
      if ( !m_MC ) fill_ntuplePV(outvtxvec,"AllPV");
      else fill_ntuplePVMC(outvtxvec,mcprimaries,"AllPV");
    }
  }

  if (righttracks.size()>2){
    scfit = m_pvtool->reconstructMultiPVFromTracks(righttracks, rightoutvtxvec);
    if ( m_fillIndividualSplits ) fill_ntuplePV(rightoutvtxvec,"RightPV");
  }
  if (lefttracks.size()>2){
    scfit = m_pvtool->reconstructMultiPVFromTracks(lefttracks, leftoutvtxvec);
    if ( m_fillIndividualSplits ) fill_ntuplePV(leftoutvtxvec,"LeftPV");
  }
  if ( (righttracks.size()>2) && (lefttracks.size()>2) ) {
    if ( m_fillLR ) fill_ntupleDiffPV(leftoutvtxvec,rightoutvtxvec,"LeftRightPV");
  }
  if (uptracks.size()>2){
    scfit = m_pvtool->reconstructMultiPVFromTracks(uptracks, upoutvtxvec);
    if ( m_fillIndividualSplits ) fill_ntuplePV(upoutvtxvec,"UpPV");
  }
  if (downtracks.size()>2){
    scfit = m_pvtool->reconstructMultiPVFromTracks(downtracks, downoutvtxvec);
    if ( m_fillIndividualSplits ) fill_ntuplePV(downoutvtxvec,"DownPV");
  }
  if ( (downtracks.size()>2) && (uptracks.size()>2) ) {
    if ( m_fillUD ) fill_ntupleDiffPV(upoutvtxvec,downoutvtxvec,"UpDownPV");
  }
  if (forwtracks.size()>2){
    scfit = m_pvtool->reconstructMultiPVFromTracks(forwtracks, forwoutvtxvec);
    if ( m_fillIndividualSplits ) fill_ntuplePV(forwoutvtxvec,"ForwPV");
  }
  if (backwtracks.size()>2){
    scfit = m_pvtool->reconstructMultiPVFromTracks(backwtracks, backwoutvtxvec);
    if ( m_fillIndividualSplits ) fill_ntuplePV(backwoutvtxvec,"BackwPV");
  }
  if ( (backwtracks.size()>2) && (forwtracks.size()>2) ) {
    if ( m_fillFB ) fill_ntupleDiffPV(forwoutvtxvec,backwoutvtxvec,"ForwBackwPV");
  }
  if ( (randAtracks.size()>2) && (randBtracks.size()>2) ) {
    scfit = m_pvtool->reconstructMultiPVFromTracks(randAtracks, a_outvtxvec);
    scfit = m_pvtool->reconstructMultiPVFromTracks(randBtracks, b_outvtxvec);
    if ( m_fillRand ) fill_ntupleDiffPV(a_outvtxvec,b_outvtxvec,"RandPV");
  }

  return StatusCode::SUCCESS ;

}

/////////////////////////////////////////////////////

void PVResolution::fill_ntuplePV(std::vector<LHCb::RecVertex> outvec, std::string name)
{

  if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
    debug() << "Filling tuple name "    << name
	    << " title " << "PV info "+name << endmsg;
  Tuple tuple=nTuple(name, "PV info "+name);

  for( auto pv : outvec ){
    LHCb::L0DUReport* report = getIfExists<LHCb::L0DUReport>(LHCb::L0DUReportLocation::Default);
    if( report ) {
      if ( ! tuple->column( "L0Decision", report->decision() ) ) return;
      if (msgLevel(MSG::DEBUG)) debug() << "L0 decision:  " << report->decision() << endmsg;
      LHCb::L0DUChannel::Map channels = report->configuration()->channels();
      for(LHCb::L0DUChannel::Map::const_iterator it = channels.begin();
          it!=channels.end();it++){
        if ( ! tuple->column( "L0_"+(*it).first ,
                              report->channelDecision( ((*it).second)->id() )))
          return;
      }
    } else {
      Warning("Can't get LHCb::L0DUReportLocation::Default (" +
              LHCb::L0DUReportLocation::Default + ")" ).ignore();
      if ( ! tuple->column( "L0Decision", -1 ) ) return;
    }
    int nForwd,nBackw;
    countTracksFwdBack( pv.tracks(), nForwd, nBackw );
    const Gaudi::SymMatrix3x3 covMat = pv.covMatrix();
    std::vector< double > chi2PerDoF;
    std::vector< int > nVeloIDs;
    std::vector< double > momentum;
    std::vector< double > transverse_momentum;
    std::vector< double > eta;
    std::vector< double > phi;
    std::vector< LHCb::LHCbID >::const_iterator lbidit;
    int nLong = 0;
    for(auto track : pv.tracks()){
      chi2PerDoF.push_back( track->chi2PerDoF() );
      int veloIDcount =
	count_if(track->lhcbIDs().begin(),track->lhcbIDs().end(),
		 [](const LHCb::LHCbID &id) { return id.isVelo(); } );
      nVeloIDs.push_back( veloIDcount );
      eta.push_back( track->pseudoRapidity() );
      phi.push_back( track->phi() );
      if ( track->checkType( LHCb::Track::Types::Long ) ) {
        nLong++;
        momentum.push_back( track->p() );
        transverse_momentum.push_back( track->pt() );
      }
    }
    tuple->farray( "trChi2PerDoF", chi2PerDoF, "nTrChi2PerDoF", 500 );
    tuple->farray( "trNVeloIDs", nVeloIDs, "nTrNVeloIDs", 500 );
    tuple->farray( "trP", momentum, "nTrP", 500 );
    tuple->farray( "trPT", transverse_momentum, "nTrPT", 500 );
    tuple->farray( "trEta", eta, "nTrEta", 500 );
    tuple->farray( "trPhi", phi, "nTrPhi", 500 );
    tuple->column( "run",m_runodin);
    tuple->column( "evt",(unsigned long long) m_eventodin);
    tuple->column( "bunchid",m_bunchid);
    tuple->column( "bxtype",m_bxtype);
    tuple->column( "time",(unsigned long long) m_odinTime);
    tuple->column( "gpsTime",(unsigned long long) m_odinEvtTime);
    tuple->column( "trNum", (int)pv.tracks().size() );
    tuple->column( "trNumLong",nLong );
    tuple->column( "trNumForw", nForwd );
    tuple->column( "trNumBackw", nBackw );
    tuple->column( "chi2",pv.chi2() );
    tuple->column( "ndof",pv.nDoF() );
    tuple->column( "pvx", pv.position().x());
    tuple->column( "pvy", pv.position().y());
    tuple->column( "pvz", pv.position().z());
    tuple->column( "pvxe", sqrt(covMat.At(0,0)));
    tuple->column( "pvye", sqrt(covMat.At(1,1)));
    tuple->column( "pvze", sqrt(covMat.At(2,2)));
    tuple->column( "cov01", covMat.At(0,1));
    tuple->column( "cov02", covMat.At(0,2));
    tuple->column( "cov12", covMat.At(1,2));
    tuple->write();
  }
}

void PVResolution::fill_ntuplePVMC(std::vector<LHCb::RecVertex> outvec,
				   std::vector< LHCb::MCVertex* > mcvs,
				   std::string name)
{

  if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
    debug() << "Filling tuple with MC name "    << name
	    << " title " << "PV info "+name << endmsg;
  Tuple tuple=nTuple(name, "PV info "+name);

  for( auto pv : outvec){
    int nForwd,nBackw;
    countTracksFwdBack( pv.tracks(), nForwd, nBackw );
    const Gaudi::SymMatrix3x3 covMat = pv.covMatrix();
    std::vector< double > chi2PerDoF;
    std::vector< int > nVeloIDs;
    std::vector< double > momentum;
    std::vector< double > transverse_momentum;
    std::vector< double > eta;
    std::vector< double > phi;
    std::vector< LHCb::LHCbID >::const_iterator lbidit;
    int nLong = 0;
    for( auto pvt : pv.tracks() ) {
      chi2PerDoF.push_back( pvt->chi2PerDoF() );
      lbidit = pvt->lhcbIDs().begin();
      int veloIDcount = 0;
      for( ; lbidit != pvt->lhcbIDs().end(); lbidit++ ) {
        if ( lbidit->isVelo() ) veloIDcount++;
      }
      nVeloIDs.push_back( veloIDcount );
      eta.push_back( pvt->pseudoRapidity() );
      phi.push_back( pvt->phi() );
      if ( pvt->checkType( LHCb::Track::Types::Long ) ) {
        nLong++;
        momentum.push_back( pvt->p() );
        transverse_momentum.push_back( pvt->pt() );
      }
    }
    double maxdist = 9999.;
    LHCb::MCVertex* bestPV(nullptr);
    for( auto mcv : mcvs ) {
      double dist = (pv.position()-mcv->position()).R();
      if ( dist < maxdist ) {
        maxdist = dist;
        bestPV = mcv;
      }
    }
    tuple->farray( "trChi2PerDoF", chi2PerDoF, "nTrChi2PerDoF", 500 );
    tuple->farray( "trNVeloIDs", nVeloIDs, "nTrNVeloIDs", 500 );
    tuple->farray( "trP", momentum, "nTrP", 500 );
    tuple->farray( "trPT", transverse_momentum, "nTrPT", 500 );
    tuple->farray( "trEta", eta, "nTrEta", 500 );
    tuple->farray( "trPhi", phi, "nTrPhi", 500 );
    tuple->column( "run",m_runodin);
    tuple->column( "evt",(unsigned long long) m_eventodin);
    tuple->column( "bunchid",m_bunchid);
    tuple->column( "bxtype",m_bxtype);
    tuple->column( "time",m_odinTime);
    tuple->column( "gpsTime",(unsigned long long) m_odinEvtTime);
    tuple->column( "trNum", (int)pv.tracks().size() );
    tuple->column( "trNumLong",nLong );
    tuple->column( "trNumForw",nForwd );
    tuple->column( "trNumBackw",nBackw );
    tuple->column( "chi2",pv.chi2() );
    tuple->column( "ndof",pv.nDoF() );
    tuple->column( "pvx", pv.position().x());
    tuple->column( "pvy", pv.position().y());
    tuple->column( "pvz", pv.position().z());
    tuple->column( "nMCPrimaries", mcvs.size());
    tuple->column( "pvxMC", bestPV->position().x());
    tuple->column( "pvyMC", bestPV->position().y());
    tuple->column( "pvzMC", bestPV->position().z());
    tuple->column( "diff_pvx", pv.position().x()-bestPV->position().x());
    tuple->column( "diff_pvy", pv.position().y()-bestPV->position().y());
    tuple->column( "diff_pvz", pv.position().z()-bestPV->position().z());
    tuple->column( "pvxe", sqrt(covMat.At(0,0)));
    tuple->column( "pvye", sqrt(covMat.At(1,1)));
    tuple->column( "pvze", sqrt(covMat.At(2,2)));
    tuple->column( "cov01", covMat.At(0,1));
    tuple->column( "cov02", covMat.At(0,2));
    tuple->column( "cov12", covMat.At(1,2));
    tuple->write();
  }
}

void PVResolution::fill_ntupleDiffPV(std::vector<LHCb::RecVertex> outvec1,
				     std::vector<LHCb::RecVertex> outvec2,
				     std::string name)
{

  if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
    debug() << "Filling tuple name "    << name
	    << " title " << "PV diff info "+name << endmsg;
  Tuple tuple=nTuple(name, "PV diff info "+name);

  tuple->column( "run",m_runodin);
  tuple->column( "evt",(unsigned long long) m_eventodin);
  tuple->column( "bunchid",m_bunchid);
  tuple->column( "bxtype",m_bxtype);
  tuple->column( "time",m_odinTime);
  tuple->column( "gpsTime",(unsigned long long) m_odinEvtTime);
  tuple->column( "nPV_1",(unsigned long long) outvec1.size());
  tuple->column( "nPV_2",(unsigned long long) outvec2.size());
  if ( 1 == outvec1.size() ) {
    auto ipv1 = outvec1.begin() ;
    int nForwd,nBackw;
    countTracksFwdBack( ipv1->tracks(), nForwd, nBackw );
    const Gaudi::SymMatrix3x3 covMat1 = ipv1->covMatrix();
    std::vector< double > chi2PerDoF;
    std::vector< int > nVeloIDs;
    std::vector< double > momentum;
    std::vector< double > transverse_momentum;
    std::vector< double > eta;
    std::vector< double > phi;
    std::vector< LHCb::LHCbID >::const_iterator lbidit;
    int nLong = 0;
    for( auto track : ipv1->tracks() ) {
      chi2PerDoF.push_back( track->chi2PerDoF() );
      lbidit = track->lhcbIDs().begin();
      int veloIDcount = 0;
      for( ; lbidit != track->lhcbIDs().end(); lbidit++ ) {
        if ( lbidit->isVelo() ) veloIDcount++;
      }
      nVeloIDs.push_back( veloIDcount );
      eta.push_back( track->pseudoRapidity() );
      phi.push_back( track->phi() );
      if ( track->checkType( LHCb::Track::Types::Long ) ) {
        nLong++;
        momentum.push_back( track->p() );
        transverse_momentum.push_back( track->pt() );
      }
    }
    tuple->farray( "trChi2PerDoF_1", chi2PerDoF, "nTrChi2PerDoF_1", 500 );
    tuple->farray( "trNVeloIDs_1", nVeloIDs, "nTrNVeloIDs_1", 500 );
    tuple->farray( "trP_1", momentum, "nTrP_1", 500 );
    tuple->farray( "trPT_1", transverse_momentum, "nTrPT_1", 500 );
    tuple->farray( "trEta_1", eta, "nTrEta_1", 500 );
    tuple->farray( "trPhi_1", phi, "nTrPhi_1", 500 );
    tuple->column( "trNum_1", (int)ipv1->tracks().size() );
    tuple->column( "trNumLong_1", nLong );
    tuple->column( "trNumForw_1", nForwd );
    tuple->column( "trNumBackw_1", nBackw );
    tuple->column( "chi2_1",ipv1->chi2() );
    tuple->column( "ndof_1",ipv1->nDoF() );
    tuple->column( "pvx_1", ipv1->position().x());
    tuple->column( "pvy_1", ipv1->position().y());
    tuple->column( "pvz_1", ipv1->position().z());
    tuple->column( "pvxe_1", sqrt(covMat1.At(0,0)));
    tuple->column( "pvye_1", sqrt(covMat1.At(1,1)));
    tuple->column( "pvze_1", sqrt(covMat1.At(2,2)));
    tuple->column( "cov01_1", covMat1.At(0,1));
    tuple->column( "cov02_1", covMat1.At(0,2));
    tuple->column( "cov12_1", covMat1.At(1,2));
  } else {
    std::vector< double > chi2PerDoF;
    std::vector< int > nVeloIDs;
    std::vector< double > momentum;
    std::vector< double > transverse_momentum;
    std::vector< double > eta;
    std::vector< double > phi;
    tuple->farray( "trChi2PerDoF_1", chi2PerDoF, "nTrChi2PerDoF_1", 500 );
    tuple->farray( "trNVeloIDs_1", nVeloIDs, "nTrNVeloIDs_1", 500 );
    tuple->farray( "trP_1", momentum, "nTrP_1", 500 );
    tuple->farray( "trPT_1", transverse_momentum, "nTrPT_1", 500 );
    tuple->farray( "trEta_1", eta, "nTrEta_1", 500 );
    tuple->farray( "trPhi_1", phi, "nTrPhi_1", 500 );
    tuple->column( "trNum_1",0 );
    tuple->column( "trNumLong_1",0);
    tuple->column( "trNumForw_1",0 );
    tuple->column( "trNumBackw_1",0 );
    tuple->column( "chi2_1",0.);
    tuple->column( "ndof_1",0);
    tuple->column( "pvx_1", 0.);
    tuple->column( "pvy_1", 0.);
    tuple->column( "pvz_1", 0.);
    tuple->column( "pvxe_1", 0.);
    tuple->column( "pvye_1", 0.);
    tuple->column( "pvze_1", 0.);
    tuple->column( "cov01_1", 0.);
    tuple->column( "cov02_1", 0.);
    tuple->column( "cov12_1", 0.);
  }
  if ( 1 == outvec2.size() ) {
    auto ipv2 = outvec2.begin() ;
    int nForwd,nBackw;
    countTracksFwdBack( ipv2->tracks(), nForwd, nBackw );
    const Gaudi::SymMatrix3x3 covMat2 = ipv2->covMatrix();
    std::vector< double > chi2PerDoF2;
    std::vector< int > nVeloIDs2;
    std::vector< double > momentum2;
    std::vector< double > transverse_momentum2;
    std::vector< double > eta2;
    std::vector< double > phi2;
    int nLong = 0;
    std::vector< LHCb::LHCbID >::const_iterator lbidit;
    for( auto track : ipv2->tracks() ) {
      chi2PerDoF2.push_back( track->chi2PerDoF() );
      lbidit = track->lhcbIDs().begin();
      int veloIDcount = 0;
      for( ; lbidit != track->lhcbIDs().end(); lbidit++ ) {
        if ( lbidit->isVelo() ) veloIDcount++;
      }
      nVeloIDs2.push_back( veloIDcount );
      eta2.push_back( track->pseudoRapidity() );
      phi2.push_back( track->phi() );
      if ( track->checkType( LHCb::Track::Types::Long ) ) {
        nLong++;
        momentum2.push_back( track->p() );
        transverse_momentum2.push_back( track->pt() );
      }
    }
    tuple->farray( "trChi2PerDoF_2", chi2PerDoF2, "nTrChi2PerDoF_2", 500 );
    tuple->farray( "trNVeloIDs_2", nVeloIDs2, "nTrNVeloIDs_2", 500 );
    tuple->farray( "trP_2", momentum2, "nTrP_2", 500 );
    tuple->farray( "trPT_2", transverse_momentum2, "nTrPT_2", 500 );
    tuple->farray( "trEta_2", eta2, "nTrEta_2", 500 );
    tuple->farray( "trPhi_2", phi2, "nTrPhi_2", 500 );
    tuple->column( "trNum_2", (int)ipv2->tracks().size() );
    tuple->column( "trNumLong_2",nLong );
    tuple->column( "trNumForw_2", nForwd );
    tuple->column( "trNumBackw_2", nBackw );
    tuple->column( "chi2_2",ipv2->chi2() );
    tuple->column( "ndof_2",ipv2->nDoF() );
    tuple->column( "pvx_2", ipv2->position().x());
    tuple->column( "pvy_2", ipv2->position().y());
    tuple->column( "pvz_2", ipv2->position().z());
    tuple->column( "pvxe_2", sqrt(covMat2.At(0,0)));
    tuple->column( "pvye_2", sqrt(covMat2.At(1,1)));
    tuple->column( "pvze_2", sqrt(covMat2.At(2,2)));
    tuple->column( "cov01_2", covMat2.At(0,1));
    tuple->column( "cov02_2", covMat2.At(0,2));
    tuple->column( "cov12_2", covMat2.At(1,2));
  } else {
    std::vector< double > chi2PerDoF2;
    std::vector< int > nVeloIDs2;
    std::vector< double > momentum2;
    std::vector< double > transverse_momentum2;
    std::vector< double > eta2;
    std::vector< double > phi2;
    tuple->farray( "trChi2PerDoF_2", chi2PerDoF2, "nTrChi2PerDoF_2", 500 );
    tuple->farray( "trNVeloIDs_2", nVeloIDs2, "nTrNVeloIDs_2", 500 );
    tuple->farray( "trP_2", momentum2, "nTrP_2", 500 );
    tuple->farray( "trPT_2", transverse_momentum2, "nTrPT_2", 500 );
    tuple->farray( "trEta_2", eta2, "nTrEta_2", 500 );
    tuple->farray( "trPhi_2", phi2, "nTrPhi_2", 500 );
    tuple->column( "trNum_2",0);
    tuple->column( "trNumLong_2",0);
    tuple->column( "trNumForw_2",0 );
    tuple->column( "trNumBackw_2",0 );
    tuple->column( "chi2_2",0.);
    tuple->column( "ndof_2",0);
    tuple->column( "pvx_2", 0.);
    tuple->column( "pvy_2", 0.);
    tuple->column( "pvz_2", 0.);
    tuple->column( "pvxe_2", 0.);
    tuple->column( "pvye_2", 0.);
    tuple->column( "pvze_2", 0.);
    tuple->column( "cov01_2", 0.);
    tuple->column( "cov02_2", 0.);
    tuple->column( "cov12_2", 0.);
  }
  if ( ( 1 == outvec1.size() ) && ( 1 == outvec2.size() ) ) {
    auto ipv1 = outvec1.begin();
    auto ipv2 = outvec2.begin();
    tuple->column( "diff_pvx", ipv1->position().x()-ipv2->position().x());
    tuple->column( "diff_pvy", ipv1->position().y()-ipv2->position().y());
    tuple->column( "diff_pvz", ipv1->position().z()-ipv2->position().z());
  }
  else {
    tuple->column( "diff_pvx", 0.);
    tuple->column( "diff_pvy", 0.);
    tuple->column( "diff_pvz", 0.);
  }
  tuple->write();
}
