/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef DUMPUTILS_H
#define DUMPUTILS_H

#include <string>
#include <boost/filesystem.hpp>

#include <fstream>
#include <type_traits>
#include <gsl/span>
#include <string>

namespace DumpUtils {

bool createDirectory(boost::filesystem::path dir);

namespace detail {
    template <typename T>
    std::ostream& write(std::ostream& os, const T& t) {
        // if you would like to know why there is a check for trivially copyable,
        // please read the 'notes' section of https://en.cppreference.com/w/cpp/types/is_trivially_copyable
        if constexpr ( std::is_same_v<T,gsl::span<const std::byte>> ) {
            return os.write( reinterpret_cast<const char*>(t.data()),t.size());
        } else if constexpr ( std::is_trivially_copyable_v<T> && !gsl::details::is_span<T>::value  ) {
            return os.write( reinterpret_cast<const char*>(&t),sizeof(T) );
        } else {
            static_assert( std::is_trivially_copyable_v<typename T::value_type> );
            return write( os, as_bytes(gsl::make_span(t)) );
        }
    }
}

class Writer {
    std::ofstream m_f;

public:
    Writer(const std::string& name) : m_f{  name,  std::ios::out | std::ios::binary } {}

    template <typename... Args> Writer& write(Args&&... args) {
         (detail::write(m_f,std::forward<Args>(args)),...);
         return *this;
    }
};

}

#endif
