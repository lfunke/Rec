/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "TsaDataDebugAlg.h"

//-----------------------------------------------------------------------------
// Implementation file for class : TsaDataDebugAlg
//
// 2007-07-17 : Chris Jones
//-----------------------------------------------------------------------------

using namespace Tf::Tsa;

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( DataDebugAlg )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
DataDebugAlg::DataDebugAlg( const std::string& name,
                            ISvcLocator* pSvcLocator)
  : GaudiHistoAlg( name , pSvcLocator ) 
{ 
  declareProperty("seedHitLocation", m_seedHitLocation = SeedHitLocation::Default);
}

//=============================================================================
// Destructor
//=============================================================================
DataDebugAlg::~DataDebugAlg() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode DataDebugAlg::initialize()
{
  const StatusCode sc = GaudiHistoAlg::initialize();
  if ( sc.isFailure() ) return sc;


  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode DataDebugAlg::execute()
{
  StatusCode sc;

  sc = debugSeedHits();

  return sc;
}

//=============================================================================
// Debug the hits
//=============================================================================
StatusCode DataDebugAlg::debugSeedHits()
{
  // load the hits
  SeedHits* hits = get<SeedHits>(m_seedHitLocation);
  debug() << "Found " << hits->size() << " Seed Hits at " << m_seedHitLocation << endmsg;

  // loop over the hits
  for ( SeedHits::const_iterator iHit = hits->begin();
        iHit != hits->end(); ++iHit )
  {
    verbose() << " -> Hit " << **iHit << endmsg;
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
// Finalize
//=============================================================================
StatusCode DataDebugAlg::finalize()
{
  return GaudiHistoAlg::finalize();
}

//=============================================================================
