/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
/** @file STHitCleaner.h
 *
 *  Header file for class : Tf::STHitCleaner
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   2005-01-10
 */
//-----------------------------------------------------------------------------

#ifndef TFTOOLS_STHitCleaner
#define TFTOOLS_STHitCleaner 1

// STL
#include <iterator>
#include <functional>
#include <map>

// From gaudi
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiAlg/GaudiTool.h"

// Tf
#include "TfKernel/ISTHitCleaner.h"

namespace Tf
{

  //-----------------------------------------------------------------------------
  /** @class STHitCleaner STHitCleaner.h
   *
   *  STHit cleaner tool
   *
   *  @author S. Hansmann-Menzemer, W. Houlsbergen, C. Jones, K. Rinnert
   *
   *  @date   2007-06-01
   *
   *  @todo Is it possible to have a common interface for OT and ST (other?) cleaning tools
   **/
  //-----------------------------------------------------------------------------
  class STHitCleaner : public extends<GaudiTool, ISTHitCleaner>
  {

  private:

    /// Max beetle occupancy job option
    unsigned int m_maxBeetleOcc;

  public:

    /// Standard Constructor
    STHitCleaner( const std::string& type,
                  const std::string& name,
                  const IInterface* parent );

    /// Tool initialization
    StatusCode initialize ( ) override;

    // Clean the given range of hits
    STHits cleanHits( const STHits::const_iterator begin,
                      const STHits::const_iterator end ) const override;

  private:

    // Clean out hot beetles
    STHits removeHotBeetles( const STHits::const_iterator begin,
                             const STHits::const_iterator end ) const;

  }; // STHitCleaner

} // namespace

#endif

