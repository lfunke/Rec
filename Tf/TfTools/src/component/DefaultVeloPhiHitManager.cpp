/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/IIncidentSvc.h"

#include "TfKernel/DefaultVeloPhiHitManager.h"


//-----------------------------------------------------------------------------
// Implementation file for class : DefaultVeloPhiHitManager
//
// 2007-08-07 : Kurt Rinnert <kurt.rinnert@cern.ch>
//-----------------------------------------------------------------------------
namespace Tf {

DECLARE_COMPONENT( DefaultVeloPhiHitManager )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
DefaultVeloPhiHitManager::DefaultVeloPhiHitManager(const std::string& type,
    const std::string& name,
    const IInterface* parent)
  : DefaultVeloHitManager<DeVeloPhiType,VeloPhiHit,2>(type, name, parent)
{
  declareInterface<DefaultVeloPhiHitManager>(this);
}


} // namespace Tf
