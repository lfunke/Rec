###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: FastVelo
################################################################################
gaudi_subdir(FastVelo v1r25)

gaudi_depends_on_subdirs(Det/STDet
                         Det/VeloDet
                         Event/DigiEvent
                         Event/RecEvent
                         Event/TrackEvent
                         GaudiAlg
                         ST/STKernel
                         Tr/TrackInterfaces
                         Tf/PatKernel)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(FastVelo
                 src/*.cpp
                 INCLUDE_DIRS Event/DigiEvent Tf/PatKernel, Tr/TrackInterfaces
                 LINK_LIBRARIES STDetLib VeloDetLib RecEvent TrackEvent GaudiAlgLib STKernelLib)

gaudi_install_python_modules()
