/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: SeedTrack.cpp,v 1.3 2007-10-09 17:56:26 smenzeme Exp $
// Include files 

// local
#include "TsaKernel/SeedTrack.h"

//-----------------------------------------------------------------------------
// Implementation file for class : SeedTrack
//
// 2007-07-18 : Chris Jones
//-----------------------------------------------------------------------------

std::ostream& Tf::Tsa::SeedTrack::fillStream(std::ostream& s) const
{
  return s << "{"
           << " select=" << select()
           << " live=" << live()
           << " nx=" << nx()
           << " nx=" << nx()
           << " nHit=" << nHit()
           << " x0=" << x0()
           << " y0=" << y0()
           << " sx=" << sx()
           << " sy=" << sy()
           << " tx=" << tx()
           << " xChis2=" << xChi2()
           << " yChis2=" << yChi2()
           << " sector=" << sector()
           << " dth=" << dth()
           << " lik=" << lik() 
           << " }";
}
