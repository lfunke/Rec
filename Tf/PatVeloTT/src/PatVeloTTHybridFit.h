/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef INCLUDE_PATVELOTTHYBRIDFIT_H
#define INCLUDE_PATVELOTTHYBRIDFIT_H 1

#include "GaudiAlg/GaudiTool.h"
#include "TrackInterfaces/IPatVeloTTFit.h"
#include "PatTTMagnetTool.h"
#include "PatKernel/PatTTHit.h"
#include "vdt/sqrt.h"

/** @class PatVeloTTHybridFit PatVeloTTHybridFit.h
 *  
 * provide an interface to the internal fit used in the PatVeloTTHybridTool
 * to initialize track states
 *
 * @author Michel De Cian, michel.de.cian@cern.ch
 * @date   2017-03-20
 */
class PatVeloTTHybridFit : public extends<GaudiTool, IPatVeloTTFit> {
public:

  /// Standard Constructor
  PatVeloTTHybridFit(const std::string& type, const std::string& name,
                     const IInterface* parent);

  StatusCode initialize() override; ///< Tool initialization

  StatusCode fitVTT( LHCb::Track& track) const override;

  void finalFit( const std::vector<PatTTHit*>& theHits, const std::array<float,8>& vars, std::array<float,3>& params ) const override;

private:
  
  DeSTDetector* m_ttDet = nullptr;
  PatTTMagnetTool*    m_PatTTMagnetTool = nullptr;  ///< Multipupose tool for Bdl and deflection

  
  
  


};
#endif // INCLUDE_PATVELOTTFIT_H

