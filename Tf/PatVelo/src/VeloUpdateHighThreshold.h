/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: VeloUpdateHighThreshold.h,v 1.1 2009-08-26 11:44:21 ocallot Exp $
#ifndef VELOUPDATEHIGHTHRESHOLD_H 
#define VELOUPDATEHIGHTHRESHOLD_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"


/** @class VeloUpdateHighThreshold VeloUpdateHighThreshold.h
 *   
 *
 *  @author Olivier Callot
 *  @date   2009-08-20
 */
class VeloUpdateHighThreshold : public GaudiAlgorithm {
public: 
  /// Standard constructor
  VeloUpdateHighThreshold( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~VeloUpdateHighThreshold( ); ///< Destructor

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;    ///< Algorithm finalization

protected:

private:
  double m_highThreshold;
};
#endif // VELOUPDATEHIGHTHRESHOLD_H
