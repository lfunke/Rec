/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: PatVeloPhiHitManager.h,v 1.4 2009-07-20 11:35:32 dhcroft Exp $
#ifndef INCLUDE_TF_PATVELOPHIHITMANAGER_H
#define INCLUDE_TF_PATVELOPHIHITMANAGER_H 1

#include "PatVeloHit.h"
#include "TfKernel/ExtendedVeloPhiHitManager.h"

// for CCE scan
#include "Kernel/IVeloCCEConfigTool.h"

namespace Tf {
  static const InterfaceID IID_PatVeloPhiHitManager( "Tf::PatVeloPhiHitManager", 1, 0 );

  /** @class PatVeloPhiHitManager PatVeloPhiHitManager.h
   * Mananges specialised phi hits for VELO space tracking. 
   *
   * @author Kurt Rinnert <kurt.rinnert@cern.ch>
   * @date   2007-08-08
   */
  class PatVeloPhiHitManager : public Tf::ExtendedVeloPhiHitManager<PatVeloPhiHit> {

    public:

      /// Retrieve interface ID
      static const InterfaceID& interfaceID() { return IID_PatVeloPhiHitManager; }

      /// Standard Constructor
      PatVeloPhiHitManager(const std::string& type,
          const std::string& name,
          const IInterface* parent);

      StatusCode initialize() override; ///< Tool initialization

      void prepareHits() override;

      void prepareHits(Station* station);        ///< Prepare hits for one station only, implementation

      void resetUsedFlagOfHits(); ///< Reset all used flags to unused

  private:
      /// Max number of clusters in phi inner region before region killed as too hot
      unsigned int m_maxPhiInner; 
      /// Max number of clusters in phi outer region before region killed as too hot
      unsigned int m_maxPhiOuter; 
      /// Use CCE scan pattern recognition
      bool m_CCEscan;
      /// Comma separated list of killed sensors to override CondDB
      std::string m_killSensorList;
      /// CCE scan sensor list to kill, set from CondDB
      std::vector<int> m_killSensorListVec;
      /// CCE sensor tool
      IVeloCCEConfigTool* m_cceTool;
  };
}
#endif // INCLUDE_TF_PATVELOPHIHITMANAGER_H

