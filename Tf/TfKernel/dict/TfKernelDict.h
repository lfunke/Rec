/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: TfKernelDict.h,v 1.4 2008-07-21 13:06:04 gguerrer Exp $
#ifndef DICT_TFKERNELDICT_H 
#define DICT_TFKERNELDICT_H 1

#include "TfKernel/TfIDTypes.h"

#include "TfKernel/OTHit.h"
#include "TfKernel/STHit.h"
#include "TfKernel/UTHit.h"
#include "TfKernel/VeloRHit.h"
#include "TfKernel/VeloPhiHit.h"

#include "TfKernel/Region.h"
#include "TfKernel/RegionID.h"

#include "TfKernel/ITTHitCreator.h"
#include "TfKernel/IUTHitCreator.h"
#include "TfKernel/IITHitCreator.h"
#include "TfKernel/IOTHitCreator.h"
#include "TfKernel/ISTHitCleaner.h"
#include "TfKernel/IUTHitCleaner.h"
#include "TfKernel/IOTHitCleaner.h"

// instantiate some templated classes, to get them into the dictionary
namespace 
{
  struct _Instantiations 
  {
    Tf::Envelope<Tf::STHit::DetectorElementType> tf_envelope_sthit;
    Tf::Envelope<Tf::UTHit::DetectorElementType> tf_envelope_uthit;
    Tf::Envelope<Tf::OTHit::DetectorElementType> tf_envelope_othit;
	
  };
}

#endif // DICT_TFKERNELDICT_H
