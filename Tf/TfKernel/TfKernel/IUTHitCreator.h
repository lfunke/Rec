/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file IUTHitCreator.h
 *
 *  Header file for class : Tf::IUTHitCreator
 *  Cloned from original ITTHitCreator.h
 *
 */
//-----------------------------------------------------------------------------

#ifndef TFKERNEL_IUTHitCreator_H
#define TFKERNEL_IUTHitCreator_H 1

#include "GaudiKernel/IAlgTool.h"
#include "TfKernel/UTHit.h"
#include "TfKernel/Region.h"
#include "TfKernel/TfIDTypes.h"

namespace Tf
{
  /// Static ID object
  static const InterfaceID IID_IUTHitCreator( "Tf::IUTHitCreator", 0, 0 );

  /** @class IUTHitCreator IUTHitCreator.h
   *
   *  Interface to UT data provider. Creates, on demand, the low level
   *  Tf::UTHit data objects and provides access to these objects by region.
   *
   *  Methods are provided to return the hits in a selected part of the UT detectors.
   *
   *  E.g. for IT :-
   *
   *  @code
   *  // Get all the hits in the IT stations
   *  Tf::UTHitRange range = hitMan->itHits();
   *
   *  // Get all the IT hits in one specific T station
   *  Tf::TStationID sta = ...;
   *  Tf::UTHitRange range = hitMan->hits(sta);
   *
   *  // Get all the IT hits in one specific layer of one T station
   *  Tf::TStationID sta = ...;
   *  Tf::TLayerID   lay = ...;
   *  Tf::UTHitRange range = hitMan->hits(sta,lay);
   *
   *  // Get all the IT hits in a specific 'region' of one layer of one T station
   *  Tf::TStationID sta = ...;
   *  Tf::TLayerID   lay = ...;
   *  Tf::ITRegionID reg = ...;
   *  Tf::UTHitRange range = hitMan->hits(sta,lay,reg);
   *  @endcode
   *
   *  Or, for UT :-
   *
   *  @code
   *  // Get all the hits in the UT stations
   *  Tf::UTHitRange range = hitMan->ttHits();
   *
   *  // Get all the UT hits in one specific UT station
   *  Tf::UTStationID sta = ...;
   *  Tf::UTHitRange range = hitMan->hits(sta);
   *
   *  // Get all the UT hits in one specific layer of one UT station
   *  Tf::UTStationID sta = ...;
   *  Tf::UTLayerID   lay = ...;
   *  Tf::UTHitRange range = hitMan->hits(sta,lay);
   *
   *  // Get all the UT hits in a specific 'region' of one layer of one UT station
   *  Tf::UTStationID sta = ...;
   *  Tf::UTLayerID   lay = ...;
   *  Tf::UTRegionID  reg = ...;
   *  Tf::UTHitRange range = hitMan->hits(sta,lay,reg);
   *  @endcode
   *
   *  In all cases the returned Range object acts like a standard vector or container :-
   *
   *  @code
   *   // Iterate over the returned range
   *  for ( Tf::UTHitRange::const_iterator iR = range.begin();
   *        iR != range.end(); ++iR )
   *  {
   *    // do something with the hit
   *  }
   *  @endcode
   *
   *  This interface also provides access to the Tf::IUTHitCreator::UTRegion object, that can be used to
   *  get detailed information about the geomterical properties of an UT region
   *
   *  @code
   *  // Get region information for IT
   *  Tf::TStationID  itsta = ...;
   *  Tf::TLayerID    itlay = ...;
   *  Tf::ITRegionID  itreg = ...;
   *  Tf::IUTHitCreator::UTRegion* region = hitMan->region(itsta,itlay,itreg);
   *  // Get region information for UT
   *  Tf::UTStationID ttsta = ...;
   *  Tf::UTLayerID   ttlay = ...;
   *  Tf::UTRegionID  ttreg = ...;
   *  Tf::IUTHitCreator::UTRegion* region = hitMan->region(ttsta,ttlay,ttreg);
   *  @endcode
   *
   *  See the Tf::IUTHitCreator::UTRegion and Tf::EnvelopeBase classes for more details.
   *
   *  @author S. Hansmann-Menzemer, W. Hulsbergen, C. Jones, K. Rinnert
   *  @date   2007-06-01
   */

  class IUTHitCreator : virtual public IAlgTool
  {
  public:

    /// The region type for UT hit
    typedef Tf::Region<UTHit> UTRegion ;

    /// Retrieve interface ID
    static const InterfaceID& interfaceID() { return IID_IUTHitCreator ; }

    /* Reset used flags of hits */
    virtual void resetUsedFlagOfHits() = 0;

    /** Load all the UT hits
     *  @return Range object for the hits ion the selected region of interest
     */
    virtual UTHitRange hits() const = 0 ;

    /** Load the UT hits for a given region of interest
     *
     * @attention This call may lead to loading on demand and
     *            may invalidate iterators returned by a previous call.
     *
     *  @param[in] iStation Station ID
     *  @param[in] iLayer   Station layer ID
     *
     *  @return Range object for the hits in the selected region of interest
     */
    virtual UTHitRange hits(const UTStationID iStation,
                            const UTLayerID iLayer) const = 0 ;

    /** Load the UT hits for a given region of interest
     *
     * @attention This call may lead to loading on demand and
     *            may invalidate iterators returned by a previous call.
     *
     *  @param[in] iStation Station ID
     *  @param[in] iLayer   Station layer ID
     *  @param[in] iRegion  Region within the layer
     *
     *  @return Range object for the hits in the selected region of interest
     */
    virtual UTHitRange hits(const UTStationID iStation,
                            const UTLayerID iLayer,
                            const UTRegionID iRegion) const = 0 ;


    /** Load the UT hits for a given region of interest
     *
     * @attention This call may lead to loading on demand and
     *            may invalidate iterators returned by a previous call.
     *
     *  @param[in] iStation Station ID
     *  @param[in] iLayer   Station layer ID
     *  @param[in] iRegion  Region within the layer
     *  @param[in] xmin     Minimum local x value for region of interest
     *  @param[in] xmax     Maximum local x value for region of interest
     *
     *  @return Range object for the hits in the selected region of interest
     */
    virtual UTHitRange hitsLocalXRange(const UTStationID iStation,
				       const UTLayerID iLayer,
				       const UTRegionID iRegion,
				       const double xminT,
				       const double xmaxT) const = 0 ;
    /** Retrieve the UT UTRegion for a certain region ID. The region
     *   knows its 'size' and gives access to its hits.
     *
     *  @param[in] iStation Station ID
     *  @param[in] iLayer   Station layer ID
     *  @param[in] iRegion  Region within the layer
     *
     *  @return Pointer to the UTRegion object
     */
    virtual const UTRegion* region(const UTStationID iStation,
                                   const UTLayerID   iLayer,
                                   const UTRegionID iRegion) const = 0 ;

     /** create a single UTHit from an UTChannelID. note: not very fast.
     * @param[in] stid UTChannelID
     * @ return a Tf::UTHit
     */
    virtual Tf::UTHit hit(LHCb::UTChannelID stid) const = 0  ;
  };
}

#endif // TFKERNEL_IUTHitCreator_H
