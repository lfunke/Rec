/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PATKERNEL_IPATMVACLASSIFIER_H
#define PATKERNEL_IPATMVACLASSIFIER_H 1

#include "GaudiKernel/IAlgTool.h"
#include "Event/Track.h"


/** @class IPatMvaClassifier PatKernel/IPatMvaClassifier.h
 *
 *  @author Adam Dendek
 *  @date   2016-03-17
 */
struct IPatMvaClassifier : extend_interfaces<IAlgTool>
{
  DeclareInterfaceID( IPatMvaClassifier, 2, 0 );
  virtual double getMvaValue(const LHCb::Track& track) const  = 0;
};
#endif //PATKERNEL_IPATMVACLASSIFIER_H
