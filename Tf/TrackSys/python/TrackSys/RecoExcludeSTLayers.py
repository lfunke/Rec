#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# -*- coding: utf-8 -*-
# @Author: Elena Graverini
# @Date:   2015-11-30 16:18:57
# @Last Modified by:   Elena Graverini
# @Last Modified time: 2015-11-30 16:56:44

from Gaudi.Configuration import *

def ExcludeSTLayers(ExcludedLayers):
    print "Removing clusters from layers", ExcludedLayers

    from Configurables import STClusterKiller, STLiteClusterKiller, STSelectClustersByChannel, STLayerSelector
    
    ttLayerSelector = STLayerSelector("TTLayerSelector")
    ttLayerSelector.DetType = "TT"
    ttLayerSelector.IgnoredLayers = ExcludedLayers
    ttLayerSelector.OutputLevel = 3
    
    itLayerSelector = STLayerSelector("ITLayerSelector")
    itLayerSelector.DetType = "IT"
    itLayerSelector.IgnoredLayers = ExcludedLayers
    itLayerSelector.OutputLevel = 3
    
    ttLiteClusterKiller = STLiteClusterKiller("TTLiteClusterKiller")
    ttLiteClusterKiller.DetType = "TT"
    ttLiteClusterKiller.SelectorType = "STLayerSelector"
    ttLiteClusterKiller.SelectorName = "TTLayerSelector"
    
    itLiteClusterKiller = STLiteClusterKiller("ITLiteClusterKiller")
    itLiteClusterKiller.DetType = "IT"
    itLiteClusterKiller.SelectorType = "STLayerSelector"
    itLiteClusterKiller.SelectorName = "ITLayerSelector"
    
    ttClusterSelector = STSelectClustersByChannel("TTClusterSelector")
    ttClusterSelector.SelectorType = "STLayerSelector"
    ttClusterSelector.SelectorName = "TTLayerSelector"
    
    itClusterSelector = STSelectClustersByChannel("ITClusterSelector")
    itClusterSelector.SelectorType = "STLayerSelector"
    itClusterSelector.SelectorName = "ITLayerSelector"
    
    ttClusterKiller = STClusterKiller("TTClusterKiller")
    ttClusterKiller.DetType = "TT"
    ttClusterKiller.SelectorType = "STSelectClustersByChannel"
    ttClusterKiller.SelectorName = "TTClusterSelector"
    
    itClusterKiller = STClusterKiller("ITClusterKiller")
    itClusterKiller.DetType = "IT"
    itClusterKiller.SelectorType = "STSelectClustersByChannel"
    itClusterKiller.SelectorName = "ITClusterSelector"
    
    GaudiSequencer("RecoDecodingSeq").Members += [ ttClusterKiller, ttLiteClusterKiller,
                                                   itClusterKiller, itLiteClusterKiller ]