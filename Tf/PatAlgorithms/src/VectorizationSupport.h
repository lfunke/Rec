/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef VECTORIZATION_SUPPORT_H
#define VECTORIZATION_SUPPORT_H

#include <type_traits>
#include <iterator>
#include <functional>


#include "Kernel/STLExtensions.h"

template <typename T,size_t N> struct vector_type;

template<typename T>
struct vector_type<T,1> { using type = T; };

template<typename T>
struct vector_type<T,2> {
    //
#ifdef __clang__
  typedef T type __attribute__ ((ext_vector_type( 2  ))); // vector of two Ts...
#else
  // icc 13 onwards should support this at well...
  typedef T type __attribute__ ((vector_size( 2 * sizeof(T) ))); // vector of two Ts...
#endif
};

template<typename T>
struct vector_type<T,4> {
#ifdef __clang__
  typedef T type __attribute__ ((ext_vector_type( 4  ))); // vector of four Ts...
#else
  // icc 13 onwards should support this at well...
  typedef T type __attribute__ ((vector_size( 4 * sizeof(T) ))); // vector of four Ts...
#endif
};

#include <utility>
#include <array>

template<typename Ret, typename Arg>
inline std::array<Ret,1> scatter_array_impl( Arg&& a, std::index_sequence<0> )
{
    return { std::forward<Arg>(a) };
}

template<typename Ret, typename Arg, std::size_t... I>
inline std::array<Ret,sizeof...(I)> scatter_array_impl( const Arg& a, std::index_sequence<I...> )
{
    return { a[I]... };
}

template<typename Ret, size_t N, typename Arg>
inline std::array<Ret,N> scatter_array(Arg&& a)
{
    return scatter_array_impl<Ret>( std::forward<Arg>(a), std::make_index_sequence<N>{} );
}

template< typename Ret, typename Arg, typename A,  size_t N, typename Vec, std::size_t... I>
inline void scatter_invoke_impl( Ret(Arg::*fun)(A), std::array<Arg*,N> a, Vec v, std::index_sequence<I...> )
{
  // ( std::invoke(fun,a[I],v[I]), ... ); // clang doesn't like this... (non-const ref to a vector element)
  ( (a[I]->*fun)(v[I]), ... );
}

template<typename Fun, typename Arg, std::size_t N, typename Vec>
inline void scatter_invoke(Fun fun, std::array<Arg,N> args, Vec v)
{
    scatter_invoke_impl( fun, args, v, std::make_index_sequence<N>{} );
}

template<typename Fun, typename Arg, typename Vec>
inline void scatter_invoke(Fun fun, std::array<Arg,1> args, Vec v)
{
    std::invoke(fun,args[0],v);
}


template <typename Iterator1, typename Iterator2, typename Fun, std::size_t... I>
void for_each_v_impl( Iterator1 first, Iterator2 last, Fun f, std::index_sequence<I...>)
{
   using diff_t = typename std::iterator_traits<Iterator1>::difference_type;
   while ( LIKELY( std::distance(first,last) >= static_cast<diff_t>(sizeof...(I)) ) ) {
       std::invoke(f,first[I]...); first += sizeof...(I);
   }
   std::for_each(first,last,f);
}

namespace detail {
#if defined(__AVX512F__)
    constexpr size_t DefaultVectorSize = 8;
#elif defined(__AVX__)
    constexpr size_t DefaultVectorSize = 4;
#elif defined(__SSE__)
    constexpr size_t DefaultVectorSize = 2;
#else
    constexpr size_t DefaultVectorSize = 1;
#endif
}

template <size_t N = detail::DefaultVectorSize, typename Iterator1, typename Iterator2, typename Fun>
void for_each_v( Iterator1&& first, Iterator2&& last, Fun f) {
    if constexpr (N==1) {
        std::for_each(std::forward<Iterator1>(first), std::forward<Iterator2>(last), std::forward<Fun>(f));
    } else {
        for_each_v_impl(std::forward<Iterator1>(first), std::forward<Iterator2>(last), std::forward<Fun>(f), std::make_index_sequence<N>{} );
    }
}


#endif
