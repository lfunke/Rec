/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiAlg/Transformer.h"
#include "Event/RecVertex.h"
#include "Event/Track.h"
#include "Event/RecVertex_v2.h"
#include "Event/Track_v2.h"
#include <assert.h>

/**
 *  Small helper to convert std::vector<LHCb::RecVertex> to LHCb::RecVertices
 */
namespace LHCb::Converters::RecVertex::v2 {
  struct fromVectorLHCbRecVertex :
    public Gaudi::Functional::Transformer<LHCb::RecVertices(const std::vector<LHCb::Event::v2::RecVertex>&, 
                                                            const LHCb::Tracks&)>{
    fromVectorLHCbRecVertex( const std::string& name, ISvcLocator* pSvcLocator ):
      Transformer(name, pSvcLocator,
                  {KeyValue{"InputVerticesName", ""}, KeyValue{"InputTracksName", ""}} ,
                  KeyValue{"OutputVerticesName", ""}){}
    /// The main function, converts the vertex and puts it into a keyed container
    LHCb::RecVertices operator()(const std::vector<LHCb::Event::v2::RecVertex>& vertices,
                                 const LHCb::Tracks& keyed_tracks) const override {
      LHCb::RecVertices converted_vertices;
      for (const auto& vertex : vertices){
        auto converted_vertex = std::make_unique<LHCb::RecVertex>(vertex.position());
        converted_vertex->setTechnique(static_cast<LHCb::RecVertex::RecVertexType>(vertex.technique()));
        converted_vertex->setChi2(vertex.chi2());
        converted_vertex->setNDoF(vertex.nDoF());
        converted_vertex->setCovMatrix(vertex.covMatrix());
        // Add tracks from keyed container.
        // The following relies on the Velo tracks being created with a key in PrPixelTracking.
        for (const auto& weightedTrack : vertex.tracks()) {
          auto track_in_keyed_container = std::find_if(std::begin(keyed_tracks), std::end(keyed_tracks),
                                                       [&weightedTrack](const auto& t) {
                                                         return ((t->nLHCbIDs() == weightedTrack.track->nLHCbIDs()) &&
                                                                 (t->containsLhcbIDs(weightedTrack.track->lhcbIDs())));
                                                       });
          assert(track_in_keyed_container != std::end(keyed_tracks));
          converted_vertex->addToTracks(*track_in_keyed_container, weightedTrack.weight);
        }
        converted_vertices.add(converted_vertex.release());
      }
      return converted_vertices;
    }
  };
  DECLARE_COMPONENT(fromVectorLHCbRecVertex)
}
