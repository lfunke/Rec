/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include <tuple>
#include <numeric>
#include <memory>

#include "GaudiAlg/Transformer.h"
#include "PrKernel/UTHitInfo.h"
#include "PrKernel/UTHitHandler.h"
#include "UTDet/DeUTDetector.h"
#include "UTDAQ/UTReadoutTool.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "Kernel/UTClusterWord.h"
#include "Kernel/UTDecoder.h"
#include "Event/UTTELL1BoardErrorBank.h"

class PrStoreUTHit : public Gaudi::Functional::Transformer<UT::HitHandler(const LHCb::RawEvent&)> {
public:
  PrStoreUTHit( const std::string& name, ISvcLocator* pSvcLocator );
  virtual StatusCode initialize() override;

  UT::HitHandler operator()(const LHCb::RawEvent&) const override;

private:
  std::vector<unsigned int> missingInAction(LHCb::span<const LHCb::RawBank*> banks) const;
  unsigned int pcnVote(LHCb::span<const LHCb::RawBank*> banks) const;
  StatusCode decodeBanks(const LHCb::RawEvent& rawEvt,
                         UT::HitHandler& fCont) const;
  std::unique_ptr<LHCb::UTTELL1BoardErrorBanks> decodeErrors(const LHCb::RawEvent& raw) const;
  bool canBeRecovered(const LHCb::UTTELL1BoardErrorBank* bank,
                      const UTClusterWord& word,
                      const unsigned int pcn) const;
  bool checkDataIntegrity(UTDecoder& decoder,
                          const UTTell1Board* aBoard,
                          const unsigned int bankSize,
                          const UTDAQ::version& bankVersion) const;

private:

  using DecoderData = std::tuple< UTDAQ::version, 
                                  UTTell1Board*,
                                  const LHCb::RawBank*, 
                                  bool,
                                  LHCb::UTTELL1BoardErrorBank*,
                                  unsigned int >;

private:

  DeUTDetector *m_utDet = nullptr;

  ToolHandle<IUTReadoutTool> m_readoutTool { this, "ReadoutTool", "UTReadoutTool" };

  mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_lostBanks{ this, "lost Banks" };
  mutable Gaudi::Accumulators::Counter<> m_noBanksFound{ this, "no banks found" };
  mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_skippedBanks{ this, "skipped Banks" };
  mutable Gaudi::Accumulators::Counter<> m_validBanks{ this, "# valid banks" };
  mutable Gaudi::Accumulators::Counter<> m_validSourceID{ this, "# valid source ID" };
  mutable Gaudi::Accumulators::Counter<> m_eventsWithError{this, "events with error banks"};
  mutable Gaudi::Accumulators::Counter<> m_skippedBanksCounter{this, "events with error banks"};
  mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_errorBanksCounter{this, "skipped Banks"};

  Gaudi::Property<bool> m_skipErrors { this,  "skipBanksWithErrors",  false };
  Gaudi::Property<bool> m_recoverMode { this,  "recoverMode",  true };

};
