/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRSTOREFTHIT_H
#define PRSTOREFTHIT_H 1

// Include files
// from Gaudi
#include <string>

#include "GaudiAlg/Transformer.h"
#include "PrKernel/PrFTHitHandler.h"
#include "PrKernel/PrFTZoneHandler.h"
#include "Event/FTLiteCluster.h"
#include "FTDet/DeFTDetector.h"

using FTLiteClusters = LHCb::FTLiteCluster::FTLiteClusters;

/** @class PrStoreFTHit PrStoreFTHit.h
 *
 *
 *  @author Renato Quagliani
 *  @date   2016-07-07
 */
class PrStoreFTHit : public Gaudi::Functional::Transformer<PrFTHitHandler<PrHit>(const FTLiteClusters&)> {

 public:
  /// Standard constructor
  PrStoreFTHit( const std::string& name, ISvcLocator* pSvcLocator );

  /// initialization
  StatusCode initialize() override;

  /// main method
  PrFTHitHandler<PrHit> operator()(const FTLiteClusters&clusters) const override;

  StatusCode buildGeometry();

 private:

  /// detector element
  DeFTDetector* m_ftDet;

  /// derived condition caching computed zones
  PrFTZoneHandler *m_zoneHandler;

  /// Cached resolution
  std::array<float,9> m_invClusResolution;

  /// partial SoA cache for mats
  std::array<float, 2<<11> m_mats_dxdy;
  std::array<float, 2<<11> m_mats_dzdy;
  std::array<float, 2<<11> m_mats_globaldy;
  std::array<float, 2<<11> m_mats_uBegin;
  std::array<float, 2<<11> m_mats_halfChannelPitch;
  std::array<float, 2<<11> m_mats_dieGap;
  std::array<float, 2<<11> m_mats_sipmPitch;
  std::array<Gaudi::XYZPointF, 2<<11> m_mats_mirrorPoint;
  std::array<Gaudi::XYZVectorF, 2<<11> m_mats_ddx;

};
#endif // PRSTOREFTHIT_H
