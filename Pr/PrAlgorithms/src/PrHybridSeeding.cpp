/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Include files
// Range v3
#include <range/v3/all.hpp>

#include "Event/Track.h"
#include "Event/StateParameters.h"
#include "Math/CholeskyDecomp.h"

// local
#include "PrLineFitterY.h"
#include "PrHybridSeeding.h"
#include "PrPlaneHybridCounter.h"
#include "Event/FTCluster.h"
#include "Event/FTLiteCluster.h"

// ASSUME statement
#include "Kernel/STLExtensions.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PrHybridSeeding
//
// @author Renato Quagliani (rquaglia@cern.ch)
// @date 2015-03-11
//-----------------------------------------------------------------------------

namespace {
  using namespace ranges;
  using std::array;
  
  constexpr float zReference = StateParameters::ZMidT;  
  
   IterPairs InitializeBB(unsigned int ZoneNumber, const PrFTHitHandler<ModPrHit>& FTHitHandler) noexcept {
      auto r = FTHitHandler.hits(ZoneNumber);
      IterPairs BB;
      BB.begin = r.begin();
      BB.end   = r.end();
      return BB;
   }

  //Update the bounds (Hiterator Pairs) and old Min => new Min searching linearly around current boundary begin
  inline void LookAroundMin( IterPairs& bound, float& oMin,float nMin,  ModPrHitConstIter eZone,
                             ModPrHitConstIter bZone, const PrFTHitHandler<ModPrHit>& FTHitHandler){
    if( nMin < oMin){ bound.begin  = FTHitHandler.get_lowerBound_lin_reverse( bZone, bound.begin, nMin);
    }else{            bound.begin  = FTHitHandler.get_lowerBound_lin( bound.begin, eZone, nMin); }
    oMin = nMin;
  }
  
  //Just grab the iterator searching around current bound begin
  inline ModPrHitConstIter LookAroundMin_it( ModPrHitConstIter& currBeg, float& oMin,float nMin,
                                             ModPrHitConstIter eZone ,ModPrHitConstIter bZone,
                                             const PrFTHitHandler<ModPrHit>& FTHitHandler){
    if( nMin < oMin){   return FTHitHandler.get_lowerBound_lin_reverse( bZone, currBeg, nMin);
    }else{              return FTHitHandler.get_lowerBound_lin( currBeg, eZone, nMin);  }
    // oMin = nMin
  }
  
  /** @brief Check if two tracks are passing through T1/T2/T3 at a distance less than "distance".
   *  @param tr1 First track in comparison
   *  @param tr2 Second track in comparison
   *  @param distance Distance to check if two tracks are close one to anohter
   *  @return bool Success, tracks are close enough
   */
  inline bool CloseEnough( const Pr::Hybrid::SeedTrack& tr1, const Pr::Hybrid::SeedTrack& tr2, float distance){
    return(  std::abs( tr1.xT1() - tr2.xT1() ) < std::abs(distance) ||
             std::abs( tr1.xT2() - tr2.xT2() ) < std::abs(distance) ||
             std::abs( tr1.xT3() - tr2.xT3() ) < std::abs(distance) );
  }

  //Update the bounds (ModPrHitConstIteror Pairs) and old Max => new Max searching linearly around current boundary end
  inline void LookAroundMax( IterPairs& bound, float& oMax,float& nMax,  ModPrHitConstIter& eZone,
                             ModPrHitConstIter& bZone,
                             const PrFTHitHandler<ModPrHit>& FTHitHandler){
    if( nMax > oMax){   bound.end = FTHitHandler.get_upperBound_lin( bound.end, eZone, nMax);
    }else{              bound.end = FTHitHandler.get_upperBound_lin_reverse(  bZone , bound.end, nMax);  }
    oMax = nMax;
  }

  //Just grab the iterator searching around current bound end
  inline ModPrHitConstIter LookAroundMax_it( ModPrHitConstIter& currEnd, float& oMax,float& nMax,
                                             ModPrHitConstIter& eZone ,ModPrHitConstIter& bZone,
                                             const PrFTHitHandler<ModPrHit>& FTHitHandler){
    if( nMax > oMax){  return FTHitHandler.get_upperBound_lin( currEnd, eZone, nMax);
    }else{             return FTHitHandler.get_upperBound_lin_reverse(  bZone , currEnd, nMax); }
    // oMax = nMax;
  }

  /** @brief Given two tracks it checks if they are clones. Clone threshold is defined by the
      amount of shared hits ( clones if nCommon > maxCommon)
  */
  bool areClones( const Pr::Hybrid::SeedTrack& tr1, const Pr::Hybrid::SeedTrack& tr2, unsigned int& maxCommon){
    unsigned int nCommon = maxCommon;
    auto itH1 = tr1.hits().begin();
    auto itH2 = tr2.hits().begin();
    auto itEnd1 = tr1.hits().end();
    auto itEnd2 = tr2.hits().end();
    while( nCommon!=0 && itH1!=itEnd1 && itH2!=itEnd2){
      if(itH1->hit->id() == itH2->hit->id()){
        --nCommon;
        ++itH1;
        ++itH2;
      }
      else if( itH1->hit->id() < itH2->hit->id() ){
        ++itH1;
      }
      else{
        ++itH2;
      }
    }
    return(nCommon==0);
  }

  /** @brief, It checks if the hough cluster satisfy the tolerance
   */
  bool CheckCluster( HitIter itBeg, HitIter itEnd, float tol){
    return ( (*(itEnd-1)).coord - (*itBeg).coord < tol);
  }
  
  /** @brief It increases the iterators for the loop through the hits in the pool m_myStereo
   */
  void IncreaseIters( HitIter& itBeg, HitIter& itEnd, unsigned int minUV,
                                       HitIter stereoEnd) {
    ++itBeg;
    itEnd = (itBeg + minUV <= stereoEnd ? itBeg+minUV : stereoEnd);
  }
  
  /** @brief Check if the found line in the hough cluster satisfies the selection criteria
   */
  bool LineOK( float minChi2Low, float minChi2High, const PrLineFitterY& line,
               const Pr::Hybrid::SeedTrack& xProje ){
    const int nHits = line.nHitsLine() + xProje.size();
    //Do some kind of weighting here par[0] par[1] ??
    const float Chi2DoFLineXProj = xProje.chi2PerDoF() + line.Chi2DoF();
    //Should check here ( maybe bottleneck for efficiency at large slope in Y )
    return (Chi2DoFLineXProj < ( nHits > 10 ? minChi2High : minChi2Low ));
  }
  
  //=========================================================================
  //  Print the information of the selected hit
  //=========================================================================
  void printHit( MsgStream& msg, const ModPrHit& mhit, std::string title = ""){
    const auto* hit = mhit.hit;
    msg << "  " << title << " "
        << format( " Plane%3d zone%2d z0 %8.2f x0  %8.3f used%2d ",
                   hit->planeCode(), hit->zone(), hit->z(), hit->x(), !mhit.isValid() );
    msg << endmsg;
  }

  //=========================================================================
  //  Print the whole track
  //=========================================================================
  void printTrack ( MsgStream& msg, const Pr::Hybrid::SeedTrack& track ){
    for( const auto& itH : track.hits() ){
      // for ( auto itH = track.hits().begin(); track.hits().end() != itH; ++itH ) {
      msg << format( "dist %7.3f dy %7.2f chi2 %7.2f ", track.distance(*itH.hit),
                     track.deltaY(*itH.hit), track.chi2(*itH.hit) );
      printHit(msg,itH);
    }
  }
  
}

namespace HoleShapeNS {
  std::string toString(const HoleShape& holeshape) {
    switch(holeshape) {
    case HoleShape::NoHole     : return "NoHole";
    case HoleShape::Rectangular: return "Rectangular";
    case HoleShape::Circular   : return "Circular";
    }
    return "";
  }  
  StatusCode parse(HoleShape& result, const std::string& input ) {
    if (input == "NoHole"     ) {result = HoleShape::NoHole     ; return StatusCode::SUCCESS;}
    if (input == "Rectangular") {result = HoleShape::Rectangular; return StatusCode::SUCCESS;}
    if (input == "Circular"   ) {result = HoleShape::Circular   ; return StatusCode::SUCCESS;}
    return StatusCode::FAILURE;
  }
}
// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PrHybridSeeding )
//=============================================================================
// Standard constructor, initializes variable
//=============================================================================

PrHybridSeeding::PrHybridSeeding( const std::string& name,
                                  ISvcLocator* pSvcLocator) :
Transformer(name, pSvcLocator,
            KeyValue{"FTHitsLocation", PrFTInfo::FTHitsLocation},
            KeyValue{"OutputName", LHCb::TrackLocation::Seed})
{
  
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode PrHybridSeeding::initialize() {
  auto sc = Transformer::initialize();
  if( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  if(msgLevel(MSG::DEBUG) ) info() << "==> Initialize" << endmsg;
  m_magFieldSvc = svc<ILHCbMagnetSvc>("MagneticFieldSvc", true);
  m_timerTool.setEnabled( m_doTiming );
  m_debugTool.setEnabled( (!(m_debugTool.name().empty())));
 
  if ( "" != m_debugTool.name() ) {    
    info()<<"Debug tool "<<m_debugTool.name()<<" loaded."<<endmsg;
  }
  if(UNLIKELY(m_doTiming)){
    m_timerTool.retrieve();
    m_timeTotal   = m_timerTool->addTimer( "PrSeeding total" );
    m_timerTool->increaseIndent(); //+1
    m_timeFromForward = m_timerTool->addTimer( "Time from Forward" );
    m_timerTool->increaseIndent(); //+2
    //Case 0
    m_timeXProjeUp[0] = m_timerTool->addTimer( "Case 0 : X Projections Up");
    m_timerTool->increaseIndent(); //+3
    m_timeCloneXUp[0] = m_timerTool->addTimer( "Case 0 : Clones X Up");
    m_timerTool->increaseIndent(); //+4
    m_timeStereoUp[0] = m_timerTool->addTimer( "Case 0 : AddStereo Up");
    m_timerTool->increaseIndent(); //+5
    m_timeFlagUp[0] = m_timerTool->addTimer(   "Case 0 : Flag Up");

    //--- Case 1
    m_timerTool->decreaseIndent(); //+4
    m_timerTool->decreaseIndent(); //+3
    m_timerTool->decreaseIndent(); //+2
    m_timeXProjeUp[1] = m_timerTool->addTimer( "Case 1 : X Projections Up");
    m_timerTool->increaseIndent(); //+3
    m_timeCloneXUp[1] = m_timerTool->addTimer( "Case 1 : Clones X Up");
    m_timerTool->increaseIndent(); //+4
    m_timeStereoUp[1] = m_timerTool->addTimer( "Case 1 : AddStereo Up");
    m_timerTool->increaseIndent(); //+5
    m_timeFlagUp[1] = m_timerTool->addTimer(   "Case 1 : Flag Up");
    m_timerTool->decreaseIndent(); //+4
    m_timerTool->decreaseIndent(); //+3
    m_timerTool->decreaseIndent(); //+2

    //--- Case 2
    m_timeXProjeUp[2] = m_timerTool->addTimer( "Case 2 : X Projections Up");
    m_timerTool->increaseIndent(); //+3
    m_timeCloneXUp[2] = m_timerTool->addTimer( "Case 2 : Clones X Up");
    m_timerTool->increaseIndent(); //+4
    m_timeStereoUp[2] = m_timerTool->addTimer( "Case 2 : AddStereo Up");
    m_timerTool->increaseIndent(); //+5
    m_timeFlagUp[2] = m_timerTool->addTimer(   "Case 2 : Flag Up");
    m_timerTool->decreaseIndent(); //+4
    m_timerTool->decreaseIndent(); //+3
    m_timerTool->decreaseIndent(); //+2

    //--- Case 0 Down
    m_timeXProjeDo[0] = m_timerTool->addTimer( "Case 0 : X Projections Down");
    m_timerTool->increaseIndent(); //+3
    m_timeCloneXDo[0] = m_timerTool->addTimer( "Case 0 : Clones X Down");
    m_timerTool->increaseIndent(); //+4
    m_timeStereoDo[0] = m_timerTool->addTimer( "Case 0 : AddStereo Dowm");
    m_timerTool->increaseIndent(); //+5
    m_timeFlagDo[0] = m_timerTool->addTimer(   "Case 0 : Flag Down");
    m_timerTool->decreaseIndent(); //+4
    m_timerTool->decreaseIndent(); //+3
    m_timerTool->decreaseIndent(); //+2

    //--- Case 1 Down
    m_timeXProjeDo[1] = m_timerTool->addTimer( "Case 1 : X Projections Down");
    m_timerTool->increaseIndent();//+3
    m_timeCloneXDo[1] = m_timerTool->addTimer( "Case 1 : Clones X Down");
    m_timerTool->increaseIndent();//+4
    m_timeStereoDo[1] = m_timerTool->addTimer( "Case 1 : AddStereo Down");
    m_timerTool->increaseIndent();//+5
    m_timeFlagDo[1] = m_timerTool->addTimer(   "Case 1 : Flag Down");
    m_timerTool->decreaseIndent();//+4
    m_timerTool->decreaseIndent();//+3
    m_timerTool->decreaseIndent();//+2

    //--- Case 2 Down
    m_timeXProjeDo[2] = m_timerTool->addTimer( "Case 2 : X Projections Down");
    m_timerTool->increaseIndent(); //+3
    m_timeCloneXDo[2] = m_timerTool->addTimer( "Case 2 : Clones X Down");
    m_timerTool->increaseIndent(); //+4
    m_timeStereoDo[2] = m_timerTool->addTimer( "Case 2 : AddStereo Down");
    m_timerTool->increaseIndent(); //+5
    m_timeFlagDo[2] = m_timerTool->addTimer(   "Case 2 : Flag Down");
    m_timerTool->decreaseIndent(); //+4
    m_timerTool->decreaseIndent(); //+3
    m_timerTool->decreaseIndent(); //+2
    m_timerTool->decreaseIndent();//+1
    m_timeClone[0] = m_timerTool->addTimer(  "RemoveClones Up");
    m_timeClone[1] = m_timerTool->addTimer(  "RemoveClones Down");
    m_timeRecover = m_timerTool->addTimer(   "RecoverTrack");
    m_timeConvert[0] = m_timerTool->addTimer("Convert Tracks Up");
    m_timeConvert[1] = m_timerTool->addTimer("Convert Tracks Down");
    m_timerTool->decreaseIndent();//0
  }

  if(UNLIKELY(m_printSettings)){
    info()<<" ===========================   The algorithm is configured to  ============================= "<< endmsg;

    if( m_FlagHits){
      info()<<"Flag the Hits" << endmsg;
    }else{
      info()<<"Not Flag the Hits"<<endmsg;
    }
    if( m_removeFlagged){
      info()<<"Will Not re-use Flagged"<<endmsg;
    }else{
      info()<<"Will re-use all flagged hits"<<endmsg;
    }
    info()<<"Forward tracking tracks as input"<<endmsg;
    if( m_removeClones){
      info()<<"Will Remove Clones"<<endmsg;
    }else{
      info()<<"Will not Remove Clones"<<endmsg;
    }
    if( m_xOnly){
      info()<<"Will use Only X Layers" <<endmsg;
    }
    info()<<" =========================================================================================== "<<endmsg;

    if( m_useFix && !m_xOnly) info()<<"WIll Use the triangle Fix for stereo layers"<<endmsg;

    info() << "==================================================="<<endmsg
           << "=================GLOBAL SETTINGS==================="<<endmsg
           << " OutputName                                     = "<<  outputLocation<0>()       << endmsg
           << " XOnly                                          = "<<  m_xOnly                   << endmsg
           << " MinXPlanes                                     = "<<  m_minXPlanes              << endmsg
           << " MaxNHits                                       = "<<  m_maxNHits                << endmsg
           << " NCases                                         = "<<  m_nCases                  << endmsg
           << " TimingMeasurement                              = "<<  m_doTiming                << endmsg
           << " dRatio                                         = "<<  m_dRatio                  << endmsg
           <<"  Use dRatioCorrection with XY position ?          "<<  m_useCorrPos              << endmsg
           << " CConst BackProj                                = "<<  m_ConstC                  << endmsg
           << "=================ADD UV Layer Settings============="                             << endmsg
           << "doAsymmUV                                       = "<<  m_doAsymmUV                << endmsg
           << "Use Triangle Fix                                = "<<  m_useFix                  << endmsg
           << "Use SecondOrder `Triangle Fixing                = "<<  m_useFix2ndOrder          << endmsg
           << "*******Fit Settings"<<endmsg
           << "==================Clone Removal and Flag Hits Settings=========="                << endmsg
           << "Remove Clones after X searh       ?             = "<<  m_removeClonesX<<endmsg
           << "Remove Clones after add stereo UV ?             = "<<  m_removeClones<<endmsg
           << "Fraction of common hits to clone kill           = "<<  m_fracCommon <<endmsg
           << "Flag the hits                                   = "<<  m_FlagHits <<endmsg
           << "Remove All Flagged                              = "<<  m_removeFlagged<<endmsg;
    info()<<"=======  Will Run "<<m_nCases<<"  Cases  ===========" <<endmsg;

    info()<<" ============================ Find X Projection Settings ======================="<<endmsg;
    info()<<" 1 ==== First-Last Layer Settings  (2 Hit Combo)  "<<endmsg;
    for(unsigned int kk =0;m_alphaCorrection.size()>kk;kk++){
      info()<<"\t Case "<<kk<<"   Rotation                  ="<<m_alphaCorrection[kk]<<endmsg;
      info()<<"\t Case "<<kk<<"   Tolerance after Rotation  ="<<m_TolFirstLast[kk]<<endmsg;
    }
    info()<<" 2 ==== Add Hit in T2 - XLayers Stations  (3 Hit Combo) "<<endmsg;
    info()<<" Allow N Parabola Seed Hits = " << m_maxParabolaSeedHits;
    for(unsigned int kk =0;m_x0Corr.size() > kk; kk++)
    {
      info() <<"\t Case "<<kk<<"       x0Rotation = "<<m_x0Corr[kk]<<endmsg;
      info() <<"\t Case "<<kk<<"    x0SlopeChange = "<<m_x0SlopeChange[kk]
             <<"    Tolerance at x0SlopeChange "<<  m_TolX0SameSign[kk]<<endmsg;
      info() <<"\t Case "<<kk<<"   x0SlopeChange2 = "<<m_x0SlopeChange2[kk]
             <<"    Tolerance at x0SlopeChange2"<< m_tolX0Oppsig[kk]<<endmsg;
      info() <<"\t Case "<<kk<<"             x0Max = "<<m_x0Cut[kk]<<"    Tolerance Up at x0Max"<< m_tolAtX0Cut[kk]
             << " Tolerance Down at x0Max "<<m_tolAtx0CutOppSig[kk]<<endmsg;
    }
    info()<<" 3 ==== Add Hit in remaining Layers (4/5/6 Hit Combo) ---- "<<endmsg;
    for( unsigned int kk =0; m_tolRemaining.size()>kk;kk++){
      info()<<"\t Case"<<kk<<"    Tolerance = "<<m_tolRemaining[kk]<<endmsg;
    }
    info()<<" 4 ==== Fit XZ Projection  "<<endmsg;
    info()<<"\t minXPlanes "<< m_minXPlanes;
    info()<<"\t dRatio " << m_dRatio<<endmsg;
    for( unsigned int kk =0; m_maxChi2HitsX.size()>kk;kk++){
      info()<<"\t Case "<<kk<<"      MaxChi2Hit X Fit    = "<<m_maxChi2HitsX[kk]<<endmsg;
      info()<<"\t Case "<<kk<<"      MaxChi2Track X Fit  = "<<m_maxChi2DoFX[kk]<<endmsg;
    }
    info()<<" Remove Clones X " << m_removeClonesX<< endmsg;
    info()<<" ========================= Add The UV part =================================== "<<endmsg;
    info()<<" ===> Hit Selection pre-Hough Clustering "<<endmsg;
    info()<<"\t Y Min                 "<< m_yMin      <<endmsg
          <<"\t Y Max                 "<< m_yMax      <<endmsg
          <<"\t UseTrFix              "<<m_useFix     <<endmsg
          <<"\t Y Min TrFix           "<< m_yMin_TrFix<<endmsg
          <<"\t Y Max TrFix           "<< m_yMax_TrFix<<endmsg
          <<"\t Asymmetric UV search  "<< m_doAsymmUV <<endmsg
          <<"\t Hole Shape            "<<m_holeShape  <<endmsg
          <<"\t Hole width X          "<<m_holeWidthX <<endmsg
          <<"\t Hole width Y          "<<m_holeWidthY <<endmsg
          <<"\t Squared radius Hole   "<<m_holeSqRadius <<endmsg
          <<"\t TrFix 2nd Order       "<<m_useFix2ndOrder<<endmsg
          <<"\t Max N Hits            "<<m_maxNHits   <<endmsg
          <<endmsg;

    info()<<" ===> Case selection Hough clusters and track fit"<<endmsg;
    for( unsigned int kk = 0; m_tolTyOffset.size()>kk; kk++){
      info()<<"========  \t Case "<<kk<<" ======"<<endmsg;
      info()<<" maxTy-minTy < " <<m_tolTyOffset[kk]<<" + "<< m_tolTySlope[kk] <<" * minTy"<<endmsg;
      info()<<"--> Special nTot Hits <11 ----- "<<endmsg;
      info()<<"max   Chi2DoFX + Chi2Line (when <11) hit < "<<m_Chi2LowLine[kk]<<endmsg;
      info()<<"max   y(z=0)                             < "<<m_maxY0Low[kk]<<endmsg;
      info()<<"max   y(zRef)                            < "<<m_maxYZrefLow[kk]<<endmsg;
      info()<<"max   Chi2 Hit (full fit)                < "<<m_maxChi2HitFullFitLow[kk]<<endmsg;
      info()<<"--> Special nTot Hits >10 ----- "<<endmsg;
      info()<<"max   Chi2DoFX + Chi2Line (when >10) hit < "<<m_Chi2HighLine[kk]<<endmsg;
      info()<<"max   Chi2 Hit (full fit)                < "<<m_maxChi2HitFullFitHigh[kk]<<endmsg;
      info()<<"--> Final Chi2DoF                        < "<<m_maxChi2PerDoF[kk]<<endmsg;
    }

    info()<<" ====================== Flag Hits ============================== " << endmsg;
    for( unsigned int kk = 0; m_SizeFlag.size()>kk;kk++){
      info()<<" ======= \t Case "<<kk<<" ======="<<endmsg;
      info()<<"\t Case"<<kk<<" Size Flag"<< m_SizeFlag[kk]<<endmsg;
      info()<<"\t Case"<<kk<<" MaxChi2DoF when 11 hits to flag : "<<m_MaxChi2Flag[kk]<<endmsg;
      info()<<"\t Case"<<kk<<" Max Back Projection when 11 hits: "<<m_MaxX0Flag[kk]<<endmsg;
    }

    info()<<" =====================  RECOVER TRACK ROUTINE =================== "<<endmsg;
    if(!m_recover){
      info()<<" WILL NOT RECOVER TRACKS"<<endmsg;
    }else{
      info()<<" WILL RECOVER TRACKS"<<endmsg;
      info()<<" All hits found are marked as used, you will recover the x/z projection not promoted if"<<endmsg;
      info()<<" NX = 6 : used  < "<<m_nusedthreshold[0]<<endmsg;
      info()<<" NX = 5 : used  < "<<m_nusedthreshold[1]<<endmsg;
      info()<<" NX = 4 : used  < "<<m_nusedthreshold[2]<<endmsg;
      info()<<" Max Nb of clusters to process in stereo hit add                   : "<<m_recoNCluster<<endmsg;
      info()<<" Min Nb of hits on recovered full track                            : "<<m_recoMinTotHits<<endmsg;
      info()<<" Min Nb of UV hits to attach at x/z proj recovered with 6 hits     : "<<m_recover_minUV[0]<<endmsg;
      info()<<" Min Nb of UV hits to attach at x/z proj recovered with 5 hits     : "<<m_recover_minUV[1]<<endmsg;
      info()<<" Min Nb of UV hits to attach at x/z proj recovered with 4 hits     : "<<m_recover_minUV[2]<<endmsg;
      info()<<" Hough cluster spread in recovering routine                        : "<<m_recoTolTy <<endmsg;
      info()<<" Max Y(0) of the track recovered                                   : "<<m_recomaxY0<<endmsg;
      info()<<" Recover Line Fit >10 hits                                         : "<<m_recoLineHigh<<endmsg;
      info()<<" Recover Line Fit <10 hits                                         : "<<m_recoLineLow<<endmsg;
      info()<<" Recover ChiDoF                                                    : "<<m_recoFinalChi2<<endmsg;
      info()<<" Recover Chi Outlier                                               : "<<m_recoChiOutlier<<endmsg;
    }
  }
  if( m_nCases >3){
    error()<<"Algorithm does not support more than 3 Cases"<<endmsg;
    return StatusCode::FAILURE;
  }

  // Zones cache, retrieved from the detector store
  registerCondition<PrHybridSeeding>(PrFTInfo::FTZonesLocation, m_zoneHandler);

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
std::vector<PrHybridSeeding::Track> PrHybridSeeding::operator()(const PrFTHitHandler<PrHit>& FTHitHandler) const {
  // Containers
  TrackCandidates trackCandidates;
  XCandidates xCandidates;
  TrackToRecover trackToRecover;

  if(msgLevel(MSG::DEBUG) ) info() << "==> Execute" << endmsg;
  if(UNLIKELY(m_doTiming) ){
    m_timerTool->start( m_timeTotal );
    m_timerTool->start( m_timeFromForward );
  }

  // Construct hit handler of ModPrHits
  // The track candidates will contain copies of the ModHits, but each will contain their
  // own index in the hit container, which can be used used to flag the original hits.
  std::vector<ModPrHit> hits; hits.reserve(FTHitHandler.hits().size());
  size_t i = 0;
  for (const auto& hit : FTHitHandler.hits().range()) {
     hits.push_back({&hit, 0, hit.planeCode(), i});
     ++i;
  }
  PrFTHitHandler<ModPrHit> hitHandler(std::move(hits), FTHitHandler.hits().offsets());

  //========================================================
  // Remove Seed segment if we pass the  Forward as Input
  //========================================================
  // This was commented by Sebastien Ponce on Oct 5th 2016
  // after mail discussion with Renato Quagliani that stated :
  // "you can comment it out, since by default the seeding is
  // not reading Forward output"
  // Commenting this was necessary to move to the functional
  // framework as inputs cannot be dependent on external values
  // Note that the property inputName has been dropped completely
  // here is how it was declared :
  //   declareProperty("InputName", m_inputName = "");
  /*
  if ( "" != m_inputName){
    debug()<<"Removing Seed Segment from Forward Tracking"<<endmsg;
    //marking hits in forward tracks as used
    for(int i = 0; i < 24; i++){
      for(auto& j : FTHitHandler.hits(i)) {
        if( j.isUsedInTrack() ) j.setUsed(); //Tuning done in PrForwardTool //TODO make this smarter?
      }
    }
    //convert forward track- T station segments to seed tracks and push them in the final container directly
    Tracks* forward = get<Tracks>( m_inputName );
    for ( auto itT = forward->begin(); forward->end() != itT; ++itT ){
      if( (*itT)->info( Track::AdditionalInfo::PatQuality, 0. ) > 0.8) continue;  //quality from 0-1 (good-bad)
      std::vector<LHCb::LHCbID> ids;
      ids.reserve(20);
      for ( std::vector<LHCb::LHCbID>::const_iterator itId = (*itT)->lhcbIDs().begin();
            (*itT)->lhcbIDs().end() != itId; ++itId ) {
        if( (*itId).isFT() && (*itId).ftID().layer() < 12 ){
          ids.push_back( *itId );
        }
      }
      Track* seed = new Track;
      seed->setLhcbIDs( ids );
      seed->setType( Track::Types::Ttrack );
      //TODO give special flag that it is already a forward (thus long) track!
      seed->setHistory( Track::History::PrSeeding );
      seed->setPatRecStatus( Track::PatRecStatus::PatRecIDs );
      seed->addToStates( (*itT)->closestState( 9000. ) );
      result.insert( seed );
    }
  }
  */

  //==========================================================
  // Hits are ready to be processed
  //==========================================================

  if( UNLIKELY(m_doTiming) ) {
    m_timerTool->stop( m_timeFromForward );
  }

  //========================================================
  //------------------MAIN SEQUENCE IS HERE-----------------
  //========================================================
  //----- Loop through lower and upper half
  for( unsigned int part= 0; 2 > part; ++part ){
    //Swap them ? externally icase & inner loop part? xCandidates
    for(unsigned int icase = 0; m_nCases > icase ; ++icase){
      if(UNLIKELY(m_doTiming)){
        if( part ==0){
          m_timerTool->start( m_timeXProjeUp[icase]);
        }else{
          m_timerTool->start( m_timeXProjeDo[icase]);
        }
      }
      xCandidates[(int)part].clear(); //x candidates up cleaned every Case!
      findXProjections(part, icase, hitHandler, xCandidates);
      if(UNLIKELY(m_doTiming)){
        if( part ==0){
          m_timerTool->stop( m_timeXProjeUp[icase]);
          m_timerTool->start( m_timeCloneXUp[icase]);
        }else{
          m_timerTool->stop( m_timeXProjeDo[icase]);
          m_timerTool->start(m_timeCloneXDo[icase]);
        }
      }
      if (m_removeClonesX) {
        removeClonesX(part, icase, m_xOnly, trackCandidates, xCandidates);
      }
      if( UNLIKELY(m_doTiming)){
        if( part==0 ){
          m_timerTool->stop( m_timeCloneXUp[icase]);
          m_timerTool->start(m_timeStereoUp[icase]);
        }else{
          m_timerTool->stop( m_timeCloneXDo[icase]);
          m_timerTool->start(m_timeStereoDo[icase]);
        }
      }

      //Add The stereo Part
      if(!m_xOnly){
        addStereo(part, icase , hitHandler, trackCandidates, xCandidates, trackToRecover);
      }
      if(UNLIKELY(m_doTiming)){
        if(part == 0){
          m_timerTool->stop( m_timeStereoUp[icase]);
          m_timerTool->start(  m_timeFlagUp[icase]);
        }else{
          m_timerTool->stop( m_timeStereoDo[icase]);
          m_timerTool->start( m_timeFlagDo[icase]);
        }
      }
      //Flag found Hits at the end of each single case ( exclude the latest one )
      //icase = 0,1,2
      //m_nCase = 1,2,3
      if(m_FlagHits && (icase +1 < m_nCases)   && !m_xOnly){
        flagHits(icase, part, trackCandidates, hitHandler);
      }
      if(UNLIKELY(m_doTiming)){
        if( part ==0 ){
          m_timerTool->stop( m_timeFlagUp[icase]);
        }else{
          m_timerTool->stop(m_timeFlagDo[icase]);
        }
      }
    }
  }
  for( unsigned int part = 0 ; part<2; part ++){
    if(UNLIKELY(m_doTiming)){
      m_timerTool->start( m_timeClone[(int)part]);
    }
    //Clone removal ( up/down )
    removeClones(part, trackCandidates);
    if(UNLIKELY(m_doTiming)){
      m_timerTool->stop( m_timeClone[ (int) part]);
    }
  }
  if( m_doTiming){
    m_timerTool->start(m_timeRecover);
  }
  if( m_recover){
    xCandidates[0].clear();
    xCandidates[1].clear();
    RecoverTrack(hitHandler, trackCandidates, xCandidates, trackToRecover);
  }
  if(UNLIKELY(m_doTiming)){
    m_timerTool->stop( m_timeRecover);
  }

  std::vector<Track> result{};
  result.reserve(trackCandidates[0].size() + trackCandidates[1].size());
  // Is it really necessary to have the timing for both parts? Without it it would be easier to not have output parameter
  for( unsigned int part=0 ; part<2; part++){
    if(UNLIKELY(m_doTiming)){
      m_timerTool->start(m_timeConvert[(int)part]);
    }
    //Convert LHCb tracks
    makeLHCbTracks(result, part, trackCandidates);
    if(UNLIKELY(m_doTiming)){
      m_timerTool->stop( m_timeConvert[(int)part]);
    }
  }
  if(msgLevel(MSG::DEBUG)) debug()<<"Making LHCb Tracks Done"<<endmsg;
  if(UNLIKELY(m_doTiming)){
    m_timerTool->stop( m_timeTotal);
  }
  return result;
}

void PrHybridSeeding::CollectUV(  Pr::Hybrid::SeedTrack& xProje,
                                  const std::vector<unsigned int>& uvZones,
                                  ModPrHits& myStereo,
                                  const PrFTHitHandler<ModPrHit>& FTHitHandler) const noexcept{
  unsigned int part = xProje.zone();
  for( const unsigned int& kk : uvZones){
    float yMin = -std::abs(m_yMax);
    float yMax =  std::abs(m_yMax);
    float dxDy = m_zoneHandler->zone(kk).dxDy();
    float zPlane = m_zoneHandler->zone(kk).z();

    float xPred = xProje.x(zPlane);
    float recDxDyZ = 1./(dxDy*zPlane);
    float recDxDy = 1./dxDy;
    if(yMin >yMax){
      std::swap( yMin, yMax);
    }
    if(part==0){
      //Upper tracks ( expect y>0 for them )
      yMin = m_yMin;
      //[-1,2500] mm
      if(kk%2==0 && m_useFix){
        //Upper track but lower module looking at
        //y looked in [-2,30] mm
        yMin = +m_yMin_TrFix;
        yMax = +m_yMax_TrFix;
      }
    }else{
      //Lower tracks ( expected y < 0 ( m_yMin need to be negative, m_yMax need to be positive) )
      yMax = -m_yMin;
      //[-2500,+1] mm
      if(kk%2!=0 && m_useFix){
        //Lower track but upper module looking at
        //y looked in [-30,+2] mm
        yMin = -m_yMax_TrFix;
        yMax = -m_yMin_TrFix;
      }
    }
    // Compute the [xMin,xMax] value in the rotated reference system based on y tolerance!
    // Due to dxDy being positive or negative ( yMin here is always < yMax ) you can have them swapped
    // x(y) ~ xPred = x(0) + dxDy*y => x(0) = xPred - dxDy*y ; xMin xMax from yMin and yMax and
    // swap them if opposite in sign
    float xMin = xPred - yMin*dxDy;
    float xMax = xPred - yMax*dxDy;
    if(xMin > xMax){
      std::swap( xMax, xMin);      //revert to properly take into account dxDy
    }
    auto itH = FTHitHandler.getIterator_lowerBound( kk   , xMin);
    auto EndLay = FTHitHandler.hits(kk).end();
    auto itEnd = FTHitHandler.get_upperBound_log( itH, EndLay, xMax);
    for( ; itEnd!= itH; ++itH ){
      const PrHit* hit = itH->hit;
      if( !itH->isValid() && m_removeFlagged ) continue;
      float y = ( xPred - hit->x() )*recDxDy;
      if(y >yMax || y <yMin ) continue; //may can skip this if
      if(m_useFix2ndOrder){
        if(part == 0){
          //Up track
          if(kk%2 != 0){
            //Up track Up modules
            if( hit->yMin()<0. ){ //leaking area to y negative in up modules
              yMin = m_yMin; yMax = m_yMax;   //[-1.0, 2500]
            }else{//y positive but cut away
              yMin = -2. + std::fabs(hit->yMin());yMax = m_yMax;}
          }else{//kk%2 ==0
            //Up track Down modules
            if( hit->yMax()<0. ){ continue;
            }else{
              yMin = -1. ;yMax = 2. + std::fabs(hit->yMax());
            }
          }
        }else{
          //Down track
          if(kk%2 == 0){
            //Down track Down modules
            if( hit->yMax()>0.){ //leaking area to y positive in down module
              yMin = -std::fabs(m_yMax) ; yMax = +1.0; //[-2500, 1.0]
            }else{//y negative but cut away
              yMin = -std::fabs(m_yMax) ;yMax = +2.-std::fabs(hit->yMax()); // [ -2500, 2.0 ]
            }
          }else{//kk%2 ==1
            //Down track Up modules
            if( hit->yMin()<0.){
              yMin = -2. - std::fabs(hit->yMin());yMax = +1.;
            }else{
              continue;
            }
          }
        }
        if(msgLevel(MSG::DEBUG)){
          debug()<<"y Hit computed    "<< y<<endmsg;
        }
      }
      if(y>yMax || y< yMin) continue;
      if (this->isInHole(xPred,y))
        continue;
      //Absolute value!
      float coord;
      if( part == 0 && y<0.){
        coord = -std::fabs((xPred-hit->x())*recDxDyZ);
        //hit->setCoord( -1.*std::fabs((xPred-hit->x())*recDxDyZ));
      }else if( part == 1 && y>0.){
        coord = -std::fabs((xPred-hit->x())*recDxDyZ);
        //hit->setCoord( -1.*std::fabs((xPred-hit->x())*recDxDyZ));
      }else{
        //part ==1 & y<0 or part ==0 & y>0
        coord = std::fabs( (xPred - hit->x() )*recDxDyZ ) ;
        //hit->setCoord( std::fabs( (xPred - hit->x() )*recDxDyZ ) );
      }
      myStereo.push_back(*itH);
      myStereo.back().coord = coord;
    }
  }
  std::sort( myStereo.begin(), myStereo.end());
}

bool PrHybridSeeding::isInHole(float x, float y) const noexcept
{
  switch(m_holeShape) {
  case HoleShape::NoHole: 
    return false;
  case HoleShape::Rectangular:
    return (std::abs(y) < m_holeWidthY && std::abs(x) < m_holeWidthX);
  case HoleShape::Circular:
    return (y*y + x*x < m_holeSqRadius);
  default:
    {
      __builtin_unreachable(); // note: we should define a macro similar to LIKELY and UNLIKELY for this purpose...
    }
  }
  //You should never reach here.
  return true;
}
void PrHybridSeeding::addStereo(unsigned int part, unsigned int iCase,
                                const PrFTHitHandler<ModPrHit>& FTHitHandler,
                                TrackCandidates& trackCandidates,
                                XCandidates& xCandidates,
                                TrackToRecover& trackToRecover) const noexcept{
  if(msgLevel(MSG::DEBUG)){ debug()<<"Adding stereo hits for part = "<<part<<" and Case = "<<iCase<<endmsg;
  }
  //reco is used here to switch on the track recovering configurables
  bool reco = false;
  if(iCase == 4){
    reco = true;
    iCase =2;
  }
  std::vector<unsigned int> uvZones;
  if(m_useFix){  uvZones.reserve(12);
  }else{         uvZones.reserve(6); }
  //---- Load the u/v layers to look for hits
  if(m_useFix){
    for( unsigned int detector_part :{0,1}){
      for( unsigned int uvZoneID :{ T1U, T1V,T2U,T2V,T3U,T3V}){
        if(msgLevel(MSG::DEBUG)){ debug()<<"Push back zone = "<<uvZoneID-detector_part<<endmsg;
          if( m_zoneHandler->zone(uvZoneID-detector_part).isX()) always()<<"ERROR LOAD"<<endmsg;
        }
        uvZones.push_back( uvZoneID - detector_part );
      }
    }
  }else{
    for( unsigned int uvZoneID :{T1U, T1V,T2U,T2V,T3U,T3V}){
      uvZones.push_back( uvZoneID - part );
    }
  }

  PrPlaneHybridCounter plCount;
  int maxUVfound = 0;
  PrLineFitterY BestLine( zReference );
  float initCoord = std::numeric_limits<float>::max();
  float ClusterSpread[3][DEPTH_HOUGH];
  ModPrHits myStereo; myStereo.reserve(100);
  HitIter initBeg[3][DEPTH_HOUGH];
  HitIter initEnd[3][DEPTH_HOUGH];

  //TODO : HoughUtils from Manuel Shiller could be used here

  if(msgLevel(MSG::DEBUG)){
    debug() << " Will loop on x-candidates case "<<iCase <<" Part = "<<part<<endmsg;
  }
  for( auto&& itT : xCandidates[part]){
    if( ! itT.valid() ) continue;
    myStereo.clear();
    CollectUV( itT , uvZones , myStereo, FTHitHandler);

    //Define the min UV hits based on the amount of hits of the track
    unsigned int minUV = 4;
    if( itT.size()==6){
      if(!reco){ minUV = m_minUV6[iCase];
      }else{
        minUV = m_recover_minUV[0];
      }
    }else if( itT.size()==5){
      if(!reco){  minUV = m_minUV5[iCase];
      }else{
        minUV = m_recover_minUV[1];
      }
    }else if( itT.size()==4){
      if(!reco){ minUV = m_minUV4[iCase];
      }else{
        minUV = m_recover_minUV[2];
      }
    }
    maxUVfound=-1;
    unsigned int firstSpace = trackCandidates[(int)part].size();
    //initialize the clusters ( here 9 )
    if(msgLevel(MSG::DEBUG)) always()<<" initializing clusters "<<endmsg;
    for( int i =0; i<3 ; ++i){
      for( int j = 0; j<DEPTH_HOUGH; ++j){
        ClusterSpread[i][j] = initCoord;
        initBeg[i][j] = myStereo.end();
        initEnd[i][j] = myStereo.end();
      }
    }
    auto itBeg = myStereo.cbegin();
    auto itEnd =  itBeg+4<=myStereo.cend() ? itBeg + 4 : myStereo.cend();
    if(msgLevel(MSG::DEBUG)){ debug()<<"Storing clusters"<<endmsg;}

    // Matrix 3xDEPTHHOUGH with 2iterators to myStereo (begin/end)
    //
    // matrix is filled advancing of steps of size 4 in myStereo when good cluster if found
    //
    //     | 6Hits(spread1) 6Hits(spread2) 6Hits(spread3) ---- 6Hits(spreadHOUGHDEPTH) |
    // M = | 5Hits(spread1) 5Hits(spread2) 5Hits(spread3) ---- 6Hits(spreadHOUGHDEPTH) |
    //     | 4Hits(spread1) 4Hits(spread2) 4Hits(spread3) ---- 6Hits(spreadHOUGHDEPTH) |
    //
    //---- spread1< spread2 < spread3 ....< spreadHOUGHDEPTH

    for( ; itEnd-itBeg>=4; IncreaseIters( itBeg, itEnd,4, myStereo.end())){
      // always steps of 4 as minimum!
      float tolTy = m_tolTyOffset[iCase] + m_tolTySlope[iCase]*(*itBeg).coord;
      if( reco ){  tolTy = m_recoTolTy; } //different tolTy when you are in the recovering routine
      for (int i = 0; i < 3; ++i){
        //3 because you have clusters with 4/5/6 hits (more  u/v layers ? CHANGE IT!)
        auto itLast = itEnd+2-i <= myStereo.end() ? itEnd+2-i : myStereo.end();
        // spread6 computed first
        float spread = (*(itLast-1)).coord - (*itBeg).coord;
        if(CheckCluster(itBeg, itLast, tolTy)){
          //cluster is fine check
          int j;
          // find the cluster worse than the current cluster
          for (j = 0; j < DEPTH_HOUGH; ++j){
            if(spread < ClusterSpread[i][j]) break;
          }
          if (j == DEPTH_HOUGH) continue;
          // Shift clusters after the found position
          for(int k = DEPTH_HOUGH-1; k > j; --k){
            initBeg[i][k] = initBeg[i][k-1];
            ClusterSpread[i][k] = ClusterSpread[i][k-1];
            initEnd[i][k] = initEnd[i][k-1];
          }
          // insert current cluster at found position
          initBeg[i][j] = itBeg;
          ClusterSpread[i][j] = spread;
          initEnd[i][j] = itLast;
          break;
        }
      }
    }
    //you have a minimal number of total hits to find on track dependent if standard 3 cases or from Recover routine
    unsigned int minTot = m_minTot[iCase];
    if(reco) minTot = m_recoMinTotHits;
    if( minTot <9 ) minTot= 9;
    //WARNING : protection for the algorithm , do not allow the user to require less than 9 hits.
    if(msgLevel(MSG::DEBUG)) info()<<"Cluster stored"<<endmsg;
    int nXinit = itT.size();
    BestLine.setXProj( itT );
    int nCandidates = 0;
    int maxNClusters = m_maxNClusters[iCase];
    float minChi2LineLow = m_Chi2LowLine[iCase];
    float minChi2LineHigh = m_Chi2HighLine[iCase];
    float maxChi2DoF = m_maxChi2PerDoF[iCase];
    if( reco){
      minTot = m_recoMinTotHits;
      maxNClusters = m_recoNCluster; //max N of clusters to process changed when in reco case.
      minChi2LineLow = m_recoLineLow;
      minChi2LineHigh = m_recoLineHigh;
      maxChi2DoF = m_recoFinalChi2;
    }
    //Matrix filled
    for( int i = 0; i<3; ++i){
      if( maxUVfound == 6 && !reco) break;
      for( int j = 0; j<DEPTH_HOUGH; ++j){
        if( maxUVfound == 6 && !reco) break;
        if(msgLevel(MSG::DEBUG)){ debug()<<"Processing cluster i ="<<i<<"  j = "<<j<<endmsg; }
        //you are at the cluster Number (1+j) +(3*i)
        //By construction the first clusters you process are the ones with smaller spread
        auto itBeg = myStereo.cbegin();
        auto itEnd = myStereo.cend();
        int nLay = 0;
        itBeg = initBeg[i][j];
        itEnd = initEnd[i][j];
        if( itBeg == myStereo.end() && itEnd == myStereo.end()){ break;}
        ExtendCluster( itBeg, itEnd , myStereo.end(), iCase,  nLay ,reco);
        //maxUVfound are the minimal UV layers to find since track quality is based on nHits
        if( nXinit + nLay < (int)minTot ) continue;
        //if you already have less than minTot layers, look to next "extended" cluster"
        if( !reco){  //recovering track routine accept also same size hits!
          if(  (int)nLay <= (int)maxUVfound || (int)nLay < (int)minUV )  continue; //<= if at the end you have only >= maxUVfound.
        }else{
          if(  (int)nLay < (int)maxUVfound || (int)nLay < (int)minUV ) continue; //<= in reco you allow to find same size tracks.
        }
        // You increased the clusters and check to have at least minUV hits and number
        // of fired layers in current clusters can kill current candidates produced
        nCandidates++;
        if( nCandidates > maxNClusters ) break;
        //Set the plane counter
        plCount.set( itBeg , itEnd );
        if(UNLIKELY( msgLevel(MSG::DEBUG))){
          debug()<<" Plane counter diff UV = "<< plCount.nbDifferentUV() <<" n Lay from extending cluter   "<< nLay<<endmsg;
        }
        nLay = plCount.nbDifferentUV();
        if( nLay == 4 && !plCount.isOKUV()){
          continue;
        }
        BestLine.setXProj( itT );
        Pr::Hybrid::SeedTrack temp( itT );
        bool fit = false;
        if( plCount.nbSingleUV() == plCount.nbDifferentUV()){
          if(msgLevel(MSG::DEBUG)){
            info()<<"Fitting line when same amount of UV in Planes"<<endmsg;
          }
          fit = BestLine.fit( itBeg,itEnd );
          if( fit && LineOK( minChi2LineLow, minChi2LineHigh, BestLine, temp) ){
            for( auto itH = itBeg; itEnd != itH; ++itH){
              temp.addHit(*itH);
            }
            temp.setYParam( BestLine.ay0(), BestLine.by());
          }
        }else{
          // More than one hit per layer in the cluster
          // Remove outliers from multiple hit in layers is forced here
          fit = BestLine.fit2( itBeg, itEnd, plCount );
          for( auto itH = itBeg; itEnd!= itH; ++itH){
            temp.addHit(*itH);
          }
          auto hitEnd = temp.hits().end();
          while( BestLine.nHitsLine() > plCount.nbDifferentUV() ){
            BestLine.fit2(temp.hits().begin(), hitEnd, plCount);
            if(msgLevel(MSG::DEBUG)){ debug()<<" Removing outliers"<<endmsg; }
            auto worstMultiple = temp.hits().end();
            float worstchi = std::numeric_limits<float>::min();
            float chi_hit = std::numeric_limits<float>::min();
            for( auto line_hit = temp.hits().begin(); line_hit != hitEnd; ++line_hit){
              if(line_hit->hit->isX()) continue;
              if( plCount.nbInPlane(line_hit->planeCode) < 2) continue;
              chi_hit = BestLine.chi2hit(*line_hit);
              if( chi_hit > worstchi ){
                worstchi = chi_hit ;
                worstMultiple = line_hit;
              }
            }
            // it will just change the m_planeCode[int] not the whole internal hit counting ! )
            plCount.removeHitInPlane(worstMultiple->planeCode);
            // Avoid relocations by swapping the worst hit to the last hit and reducing the end by one.
            std::iter_swap(hitEnd - 1, worstMultiple);
            --hitEnd;
            fit = BestLine.fit2(temp.hits().begin(), hitEnd, plCount);
          }
          // Erase the hits that were removed by swapping
          temp.hits().erase(hitEnd, temp.hits().end());
          //fit status is the Success of the fit ( no requirements on Chi2 )
          if( fit ){
            fit = LineOK( minChi2LineLow, minChi2LineHigh, BestLine, itT );
            temp.setYParam( BestLine.ay0(), BestLine.by());
          }
        }
        if( temp.size() < minTot || !fit ) continue;
        bool ok = false;
        temp.setnXnY( itT.size(), plCount.nbDifferentUV());
        ok = fitSimultaneouslyXY(temp,iCase);
        while(!ok && temp.size() > minTot ){
          ok = removeWorstAndRefit( temp, iCase);
          if( temp.size()>m_maxNHits) ok = false;
        }
        if( ok  && temp.size() >= minTot){
          Pr::Hybrid::updateChi2(temp);
          if( temp.chi2PerDoF() < maxChi2DoF ){
            temp.setCase(iCase);
            int nUVfound = temp.size()-nXinit;
            if( nUVfound > maxUVfound && !reco){
              if( (
                   ( temp.size() < 11 &&
                     (std::fabs(temp.y(0.))< m_maxY0Low[iCase] &&
                      std::fabs(temp.y(zReference)) < m_maxYZrefLow[iCase] )
                     ) ||
                   ( temp.size() > 10 ) ) ){
                if(m_removeClones){
                  std::sort(temp.hits().begin(), temp.hits().end(), compLHCbID());
                }
                maxUVfound = (int)temp.size()-nXinit;
                trackCandidates[(int)part].push_back( temp );
              }
            }else if( m_recover && reco && nUVfound >= maxUVfound &&
                      std::fabs( temp.y(0.)) < m_recomaxY0  && temp.size() >= minTot){
              maxUVfound = (int)temp.size()-nXinit;
              trackCandidates[(int)part].push_back( temp );
              //ordered by spread take one small spread passing selections.
            }
          }
        }
      }
    }
    if( m_recover && !reco && trackCandidates[(int)part].size() == firstSpace ){
      trackToRecover[(int)part].push_back( itT );
    }
    //if in reco case keep them all.
    if(  trackCandidates[(int)part].size() > firstSpace+1  ){
      for( unsigned int kk = firstSpace; trackCandidates[(int)part].size() > kk + 1 ; ++kk ){
        if(!trackCandidates[(int)part][kk].valid()) continue;
        for( unsigned int ll = kk + 1; trackCandidates[(int)part].size() > ll; ++ll ){
          if(!trackCandidates[(int)part][ll].valid())continue;
          if(trackCandidates[(int)part][ll].size() < trackCandidates[(int)part][kk].size()){
            trackCandidates[(int)part][ll].setValid( false );
          }
          else if ( trackCandidates[(int)part][ll].size() > trackCandidates[(int)part][kk].size()){
            trackCandidates[(int)part][kk].setValid( false );
          }
          else if( trackCandidates[(int)part][kk].chi2() < trackCandidates[(int)part][ll].chi2()) {
            trackCandidates[(int)part][ll].setValid( false );
          }
          else{
            trackCandidates[(int)part][kk].setValid( false );
          }
        }//end loop track Candidates for removal bad tracks
      }//loop candidate removal
    }
  }
}


void PrHybridSeeding::RecoverTrack(PrFTHitHandler<ModPrHit>& FTHitHandler,
                                   TrackCandidates& trackCandidates,
                                   XCandidates& xCandidates,
                                   TrackToRecover& trackToRecover) const noexcept{
  for( int part = 0; part< 2; part++){
    for( auto&& itT1 : trackCandidates[(int)part]){
      if( !itT1.valid() ) continue;
      for( auto&& hit : itT1.hits()){
        FTHitHandler.hit(hit.hitIndex).setInvalid();
      }
    }
  }
  PrPlaneHybridCounter plCount;
  for( int part = 0; part<2; part++){
    for( auto&& itT1 : trackToRecover[(int)part]){
      int nused_threshold = 1;
      bool recover_it = false;
      if( itT1.size() == 6){
        nused_threshold = m_nusedthreshold[0];
      }else if( itT1.size() == 5){
        nused_threshold = m_nusedthreshold[1];
      }else if( itT1.size() == 4){
        nused_threshold = m_nusedthreshold[2];
      }
      int nUsed = plCount.nUsed( itT1.hits().begin(), itT1.hits().end(), FTHitHandler);
      if( nUsed < nused_threshold ){
        if( nUsed ==0){
          recover_it = true;
        }else if( nUsed > 0){
          while(nUsed > 0){
            auto it = std::remove_if( itT1.hits().begin(), itT1.hits().end(),
                                      [&FTHitHandler](const ModPrHit& hit){ return ! FTHitHandler.hit(hit.hitIndex).isValid();});
            auto n = std::distance(it, itT1.hits().end());
            itT1.hits().erase(it, itT1.hits().end());
            nUsed -= n;
          }
          recover_it = plCount.hasT1T2T3Track( itT1.hits() );
          if( recover_it) {
            recover_it = fitXProjection( itT1 , 2 );
            Pr::Hybrid::updateChi2X(itT1);
            recover_it = true;
          }
        }
        if(recover_it){
          itT1.setRecovered(true);
        }
      }
    }
    std::sort(trackToRecover[(int)part].begin(), trackToRecover[(int)part].end(), Pr::Hybrid::SeedTrack::GreaterBySize);
    unsigned int maxCommon =0;
    for( auto itT1 = trackToRecover[(int)part].begin(); itT1!= trackToRecover[(int)part].end(); ++itT1){
      if( !(*itT1).recovered() ) continue;
      maxCommon = std::floor( 0.7*(*itT1).size() ); //70 % of lower number of hits track
      for( auto itT2 = itT1+1; itT2!= trackToRecover[(int)part].end(); ++itT2){
        if( !(*itT2).recovered()) continue;
        if( !CloseEnough( (*itT1), (*itT2), 5.)) continue;
        if( areClones((*itT1), (*itT2), maxCommon) ){
          (*itT1).setRecovered(false);
          break;
        }
      }
      if( (*itT1).recovered() ){
        xCandidates[int(part)].push_back( (*itT1));
      }
    }
    addStereo(part, 4 , FTHitHandler, trackCandidates, xCandidates, trackToRecover);
    trackToRecover[(int)part].clear();
  }
}



//=====================================================
//Add Stereo method and support methods
//=====================================================

void PrHybridSeeding::removeClonesX(unsigned int part,
                                    unsigned int icase,
                                    bool xOnly,
                                    TrackCandidates& trackCandidates,
                                    XCandidates& xCandidates) const noexcept{
  std::sort(xCandidates[(int)part].begin(), xCandidates[(int)part].end(),Pr::Hybrid::SeedTrack::GreaterBySize);
  unsigned int maxCommon = 0;
  if( ! m_xOnly ){
    for( auto itT1 = xCandidates[part].begin(); xCandidates[part].end() !=itT1; ++itT1){
      if(!(*itT1).valid() || xOnly) continue;
      for( auto itT2 = itT1 + 1; xCandidates[part].end() !=itT2; ++itT2 ){
        if(!(*itT2).valid())                continue;
        if(((*itT1).bloomfilter() & (*itT2).bloomfilter()).empty()){
          continue;
        }
        if(! CloseEnough( (*itT1), (*itT2), 2.) ) continue;
        int Compare = (*itT1).size()+(*itT2).size(); // 36 , 30 ,30  ( 6 vs 6 6 vs 5 )
        if( Compare <10 ){
          maxCommon = 1;
        }else if( Compare ==10 ){
          maxCommon = 2;
        }else if( Compare >10){
          maxCommon = 3;
        }
        if( areClones( (*itT1), (*itT2), maxCommon)){
          (*itT1).setValid(false);
          break;
        }
      }
    }
  }else{
    unsigned int maxCommon = 0;
    for( auto itT1 = xCandidates[part].begin(); xCandidates[part].end() !=itT1; ++itT1){
      if( !(*itT1).valid()) continue;
      for( auto itT2 = itT1 +1; xCandidates[part].end()!=itT2; ++itT2){
        if(!(*itT2).valid() ) continue;
        if( CloseEnough( (*itT1), (*itT2), 2.)) continue;
        int Compare = (*itT1).size()+(*itT2).size();
        if( Compare <10 ){      maxCommon = 1;}
        else if( Compare ==10 ){maxCommon = 2;}
        else if( Compare >10){  maxCommon = 3;}
        if( areClones( (*itT1), (*itT2), maxCommon)){
          (*itT1).setValid(false);
          break;
        }
      }
    }
    if( icase == m_nCases-1){ // xOnly is automatic!
      for( auto itT1 = xCandidates[part].begin() ; xCandidates[part].end()!= itT1; ++itT1){
        if((*itT1).valid()) trackCandidates[(int)part].push_back( (*itT1));
      }
    }
  }
}

void PrHybridSeeding::removeClones(unsigned int part,
                                   TrackCandidates &trackCandidates) const noexcept{
  std::sort(trackCandidates[(int)part].begin(), trackCandidates[(int)part].end(),Pr::Hybrid::SeedTrack::GreaterBySize);
  //small size in front with larger chi2
  for( auto itT1 = trackCandidates[(int)part].begin(); trackCandidates[(int)part].end()!=itT1; ++itT1){
    if( !(*itT1).valid() ) continue;
    for ( auto itT2 = itT1 + 1; trackCandidates[(int)part].end() !=itT2; ++itT2 ) {
      if( !(*itT2).valid()) continue;
      //*not updating idS !!! (to check)
      if(((*itT1).bloomfilter() & (*itT2).bloomfilter()).empty()) continue;
      if( !CloseEnough( (*itT1),(*itT2), 2.) ) continue;
      unsigned int maxCommon = std::floor( m_fracCommon * std::min( (*itT1).size(), (*itT2).size() ));
      if( areClones((*itT1), (*itT2), maxCommon)){
        (*itT1).setValid(false);
        break;
      }
    }
  }
}
void PrHybridSeeding::flagHits(unsigned int icase,
                               unsigned int part,
                               TrackCandidates &trackCandidates,
                               PrFTHitHandler<ModPrHit>& hitHandler) const noexcept{
  // The hits in the track candidates are copies, but they each contain their own index
  // in the original hit container, which is used to flag the original hits.
  //bigger size is in front
  std::sort( trackCandidates[(int)part].begin() , trackCandidates[(int)part].end() , Pr::Hybrid::SeedTrack::LowerBySize);
  for(auto& track : trackCandidates[(int)part]){
    // important the sorting for this break
    if( track.size() < m_SizeFlag[icase]) break;
    if(! track.valid()) continue;
    if( track.Case()!=icase) continue;
    // Important the sorting of before for this break
    if(!( ( track.size()==11
           && track.chi2PerDoF()< m_MaxChi2Flag[icase]
           && std::fabs( track.X0())<m_MaxX0Flag[icase])
          || ( track.size()==12 )  )
       ) continue;
    for( const auto& hit : track.hits() ){
      if( (int)part == (int) hit.hit->zone()%2) continue;
      hitHandler.hit(hit.hitIndex).setInvalid();
    }
  }
}

//=========================================================================
//  Convert to LHCb tracks
//=========================================================================
void PrHybridSeeding::makeLHCbTracks(std::vector<Track>& result, unsigned int part,
                                     const TrackCandidates &trackCandidates) const noexcept{
  for( const auto& track : trackCandidates[(int)part]){
    if ( !track.valid() ) continue;
    if ( msgLevel(MSG::DEBUG) ) debug()<<"Creating LHCb Track"<<endmsg;
    //***** EXACT SAME IMPLEMENTATION OF PatSeedingTool *****//
    auto& out = result.emplace_back();
    out.setType( Track::Type::Ttrack );
    out.setHistory( Track::History::PrSeeding );
    out.setPatRecStatus( Track::PatRecStatus::PatRecIDs);
    for( const auto& hit : track.hits() ){
      out.addToLhcbIDs(hit.hit->id());
    }
    LHCb::State temp(Gaudi::TrackVector(track.x(zReference), track.y(zReference),
                                        track.xSlope(zReference), track.ySlope(), 0.),
                     zReference, LHCb::State::Location::AtT);
    double qOverP, sigmaQOverP;
    float m_momentumScale = 35.31328;
    //float m_momentumScale = 40.3751; (PatSeeding is also using this value)
    const float scaleFactor = m_magFieldSvc->signedRelativeCurrent();
    if( m_momentumTool->calculate( &temp, qOverP, sigmaQOverP, true).isFailure()){
      if( std::abs( scaleFactor) <1.e-4){
        qOverP = (( track.cx() < 0) ? -1. : 1.) *
          ((scaleFactor < 0.) ? -1. : 1.) / Gaudi::Units::GeV;
        sigmaQOverP = 1. / Gaudi::Units::MeV;
      }
      else{
        qOverP = track.cx() * m_momentumScale / (-1.*scaleFactor);
        sigmaQOverP = 0.5 * qOverP;
      }
    }
    temp.setQOverP( qOverP);
    Gaudi::TrackSymMatrix& cov = temp.covariance();
    cov(0,0) = m_stateErrorX2;
    cov(1,1) = m_stateErrorY2;
    cov(2,2) = m_stateErrorTX2;
    cov(3,3) = m_stateErrorTY2;
    cov(4,4) = sigmaQOverP * sigmaQOverP;
    for( const float z: m_zOutputs ){
      temp.setX( track.x(z));
      temp.setY( track.y(z));
      temp.setZ(z);
      temp.setTx( track.xSlope(z));
      temp.setTy( track.ySlope());
      out.addToStates( temp );
    }
    out.setChi2PerDoF( {track.chi2()/track.nDoF(), (int) track.nDoF()});
  }
}


void PrHybridSeeding::solveParabola(const ModPrHit& h1,const ModPrHit& h2,const ModPrHit& h3,
                                    float& a1, float& b1,float& c1) const noexcept{
  const float z1 = h1.hit->z() - zReference;//
  const float z2 = h2.hit->z() - zReference;//
  const float z3 = h3.hit->z() - zReference;//
  const float x1 = h1.hit->x();
  const float x2 = h2.hit->x();
  const float x3 = h3.hit->x();
  //const float e = m_dRatio;
  const float corrZ1 = 1.+m_dRatio*z1;
  const float corrZ2 = 1.+m_dRatio*z2;
  const float corrZ3 = 1.+m_dRatio*z3;
  const float det = (z1*z1)*corrZ1*z2 + z1*(z3*z3)*corrZ3 + (z2*z2)*corrZ2*z3 - z2*(z3*z3)*corrZ3
                    - z1*(z2*z2)*corrZ2 - z3*(z1*z1)*corrZ1;
  const float recdet = 1./det;
  if( std::fabs(det) < 1e-8 ){
    b1 = (x3-x1)/(z3-z1);
    c1 = x1 - b1*z1;
    a1 = 0.f;
    return;
  }
  const float det1 = (x1)*z2 + z1*(x3) + (x2)*z3 - z2*(x3) - z1*(x2) - z3*(x1);
  const float det2 = (z1*z1)*corrZ1*x2 + x1*(z3*z3)*corrZ3 + (z2*z2)*corrZ2*x3 - x2*(z3*z3)*corrZ3
                     - x1*(z2*z2)*corrZ2 - x3*(z1*z1)*corrZ1;
  const float det3 = (z1*z1)*corrZ1*z2*x3 + z1*(z3*z3)*corrZ3*x2 +(z2*z2)*corrZ2*z3*x1
                     - z2*(z3*z3)*corrZ3*x1 - z1*(z2*z2)*corrZ2*x3 - z3*(z1*z1)*corrZ1*x2;
  a1 = recdet*det1;
  b1 = recdet*det2;
  c1 = recdet*det3;
}

//=========================================================================
//  Fit the track, return OK if fit sucecssfull
//=========================================================================
bool PrHybridSeeding::fitSimultaneouslyXY( Pr::Hybrid::SeedTrack& track , unsigned int iCase) const noexcept{
  if( track.nx() <4 || track.ny() <4 ) return false;
  float mat[15];
  float rhs[5];
  // unsigned int nHitsX = 0;
  // unsigned int nHitsStereo = 0;
  const float zRef = zReference;
  for (unsigned int loop = 0; 3 > loop ; ++loop ){
    if(loop ==1 && m_useCorrPos){
      float RadiusPosition = std::sqrt((0.0005*0.0005*track.ax()*track.ax() + 0.001*0.001*track.y(zRef)*track.y(zRef)));
      float dRatioPos = -1.*(m_dRatioPar[0] + m_dRatioPar[1]*RadiusPosition + m_dRatioPar[2]*RadiusPosition*RadiusPosition);
      track.setdRatio(dRatioPos);
    }
    std::fill(mat,mat+15,0.);
    std::fill(rhs,rhs+5,0.);
    for( const auto& modHit : track.hits() ){
      const PrHit& hit = *(modHit.hit);
      const float w = hit.w();
      const float dxdy = hit.dxDy();
      const float yOnTrack = track.yOnTrack(hit);
      const float dz = 0.001*( hit.z( yOnTrack ) - zRef );
      const float dRatio = track.dRatio();
      const float deta = dz*dz*(1. + dz*dRatio);
      const float wdz = w * dz;
      const float weta = w * deta;
      const float wdxdy = w * dxdy;
      const float wdxdydz = wdxdy * dz;
      const float dist = track.distance(hit);
      mat[0] += w;
      mat[1] += wdz;  mat[2] += wdz * dz;
      mat[3] += weta; mat[4] += weta * dz; mat[5] += weta * deta;
      mat[6] -= wdxdy;mat[7] -= wdxdydz;   mat[8] -= wdxdy * deta;  mat[9] += wdxdy * dxdy;
      mat[10] -= wdxdydz; mat[11] -= wdxdydz * dz;mat[12] -= wdxdydz * deta;mat[13] += wdxdydz * dxdy;
      mat[14] += wdxdydz * dz * dxdy;
      // fill right hand side
      rhs[0] += w * dist;
      rhs[1] += wdz * dist;
      rhs[2] += weta * dist;
      rhs[3] -= wdxdy * dist;
      rhs[4] -= wdxdydz * dist;
    }//Loop over Hits to fill the matrix
    // decompose matrix, protect against numerical trouble
    //track.setnXnY( nHitsX, nHitsStereo );
    //if(nHitsX < 4 || nHitsStereo < 4) return false;
    ROOT::Math::CholeskyDecomp<float, 5> decomp(mat);
    if (!decomp) return false;
    decomp.Solve(rhs);
    rhs[1]*=1.e-3;
    rhs[2]*=1.e-6;
    rhs[4]*=1.e-3;
    rhs[3]-=rhs[4]*zRef;
    //crazy values!
    if( (std::fabs(rhs[0]) > 1e4 || std::fabs(rhs[1]) > 5. ||
         std::fabs(rhs[2]) > 1e-3 || std::fabs(rhs[3]) > 1e4 || std::fabs(rhs[4]) > 1.)) return false;
    track.updateParameters(rhs[0],rhs[1],rhs[2],rhs[3],rhs[4]);
  }
  float maxChi2 = std::numeric_limits<float>::min();
  float chi2_onHit = std::numeric_limits<float>::max();
  // for( auto itH = track.hits().begin(); track.hits().end() != itH; ++itH ){
  for( const auto& itH :track.hits() ){
    chi2_onHit = track.chi2( *(itH.hit));
    if ( chi2_onHit > maxChi2 ){
      maxChi2 = chi2_onHit;
    }
  }//Set Max Chi2DoF
  float X0 = track.ax() - track.bx()*zReference+track.cx()*m_ConstC;
  track.setX0(X0);
  if( ( track.recovered()) ){
    if( maxChi2 < m_recoChiOutlier) return true;
    return false;
  }else{
    if( (track.size()>10) && maxChi2<m_maxChi2HitFullFitHigh[iCase]) return true;
    if( maxChi2<m_maxChi2HitFullFitLow[iCase] && track.size()<11) return true;
  }
  return false;
}

//=======================================
//Fit Only X Projection
//=======================================
bool PrHybridSeeding::fitXProjection(Pr::Hybrid::SeedTrack& track, unsigned int iCase ) const noexcept{
  if(msgLevel(MSG::DEBUG)) debug()<<"Fitting"<<endmsg;
  if(track.size()<m_minXPlanes) return false;
  float mat[6];
  float rhs[3];
  const float zRef = zReference;
  for(unsigned int loop = 0;3>loop;++loop){
    std::fill(mat,mat+6,0.);
    std::fill(rhs,rhs+3,0.);
    for( const auto& modHit : track.hits() ){
      //for( auto itH = track.hits().begin(); track.hits().end() != itH; ++itH ){
      const PrHit& hit = *(modHit.hit);
      const float dRatio = track.dRatio();
      float w = hit.w();//squared
      const float dz= 0.001*( hit.z() - zRef );
      const float deta = dz*dz*(1. + dRatio*dz);
      const float dist = track.distance(hit);
      // if(msgLevel(MSG::DEBUG)) debug()<<"Loop \t"<<loop<<"\n Distance From Hit \t"<<dist<<endmsg;
      mat[0]+= w;
      mat[1]+= w * dz;   mat[2]+= w * dz * dz;
      mat[3]+= w * deta; mat[4]+= w * dz * deta;  mat[5]+= w * deta * deta;
      rhs[0]+= w * dist;
      rhs[1]+= w * dist * dz;
      rhs[2]+= w * dist * deta;
    }
    ROOT::Math::CholeskyDecomp<float,3> decomp(mat);
    if(!decomp){
      return false;
    }
    //Solve linear system
    decomp.Solve(rhs);
    rhs[1]*=1.e-3;
    rhs[2]*=1.e-6;
    if(msgLevel(MSG::DEBUG)) {
      debug()<<"Loop \t"<<loop<<" { a = \t"<<rhs[0]<<"; b = \t"<<rhs[1]<<"; c = \t"<<rhs[2]<< "}" <<endmsg;
    }
    // protect against unreasonable track parameter corrections
    // (check that out)
    if(std::fabs(rhs[0]) > 1.e4 || std::fabs(rhs[1]) > 5. ||
       std::fabs(rhs[2]) > 1.e-3 ) return false;
    //Small corrections
    track.updateParameters(rhs[0],rhs[1],rhs[2],0.,0.);
    if(loop==0){
      track.setdRatio(m_dRatio);
    }
    //Put back later faster maybe
    if(loop >0 && std::abs(rhs[0]) < 5e-5 && std::abs(rhs[1]) < 5e-8 &&
       std::abs(rhs[2]) < 5e-11){
      break;
    }
  }
  //Compute some values on the track
  auto hitChi2 = [&track](const ModPrHit& hit) { return track.chi2(*hit.hit); };
  const auto chi2s = track.hits() | view::transform(hitChi2);
  return v3::max(chi2s) < m_maxChi2HitsX[iCase];
}

bool PrHybridSeeding::removeWorstAndRefit(Pr::Hybrid::SeedTrack& track, unsigned int iCase) const noexcept{
  //maybe useless?
  auto hitChi2 = [&track](const ModPrHit& hit) { return track.chi2(*hit.hit); };
  const auto worst = v3::max_element(track.hits(), std::less{}, hitChi2);

  if(msgLevel(MSG::DEBUG)){ debug()<<"Removing Worst UV layer"<<endmsg;}
  track.hits().erase(worst);

  if(worst->hit->isX()){
    track.setnXnY( track.nx()-1, track.ny());
  }else{
    track.setnXnY( track.nx(), track.ny()-1);
  }
  return fitSimultaneouslyXY(track, iCase);
}

//=========================================================================
//  Remove the worst hit and refit.
//=========================================================================
bool PrHybridSeeding::removeWorstAndRefitX ( Pr::Hybrid::SeedTrack& track , unsigned int iCase) const noexcept{
  if(msgLevel(MSG::DEBUG)){ debug()<<"NHits X projection passed = "<<track.size()<<endmsg;}
  if(track.size()<=m_minXPlanes) return false;
  if(msgLevel(MSG::DEBUG)) debug()<<"Removing Worst and Refitting X"<<endmsg;

  auto hitChi2 = [&track](const ModPrHit& hit) { return track.chi2(*hit.hit); };
  const auto worst = v3::max_element(track.hits(), std::less{}, hitChi2);
  track.hits().erase(worst);
  return fitXProjection(track, iCase);
}



void PrHybridSeeding::findXProjections(unsigned int part, unsigned int iCase,
                                       PrFTHitHandler<ModPrHit>& FTHitHandler,
                                       XCandidates& xCandidates) const noexcept{
  ModPrHits parabolaSeedHits;
  // List of hits in both x-layers in T2 from a 2-hit combo
  // pushed back in this container
  parabolaSeedHits.reserve(12);

  //xHitsLists is the xHits you have
  std::vector<ModPrHits> xHitsLists;
  xHitsLists.reserve(100);
  ModPrHits xHits;
  xHits.reserve(10);
  //----------  Identifying layers [start]
  //----------  Just do the 1st one here 1st layer and last one
  unsigned int firstZoneId = -1;
  unsigned int lastZoneId = -1;
  std::array<unsigned int , PrFTInfo::Numbers::NFTXLayers> zones;
  std::array<unsigned int , PrFTInfo::Numbers::NFTXLayers> planeCode;
  std::array<float        , PrFTInfo::Numbers::NFTXLayers> zLays;
  if(0 == iCase){
    firstZoneId = T1X1 - part;
    lastZoneId  = T3X2 - part;
  }else if ( 1 == iCase ){
    firstZoneId = T1X2 - part;
    lastZoneId  = T3X1 - part;
  }else if ( 2 == iCase ){
    firstZoneId = T1X1 - part;
    lastZoneId  = T3X1 - part;
  }else if ( 3 == iCase ){
    firstZoneId = T1X2 - part;
    lastZoneId  = T3X2 - part;
  }
  if(msgLevel(MSG::DEBUG)){ debug() << "iCase: " << iCase
                                    << " firstZoneId: "<< firstZoneId
                                    << " lastZoneId: "<< lastZoneId
                                    << endmsg;
  }
  if(msgLevel(MSG::DEBUG)){ debug()<<"\t Loading Case Hit in first and last Zone"<<endmsg;}

  //Array[0] = first layer in T1 for 2-hit combo
  const auto& firstZone = m_zoneHandler->zone( firstZoneId );
  const auto& lastZone = m_zoneHandler->zone( lastZoneId);
  const auto& T2_X1 = m_zoneHandler->zone( T2X1 - part);
  const auto& T2_X2 = m_zoneHandler->zone( T2X2 - part);

  zones[0]    = firstZoneId;
  zLays[0]    = firstZone.z();
  planeCode[0]= firstZone.planeCode();
  //Array[1] = last  layer in T3 for 2-hit combo
  zones[1]= lastZoneId ;
  zLays[1]= lastZone.z();
  planeCode[1]= lastZone.planeCode();
  if(msgLevel(MSG::DEBUG)){ debug()<<"Hits in last and first Zone Loaded"<<endmsg;}
  //Array[2] = T2-1st x-layers
  zones[2]=     T2X1 - part;
  zLays[2] =    T2_X1.z();
  planeCode[2]= T2_X1.planeCode();
  //Array[3] = T2-2nd x-layers
  zones[3] =           T2X2- part;
  zLays[3] =   T2_X2.z();

  for(int cpt = 0; cpt < 4; ++cpt)
    if(msgLevel(MSG::DEBUG)){ debug()<<"set the zone #" << cpt << " : " << zones[cpt] << endmsg;}

  planeCode[3]= T2_X2.planeCode();
  if(msgLevel(MSG::DEBUG)){ debug()<<"Hits in last and first Zone Loaded"<<endmsg;}
  unsigned int i=4;
  //Add extra layer HERE, if needed!!!!
  for(unsigned int xZoneId : {T1X1, T1X2, T3X1, T3X2}){
    xZoneId =xZoneId - part;
    if(xZoneId != firstZoneId && xZoneId != lastZoneId){
      const auto& Zone = m_zoneHandler->zone(xZoneId);
      zones[i] = xZoneId; zLays[i] = Zone.z();
      planeCode[i] = Zone.planeCode();
      if(msgLevel(MSG::DEBUG)){ debug()<<"set the zone #" << i << " : " << zones[i] << endmsg;}
      i++;
    }
  }
  float Zlast  = zLays[1];//zones[1]->z();
  float Zfirst = zLays[0];//zones[0]->z();
  float invZfirst = 1./Zfirst;// zones[0]->z();
  float invZlZf = 1./( Zlast-Zfirst );
  float slope = (m_tolAtX0Cut[iCase]-m_TolX0SameSign[iCase])/(m_x0Cut[iCase]-m_x0SlopeChange[iCase]);
  float slopeopp =  (m_tolAtx0CutOppSig[iCase] -m_tolX0Oppsig[iCase])/(m_x0Cut[iCase]-m_x0SlopeChange2[iCase]);
  Boundaries Bounds;
  //============Initialization
  std::array<HitIter, PrFTInfo::Numbers::NFTXLayers> endsZone;
  std::array<HitIter, PrFTInfo::Numbers::NFTXLayers> begZone;
  for(unsigned int i=0;i<PrFTInfo::Numbers::NFTXLayers;i++){
    //Always: first=0 last=1 middles=2,3 remains=4,5 (order of loops)
    Bounds[i] = InitializeBB(zones[i], FTHitHandler);
    auto r = FTHitHandler.hits( zones[i]);
    begZone[i]  = r.begin();
    endsZone[i] = r.end();
  }
  Bounds[1].end = Bounds[1].begin;//initializing both of iterators
  ModPrHitConstIter Fhit = Bounds[1].end;
  std::bitset<PrFTInfo::Numbers::NFTXLayers> first;
  first.set();
  // std::array<bool,PrFTHitHandler::Numbers::NFTXLayers> first;
  // for( unsigned int j = 0; j<PrFTHitHandler::Numbers::NFTXLayers; j++){ first[j] = true; }
  //Used to cache the position to do a "look-around search"
  std::array<float,PrFTInfo::Numbers::NFTXLayers> xminPrev, xmaxPrev;
  for(; Bounds[0].begin != Bounds[0].end; ++Bounds[0].begin) //for a hit in first layer
  {
    Fhit = Bounds[0].begin;
    const auto* fHit = Fhit->hit;
    if(msgLevel(MSG::DEBUG)){ debug()<<"fthit "<< fHit->x() << endmsg;}

    if( !Fhit->isValid() && m_removeFlagged) continue;
    float tx_inf = fHit->x() * invZfirst;
    float xProjeInf = tx_inf *Zlast ;
    float tolHp = m_TolFirstLast[iCase];
    float maxXl = xProjeInf + tx_inf*m_alphaCorrection[iCase] +tolHp;
    float minXl = xProjeInf + tx_inf*m_alphaCorrection[iCase] -tolHp;
    Bounds[1].end    = FTHitHandler.get_upperBound_log( Bounds[1].end, endsZone[1] , maxXl);
    Bounds[1].begin  = FTHitHandler.get_lowerBound_lin( Bounds[1].begin, Bounds[1].end, minXl);
    xminPrev[1] = minXl;
    xmaxPrev[1] = maxXl;

    float tx_pickedcombination;
    float x0;
    float CorrX0;
    float x0new;
    float xMax;
    float xMin;
    float max = 0.;
    float min = 0.;
    float xProjected;
    float xProjectedCorrected;
    unsigned int nfired = 1;
    for(auto Lhit=Bounds[1].begin; Lhit != Bounds[1].end; ++Lhit){
      const PrHit* lHit = Lhit->hit;
      if(!Lhit->isValid() && m_removeFlagged) continue;
      //Define the boundaries in T2 to look for parabolaSeedHits
      //( push them back now or later? ) (dependent on x(z=0) )
      //from the 2-hit combo
      tx_pickedcombination = (lHit->x() - fHit->x())*invZlZf;
      x0 = fHit->x() - tx_pickedcombination*Zfirst;
      CorrX0 = m_x0Corr[iCase]*x0;
      x0new = x0*(1.+m_x0Corr[iCase]);
      if (x0 > 0.) {
        min = x0 > m_x0SlopeChange[iCase]  ?  -slope * ( x0 - m_x0SlopeChange[iCase]) - m_TolX0SameSign[iCase]
                                           : -m_TolX0SameSign[iCase];
        max = x0 > m_x0SlopeChange2[iCase] ? slopeopp*( x0 - m_x0SlopeChange2[iCase]) + m_tolX0Oppsig[iCase]
                                           : +m_tolX0Oppsig[iCase];
      } else {
        max = x0 < - m_x0SlopeChange[iCase]  ? -slope*( x0 + m_x0SlopeChange[iCase]) + m_TolX0SameSign[iCase]
                                             : m_TolX0SameSign[iCase];
        min = x0 < - m_x0SlopeChange2[iCase] ? slopeopp*( x0 + m_x0SlopeChange2[iCase]) - m_tolX0Oppsig[iCase]
                                             : -m_tolX0Oppsig[iCase] ;
      }
      parabolaSeedHits.clear();
      nfired = 2;
      for( int i = 2; i<4; ++i){
        xProjected = x0 + zLays[i]*tx_pickedcombination ;
        xProjectedCorrected = xProjected + CorrX0 ;
        xMin =   xProjectedCorrected + min ;//may add a 1.0 mm here?
        xMax =   xProjectedCorrected + max ;//may add a 1.0 mm here?
        if( xMin > xMax){
          if(msgLevel(MSG::DEBUG)){  debug()<<" xMin is bigger than xMax in T2 layers : "<<i<<endmsg;}
          std::swap(xMin, xMax);
        }
        if( first[i] ){
          Bounds[i].begin = FTHitHandler.get_lowerBound_log( Bounds[i].begin, Bounds[i].end, xMin );
          Bounds[i].end = FTHitHandler.get_upperBound_lin( Bounds[i].begin, Bounds[i].end, xMax );
          first[i] = false;
        }else{
          LookAroundMax( Bounds[i], xmaxPrev[i], xMax, endsZone[i], begZone[i] , FTHitHandler);
          LookAroundMin( Bounds[i], xminPrev[i], xMin, Bounds[i].end, begZone[i] , FTHitHandler);
        }
        xminPrev[i] = xMin;
        xmaxPrev[i] = xMax;
        bool fired = false;
        if( Bounds[i].end - Bounds[i].begin > 0){
          for( auto itM = Bounds[i].begin ; itM!= Bounds[i].end; ++itM){
            if( !itM->isValid() && m_removeFlagged) continue;
            if( msgLevel(MSG::DEBUG)){
              debug()<<"push back parabola seed hit"<<endmsg;
            }
            parabolaSeedHits.push_back(*itM);
            fired = true;
          }
        }
        nfired += (fired);
      }
      if( nfired < 3 ){
        if(msgLevel(MSG::DEBUG)){
          debug()<<"Not enough fired layers : N fired = "<<nfired<<endmsg;
        }
        continue;
      }
      //=======================================================
      // We have 1 Hit in 1st 1 Hit in last and a
      // vector of Hits for in-between
      //=======================================================
      xHitsLists.clear();
      //=======================================================
      // Sort the ParabolaSeedHits for in-between layers by increasing distance from the Projected Corrected
      // position only when we have more than 1 ParabolaSeedHit
      //=======================================================
      if(parabolaSeedHits.size()>1){
        //Principle of the Lambda funtion, Hits sorted wrt distance from linear Projection 1st-3rd layer
        std::sort( parabolaSeedHits.begin(),parabolaSeedHits.end(),
                   [x0new,tx_pickedcombination](const ModPrHit& lhs, const ModPrHit& rhs)
                   ->bool{return std::fabs( lhs.hit->x() - ( x0new + lhs.hit->z()*tx_pickedcombination)) <
                                 std::fabs( rhs.hit->x() - (x0new + rhs.hit->z()*tx_pickedcombination));} );
      }
      if(msgLevel(MSG::DEBUG)){ info()<<"The Lambda Function Sorting end"<<endmsg;}
      unsigned int maxParabolaSeedHits = m_maxParabolaSeedHits;
      if(parabolaSeedHits.size() < m_maxParabolaSeedHits){
        maxParabolaSeedHits = parabolaSeedHits.size();
      }else{
        maxParabolaSeedHits = m_maxParabolaSeedHits;
      }
      //for a give hit in 1st layer, and a given hit in last layer
      for(unsigned int i = 0; i<maxParabolaSeedHits;++i){
        float a = 0;
        float b = 0;
        float c = 0;
        solveParabola(*Fhit, parabolaSeedHits[i], *Lhit, a, b, c); //Extrapolation with dRatio
        //===================================================
        // Look in all the other layers except the
        // 1st/last/zone except the parabolaSeedHit
        //===================================================
        //Loop on all the xZones
        float dz ;
        float xAtZ;
        float xMaxAtZ;
        float xMinAtZ;
        ModPrHitConstIter bestProj;
        float bestDist;
        ModPrHitConstIter itH;
        ModPrHitConstIter itE;
        xHits.clear();
        //Loop on remaining hits in remaining layers!
        for(int j = 2; j< PrFTInfo::Numbers::NFTXLayers; ++j) { //look in zones except the 1st and last zones
          if(msgLevel(MSG::DEBUG)){debug()<<"Selecting ParSeedHits"<<endmsg;}
          if((int) planeCode[j] == (int)parabolaSeedHits[i].planeCode) continue;
          dz = zLays[j] - zReference;
          //Prediction of the position with the cubic correction!
          xAtZ= a * dz * dz * (1. + m_dRatio* dz) + b * dz + c;
          // * (1. + dz*invZref ); Make them z-dependent and/or x0 dependent to cure for track-model error?
          xMaxAtZ = xAtZ + m_tolRemaining[iCase];
          xMinAtZ = xAtZ - m_tolRemaining[iCase]; // * (1. + dz*invZref );
          // Parabola here is larger than tolerances in x0! ( 0.75 mm up / down when x0 ~0 )
          // may we want ot make the m_tolRemaining[iCase] ( x0 dependent too ?? )
          // here you are re-using the tolerance defined before from the linear extrapolation of 2-hit combo
          itH = FTHitHandler.get_lowerBound_log(Bounds[j].begin, Bounds[j].end, xMinAtZ);
          itE = FTHitHandler.get_upperBound_lin( itH, Bounds[j].end , xMaxAtZ);
          bestProj = itE;
          bestDist = 10.; // 1 cm around predicted position!!!
          const PrHit* hit;
          for(; itH != itE ; ++itH){
            hit = itH->hit;
            if(!itH->isValid() && m_removeFlagged) continue;
            if(std::fabs(hit->x() - xAtZ)  <  bestDist){
              bestDist = std::fabs(hit->x() - xAtZ);
              bestProj = itH;
            }
          }
          if(bestProj != itE){
            xHits.push_back(*bestProj);
          }
        }//end loop xZones excluding parabolaseed hit
        //in xHits are not present the first layer and last + parabola seed hits
        if(msgLevel(MSG::DEBUG)){ debug()<<"End Loop in between zones to pick up Projection of parabola"<<endmsg;}
        //Up to here xHits contains only the remaining layers hits!
        //next parabolaSeedHits ;
        //you must find 2 hits in remaining layers for this given parabola seed hits (total min = 5)
        if( xHits.size() < 2 ) continue;
        xHits.push_back( *Fhit);
        xHits.push_back( parabolaSeedHits[i]);
        xHits.push_back( *Lhit);
        //sort the hits in xHits by LHCbID ( important for clone removal purpose )!
        std::sort(xHits.begin(), xHits.end(), compLHCbID());
        xHitsLists.push_back( xHits );
      }//End loop parabolaSeedHits
      if(msgLevel(MSG::DEBUG)){
        debug()<<"End Loop For pick up Parabola Hits and build the xHitsLists"<<endmsg;
        //-------- Remove Duplicates from search in parabolaSeedHits
        debug()<<"xHitsLists size before removing duplicates: "<<xHitsLists.size()<<endmsg;
      }
      if(xHitsLists.size() == 0){
        continue;
      }else if(xHitsLists.size() > 1){
        //---Remove Duplicates in the HitsList
        if(msgLevel(MSG::DEBUG)){debug()<<"Remove duplicate xHitsLists"<<endmsg;}
        std::stable_sort(xHitsLists.begin(), xHitsLists.end());
        xHitsLists.erase(std::unique(xHitsLists.begin(), xHitsLists.end()), xHitsLists.end());
      }
      if(msgLevel(MSG::DEBUG)){debug()<<"xHitsLists size after removing duplicates: "<<xHitsLists.size()<<endmsg;}
      //----- You have list of xHits : fit them!
      for(  auto&&xHits : xHitsLists){
        if(msgLevel(MSG::DEBUG)){ debug()<<"Fit Track"<<endmsg;}
        //------    Create the track
        Pr::Hybrid::SeedTrack temp_track( part , zReference , xHits);
        //------    Set the dRatio value for the track here
        temp_track.setdRatio(m_dRatio);
        //------    Algorithm allows to go down to 4 hits only from 6 hits track refit by construction.
        int nFits = 0;
        if(msgLevel(MSG::DEBUG) ){   debug()<<"Attempting to Fit the following Track"<<endmsg; printTrack(info(), temp_track); }
        //-----     OK is the status of the fit
        bool OK = false;
        if(temp_track.size()>m_minXPlanes){
          //----- should be always true given xHits.size() < 2 continue of before
          //----- no N = m_minXPlanes hits at first fit
          OK = fitXProjection(temp_track,iCase);
          nFits = 1;
        }
        while(!OK && temp_track.size()>m_minXPlanes){
          if(temp_track.size() <=m_minXPlanes){
            OK = false;
            break;
          }
          if( nFits==1 && temp_track.size() == 5){
            //fit for entering tracks with 5 hits was failed , do not remove hit and refit it,
            //just kill this candidate setting OK to false
            OK = false;
            break;
          }
          OK = removeWorstAndRefitX(temp_track,iCase);
          nFits++;
        }
        //---- Fit is successful and  make a x/z-candidate
        if( OK ){
          Pr::Hybrid::updateChi2X(temp_track);
          if(msgLevel(MSG::DEBUG)){ debug()<<"Chi2 Set for track = \t"<<temp_track.chi2()<<endmsg;}
        }
        if( OK &&
            temp_track.size() >= m_minXPlanes &&
            (( temp_track.chi2PerDoF() < m_maxChi2DoFX[iCase]))){
          temp_track.setCase( iCase );
          temp_track.setXT1( temp_track.x( m_zoneHandler->zone(0).z()  ));
          temp_track.setXT2( temp_track.x( m_zoneHandler->zone(12).z() ));
          temp_track.setXT3( temp_track.x( m_zoneHandler->zone(23).z() ));
          temp_track.updateIDs();//for bloom filter
          xCandidates[(int)part].push_back(temp_track); //The X Candidate is created
        }
      }//end Loop xHist:xHitsLists
    }//end loop Last Zone given a firsZone selected
  }//end loop first zone
}

void PrHybridSeeding::ExtendCluster( HitIter& itBeg, HitIter& itEnd, HitIter stereoEnd,
                                     unsigned int iCase, int &nLay, bool reco) const noexcept{
  if(msgLevel(MSG::DEBUG)){info()<<"Pop counter"<<endmsg;}
  constexpr int nUV     =  6;
  constexpr int nPlanes = 12;
  ASSUME(PrFTInfo::Numbers::NFTUVLayers == nUV);
  ASSUME(PrFTInfo::Numbers::NFTUVLayers + PrFTInfo::Numbers::NFTXLayers == nPlanes);
  std::bitset<nPlanes> planes;
  std::for_each( itBeg, itEnd, [&](const ModPrHit hit) { planes.set( hit.planeCode ); }); 
  nLay = planes.count();//Number of x planes crossed.
  float tolTy = (reco)? ((float) m_recoTolTy) : m_tolTyOffset[iCase] + (*itBeg).coord*m_tolTySlope[iCase] ;
  while( itEnd != stereoEnd
         && (itEnd->coord - itBeg->coord ) < tolTy
         && nLay< nUV ){
    //as soon as you find a sixth element exit the loop (in other case extend up to tolerance
    if(UNLIKELY( msgLevel(MSG::DEBUG))){ info()<<" Counting layers "<<endmsg;}
    planes.set(itEnd->planeCode);
    ++itEnd;
    nLay = planes.count();
  }
}


