/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRFTHITHANDLER_H
#define PRFTHITHANDLER_H 1

// Include files
#include "PrKernel/PrHit.h"
#include "PrKernel/PrFTInfo.h"
#include "PrKernel/PrHitZone.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/ObjectContainerBase.h"

#include "GaudiKernel/Range.h"

#include "TfKernel/IndexedHitContainer1D.h"


/** @class PrFTHitHandler PrFTHitHandler.h
 *  Handler of Sci-Fit hits, the object is stored in the transient event store and
 *  each algorithm reads the object from there without calling the HitManagers (tools)
 *  @author Renato Quagliani
 *  @author Sebastien Ponce
 */

template<class Hit>
class PrFTHitHandler final {
public:
   // Constants you need to change if  Sci-Fi number of layers is going to change.
  enum Numbers{
    NFTZones = 24, NFTXLayers = 6, NFTUVLayers = 6
  };

  using HitContainer = IndexedHitContainer1D<Numbers::NFTZones, Hit>;

  /// Type for range of PrFT Hits within a container
  using HitRange = typename HitContainer::HitRange;
  using HitIter = typename std::vector<Hit>::const_iterator;

  PrFTHitHandler() = default;

  PrFTHitHandler(typename HitContainer::Hits&& hits, typename HitContainer::Offsets&& offsets)
    : m_hits{std::forward<typename HitContainer::Hits>(hits),
      std::forward<typename HitContainer::Offsets>(offsets)}
  {
  }

  // Constructor with the capacity of the HitContainer
  PrFTHitHandler(int size) : m_hits(size) {};

  template<typename I>
  void insert(unsigned int lay, I&& b, I&& e) {
    m_hits.insert( std::forward<I> ( b ), std::forward<I> ( e ), lay);
  }

  template<typename ... Args>
  void addHitInZone(unsigned int lay, Args&& ... args) {
    m_hits.addHit(  std::forward_as_tuple( std::forward<Args>(args)... ), lay);
  }

  void setOffsets() {
    m_hits.setOffsets();
  }

  /// Return the current number of zones
  auto isEmpty(unsigned int lay) const{
    return m_hits.empty( lay );
  }

   // Hit handling/ sorting etc...
  struct compX{
    bool operator()(      float lv,       float rv) const { return lv < rv;}
    bool operator()(const Hit& lhs,       float rv) const { return (*this)(lhs.x(),  rv);}
    bool operator()(      float lv, const Hit& rhs) const { return (*this)(lv,       rhs.x());}
    bool operator()(const Hit& lhs, const Hit& rhs) const { return (*this)(lhs.x(),  rhs.x());}
    bool operator()(const Hit* lhs,       float rv) const { return (*this)(lhs->x(), rv);}
    bool operator()(      float lv, const Hit* rhs) const { return (*this)(lv,       rhs->x());}
    bool operator()(const Hit* lhs, const Hit* rhs) const { return (*this)(lhs->x(), rhs->x());}
  };
  struct compXreverse{
    bool operator()(      float lv,       float rv) const { return lv > rv;}
    bool operator()(const Hit& lhs,       float rv) const { return (*this)(lhs.x(),  rv);}
    bool operator()(      float lv, const Hit& rhs) const { return (*this)(lv,       rhs.x());}
    bool operator()(const Hit& lhs, const Hit& rhs) const { return (*this)(lhs.x(),  rhs.x());}
    bool operator()(const Hit* lhs,       float rv) const { return (*this)(lhs->x(), rv);}
    bool operator()(      float lv, const Hit* rhs) const { return (*this)(lv,       rhs->x());}
    bool operator()(const Hit* lhs, const Hit* rhs) const { return (*this)(lhs->x(), rhs->x());}
  };

  template <class ForwardIt, class T, class Compare>
  static ForwardIt linear_lower_bound(ForwardIt first, ForwardIt last, T value, Compare comp = std::less{} ) {
    while (first != last && comp(*first, value)) ++first;
    return first;
  }

  template <class ForwardIt, class T, class Compare>
  static ForwardIt linear_upper_bound(ForwardIt first, ForwardIt last, T value, Compare comp = std::less{} ){
    while(first != last && !comp(value, *first)) ++first;
    return first;
  }

  //search lowerBound given a layer in log time
  HitIter getIterator_lowerBound(unsigned int lay, float xMin) const {
    auto r = hits(lay);
    return std::lower_bound( r.begin(),r.end(), xMin ,
                             [](const Hit& a, float testval) { return a.x() < testval;} );
  }

  //search upperBound given a layer in log timing
  HitIter getIterator_upperBound(unsigned int lay, float xMax) const{
    auto r = hits(lay);
    return std::upper_bound( r.begin(), r.end(), xMax ,
                             [](float testval, const Hit& a) { return a.x() > testval ; });
  }

  //search lowerBound from known position to another known position ( log time)
  static HitIter get_lowerBound_log(HitIter given_it, HitIter end, float xMin) {
    return std::lower_bound(given_it, end, xMin, compX{});
  }
  //search upperBound from a known position to another known position (log time)
  static HitIter get_upperBound_log(HitIter  given_it, HitIter  end,float xMax) {
    return std::upper_bound( given_it, end, xMax, compX{});
  }
  //search lowerBound from a known position to another known position (linear time)
  static HitIter get_lowerBound_lin(HitIter  given_it, HitIter end, float xMin) {
    return linear_lower_bound( given_it , end, xMin, compX{});
  }
  //search upperBound from known position to another known position (linear time)
  static HitIter get_upperBound_lin(HitIter given_it, HitIter end, float xMax) {
    return linear_upper_bound( given_it, end, xMax, compX{});
  }
  //search with STL method a lowerBound in low/high range starting from high
  static HitIter get_lowerBound_log_reverse( HitIter low, HitIter high, float xMin) {
    return std::upper_bound( std::reverse_iterator{high}, std::reverse_iterator{low},
                             xMin, compXreverse{}).base();
  }
  //search with STL method an upper bond in low/high range starting from high
  static HitIter get_upperBound_log_reverse( HitIter low, HitIter high, float xMax) {
    return std::lower_bound( std::reverse_iterator{high}, std::reverse_iterator{low},
                             xMax, compXreverse{}).base();
  }

  //search linearly a lowerBound in low/high range starting from high
  static HitIter get_lowerBound_lin_reverse( HitIter low, HitIter high, float xMin) {
    return linear_upper_bound( std::reverse_iterator{high}, std::reverse_iterator{low},
                               xMin, compXreverse{}).base();
  }

  //search linearly an upperBound in low/high range starting from high
  static HitIter get_upperBound_lin_reverse( HitIter low, HitIter high, float xMax) {
    return linear_lower_bound( std::reverse_iterator(high), std::reverse_iterator(low),
                               xMax, compXreverse{}).base();
  }

  const HitContainer& hits() const {
    return m_hits;
  }

  inline HitRange hits(const unsigned int layer) const {
    return m_hits.range ( layer );
  }

  Hit& hit(size_t index) {
    return m_hits.hit(index);
  }

  const Hit& hit(size_t index) const {
    return m_hits.hit(index);
  }

 private:

  HitContainer m_hits;

};

#endif // PRFTHITHANDLER_H
