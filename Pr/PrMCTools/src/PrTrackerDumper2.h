/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#ifndef PRTRACKERDUMPER2_H 
#define PRTRACKERDUMPER2_H 1

// Include files
#include "GaudiAlg/Consumer.h"
#include "PrKernel/PrFTHitHandler.h"
#include "Event/MCTrackInfo.h"
#include "Event/MCParticle.h"
#include "Linker/LinkerWithKey.h"
#include "Event/MCVertex.h"
#include "PrKernel/UTHitHandler.h"
#include "PrKernel/UTHitInfo.h"
#include "Event/ODIN.h"
#include "Event/VPLightCluster.h"
#include "Linker/LinkerTool.h"
#include "Event/Track.h"

//ROOT
#include <TTree.h>
#include <TFile.h>


/** @class PrTrackerDumper PrTrackerDumper2.h
 *  Algorithm to store tracking hits to a single root file
 *  (PrTrackerDumper stores events in separate files)
 *
 *  @author Renato Quagliani
 *  @date   2017-11-06
 */
/*

*/

class PrTrackerDumper2 : public Gaudi::Functional::Consumer<void( 
                                                                const LHCb::MCParticles& ,
                                                                const LHCb::VPLightClusters & ,
                                                                const PrFTHitHandler<PrHit>&,
                                                                const UT::HitHandler&,
                                                                const LHCb::ODIN&,
                                                                const LHCb::LinksByKey& )>{
public: 

/// Standard constructor
  PrTrackerDumper2( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;
  StatusCode finalize() override;

  void operator()(
  const LHCb::MCParticles& MCParticles,
                  const LHCb::VPLightClusters& VPClusters,
  								const PrFTHitHandler<PrHit>& ftHits,
                  const UT::HitHandler& utHits,
                  const LHCb::ODIN& odin,
                  const LHCb::LinksByKey& links) const override;

  int mcVertexType(const LHCb::MCParticle& particle) const;
  const LHCb::MCVertex* findMCOriginVertex(const LHCb::MCParticle& particle, 
                                           const double decaylengthtolerance = 1.e-3) const;

  
protected:
  TFile * file = nullptr;
  TTree * tree = nullptr;

  int *eventID = nullptr;

  bool *fullInfo = nullptr;
  bool *hasSciFi = nullptr;
  bool *hasUT = nullptr;
  bool *hasVelo = nullptr;
  bool *isDown = nullptr;
  bool *isDown_noVelo = nullptr;
  bool *isLong = nullptr;
  bool *isLong_andUT = nullptr;
  double *p = nullptr;
  double *px = nullptr, *py = nullptr, *pz = nullptr;
  double *pt = nullptr;
  double *eta = nullptr, *phi = nullptr;
  double *eta_track= nullptr, *phi_track= nullptr, *chi2_track= nullptr;
  //vertex origin of the particle
  double *ovtx_x = nullptr;
  double *ovtx_y = nullptr;
  double *ovtx_z = nullptr;
  int  *pid = nullptr;
  bool *fromBeautyDecay = nullptr;
  bool *fromCharmDecay = nullptr;
  bool *fromStrangeDecay = nullptr;
  int  *DecayOriginMother_pid = nullptr;
  int  *key = nullptr;
  int *nVeloHits_track = nullptr;
  std::vector<float> *Velo_x_track = nullptr;
  std::vector<float> *Velo_y_track = nullptr;
  std::vector<float> *Velo_z_track = nullptr;
  std::vector<int> *Velo_Module_track = nullptr;
  std::vector<int> *Velo_Sensor_track = nullptr;
  std::vector<int> *Velo_Station_track = nullptr;
  std::vector<unsigned int> *Velo_lhcbID_track = nullptr;
  std::vector<unsigned int> *Velo_index_track = nullptr;



  int *nVeloHits = nullptr;
  std::vector<float> *Velo_x = nullptr;
  std::vector<float> *Velo_y = nullptr;
  std::vector<float> *Velo_z = nullptr;
  std::vector<int> *Velo_Module = nullptr;
  std::vector<int> *Velo_Sensor = nullptr;
  std::vector<int> *Velo_Station = nullptr;
  std::vector<unsigned int> *Velo_lhcbID = nullptr;
  std::vector<unsigned int> *Velo_index = nullptr;

  std::vector< float> *FT_hitx = nullptr;
  std::vector< float> *FT_hitz = nullptr;
  std::vector< float> *FT_hitw = nullptr;
  std::vector< float> *FT_hitDXDY = nullptr;
  std::vector< float> *FT_hitYMin = nullptr;
  std::vector< float> *FT_hitYMax = nullptr;
  std::vector< int> *FT_hitPlaneCode = nullptr;
  std::vector< int> *FT_hitzone = nullptr;
  std::vector<unsigned int> *FT_lhcbID = nullptr;

  int *nFTHits = nullptr;
  int *nbHits_in_UT = nullptr;
  int *nbHits_in_SciFi = nullptr;

  std::vector<float> *UT_cos = nullptr;
  std::vector<float> *UT_cosT = nullptr;
  std::vector<float> *UT_dxDy = nullptr;
  std::vector<bool>  *UT_highthreshold = nullptr;
  std::vector<unsigned int> *UT_lhcbID = nullptr;
  std::vector<int>   *UT_planeCode = nullptr;
  std::vector<float> *UT_sinT = nullptr;
  std::vector<int> *UT_size = nullptr;
  std::vector<float> *UT_tanT = nullptr;
  std::vector<float> *UT_weight = nullptr;
  std::vector<float> *UT_xAtYEq0 = nullptr;
  std::vector<float> *UT_xAtYMid = nullptr;
  std::vector<float> *UT_xMax = nullptr;
  std::vector<float> *UT_xMin = nullptr;
  std::vector<float> *UT_xT = nullptr;
  std::vector<float> *UT_yBegin = nullptr;
  std::vector<float> *UT_yEnd = nullptr;
  std::vector<float> *UT_yMax = nullptr;
  std::vector<float> *UT_yMid = nullptr;
  std::vector<float> *UT_yMin = nullptr;
  std::vector<float> *UT_zAtYEq0 = nullptr;

  int *nUTHits = nullptr;


  Gaudi::Property<std::string>m_track_location{this, "TrackLocation", "Rec/Track/Keyed/Velo", "Cointainer in which are stored reconstructed traks linked to MC particles"};

  

};
#endif // PRTRACKERDUMPER2_H
