/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRPIXELDEBUGTOOL_H
#define PRPIXELDEBUGTOOL_H 1

// Gaudi
#include "GaudiAlg/GaudiTool.h"
// Interface
#include "PrKernel/IPrDebugTool.h"

/** @class PrPixelDebugTool PrPixelDebugTool.h
 *  Debug Pixel processing using MC truth
 *
 *  @author Olivier Callot
 *  @date   2007-10-25
 */
class PrPixelDebugTool : public GaudiTool, virtual public IPrDebugTool {
 public:
  /// Standard constructor
  PrPixelDebugTool(const std::string& type, const std::string& name,
                   const IInterface* parent);
  /// Destructor
  virtual ~PrPixelDebugTool();

  bool matchKey(LHCb::LHCbID id, int key) const override;
  void printKey(MsgStream& msg, LHCb::LHCbID id) const override;

  virtual double xTrue(int key, double z);
  virtual double yTrue(int key, double z);

};
#endif
