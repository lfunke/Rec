/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from Gaudi
#include "GaudiAlg/Transformer.h"

#include "PrKernel/PrFTHitHandler.h"
#include "Event/Track.h"
#include "Event/StateParameters.h"
#include "Event/FTCluster.h"
#include "Event/MCTrackInfo.h"
#include "Linker/LinkedTo.h"
//-----------------------------------------------------------------------------
// Implementation file for class : PrCheatedSciFiTracking
//
// 2015-03-23 : Michel De Cian
//-----------------------------------------------------------------------------


/** @class PrCheatedSciFiTracking PrCheatedSciFiTracking.h
 *
 *  Cheated track reconstruction in the SciFi.
 *  It creates tracks by getting all Clusters associated to a reconstructible MCParticle.
 *  Cuts can be set on the minimum number of total hits, hits in x layers and hits in stereo layers.
 *  Top and bottom modules are not mixed in the x layers.
 *
 * - HitManagerName: Name of the hit manager
 * - DecodeData: Decode the data
 * - OutputName: Name of the output location
 * - NumZones: Number of zones (normally 2 x number of layers if no y-segmentation)
 * - MinXHits: Minimum number of required hits in x layers to make a track.
 * - MinStereoHits: Minimum number of required hits in stereo layers to make a track.
 * - MinTotHits: Minimum number of total hits to make a track.
 *
 *
 *  @author Michel De Cian
 *  @date   2015-03-23
 */
class PrCheatedSciFiTracking : public Gaudi::Functional::Transformer<LHCb::Tracks(const PrFTHitHandler<PrHit>&,
                                                                                  const LHCb::MCParticles&,
                                                                                  const LHCb::MCProperty&)> {
public:
  /// Standard constructor
  PrCheatedSciFiTracking( const std::string& name, ISvcLocator* pSvcLocator );

  /// make cheated tracks by getting the clusters matched to an MCParticle
  LHCb::Tracks operator()(const PrFTHitHandler<PrHit>&,
                          const LHCb::MCParticles&,
                          const LHCb::MCProperty&) const override;

private:

  Gaudi::Property<int> m_numZones = { this, "NumZones",   24 };
  Gaudi::Property<int> m_minXHits = { this, "MinXHits",    5 };
  Gaudi::Property<int> m_minStereoHits = { this, "MinStereoHits", 5 };
  Gaudi::Property<int> m_minTotHits = { this, "MinTotHits", 10 };

};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PrCheatedSciFiTracking )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrCheatedSciFiTracking::PrCheatedSciFiTracking( const std::string& name,
                                        ISvcLocator* pSvcLocator)
: Transformer(name, pSvcLocator,
              { KeyValue{"FTHitsLocation", PrFTInfo::FTHitsLocation},
                KeyValue{"MCParticleLocation", LHCb::MCParticleLocation::Default},
                KeyValue{"MCPropertyLocation", LHCb::MCPropertyLocation::TrackInfo} },
              KeyValue{"OutputName", LHCb::TrackLocation::Seed})
{
}

//=============================================================================
// make cheated tracks by getting the clusters matched to an MCParticle
//=============================================================================
LHCb::Tracks PrCheatedSciFiTracking::operator()(const PrFTHitHandler<PrHit>& FTHitHandler,
                                                const LHCb::MCParticles& mcParts,
                                                const LHCb::MCProperty& mcProps) const
{
  LHCb::Tracks result;

  // TODO: avoid direct use of evtSvc(), once there is a way to do so for LinkedTo & MCTrackInfo...
  LinkedTo<LHCb::MCParticle, LHCb::FTCluster> myClusterLink ( evtSvc(), msgSvc(), LHCb::FTClusterLocation::Default );

  MCTrackInfo trackInfo( mcProps );

  for( const LHCb::MCParticle* mcPart : mcParts ) {

    //const bool isLong = trackInfo.hasVeloAndT( mcPart );
    //const bool isDown = trackInfo.hasT( mcPart ) && trackInfo.hasTT( mcPart );
    const bool isSeed = trackInfo.hasT( mcPart );
    //if( !isLong && !isDown ) continue;
    if( !isSeed ) continue;
    auto tmp = std::make_unique< LHCb::Track > ();
    tmp->setType( LHCb::Track::Types::Ttrack );
    tmp->setHistory( LHCb::Track::History::PrSeeding );
    tmp->setPatRecStatus( LHCb::Track::PatRecStatus::PatRecIDs );

    std::vector<int> firedXLayers(m_numZones.value(),0);
    std::vector<int> firedStereoLayers(m_numZones.value(),0);
    int totHits = 0;

    // -- loop over all zones
    for(int iZone = 0; iZone < m_numZones.value(); ++iZone){
      // -- loop over all hits in a zone
      for(const auto& hit : FTHitHandler.hits(iZone) ) {
        bool found = false;
        for(const LHCb::MCParticle* mcPart1 = myClusterLink.first( hit.id().ftID() ) ;
            mcPart1 && !found;
            mcPart1 = myClusterLink.next() ){
          found = ( mcPart1 == mcPart);
        }
        if( hit.isX() && found ){
          if( firedXLayers[iZone] == 0) firedXLayers[iZone]++;
        }
        if( !(hit.isX()) && found ){
          if( firedStereoLayers[iZone] == 0) firedStereoLayers[iZone]++;
        }
        if( found ){
          totHits++;
          tmp->addToLhcbIDs( hit.id() );
        }
      }
    }
    int sumLowerX = 0;
    int sumUpperX = 0;
    int sumStereo = 0;
    for(int i = 0; i < m_numZones.value(); i += 2){
      sumLowerX += firedXLayers[i];
    }
    for(int i = 1; i < m_numZones.value(); i += 2){
      sumUpperX += firedXLayers[i];
    }
    for(int i = 0; i < m_numZones.value(); i++){
      sumStereo += firedStereoLayers[i];
    }
    debug() << "sumLowerX: " << sumLowerX
            << " sumUpperX " << sumUpperX
            << " sumStereo " << sumStereo
            << " totHits "   << totHits << endmsg;

    if( (sumLowerX < m_minXHits.value() && sumUpperX < m_minXHits.value()) ||
        sumStereo < m_minStereoHits.value() ||
        totHits < m_minTotHits.value()) {
      continue;
    }
    // -- these are obviously useless numbers, but it should just make the checkers happy
    double qOverP = 0.01;
    LHCb::State tState;
    double z = StateParameters::ZEndT;
    tState.setLocation( LHCb::State::Location::AtT );
    tState.setState( 100, 50, z, 0.1, 0.1, qOverP );
    //tState.setCovariance( m_geoTool->covariance( qOverP ) );
    tmp->addToStates( tState );
    result.insert( tmp.release() );
  }
  return result;
}




