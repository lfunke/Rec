/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef RECALGS_PROCSTATABORTMONI_H
#define RECALGS_PROCSTATABORTMONI_H 1

// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiAlg/Consumer.h"

// GaudiUtils
#include "GaudiUtils/HistoLabels.h"

// Event model
#include "Event/ProcStatus.h"

// AIDA
#include "AIDA/IProfile1D.h"

/** @class ProcStatAbortMoni ProcStatAbortMoni.h
 *
 *  Monitor for abort rates in ProcStat
 *
 *  @author Chris Jones
 *  @date   2010-07-16
 */

class ProcStatAbortMoni final
: public Gaudi::Functional::Consumer<void(const LHCb::ProcStatus&),
                                     Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>>
{
public:

  /// Standard constructor
  ProcStatAbortMoni( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  void operator()(const LHCb::ProcStatus&) const override;   ///< Algorithm execution

private:

  /// List of subsystems
  Gaudi::Property<std::vector<std::string>> m_subSystems
  { this,  "SubSystems", { "Overall","Hlt","VELO","TT","IT","OT",
                           "Tracking","Vertex", "RICH","CALO",
                           "MUON","PROTO" } };

  /// cache the histogram pointer
  AIDA::IProfile1D * m_h = nullptr;

};

#endif // RECALGS_PROCSTATABORTMONI_H
