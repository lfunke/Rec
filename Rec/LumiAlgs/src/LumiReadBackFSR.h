/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LUMIREADBACKFSR_H
#define LUMIREADBACKFSR_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

// event model
#include "Event/HltLumiSummary.h"
#include "Event/LumiFSR.h"
#include "Event/LumiIntegral.h"

// local
#include "LumiIntegrator.h"

/** @class LumiReadBackFSR LumiReadBackFSR.h
 *
 *
 *  @author Jaap Panman
 *  @date   2009-02-27
 */
class LumiReadBackFSR : public GaudiAlgorithm {
public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;    ///< Algorithm finalization

private:
  Gaudi::Property<std::string> m_ToolName { this, "IntegratorToolName", "LumiIntegrator" };                       // name of tool for normalization

  ILumiIntegrator *m_integratorTool = nullptr;  // tool to integrate luminosity

};
#endif // LUMIREADBACKFSR_H
