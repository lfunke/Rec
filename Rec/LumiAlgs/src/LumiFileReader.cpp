/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// event model
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "Event/FileId.h"
#include "Event/LumiCounters.h"

// local
#include "LumiFileReader.h"

//-----------------------------------------------------------------------------
// Implementation file for class : LumiFileReader
//
// 2009-10-06 : Jaap Panman
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( LumiFileReader )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
LumiFileReader::LumiFileReader( const std::string& name,
                                ISvcLocator* pSvcLocator)
  : GaudiAlgorithm ( name , pSvcLocator )
{
  // skip if not a lumi event...
  setProperty("RequireObjects", std::vector<std::string>{ LHCb::HltLumiSummaryLocation::Default } ).ignore();
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode LumiFileReader::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  m_nbEvents    = 0;
  m_totDataSize = 0;

  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode LumiFileReader::execute() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

  // Retrieve the RawEvent:
  LHCb::RawEvent* event = getIfExists<LHCb::RawEvent>(m_rawEventLocation);
  if( !event) {
    StatusCode sc = Warning("RawBank cannot be loaded",StatusCode::FAILURE);
    return sc;
  }
  std::vector<unsigned int> id_bank; id_bank.reserve(4);
  // Get the buffers associated with the HltLumiSummary
  const auto& banks = event->banks( LHCb::RawBank::FileID );
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Bank size: " << banks.size() << endmsg;
  // Now copy the information from all banks (normally there should only be one)
  for (const auto& bank :  banks ) {
    // get now the raw data
    // The data part
    const unsigned int* begin = bank->begin<unsigned int>();
    const unsigned int* end   = bank->end<unsigned int>();
    id_bank.insert( id_bank.end(), begin, end ) ;

    // keep statistics
    int totDataSize = std::distance(begin,end);
    m_totDataSize += totDataSize;
    m_nbEvents++;

    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Bank size: ";
      debug() << format( "%4d ", bank->size() )
              << "Total Data bank size " << totDataSize << endmsg;
    }
    // decode ID
    std::string s = m_fileId.fileIDint2str(id_bank);
    if ( msgLevel(MSG::INFO) ) info() << "RunInfo record: " << s << endmsg;
    break;
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode LumiFileReader::finalize() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  if ( 0 < m_nbEvents ) {
    m_totDataSize /= m_nbEvents;
    info() << "Average event size : " << format( "%7.1f words", m_totDataSize )
           << endmsg;
  }

  return GaudiAlgorithm::finalize();  // must be called after all other actions
}


//=============================================================================
