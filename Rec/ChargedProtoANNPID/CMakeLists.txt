###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: ChargedProtoANNPID
################################################################################
gaudi_subdir(ChargedProtoANNPID v2r15)

gaudi_depends_on_subdirs(Event/MCEvent
                         Event/PhysEvent
                         Event/RecEvent
                         GaudiAlg
                         Rec/RecInterfaces
                         Tr/TrackInterfaces)

#find_package(NeuroBayesExpert)
#if(NEUROBAYESEXPERT_FOUND)
#  add_definitions(-D_ENABLE_NEUROBAYES)
#  include_directories(${NEUROBAYESEXPERT_INCLUDE_DIRS})
#else()
#  message(WARNING "Not using NeuroBayesExpert")
#endif()

find_package(ROOT COMPONENTS TMVA)
find_package(VDT)

# Runtime requirement for ROOT TMVA since 6.14
find_package(BLAS)

find_package(Boost)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

# Disable all but leak sanitizers for all factory compilations that include TMVA 
# auto-generated code, as these tend to explode compilation times..
if ( SANITIZER_ENABLED AND NOT SANITIZER_ENABLED STREQUAL "liblsan.so" )
 message(STATUS "Disabling ${SANITIZER_ENABLED} sanitizer for TMVA generated code.")
 set_property(SOURCE src/TMVAImpFactory-Bs2MuMuDev1.cpp APPEND_STRING PROPERTY COMPILE_FLAGS " -fno-sanitize=all " )
 set_property(SOURCE src/TMVAImpFactory-Bs2MuMuDev2.cpp APPEND_STRING PROPERTY COMPILE_FLAGS " -fno-sanitize=all " )
 set_property(SOURCE src/TMVAImpFactory-MC12TuneV2.cpp  APPEND_STRING PROPERTY COMPILE_FLAGS " -fno-sanitize=all " )
 set_property(SOURCE src/TMVAImpFactory-MC12TuneV3.cpp  APPEND_STRING PROPERTY COMPILE_FLAGS " -fno-sanitize=all " )
 set_property(SOURCE src/TMVAImpFactory-MC12TuneV4.cpp  APPEND_STRING PROPERTY COMPILE_FLAGS " -fno-sanitize=all " )
 set_property(SOURCE src/TMVAImpFactory-MC15TuneV1.cpp  APPEND_STRING PROPERTY COMPILE_FLAGS " -fno-sanitize=all " )
 set_property(SOURCE src/TMVAImpFactory-MCUpTuneV1.cpp  APPEND_STRING PROPERTY COMPILE_FLAGS " -fno-sanitize=all " )
endif()

gaudi_add_library(ChargedProtoANNPID-yPIDLib
                  src/yPID/MC15TuneV1/*/*.cpp
                  PUBLIC_HEADERS src/yPID/MC15TuneV1/keras)

gaudi_add_module(ChargedProtoANNPID
                 src/*.cpp
                 INCLUDE_DIRS ROOT VDT Rec/RecInterfaces Tr/TrackInterfaces
                 LINK_LIBRARIES ROOT VDT MCEvent PhysEvent RecEvent GaudiAlgLib ChargedProtoANNPID-yPIDLib)

#if(NEUROBAYESEXPERT_FOUND)
#  target_link_libraries(ChargedProtoANNPID ${NEUROBAYESEXPERT_LIBRARIES})
#endif()

gaudi_install_python_modules()
