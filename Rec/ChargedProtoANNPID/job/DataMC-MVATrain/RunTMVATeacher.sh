#!/bin/bash
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Data files
export ROOTDIR="/r03/lhcb/jonesc/ANNPID/ProtoParticlePIDtuples/Test/V1/StripFiltered"
#export ROOTDIR="/Volumes/2TB-Mobile/ANNPID/StripFiltered"

#export TRACK="Long"
#export TRACK="Downstream"
export TRACK="Upstream"

export NAME="2015DataMCDiff"
export SIGNALFILE=${ROOTDIR}"/2015-S23r1-ANNPID-BHADRONCOMPLETEEVENT.root:ANNPID/DecayTree"
export BCKGRDFILE=${ROOTDIR}"/Incb-DevMCJun2015-S23r1-ANNPID-Pythia8.root:ANNPID/DecayTree"

#export NAME="2012DataMCDiff"
#export SIGNALFILE=${ROOTDIR}"/2012-S20-ANNPID-BHADRONCOMPLETEEVENT.root:ANNPID/DecayTree"
#export BCKGRDFILE=${ROOTDIR}"/Incb-MC2012-S20-ANNPID-Pythia8.root:ANNPID/DecayTree"

# Run the teacher
export OUTPUTDIR="output-"${NAME}"-"${TRACK}
mkdir -p ${OUTPUTDIR}
cp TMVAInputs"-"${TRACK}.txt ${OUTPUTDIR}/weights/
nice -n 10 TMVATeacher.exe --OutputDir ${OUTPUTDIR} --Name ${NAME}"-"${TRACK} --SignalCuts TMVACuts"-"${TRACK}.txt --BckgCuts TMVACuts"-"${TRACK}.txt --TrainingParameters TMVATraining.txt --Inputs TMVAInputs"-"${TRACK}.txt --SignalTrainingFile $SIGNALFILE --BckgTrainingFile $BCKGRDFILE 2>&1 | tee ${OUTPUTDIR}/TMVATrain"-"${NAME}"-"${TRACK}.log

exit 0
