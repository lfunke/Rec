/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-------------------------------------------------------------------------------
/** @file ChargedProtoANNPIDAlg.h
 *
 *  Declaration file for ANN Combined PID algorithm ANNGlobalPID::ChargedProtoANNPIDAlg
 *
 *  @author Chris Jones       Christopher.Rob.Jones@cern.ch
 *  @date   10/09/2010
 */
//-------------------------------------------------------------------------------

#pragma once

// STL
#include <sstream>
#include <fstream>
#include <memory>

// Gaudi
#include "GaudiKernel/IJobOptionsSvc.h"

// FPE exception protection
#include "Kernel/FPEGuard.h"

// local
#include "ChargedProtoANNPIDAlgBase.h"

// boost
#include "boost/algorithm/string.hpp"
#include "boost/format.hpp"

namespace ANNGlobalPID
{

  //-----------------------------------------------------------------------------
  /** @class ChargedProtoANNPIDAlg ChargedProtoANNPIDAlg.h
   *
   *  Adds ANN PID information to ProtoParticles
   *
   *  @author Chris Jones
   *  @date   2010-03-09
   */
  //-----------------------------------------------------------------------------

  class ChargedProtoANNPIDAlg final : public ChargedProtoANNPIDAlgBase
  {

  public:

    /// Standard constructor
    ChargedProtoANNPIDAlg( const std::string& name, ISvcLocator* pSvcLocator );

    virtual ~ChargedProtoANNPIDAlg( ) = default; ///< Destructor

    StatusCode initialize() override;    ///< Algorithm initialization
    StatusCode execute() override;    ///< Algorithm execution

  private:

    /// Track type
    std::string m_trackType;

    /// PID type
    std::string m_pidType;

    /// The version of the PID networks training to use
    std::string m_netVersion;

    /// Suppress all printout from the ANN experts
    bool m_suppressANNPrintout { true };

    /// Network Configuration
    std::unique_ptr<NetConfig> m_netConfig;

    /// The extra info to fill on the ProtoParticle
    LHCb::ProtoParticle::additionalInfo m_protoInfo = LHCb::ProtoParticle::additionalInfo::NoPID;

    /// The track type for this instance
    LHCb::Track::Types m_tkType = LHCb::Track::Types::TypeUnknown;

  };

}
