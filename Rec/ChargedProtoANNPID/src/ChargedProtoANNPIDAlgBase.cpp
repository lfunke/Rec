/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file ChargedProtoANNPIDAlgBase.cpp
 *
 * Implementation file for algorithm ANNGlobalPID::ChargedProtoANNPIDAlgBase
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 2010-03-09
 */
//-----------------------------------------------------------------------------

// local
#include "ChargedProtoANNPIDAlgBase.h"
#include "ChargedProtoANNPIDCommonBase.icpp"
template class ANNGlobalPID::ChargedProtoANNPIDCommonBase<GaudiTupleAlg> ;

//-----------------------------------------------------------------------------
