/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <vector>
#include <unordered_map>
#include <string>

using namespace std;


#ifndef PREPROCESSING_H
#define PREPROCESSING_H

class AbstractTransformer {
public:
	virtual void read_transforms(string) = 0;
	virtual std::vector<double> transform(std::vector<double>, std::vector<string>) = 0;
	virtual ~AbstractTransformer() {};
};

class StandardTransformer: public AbstractTransformer {
protected:
	std::unordered_map<std::string, std::pair<double, double> > feature_values; // HashMap feature_name -> (mean, std)

public:
	void read_transforms(string) override;
	std::vector<double> transform(std::vector<double>, std::vector<string>) override;
};


// Transformer
class IronTransformer: public AbstractTransformer {

protected:

	std::vector<string> feature_names;
	std::vector< std::vector<double> > feature_values;
	std::vector< std::vector<double> > feature_percentiles;

public:
	void read_transforms(string) override;
	std::vector<double> transform(std::vector<double>, std::vector<string>) override;
};

// Helping functions
std::vector<double> select_features(std::vector<double>, std::vector<string>, std::vector<string>);
std::vector<double> convert_DLL_to_LL(std::vector<double>);
std::vector<double> compute_cum_sum(std::vector<double>);
double sum_features(std::vector<double>, std::vector<string>, std::vector<string>);
void add_constructed_features(std::vector<double> &, std::vector<string> &);

// The main function
void preprocess(std::vector<double> &, std::vector<string> &, IronTransformer, const bool&);
void preprocess(std::vector<double> &, std::vector<string> &, IronTransformer);

void preprocess(std::vector<double> &, std::vector<string> &, StandardTransformer, const bool&);
void preprocess(std::vector<double> &, std::vector<string> &, StandardTransformer);


#endif
