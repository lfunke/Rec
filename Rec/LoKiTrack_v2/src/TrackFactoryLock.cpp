/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/Report.h"
#include "LoKi_v2/TrackFactoryLock.h"
#include "LoKi_v2/TrackEngineActor.h"
// ============================================================================
/** @file
 *  Implementation file for class LoKi::Hybrid::Pr::TrackFactoryLock
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV, Sascha Stahl
 */
// ============================================================================
// constructor
// ============================================================================
LoKi::Hybrid::Pr::TrackFactoryLock::TrackFactoryLock
( const LoKi::Pr::ITrackFunctorAntiFactory* tool    ,
  const LoKi::Context&                  context ) 
  : m_tool ( tool )
{
  LoKi::Hybrid::Pr::TrackEngineActor& actor =
    LoKi::Hybrid::Pr::TrackEngineActor::instance() ;
  // connect the tool to the actor
  StatusCode sc = actor.connect ( m_tool.getObject () , context ) ;
  if ( sc.isFailure () )
  { LoKi::Report::Error
      ( "LoKi::Hybrid::Pr::TrackFactoryLock: error from connectTool", sc ) .ignore() ; }
}
// ============================================================================
// destructor
// ============================================================================
LoKi::Hybrid::Pr::TrackFactoryLock::~TrackFactoryLock()
{
  LoKi::Hybrid::Pr::TrackEngineActor& actor =
    LoKi::Hybrid::Pr::TrackEngineActor::instance() ;
  // connect the tool to the actor
  StatusCode sc = actor.disconnect ( m_tool.getObject () ) ;
  if ( sc.isFailure () )
  { LoKi::Report::Error
      ( "LoKi::Hybrid::Pr::TrackFactoryLock: error from releaseTool", sc ) .ignore() ; }
}
// ============================================================================
// The END
// ============================================================================
