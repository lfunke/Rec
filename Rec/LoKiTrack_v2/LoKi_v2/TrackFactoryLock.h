/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#pragma once
// ============================================================================
// $URL$
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/Interface.h"
#include "LoKi_v2/ITrackFunctorAntiFactory.h"
#include "LoKi/Context.h"
// ============================================================================
namespace LoKi
{
  // ==========================================================================
  namespace Hybrid::Pr
  {
    // ========================================================================
    /** @class TrackFactoryLock LoKi_v2/TrackFactoryLock.h
     *
     *  Helper class (sentry) to connect ITrackFunctorAntiFactory to TrackEngine
     *
     *  @author Vanya BELYAEV, Sascha Stahl
     *
     */
    class TrackFactoryLock
    {
    public:
      // ======================================================================
      /// contructor : Lock
      TrackFactoryLock  
	( const LoKi::Pr::ITrackFunctorAntiFactory* tool    , 
        const LoKi::Context&                  context ) ;
      /// destructor : UnLock
      virtual ~TrackFactoryLock () ;                    // destrcutor : UnLock
      // ======================================================================
    private:
      // ======================================================================
      /// no default constructor
      TrackFactoryLock () ;                     // no default constructor
      /// no copy constructor
      TrackFactoryLock ( const TrackFactoryLock& ) ; // no copy constructor
      /// no assignement opeartor
      TrackFactoryLock& operator = ( const TrackFactoryLock& ) ;
      // ======================================================================
    private:
      // ======================================================================
      /// the tool itself
      LoKi::Interface<LoKi::Pr::ITrackFunctorAntiFactory> m_tool ; // the tool
      // ======================================================================
    } ;
    // ========================================================================
  } //                                        The end of namespace LoKi::Hybrid
  // ==========================================================================
} //                                                  The end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
// ============================================================================
