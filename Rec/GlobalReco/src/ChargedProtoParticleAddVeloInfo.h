/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file ChargedProtoParticleAddVeloInfo.h
 *
 * Header file for algorithm ChargedProtoParticleAddVeloInfo
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 29/03/2006
 */
//-----------------------------------------------------------------------------

#ifndef GLOBALRECO_ChargedProtoParticleAddVeloInfo_H
#define GLOBALRECO_ChargedProtoParticleAddVeloInfo_H 1

// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

// Event
#include "Event/ProtoParticle.h"

// interfaces
#include "TrackInterfaces/ITrackVelodEdxCharge.h"

/** @class ChargedProtoParticleAddVeloInfo ChargedProtoParticleAddVeloInfo.h
 *
 *  Updates the VELO PID (dE/dx) information stored in the ProtoParticles
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date 28/08/2009
 */

class ChargedProtoParticleAddVeloInfo final : public GaudiAlgorithm
{

public:

  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode execute() override;       ///< Algorithm execution

private:

  /// Add Velo dE/dx information to the given ProtoParticle
  bool addVelodEdx( LHCb::ProtoParticle * proto ) const;

  ///< Location of the ProtoParticles in the TES
  // default locations from context()
  Gaudi::Property<std::string> m_protoPath { this, "ProtoParticleLocation",
                                             (context() == "HLT" || context() == "Hlt" ) ? LHCb::ProtoParticleLocation::HltCharged
                                                                                         : LHCb::ProtoParticleLocation::Charged };

  /// Velo dE/dx charge tool
  ToolHandle<ITrackVelodEdxCharge> m_velodEdx { this, "VeloCharge", "TrackVelodEdxCharge/VeloCharge" };

};

#endif // GLOBALRECO_ChargedProtoParticleAddVeloInfo_H
