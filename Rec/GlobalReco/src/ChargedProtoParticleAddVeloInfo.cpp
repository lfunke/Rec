/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
/** @file ChargedProtoParticleAddVeloInfo.cpp
 *
 * Implementation file for algorithm ChargedProtoParticleAddVeloInfo
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 28/08/2009
 */
//-----------------------------------------------------------------------------

// local
#include "ChargedProtoParticleAddVeloInfo.h"

//=============================================================================
// Main execution
//=============================================================================
StatusCode ChargedProtoParticleAddVeloInfo::execute()
{
  // ProtoParticle container
  auto * protos = getIfExists<LHCb::ProtoParticles>(m_protoPath);
  if ( !protos )
  {
    return Warning( "No existing ProtoParticle container at " +
                    m_protoPath + " thus do nothing.",
                    StatusCode::SUCCESS );
  }

  // Loop over proto particles and fill dE/dx
  for ( auto * P : *protos ) { addVelodEdx(P); }

  counter( "VeloDEDX ==> " + m_protoPath ) += protos->size();

  return StatusCode::SUCCESS;
}

//=============================================================================

//=============================================================================
// Add VELO dE/dx info to the protoparticle
//=============================================================================
bool
ChargedProtoParticleAddVeloInfo::addVelodEdx( LHCb::ProtoParticle * proto ) const
{
  // clear current Velo info
  proto->removeVeloInfo();

  // get velo charge
  double veloNtks(0);
  const auto sc = m_velodEdx->nTracks( proto->track(), veloNtks );
  if ( sc )
  { proto->addInfo( LHCb::ProtoParticle::additionalInfo::VeloCharge, veloNtks ); }

  // return status
  return sc.isSuccess();
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( ChargedProtoParticleAddVeloInfo )
