/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file FutureChargedProtoParticleAddPrsInfo.h
 *
 * Header file for algorithm FutureChargedProtoParticleAddPrsInfo
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 29/03/2006
 */
//-----------------------------------------------------------------------------

#ifndef GLOBALRECO_FutureChargedProtoParticleAddPrsInfo_H
#define GLOBALRECO_FutureChargedProtoParticleAddPrsInfo_H 1

// from Gaudi
#include "FutureChargedProtoParticleCALOFUTUREBaseAlg.h"

/** @class FutureChargedProtoParticleAddPrsInfo FutureChargedProtoParticleAddPrsInfo.h
 *
 *  Updates the CALO 'BREM' information stored in the ProtoParticles
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date 28/08/2009
 */

class FutureChargedProtoParticleAddPrsInfo final : public FutureChargedProtoParticleCALOFUTUREBaseAlg
{

public:

  /// Standard constructor
  FutureChargedProtoParticleAddPrsInfo( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override;       ///< Algorithm execution

private:

  /// Load the Calo Prs tables
  bool getPrsData();

  /// Add Calo Prs information to the given ProtoParticle
  bool addPrs( LHCb::ProtoParticle * proto ) const;

private:

  std::string m_protoPath; ///< Location of the ProtoParticles in the TES

  std::string m_inPrsPath ;
  std::string m_prsEPath ;
  std::string m_prsPIDePath ;

  const LHCb::CaloFuture2Track::ITrAccTable*  m_InPrsTable = nullptr;
  const LHCb::CaloFuture2Track::ITrEvalTable*  m_PrsETable = nullptr;
  const LHCb::CaloFuture2Track::ITrEvalTable* m_dllePrsTable = nullptr;

};

#endif // GLOBALRECO_FutureChargedProtoParticleAddPrsInfo_H
