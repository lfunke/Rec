/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file FutureChargedProtoParticleAddPrsInfo.cpp
 *
 * Implementation file for algorithm FutureChargedProtoParticleAddPrsInfo
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 28/08/2009
 */
//-----------------------------------------------------------------------------

// local
#include "FutureChargedProtoParticleAddPrsInfo.h"

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FutureChargedProtoParticleAddPrsInfo )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
FutureChargedProtoParticleAddPrsInfo::
FutureChargedProtoParticleAddPrsInfo( const std::string& name,
                                ISvcLocator* pSvcLocator)
  : FutureChargedProtoParticleCALOFUTUREBaseAlg ( name , pSvcLocator )
{
  // default locations from context()

  using namespace LHCb::CaloFuture2Track;
  using namespace LHCb::CaloFutureIdLocation;
  using namespace LHCb::CaloFutureAlgUtils;

  m_protoPath         = LHCb::ProtoParticleLocation::Charged ;
  m_inPrsPath         = PathFromContext( context() , InPrs   );
  m_prsEPath          = PathFromContext( context() , PrsE    );
  m_prsPIDePath       = PathFromContext( context() , PrsPIDe );

  declareProperty("ProtoParticleLocation"      , m_protoPath        );
  declareProperty("InputInPrsLocation"         , m_inPrsPath        );
  declareProperty("InputPrsELocation"          , m_prsEPath         );
  declareProperty("InputPrsPIDeLocation"       , m_prsPIDePath      );

}

//=============================================================================
// Main execution
//=============================================================================
StatusCode FutureChargedProtoParticleAddPrsInfo::execute()
{
  // Load the Brem data
  const bool sc = getPrsData();
  if ( !sc )
  {
    return Warning( "No BREM data -> ProtoParticles will not be changed.", StatusCode::SUCCESS );
  }

  // ProtoParticle container
  auto * protos = getIfExists<LHCb::ProtoParticles>(m_protoPath);
  if ( !protos )
  {
    if ( msgLevel(MSG::DEBUG) ) debug() << "No existing ProtoParticle container at "
                                        <<  m_protoPath<<" thus do nothing."<<endmsg;
    return StatusCode::SUCCESS;
  }

  // Loop over proto particles and update PRS info
  for ( auto * proto : *protos ) { addPrs(proto); }

  if ( counterStat->isQuiet() ) counter("PrsPIDs("+context()+") ==> " + m_protoPath )+= protos->size();

  return StatusCode::SUCCESS;
}

//=============================================================================

//=============================================================================
// Loads the Calo Prs data
//=============================================================================
bool FutureChargedProtoParticleAddPrsInfo::getPrsData()
{
  const bool sc1 = loadCaloTable(m_InPrsTable  , m_inPrsPath);
  const bool sc2 = loadCaloTable(m_PrsETable   , m_prsEPath);
  const bool sc3 = loadCaloTable(m_dllePrsTable, m_prsPIDePath);
  const bool sc  = sc1 && sc2 && sc3;
  if ( sc && msgLevel(MSG::DEBUG) ) debug() << "PRS PID SUCCESSFULLY LOADED" << endmsg;
  return sc;
}

//=============================================================================
// Add Calo Prs info to the protoparticle
//=============================================================================
bool FutureChargedProtoParticleAddPrsInfo::addPrs( LHCb::ProtoParticle * proto ) const
{
  // clear PRS info
  proto->removeCaloPrsInfo();

  // Add new info

  bool hasPrsPID = false;

  const auto aRange = m_InPrsTable -> relations ( proto->track() ) ;
  if( !aRange.empty() )
  {
    hasPrsPID = aRange.front().to();
    if( hasPrsPID )
    {
      if ( msgLevel(MSG::VERBOSE) )verbose() << " -> The track is in Prs acceptance"  << endmsg;
      proto->addInfo(LHCb::ProtoParticle::additionalInfo::InAccPrs , true );

      // Get the PrsE (intermediate) estimator
      {
        const auto vRange = m_PrsETable -> relations ( proto->track() ) ;
        if ( !vRange.empty() ) { proto->addInfo(LHCb::ProtoParticle::additionalInfo::CaloPrsE,  vRange.front().to() ); }
      }

      // Get the Prs DLL(e)
      {
        const auto vRange = m_dllePrsTable -> relations ( proto->track() ) ;
        if ( !vRange.empty() ) { proto->addInfo(LHCb::ProtoParticle::additionalInfo::PrsPIDe , vRange.front().to() ); }
      }

      if ( msgLevel(MSG::VERBOSE) )
        verbose() << " -> Prs PID : "
                  << " PrsE       =" <<  proto->info(LHCb::ProtoParticle::additionalInfo::CaloPrsE, -999.)
                  << " Dlle (Prs)  =" <<  proto->info(LHCb::ProtoParticle::additionalInfo::PrsPIDe , -999.)
                  << endmsg;

    }
    else
    {
      if ( msgLevel(MSG::VERBOSE) )verbose() << " -> The track is NOT in Prs acceptance"  << endmsg;
    }
  }
  else
  {
    if ( msgLevel(MSG::VERBOSE) )verbose() << " -> No entry for that track in the Prs acceptance table"  << endmsg;
  }

  return hasPrsPID;
}
