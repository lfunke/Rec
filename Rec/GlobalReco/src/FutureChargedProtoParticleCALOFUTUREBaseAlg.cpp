/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
/** @file FutureChargedProtoParticleCALOFUTUREBaseAlg.cpp
 *
 * Implementation file for algorithm FutureChargedProtoParticleCALOFUTUREBaseAlg
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 28/08/2009
 */
//-----------------------------------------------------------------------------

// local
#include "FutureChargedProtoParticleCALOFUTUREBaseAlg.h"

//-----------------------------------------------------------------------------

//=============================================================================
// Initialization
//=============================================================================
StatusCode FutureChargedProtoParticleCALOFUTUREBaseAlg::initialize()
{
  const StatusCode sc = GaudiAlgorithm::initialize();
  if ( sc.isFailure() ) return sc;

  counterStat = tool<IFutureCounterLevel>("FutureCounterLevel");
  m_estimator = tool<ICaloFutureHypoEstimator>("CaloFutureHypoEstimator","CaloFutureHypoEstimator",this);

  return sc;
}
