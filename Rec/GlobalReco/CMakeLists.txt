###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: GlobalReco
################################################################################
gaudi_subdir(GlobalReco v6r55)

gaudi_depends_on_subdirs(Calo/CaloInterfaces
                         Calo/CaloUtils
                         CaloFuture/CaloFutureInterfaces
                         CaloFuture/CaloFutureUtils
                         Det/CaloDet
                         Event/RecEvent
                         GaudiAlg
                         Kernel/Relations
                         Rec/ChargedProtoANNPID
                         Tr/TrackInterfaces)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(GlobalReco
                 src/*.cpp
                 INCLUDE_DIRS Tr/TrackInterfaces
                 LINK_LIBRARIES CaloUtils CaloFutureUtils CaloDetLib RecEvent GaudiAlgLib RelationsLib)

gaudi_install_python_modules()


gaudi_add_test(QMTest QMTEST)
