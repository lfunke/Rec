/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// STD & STL
// ============================================================================
#include <algorithm>
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/ToStream.h"
// ============================================================================
// local
// ============================================================================
#include "LoKi/TrackIDs.h"
// ============================================================================
// ============================================================================
/** @file
 *  Implementation file for class : TrackIDs
 *  Collection of functors that deals with LHCbIDs for Tracks
 *  (on request from Wouter Hulsbergen)
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 *  @date   2009-12-17
 */
// ============================================================================
namespace
{
  static_assert( std::numeric_limits<unsigned int>::is_integer    &&
                 std::numeric_limits<unsigned int>::is_specialized,
                 "" ) ;


  static const auto s_countIDs_tbl = std::array{
                  std::pair{ &LHCb::LHCbID::isVelo,       "isVelo"       },
                  std::pair{ &LHCb::LHCbID::isVeloR,      "isVeloR"      },
                  std::pair{ &LHCb::LHCbID::isVeloPhi,    "isVeloPhi"    },
                  std::pair{ &LHCb::LHCbID::isVeloPileUp, "isVeloPileUp" },
                  std::pair{ &LHCb::LHCbID::isVP,         "isVP"         },
                  std::pair{ &LHCb::LHCbID::isTT,         "isTT"         },
                  std::pair{ &LHCb::LHCbID::isIT,         "isIT"         },
                  std::pair{ &LHCb::LHCbID::isST,         "isST"         },
                  std::pair{ &LHCb::LHCbID::isOT,         "isOT"         },
                  std::pair{ &LHCb::LHCbID::isRich,       "isRich"       },
                  std::pair{ &LHCb::LHCbID::isCalo,       "isCalo"       },
                  std::pair{ &LHCb::LHCbID::isMuon,       "isMuon"       },
                  // Backwards compatability
                  std::pair{ &LHCb::LHCbID::isVP,         "isVeloPix"    } };

  static const auto s_countOTIDs_tbl = std::array{
                  std::pair{ &LHCb::OTChannelID::geometry,                "geometry"                    },
                  std::pair{ &LHCb::OTChannelID::sequentialUniqueLayer,   "sequentialUniqueLayer"       },
                  std::pair{ &LHCb::OTChannelID::sequentialUniqueQuarter, "sequentialUniqueQuarter"     },
                  std::pair{ &LHCb::OTChannelID::sequentialUniqueModule,  "sequentialUniqueModule"      },
                  std::pair{ &LHCb::OTChannelID::sequentialUniqueOtis,    "sequentialUniqueOtis"        },
                  std::pair{ &LHCb::OTChannelID::sequentialUniqueStraw,   "sequentialUniqueStraw"       },
                  std::pair{ &LHCb::OTChannelID::channelID,               "channelID"                   },
                  std::pair{ &LHCb::OTChannelID::channelID,               "channel"                     },
                  std::pair{ &LHCb::OTChannelID::tdcTime,                 "tdcTime"                     },
                  std::pair{ &LHCb::OTChannelID::module,                  "module"                      },
                  std::pair{ &LHCb::OTChannelID::quarter,                 "quarter"                     },
                  std::pair{ &LHCb::OTChannelID::layer,                   "layer"                       },
                  std::pair{ &LHCb::OTChannelID::straw,                   "straw"                       },
                  std::pair{ &LHCb::OTChannelID::station,                 "station"                     },
                  std::pair{ &LHCb::OTChannelID::uniqueModule,            "uniqueModule"                },
                  std::pair{ &LHCb::OTChannelID::uniqueQuarter,           "uniqueQuarter"               },
                  std::pair{ &LHCb::OTChannelID::uniqueLayer,             "uniqueLayer"                 },
                  std::pair{ &LHCb::OTChannelID::uniqueStraw,             "uniqueStraw"                 } };

  static const auto s_countSTIDs_tbl = std::array{
                  std::pair{ &LHCb::STChannelID::strip,           "strip"            },
                  std::pair{ &LHCb::STChannelID::sector,          "sector"           },
                  std::pair{ &LHCb::STChannelID::detRegion,       "detRegion"        },
                  std::pair{ &LHCb::STChannelID::detRegion,       "region"           },
                  std::pair{ &LHCb::STChannelID::layer,           "layer"            },
                  std::pair{ &LHCb::STChannelID::station,         "station"          },
                  std::pair{ &LHCb::STChannelID::uniqueLayer,     "uniqueLayer"      },
                  std::pair{ &LHCb::STChannelID::uniqueDetRegion, "uniqueDetRegion"  },
                  std::pair{ &LHCb::STChannelID::uniqueDetRegion, "uniqueRegion"     },
                  std::pair{ &LHCb::STChannelID::uniqueSector,    "uniqueSector"     },
                  std::pair{ &LHCb::STChannelID::type,            "type"             },
                  std::pair{ &LHCb::STChannelID::channelID,       "channelID"        },
                  std::pair{ &LHCb::STChannelID::channelID,       "channel"          } };

  static const auto s_countVeloIDs_tbl2 = std::array{
                  std::pair{ &LHCb::VeloChannelID::channelID,  "channelID" },
                  std::pair{ &LHCb::VeloChannelID::channelID,  "channel"   },
                  std::pair{ &LHCb::VeloChannelID::strip,      "strip"     },
                  std::pair{ &LHCb::VeloChannelID::sensor,     "sensor"    } };

  static const auto s_countVeloIDs_tbl1 = std::array{
                  std::pair{ &LHCb::VeloChannelID::isPileUp,   "isPileUp" },
                  std::pair{ &LHCb::VeloChannelID::isRType,    "isRType"  },
                  std::pair{ &LHCb::VeloChannelID::isPhiType,  "isPhiType"} };

  static const auto s_countVeloIDs_tbl3 = std::array{
                  std::pair{ "isPileUp",  &LHCb::VeloChannelID::isPileUp  },
                  std::pair{ "pileUp",    &LHCb::VeloChannelID::isPileUp  },
                  std::pair{ "PileUp",    &LHCb::VeloChannelID::isPileUp  },
                  std::pair{ "isRType",   &LHCb::VeloChannelID::isRType   },
                  std::pair{ "RType",     &LHCb::VeloChannelID::isRType   },
                  std::pair{ "rType",     &LHCb::VeloChannelID::isRType   },
                  std::pair{ "R",         &LHCb::VeloChannelID::isRType   },
                  std::pair{ "r",         &LHCb::VeloChannelID::isRType   },
                  std::pair{ "isPhiType", &LHCb::VeloChannelID::isPhiType },
                  std::pair{ "PhiType",   &LHCb::VeloChannelID::isPhiType },
                  std::pair{ "phiType",   &LHCb::VeloChannelID::isPhiType },
                  std::pair{ "Phi",       &LHCb::VeloChannelID::isPhiType },
                  std::pair{ "phi",       &LHCb::VeloChannelID::isPhiType } };

}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountIDs::CountIDs ( LoKi::Track::CountIDs::PMF pmf )
: m_pmf  ( pmf  )
{
  auto i = std::find_if( s_countIDs_tbl.begin(), s_countIDs_tbl.end(),
                         [&](const auto& p) { return p.first==pmf; } );
  if ( i != s_countIDs_tbl.end() ) m_nick = i->second;

  Assert ( 0 != m_pmf , "Invalid LHCb::LHCbID member function!" ) ;
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountIDs::CountIDs
( const std::string& nick )
  : LoKi::AuxFunBase ( std::tie ( nick ) )
  , m_nick ( nick )
{
  auto i = std::find_if( s_countIDs_tbl.begin(), s_countIDs_tbl.end(),
                         [&](const auto& p) { return p.second==m_nick; } );
  if ( i != s_countIDs_tbl.end() ) m_pmf = i->first;
  if ( !m_pmf ) Exception ( "Invalid LHCb::LHCbID member function: '" + m_nick + "'" ) ;

}
// ============================================================================
// MANDATORY: clone method ("virtual consttructor")
// ============================================================================
LoKi::Track::CountIDs*
LoKi::Track::CountIDs::clone() const
{ return  new LoKi::Track::CountIDs ( *this ) ; }
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Track::CountIDs::result_type
LoKi::Track::CountIDs::operator() ( LoKi::Track::CountIDs::argument t ) const
{
  //
  if ( UNLIKELY( !t ) ) {
    Error ("LHCb::Track* points to NULL, return -1") ;
    return -1 ;
  }
  //
  const auto&  lhcbids = t->lhcbIDs() ;
  //
  return std::count_if ( lhcbids.begin() , lhcbids.end() ,
                         [&](const LHCb::LHCbID& id) { return (id.*m_pmf)(); } );
}
// ============================================================================
// OPTIONAL: nice printout
// ============================================================================
std::ostream& LoKi::Track::CountIDs::fillStream( std::ostream& s ) const
{ return s << " TrIDC( '" << m_nick << "' ) " ; }

// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountOTIDs::CountOTIDs ( LoKi::Track::CountOTIDs::PMF pmf ,
                                      const unsigned int            i   )
: m_pmf   ( pmf   )
, m_uints ( 1 , i )
{
  setNick ( m_pmf ) ;
  Assert ( 0 != m_pmf , "Invalid LHCb::OTchannelID member function!" ) ;
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountOTIDs::CountOTIDs ( LoKi::Track::CountOTIDs::PMF     pmf ,
                                      const std::vector<unsigned int>& i   )
: m_pmf   ( pmf   )
, m_uints ( i     )
{
  setNick ( m_pmf ) ;
  Assert ( 0 != m_pmf , "Invalid LHCb::OTchannelID member function!" ) ;
  Assert ( !m_uints.empty() , "Empty vector of values is specified!" ) ;
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountOTIDs::CountOTIDs ( const unsigned int           imin ,
                                      LoKi::Track::CountOTIDs::PMF pmf  ,
                                      const unsigned int           imax )
: m_pmf   ( pmf   )
, m_imin  ( imin )
, m_imax  ( imax )
{
  setNick ( m_pmf ) ;
  Assert ( 0 != m_pmf    , "Invalid LHCb::OTChannelID member function!" ) ;
  Assert ( imin <= imax  , "Invalid range of values" ) ;
}
// ============================================================================
// set nick
// ============================================================================
void LoKi::Track::CountOTIDs::setNick ( LoKi::Track::CountOTIDs::PMF pmf )
{
  auto i = std::find_if( s_countOTIDs_tbl.begin(), s_countOTIDs_tbl.end(),
                         [&](const auto& p) { return p.first == pmf; } );
  if ( i !=  s_countOTIDs_tbl.end() ) m_nick = i->second;
  else Exception ( "Invalid LHCb::OTChannelID member function!") ;
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountOTIDs::CountOTIDs ( const std::string&               nick ,
                                      const std::vector<unsigned int>& i    )
: LoKi::AuxFunBase ( std::tie ( nick , i ) )
, m_uints ( i     )
, m_nick  ( nick  )
{
  setPmf ( m_nick ) ;
  Assert ( 0 != m_pmf , "Invalid LHCb::OTChannelID member function: '" + m_nick + "'" ) ;
  Assert ( !m_uints.empty() , "Empty vector of values is specified!" ) ;
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountOTIDs::CountOTIDs ( const std::string&  nick ,
                                      const unsigned int  i    )
: LoKi::AuxFunBase ( std::tie ( nick , i ) )
, m_uints ( 1 , i )
, m_nick  ( nick  )
{
  setPmf ( m_nick ) ;
  Assert ( 0 != m_pmf , "Invalid LHCb::OTChannelID member function: '" + m_nick + "'" ) ;
}

// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountOTIDs::CountOTIDs ( const unsigned int  imin ,
                                      const std::string&  nick ,
                                      const unsigned int  imax )
: LoKi::AuxFunBase ( std::tie ( imin , nick , imax  ) )
, m_imin  ( imin  )
, m_imax  ( imax  )
, m_nick  ( nick  )
{
  setPmf ( m_nick ) ;
  Assert ( 0 != m_pmf   , "Invalid LHCb::OTChannelID member function: '" + m_nick + "'" ) ;
  Assert ( imin <= imax  , "Invalid range of values" ) ;
}
// ============================================================================
// set PMF properly
// ============================================================================
void LoKi::Track::CountOTIDs::setPmf ( const std::string& nick )
{
  auto i = std::find_if(s_countOTIDs_tbl.begin(), s_countOTIDs_tbl.end(),
                        [&](const auto& p) { return p.second == nick; } );
  if ( i!=s_countOTIDs_tbl.end() ) m_pmf = i->first;
  else Exception ( "Invalid LHCb::OTChannelID member function '" + nick + "'") ;
}
// ============================================================================
// MANDATORY: clone method ('virtual constructor')
// ============================================================================
LoKi::Track::CountOTIDs*
LoKi::Track::CountOTIDs::clone() const
{ return new LoKi::Track::CountOTIDs ( *this ) ; }
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Track::CountOTIDs::result_type
LoKi::Track::CountOTIDs::operator()( LoKi::Track::CountOTIDs::argument t ) const
{
  //
  if ( UNLIKELY(!t) ) {
    Error ("LHCb::Track* points to NULL, return -1") ;
    return -1 ;
  }
  //
  const auto& lhcbids = t->lhcbIDs() ;
  //
  size_t res = 0 ;
  //
  for ( auto id = lhcbids.begin() ; lhcbids.end() != id ; ++id )
  {
    if ( ! id->isOT() ) { continue ; }
    //
    const unsigned int r = (id->otID().*m_pmf) () ;
    if      ( m_uints.empty()     ) { if ( m_imin <= r && r <= m_imax ) { ++res ; } }
    else if ( 1 == m_uints.size() ) { if ( m_uints.front() == r       ) { ++res ; } }
    else {
      if  ( m_uints.end() != std::find ( m_uints.begin() , m_uints.end() , r ) ) { ++res ; }
    }
  }
  //
  return res;
}
// ============================================================================
// OPTIONAL: nice printout
// ============================================================================
std::ostream& LoKi::Track::CountOTIDs::fillStream( std::ostream& s ) const
{
  s << " TrOTIDC( " ;
  //
  if      ( m_uints.empty()     ) { s << m_imin << " ,'" << m_nick << "', "<< m_imax ; }
  else if ( 1 == m_uints.size() ) { s << "'" << m_nick << "', " << m_uints.front()     ; }
  else {
    s << "'" << m_nick << "', " ;
    Gaudi::Utils::toStream ( m_uints , s ) ;
  }
  //
  return s << " ) " ;
}



// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountSTIDs::CountSTIDs ( LoKi::Track::CountSTIDs::PMF pmf ,
                                      const unsigned int            i   )
: m_pmf   ( pmf   )
, m_uints ( 1 , i )
{
  setNick ( m_pmf ) ;
  Assert ( 0 != m_pmf , "Invalid LHCb::STChannelID member function!" ) ;
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountSTIDs::CountSTIDs ( LoKi::Track::CountSTIDs::PMF    pmf ,
                                      const std::vector<unsigned int>& i   )
: m_pmf   ( pmf   )
, m_uints ( i     )
{
  setNick ( m_pmf ) ;
  Assert ( 0 != m_pmf , "Invalid LHCb::STChannelID member function!" ) ;
  Assert ( !m_uints.empty() , "Empty vector of values is specified!" ) ;
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountSTIDs::CountSTIDs ( const unsigned int            imin ,
                                      LoKi::Track::CountSTIDs::PMF pmf  ,
                                      const unsigned int            imax )
: m_pmf   ( pmf   )
, m_imin  ( imin )
, m_imax  ( imax )
{
  setNick ( m_pmf ) ;
  Assert ( 0 != m_pmf    , "Invalid LHCb::STChannelID member function!" ) ;
  Assert ( imin <= imax  , "Invalid range of values" ) ;
}
// ============================================================================
// set nick
// ============================================================================
void LoKi::Track::CountSTIDs::setNick ( LoKi::Track::CountSTIDs::PMF pmf )
{
  auto i = std::find_if( s_countSTIDs_tbl.begin(), s_countSTIDs_tbl.end(),
                         [&](const auto& p) { return p.first == pmf; } );
  if ( i != s_countSTIDs_tbl.end() ) m_nick = i->second ;
  else Exception ( "Invalid LHCb::STChannelID member function!") ;
}

// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountSTIDs::CountSTIDs ( const std::string&               nick ,
                                      const std::vector<unsigned int>& i    )
: LoKi::AuxFunBase ( std::tie ( nick , i  ) )
, m_nick  ( nick  )
{
  setPmf ( m_nick ) ;
  Assert ( 0 != m_pmf , "Invalid LHCb::STChannelID member function: '" + m_nick + "'" ) ;
  Assert ( !m_uints.empty() , "Empty vector of values is specified!" ) ;
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountSTIDs::CountSTIDs ( const std::string&  nick ,
                                      const unsigned int  i    )
: LoKi::AuxFunBase ( std::tie ( nick , i  ) )
, m_uints ( 1 , i )
, m_nick  ( nick  )
{
  setPmf ( m_nick ) ;
  Assert ( 0 != m_pmf , "Invalid LHCb::STChannelID member function: '" + m_nick + "'" ) ;
}

// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountSTIDs::CountSTIDs ( const unsigned int  imin ,
                                      const std::string&  nick ,
                                      const unsigned int  imax )
: LoKi::AuxFunBase ( std::tie ( imin, nick , imax ) )
, m_imin  ( imin  )
, m_imax  ( imax  )
, m_nick  ( nick  )
{
  setPmf ( m_nick ) ;
  Assert ( 0 != m_pmf   , "Invalid LHCb::OTChannelID member function: '" + m_nick + "'" ) ;
  Assert ( imin <= imax  , "Invalid range of values" ) ;
}
// ============================================================================
// set PMF properly
// ============================================================================
void LoKi::Track::CountSTIDs::setPmf ( const std::string& nick )
{
  auto i = std::find_if( s_countSTIDs_tbl.begin(), s_countSTIDs_tbl.end(),
                         [&](const auto& p) { return p.second == nick; } );
  if ( i != s_countSTIDs_tbl.end() ) m_pmf = i->first;
  else Exception ( "Invalid LHCb::STChannelID member function '" + nick + "'");
}


// ============================================================================
// MANDATORY: clone method ('virtual constructor')
// ============================================================================
LoKi::Track::CountSTIDs*
LoKi::Track::CountSTIDs::clone() const
{ return new LoKi::Track::CountSTIDs ( *this ) ; }
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Track::CountSTIDs::result_type
LoKi::Track::CountSTIDs::operator()
  ( LoKi::Track::CountSTIDs::argument t ) const
{
  //
  if ( UNLIKELY(!t) ) {
    Error ("LHCb::Track* points to NULL, return -1") ;
    return -1 ;
  }
  //
  const auto& lhcbids = t->lhcbIDs() ;
  //
  size_t res = 0 ;
  //
  for ( auto id = lhcbids.begin() ; lhcbids.end() != id ; ++id )
  {
    if ( ! id->isST() ) { continue ; }
    //
    const unsigned int r = (id->stID().*m_pmf) () ;
    if      ( m_uints.empty()     ) { if ( m_imin <= r && r <= m_imax ) { ++res ; } }
    else if ( 1 == m_uints.size() ) { if ( m_uints.front() == r       ) { ++res ; } }
    else
    {
      if  ( m_uints.end() != std::find ( m_uints.begin() , m_uints.end() , r ) ) { ++res ; }
    }
  }
  //
  return res;
}
// ============================================================================
// OPTIONAL: nice printout
// ============================================================================
std::ostream& LoKi::Track::CountSTIDs::fillStream ( std::ostream& s ) const
{
  s << " TrSTIDC( " ;
  //
  if      ( m_uints.empty()     ) { s << m_imin << " ,'" << m_nick << "', "<< m_imax ; }
  else if ( 1 == m_uints.size() ) { s << "'" << m_nick << "', " << m_uints.front()     ; }
  else {
    s << "'" << m_nick << "', " ;
    Gaudi::Utils::toStream ( m_uints , s ) ;
  }
  //
  return s << " ) " ;
}

// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountVeloIDs::CountVeloIDs ( LoKi::Track::CountVeloIDs::PMF2 pmf ,
                                          const unsigned int               i   )
: m_pmf2  ( pmf   )
, m_uints ( 1 , i )
{
  setNick ( m_pmf2 ) ;
  Assert ( 0 != m_pmf2 , "Invalid LHCb::VeloChannelID member function!" ) ;
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountVeloIDs::CountVeloIDs ( LoKi::Track::CountVeloIDs::PMF2 pmf ,
                                          const std::vector<unsigned int>& i   )
: m_pmf2  ( pmf   )
, m_uints ( i     )
{
  setNick ( m_pmf2 ) ;
  Assert ( 0 != m_pmf2 , "Invalid LHCb::VeloChannelID member function!" ) ;
  Assert ( !m_uints.empty() , "Empty vector of values is specified!" ) ;
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountVeloIDs::CountVeloIDs ( const unsigned int               imin ,
                                          LoKi::Track::CountVeloIDs::PMF2 pmf  ,
                                          const unsigned int               imax )
: m_pmf2  ( pmf   )
, m_imin  ( imin )
, m_imax  ( imax )
{
  setNick ( m_pmf2 ) ;
  Assert ( 0 != m_pmf2 , "Invalid LHCb::OTChannelID member function!" ) ;
  Assert ( imin <= imax  , "Invalid range of values" ) ;
}
// ============================================================================
// set nick
// ============================================================================
void LoKi::Track::CountVeloIDs::setNick ( LoKi::Track::CountVeloIDs::PMF2 pmf )
{
  auto i = std::find_if( s_countVeloIDs_tbl2.begin(), s_countVeloIDs_tbl2.end(),
                         [&](const auto& p) { return p.first == pmf; } );
  if ( i != s_countVeloIDs_tbl2.end() ) m_nick = i->second;
  else Exception ( "Invalid LHCb::VeloChannelID member function!") ;
}

// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountVeloIDs::CountVeloIDs ( const std::string&               nick ,
                                          const std::vector<unsigned int>& i    )
: LoKi::AuxFunBase ( std::tie ( nick , i ) )
, m_uints ( i     )
, m_nick  ( nick  )
{
  setPmf ( m_nick ) ;
  Assert ( 0 != m_pmf2 , "Invalid LHCb::VeloChannelID member function: '" + m_nick + "'" ) ;
  Assert ( !m_uints.empty() , "Empty vector of values is specified!" ) ;
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountVeloIDs::CountVeloIDs( const std::string&  nick ,
                                         const unsigned int  i    )
: LoKi::AuxFunBase ( std::tie ( nick , i ) )
, m_uints ( 1 , i )
, m_nick  ( nick  )
{
  setPmf ( m_nick ) ;
  Assert ( 0 != m_pmf2 , "Invalid LHCb::VeloChannelID member function: '" + m_nick + "'" ) ;
}

// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountVeloIDs::CountVeloIDs ( const unsigned int  imin ,
                                          const std::string&  nick ,
                                          const unsigned int  imax )
: LoKi::AuxFunBase ( std::tie ( imin , nick , imax ) )
, m_imin  ( imin  )
, m_imax  ( imax  )
, m_nick  ( nick  )
{
  setPmf ( m_nick ) ;
  Assert ( 0 != m_pmf2  , "Invalid LHCb::VeloChannelID member function: '" + m_nick + "'" ) ;
  Assert ( imin <= imax  , "Invalid range of values" ) ;
}
// ============================================================================
// set PMF properly
// ============================================================================
void LoKi::Track::CountVeloIDs::setPmf ( const std::string& nick )
{
  auto i = std::find_if( s_countVeloIDs_tbl2.begin(), s_countVeloIDs_tbl2.end(),
                         [&](const auto& p) { return p.second == nick; } );
  if ( i != s_countVeloIDs_tbl2.end() ) m_pmf2 = i->first;
  else Exception ( "Invalid LHCb::VeloChannelID member function '" + nick + "'") ;
}

// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountVeloIDs::CountVeloIDs ( LoKi::Track::CountVeloIDs::PMF1 pmf )
: m_pmf1  ( pmf   )
{
  auto i = std::find_if( s_countVeloIDs_tbl1.begin(), s_countVeloIDs_tbl1.end(),
                         [&](const auto& p) { return p.first == m_pmf1; } );
  if ( i != s_countVeloIDs_tbl1.end() ) m_nick = i->second;
  else Exception("Invalid LHCb::VeloChannelID memebr function") ;
  Assert ( 0 != m_pmf1  , "Invalid LHCb::VeloChannelID member function:" ) ;
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountVeloIDs::CountVeloIDs ( const std::string& nick  )
: LoKi::AuxFunBase ( std::tie ( nick ) )
, m_nick  ( nick )
{
  auto i = std::find_if( s_countVeloIDs_tbl3.begin(), s_countVeloIDs_tbl3.end(),
                         [&](const auto& p) { return p.first == m_nick; } );
  if ( i != s_countVeloIDs_tbl3.end() ) m_pmf1 = i->second;
  Assert ( 0 != m_pmf1  , "Invalid LHCb::VeloChannelID member function '" + m_nick + "'" ) ;
}
// ============================================================================
// MANDATORY: clone method ('virtual constructor')
// ============================================================================
LoKi::Track::CountVeloIDs*
LoKi::Track::CountVeloIDs::clone() const
{ return new LoKi::Track::CountVeloIDs ( *this ) ; }
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Track::CountVeloIDs::result_type
LoKi::Track::CountVeloIDs::operator() ( LoKi::Track::CountVeloIDs::argument t ) const
{
  //
  //
  if ( UNLIKELY(!t) ) {
    Error ("LHCb::Track* points to NULL, return -1") ;
    return -1 ;
  }
  //
  const auto& lhcbids = t->lhcbIDs() ;
  //
  size_t res = 0 ;
  //
  for ( auto id = lhcbids.begin() ; lhcbids.end() != id ; ++id )
  {
    if ( ! id->isVelo() ) { continue ; }
    //
    const LHCb::VeloChannelID velo = id->veloID() ;
    //
    if ( m_pmf1 ) { if ( (velo.*m_pmf1)() ) { ++res ; } }
    else {
      const unsigned int r = (velo.*m_pmf2) () ;
      if      ( m_uints.empty()     ) { if ( m_imin <= r && r <= m_imax ) { ++res ; } }
      else if ( 1 == m_uints.size() ) { if ( m_uints.front() == r       ) { ++res ; } }
      else {
        if  ( m_uints.end() != std::find ( m_uints.begin() , m_uints.end() , r ) ) { ++res ; }
      }
    }
  }
  //
  return res;
}
// ============================================================================
// OPTIONAL: nice printout
// ============================================================================
std::ostream& LoKi::Track::CountVeloIDs::fillStream ( std::ostream& s ) const
{
  s << " TrVELOIDC( " ;
  //
  if ( m_pmf1 ) { return s << "'" << m_nick << "' ) " ; }
  //
  if      ( m_uints.empty()     ) { s << m_imin << " ,'" << m_nick << "', "<< m_imax ; }
  else if ( 1 == m_uints.size() ) { s << "'" << m_nick << "', " << m_uints.front()     ; }
  else {
    s << "'" << m_nick << "', " ;
    Gaudi::Utils::toStream ( m_uints , s ) ;
  }
  //
  return s << " ) " ;
}


// ============================================================================
// The END
// ============================================================================
