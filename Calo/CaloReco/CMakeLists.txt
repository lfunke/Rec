###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: CaloReco
################################################################################
gaudi_subdir(CaloReco v5r28)

gaudi_depends_on_subdirs(Calo/CaloDAQ
                         Calo/CaloInterfaces
                         Calo/CaloPIDs
                         Calo/CaloUtils
                         Kernel/Relations)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(CaloReco
                 src/*.cpp
                 INCLUDE_DIRS Calo/CaloDAQ
                 LINK_LIBRARIES CaloUtils RelationsLib)

gaudi_install_python_modules()

gaudi_env(SET CALORECOOPTS \${CALORECOROOT}/options)

