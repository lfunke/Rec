###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: CaloMoniDst
################################################################################
gaudi_subdir(CaloMoniDst v5r21)

gaudi_depends_on_subdirs(Associators/MCAssociators
                         Calo/CaloInterfaces
                         Calo/CaloKernel
                         Calo/CaloUtils
                         Event/DAQEvent
                         Event/L0Event
                         Event/LinkerEvent
                         Event/PhysEvent
                         Event/TrackEvent
                         Kernel/Relations
                         Tr/TrackInterfaces)

find_package(AIDA)
find_package(Boost)
find_package(GSL)
find_package(ROOT)

include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(CaloMoniDst
                 src/*.cpp
                 INCLUDE_DIRS ROOT AIDA Boost GSL AIDA Associators/MCAssociators Calo/CaloKernel Tr/TrackInterfaces
                 LINK_LIBRARIES ROOT Boost GSL AIDA CaloUtils DAQEventLib L0Event LinkerEvent PhysEvent TrackEvent RelationsLib)

gaudi_install_python_modules()

gaudi_env(SET CALOMONIDSTOPTS \${CALOMONIDSTROOT}/options)

