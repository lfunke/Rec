/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CHECKCALOHYPOREF_H
#define CHECKCALOHYPOREF_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "CaloInterfaces/ICounterLevel.h"

/** @class CheckCaloHypoRef CheckCaloHypoRef.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2012-05-14
 */
class CheckCaloHypoRef : public GaudiAlgorithm {
public:
  /// Standard constructor
  CheckCaloHypoRef( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override;    ///< Algorithm execution

private:
  Gaudi::Property<std::vector<std::string>> m_inputs {this, "CaloHypos"};
  ICounterLevel* counterStat = nullptr;
};
#endif // CHECKCALOHYPOREF_H
