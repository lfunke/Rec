/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: MuonPIDChecker.h,v 1.10 2010-03-22 02:54:25 polye Exp $
#ifndef MUONPIDCHECKER_H 
#define MUONPIDCHECKER_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"

// from STL
#include <string>

// From event packages
#include "Event/MuonPID.h"
#include "Event/Track.h"
#include "Event/MCParticle.h"
#include "Event/MCVertex.h"
#include "Event/RecHeader.h"

#include "Event/MuonCoord.h"
#include "Event/MuonDigit.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonBasicGeometry.h"



/** @class MuonPIDChecker MuonPIDChecker.h
 *  Monitor MuonPID multiplicities and spectrum 
 *
 *  @author Erica Polycarpo Macedo
 *  @date   2010-03-20
 */
class MuonPIDChecker : public GaudiHistoAlg{
public: 
  /// Standard constructor
  using GaudiHistoAlg::GaudiHistoAlg;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;    ///< Algorithm finalization

private:
  // Properties 
  // TES path of the tracks to analyse
  Gaudi::Property<std::string> m_TracksPath 
    {this, "TrackLocation", LHCb::TrackLocation::Default, "Source of track to ID"};

  // TES path to MuonPIDs
  Gaudi::Property<std::string> m_MuonPIDsPath 
    {this, "MuonIDLocation", LHCb::MuonPIDLocation::Default, "Source of MuonPID"};

  // TES path of the muon tracks to analyse
  Gaudi::Property<std::string> m_MuonTracksPath 
    {this, "MuonTrackLocation", LHCb::TrackLocation::Muon, "Source of MuonTracks"};

  ///< Different levels of histos
  Gaudi::Property<int> m_HistosOutput 
    {this, "HistosOutput", 3, 
    "OutputLevel for Histograms; 0=None, 1=Online, 2=OfflineExpress, 3=OfflineFull, 4=Expert"};
  
  Gaudi::Property<int> m_TrackType 
    {this, "TrackType", 0, "Look at Long,Downstream or Both types of tracks"};
  
  ///< swap between running on data and MC
  Gaudi::Property<bool> m_RunningMC 
    {this, "RunningMC", false, "Swap between real and MC data"};

  Gaudi::Property<std::vector<float>> m_monitCutValues 
    {this, "MonitorCutValues", 
    // IsMuonLoose, IsMuon, MuProb, MuProb, DLL, DLL, NShared, NShared
    {1., 1., 0.9, 0.9, 1.4, 1.4, 1., 1.}};

  // Limits for the DLL histos
  Gaudi::Property<float> m_DLLlower { this, "DLLlower", -1., "lower limit for DLL histo"};
  Gaudi::Property<float> m_DLLupper { this, "DLLupper", 6. , "upper limit for DLL histo"};
  Gaudi::Property<int>   m_DLLnbins { this, "DLLnbins", 35 , "number of bins for DLL histo"};

  //Number of cuts to be monitored
  int m_nMonitorCuts;
  // Muon Detector geometry 
  unsigned int m_NStation;     // Number of stations
  unsigned int m_NRegion;      //Number of regions

  // fill local arrays of pad sizes and region sizes
  DeMuonDetector* m_mudet = nullptr;
  // These are indexed [station]
  std::vector<std::string> m_stationNames;  //Names of the station
  std::vector<double> m_stationZ; // station position

  // Track Info
  double  m_Trp0,m_TrpT;
  int m_TrRegionM2;
  std::vector<double> m_trackX; // position of track in x(mm) in each station
  std::vector<double> m_trackY; // position of track in y(mm) in each station  

  unsigned int m_TrIsPreSel,m_TrIsMuon,m_TrIsMuonLoose,m_TrNShared;
  // MuonPID   
  double  m_TrMuonLhd,m_TrNMuonLhd;
  // MuonTrack
  double m_TrDist2, m_Trquality, m_TrCLquality, m_TrChi2, m_TrCLarrival;
  unsigned int m_Trnhitsfoi[20];

  // MC info
  double       m_TrMCp0, m_TrzDecay;
  unsigned int m_TrnLinks;
  unsigned int m_TrType;

  // methods
  StatusCode trackExtrapolate(const LHCb::Track& track);
  int findTrackRegion(const int sta);
  void resetTrInfo();
  void getMuonPIDInfo(const LHCb::Track& track, const LHCb::MuonPIDs* pMuid);
  void getMuonTrackInfo(const LHCb::Track& track, const LHCb::Tracks* muTracks);
  StatusCode getTrackInfo(const LHCb::Track& track);
  void fillPreSelPlots(int level);
  void fillIMLPlots(int level);
  void fillIMPlots(int level);
  void fillMultiplicityPlots(int level);
  void fillHitMultPlots(int level);
  double getRateError( double rate , int den);

  // Event  counters
  unsigned int m_nTr;
  unsigned int m_nTrPreSel;
  unsigned int m_nTrIsMuonLoose;
  unsigned int m_nTrIsMuon;
  
  // Run counters
  std::vector<unsigned int> m_ntotTr;

};
#endif // MUONPIDCHECKER_H
