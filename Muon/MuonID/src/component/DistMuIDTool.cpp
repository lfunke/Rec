/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "DistMuIDTool.h"
#include "TrackKernel/TrackTraj.h"
#include "GaudiAlg/FunctionalDetails.h"
#include "range/v3/view/chunk.hpp"


//-----------------------------------------------------------------------------
// Implementation file for class : DistMuIDTool
//
// 2009-03-12 : Jose Angel Hernando Morata
//              Xabier Cid Vidal
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( DistMuIDTool )


//=============================================================================

StatusCode DistMuIDTool::muonQuality(LHCb::Track& muTrack, double& Quality)
{

  StatusCode sc;
  if (muTrack.states().size()>1) warning()<<"MUTRACK WITH MORE THAN ONE SEED STATE ON IT"<<endmsg;
  else if (muTrack.states().empty())
  {
    sc = StatusCode{301};
    Quality=0;
    return Error("MUTRACK WITHOUT ANY SEED STATE ON IT",sc);
  }

  if (muTrack.lhcbIDs().empty())
  {
    sc = StatusCode{302};
    Quality=0;
    return Error("NO LHCbIDs ON TRACK. IMPOSSIBLE TO CALCULATE QUALITY",sc);
  }

  double p = muTrack.ancestors().at(0).target()->p();
  std::bitset<4> sts_used{0xf};// { true,true,true,true };
  if (m_applyIsmuonHits){
    if(p>m_PreSelMomentum && p<m_MomentumCuts[0]){
      //M2 &&M3 && M4
      sts_used[3] = false;
    }
  }
  if (msgLevel(MSG::DEBUG) ) debug()<< "applyIsMuonHits"<< m_applyIsmuonHits
                                    <<", sts_used="<<sts_used.to_string()<<endmsg;

  sc=computeDistance(muTrack,Quality,sts_used);
  if (sc.isFailure()) Quality=0;
  return sc;
}


StatusCode DistMuIDTool::computeDistance(const LHCb::Track& muTrack, double& dist, std::bitset<4> sts_used)
{

  StatusCode sc;
  if (msgLevel(MSG::DEBUG) ) debug()<<"z_stations="<<m_zstations<<endmsg;

  sc=makeStates(muTrack);
  if (sc.isFailure()) {
    sc = StatusCode{303};
    return sc;
  }

  if (msgLevel(MSG::DEBUG) ) debug()<<"states ready"<<endmsg;
  //initialize distance^2
  double dist2=0.0;
  unsigned int nsts=0;
  //for each lhcbid in new track, add up distance^2
  if (msgLevel(MSG::DEBUG) ) debug()<<"for each lhcbid in new track, add up distance^2"<<endmsg;
  auto traj = LHCb::TrackTraj{muTrack};
  auto ids = muTrack.lhcbIDs();
  auto pivot = std::remove_if( ids.begin(), ids.end(), [](const auto& id) { return !id.isMuon(); } );
  ids.erase(pivot,ids.end());
  std::vector<LHCb::Measurement*> tmp; tmp.reserve(2*ids.size());
  m_measProvider->addToMeasurements( ids , tmp, traj);
  for (auto&& [id, meas ] : Gaudi::Functional::details::zip::range( ids, tmp | ranges::view::chunk(2) )) {
    //find correspondent measpair given lhcbid
    assert( meas.size() == 2 );
    MeasPair mym;
    mym.first.reset(   meas[0] );
    mym.second.reset(  meas[1] );
    if (msgLevel(MSG::DEBUG) ) {
      debug()<<"mym.first="<<mym.first.get()<<endmsg;
      debug()<<"mym.second="<<mym.second.get()<<endmsg;
    }

    int ist=id.muonID().station();
    if (!sts_used[ist-1]) continue;

    if (msgLevel(MSG::DEBUG) ) debug()<<"Number of station is="<<ist<<endmsg;
    //calculate geom distances and pads
    double dist_parx2=std::pow(m_muonProvider->distx(mym, m_states[ist]),2);
    double dist_pary2=std::pow(m_muonProvider->disty(mym, m_states[ist]),2);
    if (msgLevel(MSG::DEBUG) ) {
      debug()<<"dist_parx2="<<dist_parx2<<endmsg;
      debug()<<"dist_pary2="<<dist_pary2<<endmsg;
    }
    double padx2=12*std::pow(mym.first->errMeasure(),2);
    double pady2=12*std::pow(mym.second->errMeasure(),2);
    if (msgLevel(MSG::DEBUG) ) {
      debug()<<"padx2="<<padx2<<endmsg;
      debug()<<"pady2="<<pady2<<endmsg;
    }
    //add x and y contributions
    dist2+=dist_parx2/padx2;
    dist2+=dist_pary2/pady2;
    if (msgLevel(MSG::DEBUG) ) debug()<<"dist2="<<dist2<<endmsg;
    ++nsts;
  }

  //return distance divided by number of stations added
  dist = ( nsts>0 ? dist2/nsts : 0. );

  return StatusCode::SUCCESS;
}

//=============================================================================
