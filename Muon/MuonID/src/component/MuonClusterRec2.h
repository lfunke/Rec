/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef COMPONENT_MUONCLUSTERREC2_H
#define COMPONENT_MUONCLUSTERREC2_H 1

#include <array>
#include <tuple>
#include <vector>

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "MuonInterfaces/IMuonClusterRec2.h"            // Interface
#include "Kernel/MuonTileID.h"


/** @class MuonClusterRec2 MuonClusterRec2.h component/MuonClusterRec2.h
 *
 *  clustering tool for the muon detector
 *  @author Florian Lemaitre, Marco Santimaria, Manuel Schiller
 *  @date   2017-05-04
 */
class MuonClusterRec2 : public extends<GaudiTool, IMuonClusterRec2> {
public:
  /// Standard constructor
  using extends<GaudiTool, IMuonClusterRec2>::extends;

  ClusterVars clusters(LHCb::span<const MuonLogPad> pads) const override;

private:

  typedef std::array<LHCb::MuonTileID*, 5> Neighbourhood;
  typedef std::vector<LHCb::MuonTileID> Tiles;
  //typedef std::vector<Neighbourhood::iterator> TilesEnd;
  typedef std::vector<std::ptrdiff_t> TilesEnd;
  typedef std::vector<std::pair<Neighbourhood,std::ptrdiff_t>> Neigh;
  typedef std::tuple<Tiles, Neigh> ClusteringContext;

  ClusteringContext setupClustering(LHCb::span<const MuonLogPad> pads) const;
  void doClustering(ClusteringContext& ctx) const;

};
#endif // COMPONENT_MUONCLUSTERREC2_H
