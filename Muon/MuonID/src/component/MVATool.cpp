/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** Implementation of MVATool
 *
 * 2017-05-29: Ricardo Vazquez Gomez
 */
#include "MVATool.h"
#define SQRT3 1.7320508075688772 
#define INVSQRT3 0.5773502691896258
#define MSFACTOR 5.552176750308537 

DECLARE_COMPONENT(MVATool)

// Some utility things. Is there a better place to put them?
template<class T>
const inline T squared(const T& a) {
  return a*a;
}

template <size_t N, class T>
std::array<T,N> make_array(const T &v) {
  std::array<T,N> ret;
  ret.fill(v);
  return ret;
}

/*
 * Contructor
 * Also declares an interface to that the tool can be obtained in Gaudi via
 * tool<...>(...)
 */
MVATool::MVATool(const std::string& type, const std::string& name, const IInterface* parent)
    : base_class(type, name, parent){
  declareInterface<IMVATool>(this);
}

/*
 * Initialises the tool.
 */
auto MVATool::initialize() -> StatusCode {
  auto sc = GaudiTool::initialize();
  if (sc.isFailure()) {
    return sc;
  }

  muonTool_ = tool<ICommonMuonTool>("CommonMuonTool");
  // get first used station
  firstUsedStation = muonTool_->getFirstUsedStation();

  // Load station information from detector description
  m_det = getDet<DeMuonDetector>(DeMuonLocation::Default);
  nStations = m_det->stations();
  m_stationZ.reserve(nStations);
  for (unsigned s = 0; s != nStations; ++s) {
    m_stationZ.push_back(m_det->getStationZ(s));
  }

  if (m_runCatBoostBatch) {
      if (m_useStandAloneCatboost) {
	  error() << "UseCatboostStandaloneEvaluator can't be specfied " \
		  << "together with RunCatBoostBatch. Which of them you want to run?" \
		  << endmsg;
	  return StatusCode::FAILURE;
      }
      m_rawCatBoostModel = CalcerHolderType(ModelCalcerCreate(), ModelCalcerDelete);
      if (!LoadFullModelFromFile(m_rawCatBoostModel.get(), m_CatBoostModelPath.value().c_str())) {
	  error() << "Failed to load CatBoost model. Likely cases are " \
		  << "a malformed or missing file. Model file: " << m_CatBoostModelPath \
		  << std::endl;
	  error() << "Message from Catboost:"<< std::endl;
	  error() << GetErrorString();
	  error() << endmsg;
	  return StatusCode::FAILURE;
      }
  } else {
      try {
	  if (m_useStandAloneCatboost) {
	      m_StandAloneCatBoostModel = \
		  std::make_unique<NCatboostStandalone::TOwningEvaluator>(m_CatBoostModelPath);
	  } else {
	      m_CatBoostModel = ModelCalcerWrapper(m_CatBoostModelPath);
	  }
      } catch (const std::runtime_error& e) {
	  error() << "Failed to load CatBoost model. Likely cases are " \
		  << "a malformed or missing file. Model file: " << m_CatBoostModelPath \
		  << std::endl;
	  error() << "Message from Catboost:"<< std::endl;
	  error() << e.what();
	  error() << endmsg;
	  return StatusCode::FAILURE;
      }
  }
  if (m_runTMVA) {
    // not a const vector due to TMVA
    std::vector<std::string> inputVars = {
      "DTm2","DTm3","DTm4","DTm5","T1m2","T1m3","T1m4","T1m5",
      "Cross2","Cross3","Cross4","Cross5",
      "RXdm2_ms","RXdm3_ms","RXdm4_ms","RXdm5_ms",
      "RYdm2_ms","RYdm3_ms","RYdm4_ms","RYdm5_ms"};
    m_TMVA_BDT = std::make_unique<
      ReadBDTD_mva_pion_300_5_25_05_2_masscut_withPreweight_NoNshared_NewMS>(inputVars);
  }
  return sc;
}

void MVATool::compute_features(const LHCb::Track& muTrack, const CommonConstMuonHits &hits,
			       LHCb::span<float> result) const noexcept {
  // result must be preallocated with nFeatures

  // linear extrapolation without using extrapolators
  const auto extrapolation = muonTool_->extrapolateTrack(muTrack); 

  // common factor for the multiple scattering error. Taken from formula.
  // commonFactor = 13.6/muTrack.p()/sqrt(6.);
  float commonFactor = MSFACTOR/muTrack.p();

  // Get closests hits to track
  std::array<const CommonMuonHit*, used_stations> closestHits = 
    make_array<used_stations>(static_cast<const CommonMuonHit*>(nullptr));
  std::array<float, used_stations> minDist = 
    make_array<used_stations>(std::numeric_limits<float>::infinity());
  for (const CommonMuonHit* ih: hits) {
    const LHCb::LHCbID id = ih->tile();
    const unsigned s = id.muonID().station();
    if (s < firstUsedStation) {
      continue;
    }
    const float distSeedHit = squared(ih->x() - extrapolation[s].first) +
      squared(ih->y() - extrapolation[s].second);
    const unsigned stationFromFirstUsed = s - firstUsedStation;
    if (distSeedHit < minDist[stationFromFirstUsed]) {
      minDist[stationFromFirstUsed] = distSeedHit;
      closestHits[stationFromFirstUsed] = ih;
    }
  }

  for (size_t stationFromFirstUsed = 0;
       stationFromFirstUsed != used_stations;
       ++stationFromFirstUsed) {
    const size_t st = stationFromFirstUsed + firstUsedStation;
    const CommonMuonHit* matchedHit = closestHits[stationFromFirstUsed];
    if (! matchedHit) {
      for (size_t i = 0; i < n_features_per_station; ++i) {
	result[i * used_stations + stationFromFirstUsed] = default_features[i];
      }
    } else {
      result[time_index + stationFromFirstUsed] = matchedHit->time();
      result[dt_index + stationFromFirstUsed] = matchedHit->deltaTime();
      if (matchedHit->uncrossed()) {
	result[crossed_index + stationFromFirstUsed] = matchedHit->uncrossed();
      } else {
	result[crossed_index + stationFromFirstUsed] = 2.;
      }
        
      // compute the Error due to the RMS taking into account the whole traversed material. Common error for X and Y.
      const float travDist = sqrt(squared(m_stationZ[st]-m_stationZ[0]) +
				  squared(extrapolation[st].first-extrapolation[0].first) +
				  squared(extrapolation[st].second-extrapolation[0].second));
      // errMS = commonFactor*travDist*sqrt(travDist/17.58);
      const float errMS = commonFactor*travDist*sqrt(travDist)*0.23850119787527452; 
      if (std::abs(extrapolation[st].first-matchedHit->x()) != 2000) {
	result[resX_index + stationFromFirstUsed] =
	  (extrapolation[st].first - matchedHit->x())/sqrt(squared(matchedHit->dx()*INVSQRT3)+errMS*errMS); // multiply instead of divide 
      } else {
	result[resX_index + stationFromFirstUsed] = defaultRes;
      }
      if (std::abs(extrapolation[st].second-matchedHit->y()) != 2000) {
	result[resY_index + stationFromFirstUsed] =
	  (extrapolation[st].second - matchedHit->y())/sqrt(squared(matchedHit->dy()*INVSQRT3)+errMS*errMS); // mutiply instead of divide
      } else {
	result[resY_index + stationFromFirstUsed] = defaultRes;
      }
    }
  }
  if (msgLevel(MSG::DEBUG)) {
    debug() << "Input vars to the BDT: ";
    for (auto feature = result.begin(); feature != result.end() - 1; ++feature) {
      debug() << *feature << ", ";
    }
    debug() << result[nFeatures - 1] << endmsg;
  }
}

MVATool::MVAResponses MVATool::calcBDT(const LHCb::Track& muTrack,
				       const CommonConstMuonHits &hits) const noexcept {
  // Computes TMVA and catboost values, catboost evaluator is choosen
  // according to RunCatBoostBatch and UseCatboostStandaloneEvaluator flags
  std::vector<float> Input(nFeatures);
  LHCb::span<float> InputSpan(Input.data(), Input.data() + nFeatures);
  compute_features(muTrack, hits, InputSpan);

  MVAResponses predictions;
  if (m_runCatBoostBatch) {
    // batch of a single feature
    predictions.CatBoost = calcBDTBatch(Input).front();
  } else if (m_useStandAloneCatboost) {
      predictions.CatBoost =
	  m_StandAloneCatBoostModel->Apply(Input,
					   NCatboostStandalone::EPredictionType::RawValue);
  } else {
    predictions.CatBoost = m_CatBoostModel.CalcFlat(Input);
  }

  if (m_runTMVA) {
    predictions.TMVA = calcTMVAonFeatures(InputSpan);
  }

  if (msgLevel(MSG::DEBUG)) {
    debug() << "CatBoost = " << predictions.CatBoost << endmsg;
  }
  return predictions;
}

double MVATool::calcTMVAonFeatures(LHCb::span<float> features) const noexcept {
  // Computes only the TMVA value
  // Features correspond to single example
  const std::vector<double> InputDouble(features.begin(), features.end());  
  return m_TMVA_BDT->GetMvaValue(InputDouble);
}

std::vector<double> MVATool::calcBDTBatch(std::vector<float> features) const noexcept {
  // features is a nice continuous vector where features for each track
  // lie consecutively
  const size_t nTracks = features.size() / nFeatures;
  std::vector<const float*> featuresPtrs(nTracks);
  for (size_t track_index = 0; track_index < nTracks; ++track_index) {
    featuresPtrs[track_index] = features.data() + track_index*nFeatures;
  }
  std::vector<double> result(nTracks);
  CalcModelPredictionFlat(m_rawCatBoostModel.get(),
			  nTracks,
			  featuresPtrs.data(),
			  nFeatures,
			  result.data(),
			  result.size());
  // Matches the printout of the calcBDT
  if (msgLevel(MSG::DEBUG)) {
    for (double score: result) {
      debug() << "CatBoost = " << score << endmsg;
    }
  }
  return result;
}
