/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef COMPONENT_MUONIDPLUSALG_H 
#define COMPONENT_MUONIDPLUSALG_H 1

#include "GaudiAlg/GaudiAlgorithm.h"
#include "Event/Track.h"
#include "Event/MuonPID.h"

struct IMuonIDTool;

/** @class MuonIDPlusAlg MuonIDPlusAlg.h component/MuonIDPlusAlg.h
 *  
 *  novel MuonID algorithm based on the MuonIDPlusTool
 *
 *  @author Giacomo Graziani
 *  @date   2015-11-18
 */
class MuonIDPlusAlg : public GaudiAlgorithm {
public: 
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution

private:
  IMuonIDTool* m_muonIDtool = nullptr;
  
  Gaudi::Property<std::string> m_BestTrackLocation 
    {this, "TrackLocation", LHCb::TrackLocation::Default, 
    "input track location"};

  Gaudi::Property<std::string> m_MuonPIDsPath 
    {this, "MuonIDLocation", LHCb::MuonPIDLocation::Default, 
    "output MuonPID location"};

  Gaudi::Property<std::string> m_MuonTracksPath 
    {this, "MuonTrackLocation", LHCb::TrackLocation::Muon, 
    "output MuonTracks location"};
};
#endif // COMPONENT_MUONIDPLUSALG_H
