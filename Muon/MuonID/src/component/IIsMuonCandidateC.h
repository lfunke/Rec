/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef IISMUONCANDIDATEC_H 
#define IISMUONCANDIDATEC_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"
#include "Event/Track.h"


/** @class IIsMuonCandidateC IIsMuonCandidateC.h
 *  
 *
 *  @author Jose Angel Hernando Morata
 *  @author Xabier Cid Vidal
 *  @date   2009-03-11
 */
struct IIsMuonCandidateC : extend_interfaces<IAlgTool>{

  // Return the interface ID
  DeclareInterfaceID( IIsMuonCandidateC, 2, 0 );
  
  virtual bool IsMuonCandidate(const LHCb::Track& muTrack)=0;
  virtual bool IsMuonCandidate(const std::vector<LHCb::LHCbID>& LHCbIDs,const double& mom)=0;

};
#endif // IISMUONCANDIDATEC_H
