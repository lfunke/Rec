/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef DICT_MUONIDDICT_H 
#define DICT_MUONIDDICT_H 1

// #include "MuonID/ICLTool.h"
// #include "MuonID/ImuIDTool.h"
// #include "MuonID/IIsMuonCandidateC.h"
// #include "MuonID/SmartMuonMeasProvider.h"
// #include "MuonID/IGetArrival.h"

//#include "../src/ICLTool.h"
#include "MuonID/IMuonIDTool.h"
//#include "../src/IIsMuonCandidateC.h"
//#include "../src/SmartMuonMeasProvider.h"
//#include "../src/IGetArrival.h"

//namespace 
//{
//  struct _Instantiations 
//  {
//    std::pair<LHCb::Measurement*,LHCb::Measurement*> _measpair_instance;
//    std::vector< std::pair< LHCb::Measurement*, LHCb::Measurement*> > _measpairlist_instance;
//  };

//}

#endif // DICT_MUONIDDICT_H

