/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONMATCH_VELOUT_H
#define MUONMATCH_VELOUT_H 1

// STL
#include <vector>
#include <string>
#include <tuple>

// from Gaudi
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiAlg/ISequencerTimerTool.h"
#include "GaudiAlg/Transformer.h"

// from LHCb
#include "Kernel/ILHCbMagnetSvc.h"

// from MuonID
#include "MuonID/MuonHitHandler.h"
#include "MuonID/CommonMuonHit.h"

// from MuonMatch
#include "MuonMatch/Definitions.h"
#include "MuonMatch/HitMatching.h"

class MuonMatchVeloUT;
namespace {
  using namespace MuonMatch;
  using WindowBounds = std::tuple<double, double, double, double>;
  using getHitValErr = std::function<std::tuple<double, double> ( const Hit& )>;
}

/** @class MuonMatchVeloUT MuonMatchVeloUT.h
 *
 *  @author Miguel Ramos Pernas, Roel Aaij, Vasileios Syropoulos
 *  @date   2010-12-02
 *
 *  @author Miguel Ramos Pernas
 *  @date   2017-10-05
 */

class MuonMatchVeloUT : public Gaudi::Functional::Transformer<Tracks(const Tracks&, const MuonHitHandler&)> {

public:

  MuonMatchVeloUT( const std::string& name, ISvcLocator *pSvcLocator );

  /// Initialize the instance
  StatusCode initialize() override {

    // Initialize transformer
    auto sc = Transformer::initialize();
    if ( sc.isFailure() ) return sc;

    // Initialize magnetic field service
    m_fieldSvc = svc<ILHCbMagnetSvc>("MagneticFieldSvc", true);

    return StatusCode::SUCCESS;
  }

  /// Match the given tracks to the seed
  std::optional<Track> match( const Track& seed, const MuonHitHandler& hit_handler ) const;

  /// Gaudi-functional method
  Tracks operator() ( const Tracks& seeds, const MuonHitHandler& hit_handler ) const override {

    m_seedCount += seeds.size();

    Tracks matched;

    for ( const auto& seed : seeds ) {

      auto t = this->match(seed, hit_handler);

      if ( t )
	matched.emplace_back(*t);
    }

    m_matchCount += matched.size();

    return matched;
  }

private:

  /// Magnetic field service
  ILHCbMagnetSvc* m_fieldSvc = 0;

  /// Counter for the number of input seeds
  mutable Gaudi::Accumulators::AveragingCounter<> m_seedCount{this, "#seeds"};

  /// Counter for the matched tracks
  mutable Gaudi::Accumulators::AveragingCounter<> m_matchCount{this, "#matched"};

  Gaudi::Property<double> m_kickOffset  {this, "KickOffset", 175*Gaudi::Units::MeV};
  Gaudi::Property<double> m_kickScale   {this, "KickScale",  1255*Gaudi::Units::MeV};
  Gaudi::Property<bool>   m_setQOverP   {this, "SetQOverP", false};

  // Properties for Velo seeds
  Gaudi::Property<double> m_za {this, "MagnetPlaneParA", +5.412*Gaudi::Units::m};
  Gaudi::Property<double> m_zb {this, "MagnetPlaneParB", -3.567*Gaudi::Units::m};
  Gaudi::Property<double> m_ca {this, "MagnetCorrParA",  +25.17*Gaudi::Units::mm};
  Gaudi::Property<double> m_cb {this, "MagnetCorrParB",  -701.5*Gaudi::Units::mm};

  Gaudi::Property<double> m_maxPt {this, "MaxPt", std::numeric_limits<double>::max()};

  Gaudi::Property<std::array<std::pair<double,double>,4>> m_window {this,
      "Window", {
      std::pair<double, double>{100*Gaudi::Units::mm, 200*Gaudi::Units::mm},
      std::pair<double, double>{200*Gaudi::Units::mm, 400*Gaudi::Units::mm},
      std::pair<double, double>{300*Gaudi::Units::mm, 500*Gaudi::Units::mm},
      std::pair<double, double>{400*Gaudi::Units::mm, 600*Gaudi::Units::mm}
    }
  };

  // General properties
  Gaudi::Property<double> m_maxChi2DoFX {this, "MaxChi2DoFX", 20};
  Gaudi::Property<bool> m_fitY {this, "FitY", false};

  /** Corrections for errors.
   *   - Corrections on the error are applied by multiplying 2 * d{x,y} with the
   *     correction for M2-M5,
   *   - mx and my are coefficients of a polynomial in 1/p for the magnet hit,
   *     that gives the error on the hit in the magnet.
   *  x, y, mx and my must be present. x and y must contain 4 values, mx and my three.
   */

  Gaudi::Property<std::array<double,3>> m_errCorrect_mx {this, "ErrorCorrectionsMX", {1., 0., 0.}};
  Gaudi::Property<std::array<double,3>> m_errCorrect_my {this, "ErrorCorrectionsMY", {1., 0., 0.}};
  Gaudi::Property<std::array<double,4>> m_errCorrect_x {this, "ErrorCorrectionsX", {1., 1., 1., 1.}};
  Gaudi::Property<std::array<double,4>> m_errCorrect_y {this, "ErrorCorrectionsY", {1., 1., 1., 1.}};

  /// Get the first estimation of the magnet focal plane position from "tx2".
  inline MagnetPositionXZ magnetFocalPlaneFromTx2( const LHCb::State* state ) const {

    const double z = m_za + m_zb*state->tx()*state->tx();

    return MagnetPositionXZ{xStraight(state, z), z};
  }

  /// Add hits to the given candidate and return whether it can be fitted
  inline std::vector<MuonChamber> afterKickMuChs( const TrackType& tt ) const
  {
    // When modifying these lines, please also consider modifying the method
    // "firstMuCh" accordingly.
    switch ( tt )
      {
      case LowP:
	return {M2};

      case MediumP:
	return {M3, M2};

      case HighP:
	return {M4, M3, M2};

      default:
	return {};
      }
  }

  /** Get the corrected magnet focal plane position of a given candidate
   *
   * Momentum-dependent correction of magnet.z as a function of p in GeV, and
   * magnet.x as a function of magnet.z.
   */
  inline MagnetPositionXZ corrMagnetFocalPlane( const LHCb::State* state, const Hit& hit, const MagnetPositionXZ& magnet ) const
  {
    // Initial slope and momentum estimate
    const auto slope = (hit.x - magnet.x)/(hit.z - magnet.z);
    const auto mom   = this->momentum(slope - state->tx());
    const auto zm    = magnet.z + m_ca + m_cb*Gaudi::Units::GeV/mom;

    return MagnetPositionXZ{xStraight(state, zm), zm};
  }

  /// Calculate the slope using the minimum momentum
  inline double dtx( const Track& track ) const
  {
    return m_kickScale/(track.p() - m_kickOffset);
  }

  /// Add hits to the given candidate and return whether it can be fitted
  std::optional<Hit> findHit( const MuonChamber& much, const LHCb::State* state, const Hit& magnet_hit, const MuonHitHandler& hit_handler, const double slope ) const;

  /// First muon station for a given track type.
  inline std::vector<MuonChamber> firstMuCh( const TrackType& tt ) const
  {
    switch ( tt )
      {
      case LowP:
	return {M3};

      case MediumP:
	return {M5, M4};

      case HighP:
	return {M5};

      default:
	return {};
      }
  }

  /** Calculate the window to search in the first station.
   *  Returns "xmin", "xmax", "ymin", "ymax".
   */
  WindowBounds firstStationWindow( const Track& track,
				   const LHCb::State* state,
				   const CommonMuonStation& station,
				   const MagnetPositionXZ &magnet ) const;

  /** Calculate straight line fit using linear-least-squares
   *  Code inspired by numerical recipes, section 15.2
   */
  FitResult fit( const getHitValErr hitValErr, const Hit &magnetHit, const Hits &hits ) const;

  /// Fit the given candidate
  GlobalFitResult fitHits( const Hits& hits, const Hit& magnetHit, const LHCb::State* state ) const;

  /// Create a hit from the given CommonMuonHit, applying the necessary hits
  inline Hit makeMuonHit( const CommonMuonHit& hit ) const {

    return Hit{
      hit.x(),
	2.*hit.dx()*m_errCorrect_x[hit.station()],
	hit.y(),
	2.*hit.dy()*m_errCorrect_y[hit.station()],
	hit.z()};
  };

  /// Get the momentum given the slope in "x"
  inline double momentum( double dtx ) const
  {
    return m_kickScale/std::fabs(dtx) + m_kickOffset;
  }

  /** Calculate the window to search in the given station.
   *  Not to be applied in the first station.
   *  Returns "xmin", "xmax", "ymin", "ymax".
   */
  WindowBounds stationWindow( const LHCb::State* state,
			      const CommonMuonStation& station,
			      const MagnetPositionXZ &magnet,
			      const double slope ) const;

  /** Calculate the maximum tangent value.
   *  Use sum rule for tan and approximate "tan(s)" with "s" to calculate
   *  window in x.
   */
  inline double tanMax( const Track& track, const LHCb::State* state ) const
  {
    const double d = this->dtx(track);
    const double t = state->tx();

    return (t + d)/(1 - t*d);
  }

  /** Calculate the minimum tangent value.
   *  Use sum rule for tan and approximate "tan(s)" with "s" to calculate
   *  window in x.
   */
  inline double tanMin( const Track& track, const LHCb::State* state ) const
  {
    const double d = this->dtx(track);
    const double t = state->tx();

    return (t - d)/(1 + t*d);
  }

};

#endif // MUONMATCH_VELOUT_H
