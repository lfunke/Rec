/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#ifndef MUONMATCH_HITMATCHING_H
#define MUONMATCH_HITMATCHING_H 1

// STL
#include <vector>
#include <algorithm>

// LHCb
#include "Event/Track.h"
#include "Event/State.h"

// from MuonID
#include "MuonID/CommonMuonHit.h"

// from MuonMatch
#include "MuonMatch/Definitions.h"


namespace MuonMatch {

  /** @class GlobalFitResult HitMatching.h
   *  MuonMatch global fit result.
   *
   *  @author Miguel Ramos Pernas
   *  @date   2017-10-05
   */

  struct GlobalFitResult final
  {

    /// Slope
    double slope;

    /// Momentum
    double p;

    /// Chi2 per degrees of freedom
    double chi2ndof;
  };

  /** @class FitResult HitMatching.h
   *  MuonMatch fit result.
   *
   *  @author Miguel Ramos Pernas
   *  @date   2017-10-05
   */

  struct FitResult final
  {

    /// a0
    double a0;

    /// Slope
    double slope;

    /// Chi2
    double chi2;

    /// nDoF
    size_t ndof;
  };

  // Define the policy to create a container of hits for matching. The first
  // hit is the magnet hit, while the second is the first hit matched in
  // the sequence.

  /// Build a vector of hits to be matched. The magnet hit is placed first.
  inline Hits matchingHits( const Hit& magnet_hit, const Hit& first_hit ) {

    Hits hits{magnet_hit, first_hit};
    hits.reserve(5);

    return hits;
  }

  namespace hit_iteration {

    /// Iterator including the magnet hit
    inline auto cbegin( const Hits& hits ) { return std::cbegin(hits); }

    /// Iterator skipping the first hit (magnet hit)
    inline auto cbegin_skip_magnet_hit( const Hits& hits ) { return std::next(std::cbegin(hits)); }

    /// End iterator
    inline auto cend( const Hits& hits ) { return std::cend(hits); }
  }

}

#endif // MUONMATCH_HITMATCHING_H
