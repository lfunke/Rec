###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
 =============================================================
 Class to configure MuonMatch tools and algos. To import do
 'from MuonMatch import ConfiguredMuonMatch'
 =============================================================
"""

__author__ = 'Miguel Ramos Pernas'
__email__  = 'miguel.ramos.pernas@cern.ch'


from Gaudi.Configuration import *
from Configurables import GaudiSequencer, MuonMatchVeloUT
from GaudiKernel import SystemOfUnits as Units


class ConfiguredMuonMatchSequence:

    def __init__( self, ):
        """
        Initialization of the class
        """
        log.debug("# INITIALIZING")

    def configureMatchVeloUT( self, tool ):
        """
        Configure the MatchVeloUT tool
        """
        log.debug("# CONFIGURING MATCHVELOUT TOOL")

    def getSeq( self ):
        """
        Return the sequences for each muon-matching tool
        """
        log.debug("# APPLYING GENERAL MUONMATCH CONFIGURATION")
        seq = GaudiSequencer("MuonMatchSeq")

        veloutmatch = MuonMatchVeloUT()
        self.configureMatchVeloUT(veloutmatch)
        seq.Members.append(veloutmatch)

        return seq
