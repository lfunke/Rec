/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONFASTHWTOOL_H
#define MUONFASTHWTOOL_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Point3DTypes.h"

// local
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonBasicGeometry.h"
#include "MuonInterfaces/IMuonFastHWTool.h"
#include "Kernel/MuonLayout.h"
#include "Kernel/MuonTileID.h"

/** @class MuonFastHWTool MuonFastHWTool.h
 *
 *  Convert an MuonTileID into an xyz position in the detector (with size)
 *  No abstract interface as I do not want to make more than one of these...
 *
 *  @author David Hutchcroft
 *  @date   07/03/2002
 */
class MuonFastHWTool : public GaudiTool,
                    virtual public IMuonFastHWTool {
public:
  /// Standard constructor
  MuonFastHWTool( const std::string& type,
                 const std::string& name,
                 const IInterface* parent);

  /** Calculate the x,y,z and dx,dy,dz of a MuonTileID in mm
   * this ignores gaps: these can never be read out independently
   */
  StatusCode GetHWName(LHCb::MuonTileID tile,int& ODENum,int &SyncOne,int &SynchTwo) const override;
  StatusCode GetI2C(LHCb::MuonTileID tile,int& I2C) const override;
  StatusCode GetFEBInCh(LHCb::MuonTileID tile,int& FEB) const override;
  StatusCode GetFEBInI2C(LHCb::MuonTileID tile,int& FEB) const override;

  StatusCode initialize() override;
  void initFEB();
  LHCb::MuonTileID transformTile(int quarter,int chX,int chY,LHCb::MuonTileID tile) const override;

  int getRealLayoutNumber(int station,int region) const override
  { return m_realLayout[station*4+region]; }

  int getI2CNumber(int station,int region,int lay) const override
  { return m_daisy[station*4+region][lay]; }

  int getFEBInI2CNumber(int station,int region,int lay,int daisy) const override
  { return m_cardiac[station*4+region][lay][daisy]; }

  int getChamberGridX(int region) const override
  { return m_chamberX[region]; }

  int getChamberGridY(int region) const override
  { return m_chamberY[region]; }

  MuonLayout getLayoutInChamber(int station, int region,int lay) const override
  { return m_padInChLayout[2*(station*4+region)+lay]; }

  void fillTileParameter(LHCb::MuonTileID tile,int lay,int i2c,int feb);

  const std::vector<LHCb::MuonTileID>& getListOfTile(int station,int region,int lay,int I2C,int feb)const override
  { return m_padInCh[station*4+region][lay][I2C][feb]; }

private:
  int getPadChIndex(LHCb::MuonTileID tile) const;
  StatusCode getGlobalIndex(LHCb::MuonTileID tile,int &result,int& index,int& lay) const;


  void setI2CFeb(int xmin, int xmax, int ymin, int ymax,int offsetX,int index,int lay,int d, int c,LHCb::MuonTileID  tilePart);

  int m_stationNumber = 0; // Number of stations
  int m_regionNumber = 0; // Number of regions

  const int layout[20] = {
    1, 1, 1, 1,
    2, 2, 2, 2,
    2, 2, 2, 2,
    1, 2, 2, 2,
    1, 2, 2, 2,
  };
  const int m_layoutX[2][20] = {
  //  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
    {24, 24, 24, 24, 48, 48, 48, 48, 48, 48, 48, 48, 12, 12, 12, 12, 12, 12, 12, 12},
    { 0,  0,  0,  0,  8,  4,  2,  2,  8,  4,  2,  2,  0,  4,  2,  2,  0,  4,  2,  2},
  };
  const int m_layoutY[2][20] = {
  //  0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16,17,18,19
    { 8, 8, 8, 8, 1, 2, 2, 2, 1, 2, 2, 2, 8, 2, 2, 2, 8, 2, 2, 2},
    { 0, 0, 0, 0, 8, 8, 8, 8, 8, 8, 8, 8, 0, 8, 8, 8, 0, 8, 8, 8},
  };

  const int m_realLayout[20] = { 
    1, 1, 1, 1,
    2, 2, 1, 1,
    2, 2, 1, 1,
    1, 1, 1, 1,
    1, 1, 1, 1,
  };
  const int m_chamberX[4] = {1, 1, 1, 2};
  const int m_chamberY[4] = {1, 2, 4, 8};

  const int m_daisy[20][2] = {
    {4, 0}, {4, 0}, {2, 0}, {1, 0}, // M1
    {1, 2}, {1, 2}, {2, 0}, {1, 0}, // M2
    {1, 2}, {1, 2}, {2, 0}, {1, 0}, // M3
    {2, 0}, {1, 0}, {1, 0}, {1, 0}, // M4
    {2, 0}, {1, 0}, {1, 0}, {1, 0}, // M5
  };
  const int m_cardiac[20][2][4] = {
    {{6,6,6,6}, {0,0,0,0}}, // M1R1
    {{4,8,4,8}, {0,0,0,0}}, // M1R2
    {{6,6,0,0}, {0,0,0,0}}, // M1R3
    {{3,0,0,0}, {0,0,0,0}}, // M1R4
    //
    {{6,0,0,0}, {4,4,0,0}}, // M2R1
    {{6,0,0,0}, {4,4,0,0}}, // M2R2
    {{6,6,0,0}, {0,0,0,0}}, // M2R3
    {{3,0,0,0}, {0,0,0,0}}, // M2R4
    //
    {{6,0,0,0}, {4,4,0,0}}, // M3R1
    {{6,0,0,0}, {4,4,0,0}}, // M3R2
    {{6,6,0,0}, {0,0,0,0}}, // M3R3
    {{3,0,0,0}, {0,0,0,0}}, // M3R4
    //
    {{6,6,0,0}, {0,0,0,0}}, // M4R1
    {{6,0,0,0}, {0,0,0,0}}, // M4R2
    {{6,0,0,0}, {0,0,0,0}}, // M4R3
    {{3,0,0,0}, {0,0,0,0}}, // M4R4
    //
    {{6,6,0,0}, {0,0,0,0}}, // M5R1
    {{6,0,0,0}, {0,0,0,0}}, // M5R2
    {{6,0,0,0}, {0,0,0,0}}, // M5R3
    {{3,0,0,0}, {0,0,0,0}}, // M5R4
  };

  std::vector<MuonLayout> m_padInChLayout;
  std::vector<LHCb::MuonTileID> m_padInCh[20][2][4][8];

  std::vector<int> m_I2C[20][2];
  std::vector<int> m_FEB[20][2];
  std::vector<int> m_ODENum[80][2];
  std::vector<int> m_synchNum[80][2];

  DeMuonDetector* m_muonDetector = nullptr;
};
#endif // MUONFASTHWTOOL_H
