/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONREAD_H
#define MUONREAD_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h" //add sim 01.03.07
#include "MuonDet/DeMuonDetector.h"
#include "Event/MCParticle.h"
#include "MuonDAQ/IMuonRawBuffer.h"
#include "Linker/LinkedTo.h"
#include "Linker/LinkedFrom.h"
#include "Event/MuonDigit.h"

/** @class MuonRead MuonRead.h
 *
 *
 *  @author Alessia Satta
 *  @date   2006-11-13
 */
class MuonRead : public GaudiTupleAlg {
public:
  /// Standard constructor
  using GaudiTupleAlg::GaudiTupleAlg;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  void searchNature(LHCb::MuonTileID tile,LHCb::MCParticle*& pp);

  DeMuonDetector* m_muonDet = nullptr;
  IMuonRawBuffer* m_muonBuffer = nullptr;
  std::vector<LHCb::MuonTileID> m_muonDigit;

  Gaudi::Property<std::string> m_muonTracksName
    {this, "MuonTracksName", "/Event/Rec/Muon/MuonsForAlignment"};


};
#endif // MUONREAD_H
