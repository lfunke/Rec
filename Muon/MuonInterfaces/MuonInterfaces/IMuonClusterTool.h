/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONTOOLS_IMUONCLUSTERTOOL_H
#define MUONTOOLS_IMUONCLUSTERTOOL_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

/** @class IMuonClusterTool IMuonClusterTool.h MuonTools/IMuonClusterTool.h
 *
 *
 *  @author Alessia Satta
 *  @date   2010-01-15
 */
struct IMuonClusterTool : extend_interfaces<IAlgTool> {
  DeclareInterfaceID( IMuonClusterTool, 2, 0 );
  virtual StatusCode doCluster(const std::string& Input,
                               const std::string& Output) =0;

};
#endif // MUONTOOLS_IMUONCLUSTERTOOL_H
