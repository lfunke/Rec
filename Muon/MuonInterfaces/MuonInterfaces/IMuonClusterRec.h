/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONTRACKREC_IMUONCLUSTERREC_H
#define MUONTRACKREC_IMUONCLUSTERREC_H 1

// Include files
// from STL
#include <vector>
// from Gaudi
#include "GaudiKernel/IAlgTool.h"

class MuonHit;
class MuonLogPad;
class DeMuonDetector;
class IMuonFastPosTool;

/** @class IMuonClusterRec IMuonClusterRec.h MuonTrackRec/IMuonClusterRec.h
 *
 *  Interface to clustering algorithm for standalone muon reconstruction
 *  @author Giacomo GRAZIANI
 *  @date   2009-10-15
 */
struct IMuonClusterRec : extend_interfaces<IAlgTool> {

  DeclareInterfaceID( IMuonClusterRec, 4, 0);

  virtual const std::vector<MuonHit>& clusters(const std::vector<MuonLogPad>& pads,
                                                bool force=false) =0;


};
#endif // MUONTRACKREC_IMUONCLUSTERREC_H
