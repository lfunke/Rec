/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONDET_IMUONFASTHWTOOL_H
#define MUONDET_IMUONFASTHWTOOL_H 1

// Include files
#include "Kernel/MuonTileID.h"

// GaudiKernel
#include "GaudiKernel/IAlgTool.h"

/** @class IMuonHWTool IMuonHWTool.h MuonTools/IMuonHWTool.h
 *
 *  Interface for the tools to convert MuonTileID to coordinates
 *
 *  @author David Hutchcroft
 *  @date   11/03/2002
 */

struct IMuonFastHWTool : extend_interfaces<IAlgTool> {

  /** static interface identification
   *  @return unique interface identifier
   */
  DeclareInterfaceID( IMuonFastHWTool , 6 , 0 );

  /** Calculate the x,y,z and dx,dy,dz of a MuonTileID in mm
   *  this ignores gaps: these can never be read out independently
   */
  virtual StatusCode GetHWName(LHCb::MuonTileID tile,int& ODENum,int &SyncOne,int &SynchTwo) const =0;
  virtual StatusCode GetI2C(LHCb::MuonTileID tile,int& I2C) const =0;
  virtual StatusCode GetFEBInCh(LHCb::MuonTileID tile,int& FEB) const =0;
  virtual StatusCode GetFEBInI2C(LHCb::MuonTileID tile,int& FEB) const =0;
  virtual int getRealLayoutNumber(int station,int region) const =0;


  virtual int getI2CNumber(int station,int region,int layout) const =0;


  virtual int getFEBInI2CNumber(int station,int region,int layout,int daisy) const =0;


  virtual int getChamberGridX(int region) const =0;


  virtual int getChamberGridY(int region) const =0;


  virtual MuonLayout getLayoutInChamber(int station, int region,int lay) const =0;
  virtual const std::vector<LHCb::MuonTileID>& getListOfTile(int station,int region,int layout,int I2C,int feb) const =0;

  virtual LHCb::MuonTileID transformTile(int quarter,int chX,int chY,
                                        LHCb::MuonTileID tile) const =0;


};
#endif // MUONDET_IMUONFASTHWTOOL_H
