/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONNNET_IMUONTRACKREC_H
#define MUONNNET_IMUONTRACKREC_H 1

// Include files
// from STL
#include <string>
#include <map>
#include <vector>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

class MuonHit;
class MuonTrack;
class MuonNeuron;


/** @class IMuonTrackRec IMuonTrackRec.h MuonTrackrec/IMuonTrackRec.h
 *
 *
 *  @author Giovanni Passaleva / Giacomo Graziani
 *  @date   2008-04-11
 */
struct IMuonTrackRec : extend_interfaces<IAlgTool> {

  // Return the interface ID
  DeclareInterfaceID( IMuonTrackRec, 2, 0 );

  virtual const std::vector<MuonHit>* trackhits() const =0;
  virtual const std::vector<MuonTrack>& tracks() const  =0;
  virtual const std::vector<MuonNeuron*>* useneurons()  =0;
  virtual const std::vector<MuonNeuron*>* allneurons()  =0;
  virtual bool recOK() =0;
  virtual bool tooManyHits() =0;
  virtual bool clusteringOn() =0;
  virtual void setZref(double Zref) =0;
  virtual void setPhysicsTiming(bool PhysTiming) =0;
  virtual void setAssumeCosmics(bool AssumeCosmics) =0;
  virtual void setAssumePhysics(bool AssumePhysics) =0;
  virtual void setSeedStation(int seedS) = 0;
  virtual void setSkipStation(int skipS) = 0;
  virtual StatusCode copyToLHCbTracks() = 0;
};
#endif // MUONNNET_IMUONTRACKREC_H
