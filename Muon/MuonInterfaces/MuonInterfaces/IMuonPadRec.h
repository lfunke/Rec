/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONMONITOR_IMUONPADREC_H
#define MUONMONITOR_IMUONPADREC_H 1

#include <vector>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"
#include "MuonInterfaces/MuonLogPad.h"

// forward decl.
class MuonLogHit;



/** @class IMuonPadRec IMuonPadRec.h MuonTrackRec/IMuonPadRec.h
 *
 *
 *  @author
 *  @date   2008-01-25
 */
struct IMuonPadRec : extend_interfaces<IAlgTool> {

  // Return the interface ID
  DeclareInterfaceID ( IMuonPadRec, 2, 0 );

  virtual StatusCode buildLogicalPads(const std::vector<MuonLogHit>& myhits ) = 0;
  virtual const std::vector<MuonLogPad>& pads() = 0;


};
#endif // MUONMONITOR_IMUONPADREC_H
