/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef ISTCLUSTERCOLLECTOR_H
#define ISTCLUSTERCOLLECTOR_H 1

#include "GaudiKernel/IAlgTool.h"

/** @class ISTClusterCollector ISTClusterCollector.h
 *
 *  Interface Class for collecting STClusters around a track
 *
 *  @author M.Needham
 *  @date   27/3/2009
 */

#include <vector>
#include "Event/Track.h"

namespace LHCb{
  class STCluster;
}


struct ISTClusterCollector : extend_interfaces<IAlgTool> {

  DeclareInterfaceID(ISTClusterCollector,1 ,0);

  typedef struct {
    LHCb::STCluster* cluster;
    double residual;
  } Hit;

  typedef std::vector<Hit> Hits;

  /// collect the hits
  virtual StatusCode execute(const LHCb::Track& track,
                             Hits& outputCont) const = 0;

};

#endif // ISTCLUSTERCOLLECTOR_H
