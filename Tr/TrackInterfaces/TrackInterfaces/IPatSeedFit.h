/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKINTERFACES_IPATSEEDFIT_H
#define TRACKINTERFACES_IPATSEEDFIT_H 1

#include <vector>
#include "GaudiKernel/IAlgTool.h"
#include "Kernel/LHCbID.h"
#include "Event/State.h"


// forward declaration(s)
class PatSeedTrack;

/** @class IPatSeedFit IPatSeedFit.h
 *
 * provide a convenient interface to the internal fit used in the PatSeeding
 * algorithm in the pattern reco
 *
 * @author Manuel Tobias Schiller <schiller@physi.uni-heidelberg.de>
 * @date   2009-01-28
 */
struct IPatSeedFit : extend_interfaces<IAlgTool> {

  DeclareInterfaceID(IPatSeedFit, 2, 0);

  virtual StatusCode fitSeed( const std::vector<LHCb::LHCbID> lhcbIDs,
			      std::vector<LHCb::State> *states) const = 0;

  // fit a PatSeedTrack
  virtual StatusCode fitTrack( PatSeedTrack& track, double maxChi2,
		  unsigned minPlanes, bool xOnly, bool forceDebug ) const = 0;

};
#endif // INCLUDE_IPATSEEDFIT_H
