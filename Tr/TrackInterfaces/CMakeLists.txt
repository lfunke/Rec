###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: TrackInterfaces
################################################################################
gaudi_subdir(TrackInterfaces v6r0)

gaudi_depends_on_subdirs(Det/DetDesc
                         Event/RecEvent
                         Event/TrackEvent
                         GaudiKernel
                         Tr/TrackKernel
                         Kernel/LHCbKernel)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})
find_path(RANGES_V3_INCLUDE_DIR NAMES range/v3/utility/any.hpp)

gaudi_add_dictionary(TrackInterfaces
                     dict/TrackInterfacesDict.h
                     dict/TrackInterfacesDict.xml
                     LINK_LIBRARIES DetDescLib RecEvent TrackEvent GaudiKernel LHCbKernel TrackKernel
                     OPTIONS "-U__MINGW32__")

gaudi_install_headers(TrackInterfaces)

