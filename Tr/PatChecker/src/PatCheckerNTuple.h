/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: PatCheckerNTuple.h,v 1.1.1.1 2007-10-09 18:41:19 smenzeme Exp $
#ifndef PATCHECKERNTUPLE_H 
#define PATCHECKERNTUPLE_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"


/** @class PatCheckerNTuple PatCheckerNTuple.h
 *  
 *   Algorithm to produce an NTuple to check Pat tracks.
 *  @author Olivier Callot
 *  @date   2006-05-11
 *  @update for A-Team framework 2007-08-20
 */


  class PatCheckerNTuple : public GaudiTupleAlg {
  public: 
    /// Standard constructor
    PatCheckerNTuple( const std::string& name, ISvcLocator* pSvcLocator );
    
    virtual ~PatCheckerNTuple( ); ///< Destructor
    
    StatusCode initialize() override;    ///< Algorithm initialization
    StatusCode execute() override;    ///< Algorithm execution
    StatusCode finalize() override;    ///< Algorithm finalization
    
  protected:
    
  private:
    std::string m_tupleName;
    std::string m_container;
    
  };

#endif // PATCHECKERNTUPLE_H
