/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Kernel/VeloChannelID.h"

#include "DownstreamGhostClassification.h"

#include "Event/Track.h"


DECLARE_COMPONENT( DownstreamGhostClassification )

using namespace LHCb;

DownstreamGhostClassification::DownstreamGhostClassification(const std::string& type,
                     const std::string& name,
                     const IInterface* parent):
  TrackGhostClassificationBase(type, name, parent){

}

DownstreamGhostClassification::~DownstreamGhostClassification(){
  // destructer
}

StatusCode DownstreamGhostClassification::specific(LHCbIDs::const_iterator& start,
                                       LHCbIDs::const_iterator& stop,
                                       LHCb::GhostTrackInfo& tinfo) const{

  // split into velo and T hits
  LHCbIDs::const_iterator iter = start;
  LHCbIDs ttHits;  ttHits.reserve(20);
  LHCbIDs tHits;  tHits.reserve(20);
  for (; iter != stop; ++iter){
    if (iter->detectorType() == LHCbID::channelIDtype::TT){
      ttHits.push_back(*iter);
    }
    else if (iter->detectorType() == LHCbID::channelIDtype::OT ||
             iter->detectorType() == LHCbID::channelIDtype::IT ){
      tHits.push_back(*iter);
    }
  } // for iter


  // match the T Hits
  LHCb::GhostTrackInfo::LinkPair tMatch = bestPair(tHits);

  // match the velo Hits
  LHCb::GhostTrackInfo::LinkPair ttMatch = bestPair(ttHits);

  if (tMatch.first == 0 || tMatch.second < m_purityCut){
     tinfo.setClassification(GhostTrackInfo::Classification::GhostParent);
  }

  if (isMatched(tMatch) && isMatched(ttMatch) && tMatch.first != ttMatch.first){
     tinfo.setClassification(LHCb::GhostTrackInfo::Classification::InconsistentParts);
  }

  return StatusCode::SUCCESS;
}
