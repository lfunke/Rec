/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _LongGhostClassification_H
#define _LongGhostClassification_H

#include "TrackGhostClassificationBase.h"
#include "Event/Track.h"



class LongGhostClassification: public TrackGhostClassificationBase {

public:

  /// constructer
  LongGhostClassification(const std::string& type,
                               const std::string& name,
                               const IInterface* parent);

  /** destructer */
  virtual ~LongGhostClassification();

  /**
   *  Check this is a ghost .
   *  specialize for long tracks [check velo and T separately]
   *  @param aTrack to link
   *  @return bool true if a ghost
  */
  bool isGhost(LHCbIDs::const_iterator& start,
               LHCbIDs::const_iterator& stop) const override;

  bool isGhost(const LHCb::Track& aTrack) const override;

 private:

 StatusCode specific(LHCbIDs::const_iterator& start,
               LHCbIDs::const_iterator& stop, LHCb::GhostTrackInfo& tinfo) const override;

};

// Dummy override to avoid "partially overridden" warnings from icc
inline bool LongGhostClassification::isGhost(const LHCb::Track& aTrack) const
{
  return TrackGhostClassificationBase::isGhost(aTrack);
}
#endif
