/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _UpstreamGhostClassification_H
#define _UpstreamGhostClassification_H

#include "TrackGhostClassificationBase.h"
#include "Event/Track.h"



class UpstreamGhostClassification: public TrackGhostClassificationBase {

public:

  /// constructer
  UpstreamGhostClassification(const std::string& type,
                               const std::string& name,
                               const IInterface* parent);

  /** destructer */
  virtual ~UpstreamGhostClassification();


 private:

 StatusCode specific(LHCbIDs::const_iterator& start,
               LHCbIDs::const_iterator& stop, LHCb::GhostTrackInfo& tinfo) const override;


};



#endif
