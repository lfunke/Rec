/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

/** @class VPExpectation VPExpectation.h
 *
 *  Tool to estimate how many hits are expected in the VELO
 *
 *  @author Thomas Bird
 *  @date   05/12/2012
 */

#include "GaudiAlg/GaudiTool.h"

// LHCb
// Event/TrackEvent
#include "Event/Track.h"

#include "TrackInterfaces/IVPExpectation.h"

class DeVP;
class DeVPSensor;

class VPExpectation : public extends<GaudiTool,IVPExpectation> {

 public:
  /// Constructor
  using extends::extends;

  /// Tool initialization
  StatusCode initialize() override;

  /** Returns number of hits expected, from zFirst to endVelo
   *
   *  @param track Reference to the Track to test
   *  @return number of hits expected
   */
  int nExpected(const LHCb::Track &track) const override;

  /** Returns info on expected hits
   *
   *  @param track Reference to the Track to test
   *  @return Info
   */
  IVPExpectation::Info expectedInfo(const LHCb::Track &track) const override;

  /** Returns number of hits expected, from zStart to zStop
   *
   *  @param track Reference to the Track to test
   *  @param zStart Start of scan range
   *  @param zStop End of scan range
   *  @return Info
   */
  int nExpected(const LHCb::Track &track, const double zStart,
                const double zStop) const override;

  /** Returns expected hits info, from zStart to zStop
   *
   *  @param track Reference to the Track to test
   *  @param zStart Start of scan range
   *  @param zStop End of scan range
   *  @return Info
   */
  IVPExpectation::Info expectedInfo(const LHCb::Track &track,
                                    const double zStart,
                                    const double zStop) const override;

  /** Returns number of hits missed, from zBeamLine to firstHit
   *
   *  @param track Reference to the Track to test
   *  @return number of hits missed before first hit
   */
  int nMissed(const LHCb::Track &track) const override;

  /** Returns number of hits missed, from z to firstHit
   *
   *  @param track Reference to the Track to test
   *  @param z z to start from
   *  @return number of hits missed before first hit
   */
  int nMissed(const LHCb::Track &track, const double z) const override;

  /** Returns true if track passes through the active area of a sensor
   *
   *  @param track Reference to the Track to test
   *  @param sensorNum sensor number
   *  @return true if track crosses a working pixel
   */
  bool isInside(const LHCb::Track &track,
                const unsigned int sensorNum) const override;

 private:
  IVPExpectation::Info scan(const LHCb::Track &track, const double zStart,
                            const double zStop) const;

  bool isInside(const DeVPSensor *sensor, const double x,
                const double y) const;

  double zMin(const LHCb::Track &track) const;
  double zMax(const LHCb::Track &track) const;

  int nFound(const LHCb::Track &track, const double zStart,
             const double zStop) const;

  double zBeamLine(const LHCb::Track &track) const;

  DeVP *m_det = nullptr;
};

