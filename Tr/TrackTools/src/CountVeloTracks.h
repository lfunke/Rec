/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKTOOLS_CountVeloTracks_H
#define TRACKTOOLS_CountVeloTracks_H

//-----------------------------------------------------------------------------
/** @class CountVeloTracks CountVeloTracks.h
 *
 *  Tool for counting the distinct VELO tracks in an event
 *
 *  Based on code written by M Needham
 *
 *  @author David Hutchcroft David.Hutchcroft@cern.ch
 *
 *  @date   21/1/2011
 */
//-----------------------------------------------------------------------------

#include "GaudiAlg/GaudiTool.h"
#include "Kernel/ICountContainedObjects.h"

struct CountVeloTracks : extends<GaudiTool, ICountContainedObjects>
{

  /// constructor
  using extends::extends;

  /** Returns number of distinct VELO tracks contributing to the container
  *
  *  @param tracks reference to LHCb::Tracks container
  *
  *  @return number of distinct VELO tracks
  */
  unsigned int nObj ( const ObjectContainerBase * cont ) const override;

};

#endif // TRACKTOOLS_CountVeloTracks_H
