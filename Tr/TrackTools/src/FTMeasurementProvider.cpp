/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class FTMeasurementProvider FTMeasurementProvider.cpp
 *
 * Implementation of FTMeasurementProvider
 * see interface header for description
 *
 *  @author Wouter Hulsbergen
 *  @date   30/12/2005
 */

#include "TrackInterfaces/IMeasurementProvider.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/DataObjectHandle.h"
#include "GaudiKernel/ToolHandle.h"
#include "Event/FTMeasurement.h"
#include "FTDet/DeFTDetector.h"
#include "Event/FTLiteCluster.h"

using FTLiteClusters = LHCb::FTLiteCluster::FTLiteClusters;

class FTMeasurementProvider final : public extends< GaudiTool,
                                                    IMeasurementProvider >
{
public:

  /// constructor
  using extends::extends;

  StatusCode initialize() override;

  void addToMeasurements( LHCb::span<LHCb::LHCbID> ids,
                          std::vector<LHCb::Measurement*>& measurements,
                          const LHCb::ZTrajectory<double>& reftraj) const override ;

  StatusCode load( LHCb::Track&  ) const override {
    return Error( "sorry, MeasurementProviderBase::load not implemented" );
  }

private:
  LHCb::FTMeasurement* ftmeasurement( const LHCb::LHCbID& id, const FTLiteClusters* ) const  ;

  const DeFTDetector* m_det = nullptr;
  DataObjectReadHandle<FTLiteClusters> m_clustersDh {
      this, "ClusterLocation", LHCb::FTLiteClusterLocation::Default };
} ;

//=============================================================================
// Declare to tool factory
//=============================================================================

DECLARE_COMPONENT( FTMeasurementProvider )

//-----------------------------------------------------------------------------
/// Initialize
//-----------------------------------------------------------------------------

StatusCode FTMeasurementProvider::initialize() {
  StatusCode sc = GaudiTool::initialize();
  if( sc.isFailure() ) { return Error( "Failed to initialize!", sc ); }

  // Retrieve the detector element
  m_det = getDet<DeFTDetector>( DeFTDetectorLocation::Default ) ;

  return sc;
}

//-----------------------------------------------------------------------------
/// Create measurements for list of LHCbIDs
//-----------------------------------------------------------------------------

void FTMeasurementProvider::addToMeasurements( LHCb::span<LHCb::LHCbID> ids,
                                               std::vector<LHCb::Measurement*>& measurements,
                                               const LHCb::ZTrajectory<double>& ) const
{
  measurements.reserve(measurements.size()+ids.size());
  assert( std::all_of( ids.begin(), ids.end(), [](LHCb::LHCbID id) { return id.isFT(); } ) );
  std::transform( ids.begin(), ids.end(), std::back_inserter(measurements),
                  [&,clusters=m_clustersDh.get()](const LHCb::LHCbID& id)
                  {
                      /// The clusters are not sorted anymore, so we can use a find_if
                      /// to find the element corresponding to the channel ID
                      const auto& c = clusters->range();
                      auto itH = id.isFT() ? std::find_if( c.begin(),  c.end(),
                                               [&id](const LHCb::FTLiteCluster clus)
                                               { return clus.channelID() == id.ftID(); })
                                           : c.end();
                      return ( itH != c.end() ) ?  new LHCb::FTMeasurement( *itH, *m_det ) : nullptr;
                  });
}
