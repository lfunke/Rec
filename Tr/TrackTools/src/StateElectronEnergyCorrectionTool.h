/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKTOOLS_STATEELECTRONENERGYCORRECTIONTOOL_H
#define TRACKTOOLS_STATEELECTRONENERGYCORRECTIONTOOL_H 1

// Include files
// -------------
// from Gaudi
#include "GaudiAlg/GaudiTool.h"

// from TrackInterfaces
#include "TrackInterfaces/IStateCorrectionTool.h"

// from TrackEvent
#include "Event/State.h"

/** @class StateElectronEnergyCorrectionTool
 *
 *  This state correction tool applies a dE/dx energy loss correction
 *  to electrons
 *
 *  @author Eduardo Rodrigues
 *  @date   2006-08-18
 *  (Original code taken from the master extrapolator)
 */
class StateElectronEnergyCorrectionTool : public extends<GaudiTool, IStateCorrectionTool> {
public:
  /// Standard constructor
  using extends::extends;
  /// Correct a State for electron dE/dx energy losses
  void correctState( LHCb::State& state,
                     const Material* material,
                     std::any& cache,
                     double wallThickness,
                     bool upstream,
                     double ) const override;

private:
  // Job options
  Gaudi::Property<double> m_maxRadLength { this, "MaximumRadLength", 10. };   ///< maximum radiation length (in mm)

};
#endif // TRACKTOOLS_STATEELECTRONENERGYCORRECTIONTOOL_H
