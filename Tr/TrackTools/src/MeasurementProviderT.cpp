/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class MeasurementProviderT MeasurementProviderT.cpp
 *
 * Implementation of templated MeasurementProvider tool
 * see interface header for description
 *
 *  @author W. Hulsbergen
 *  @date   07/06/2007
 */

#include "TrackInterfaces/IMeasurementProvider.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/ToolHandle.h"
#include <type_traits>
#include "GaudiKernel/DataObjectHandle.h"
#include "Event/TrackParameters.h"


template <typename T>
class MeasurementProviderT final : public extends< GaudiTool, IMeasurementProvider >
{
public:
  using extends::extends;

  StatusCode initialize() override ;
  void addToMeasurements( LHCb::span<LHCb::LHCbID> ids,
                          std::vector<LHCb::Measurement*>& measurements,
                          const LHCb::ZTrajectory<double>& reftraj) const override ;

  StatusCode load( LHCb::Track&  ) const override {
    info() << "sorry, MeasurementProviderBase::load not implemented" << endmsg ;
    return StatusCode::FAILURE ;
  }

private:
  std::unique_ptr<LHCb::Measurement> measurement( const typename T::Cluster& cluster, bool localY   ) const ;
  std::unique_ptr<LHCb::Measurement> measurement( const typename T::Cluster& cluster, const LHCb::ZTrajectory<double>& refvector,
                                          bool localY ) const ;

  DataObjectReadHandle<typename T::ClusterContainerType> m_clustersDH { this,  "ClusterLocation", T::defaultClusterLocation() };

  Gaudi::Property<bool> m_useReference { this, "UseReference", true };
  ToolHandle<typename T::PositionToolType> m_positiontool = { T::positionToolName() };
  const typename T::DetectorType* m_det = nullptr;
};

// local
#include "GaudiKernel/IIncidentSvc.h"
#include "Event/StateVector.h"
#include "Event/Measurement.h"
#include "TrackKernel/ZTrajectory.h"

//-----------------------------------------------------------------------------
/// Initialize
//-----------------------------------------------------------------------------

template <typename T>
StatusCode MeasurementProviderT<T>::initialize()
{
  StatusCode sc = extends::initialize();
  if( sc.isFailure() ) { return Error( "Failed to initialize!", sc ); }

  // Retrieve the detector element
  m_det = getDet<typename T::DetectorType>( T::defaultDetectorLocation() );

  return sc;
}

////////////////////////////////////////////////////////////////////////////////////////
// Template instantiations using Traits classes
////////////////////////////////////////////////////////////////////////////////////////

#include "TrackInterfaces/IVeloClusterPosition.h"
#include "TrackInterfaces/IVPClusterPosition.h"
#include "TrackInterfaces/ISTClusterPosition.h"
#include "TrackInterfaces/IUTClusterPosition.h"

#include "Event/STCluster.h"
#include "Event/STLiteCluster.h"
#include "Event/UTCluster.h"
#include "Event/UTLiteCluster.h"
#include "PrKernel/UTHitHandler.h"
#include "Event/VeloCluster.h"
#include "Event/VeloLiteCluster.h"
#include "Event/VPCluster.h"
#include "Event/VPLightCluster.h"

#include "Event/VeloRMeasurement.h"
#include "Event/VeloLiteRMeasurement.h"
#include "Event/VeloPhiMeasurement.h"
#include "Event/VeloLitePhiMeasurement.h"
#include "Event/STMeasurement.h"
#include "Event/STLiteMeasurement.h"
#include "Event/UTLiteMeasurement.h"
#include "Event/VPMeasurement.h"

#include "STDet/DeSTDetector.h"
#include "UTDet/DeUTDetector.h"
#include "VeloDet/DeVelo.h"
#include "VPDet/DeVP.h"

namespace MeasurementProviderTypes {

  //////////// Velo
  struct VeloBase {
    using PositionToolType = IVeloClusterPosition  ;
    using DetectorType = DeVelo;
    static std::string positionToolName() { return "VeloClusterPosition" ; }
    static const std::string& defaultDetectorLocation() { return DeVeloLocation::Default ; }
    static LHCb::VeloChannelID channelId( const LHCb::LHCbID& id ) { return id.veloID(); }
  };

  struct VeloPhiBase : VeloBase {
    static bool checkType(const LHCb::LHCbID& id) { return id.isVelo() && id.veloID().isPhiType() ; }
    static double nominalZ(const DeVelo& det, const LHCb::LHCbID& id) { return det.phiSensor( id.veloID() )->z() ; }
  };
  struct VeloRBase : VeloBase {
    static bool checkType(const LHCb::LHCbID& id) { return id.isVelo() && id.veloID().isRType() ; }
    static double nominalZ(const DeVelo& det, const LHCb::LHCbID& id) { return det.rSensor( id.veloID() )->z() ; }
  };

  struct VeloLiteBase {
    using Cluster = LHCb::VeloLiteCluster;
    using ClusterContainerType = LHCb::VeloLiteCluster::VeloLiteClusters;
    static const std::string& defaultClusterLocation() { return LHCb::VeloLiteClusterLocation::Default ; }
  };
  struct VeloFullBase {
    using Cluster = LHCb::VeloCluster;
    using ClusterContainerType = LHCb::VeloClusters ;
    static const std::string& defaultClusterLocation() { return LHCb::VeloClusterLocation::Default ; }
  };

  struct VeloR : VeloFullBase, VeloRBase {
    using MeasurementType = LHCb::VeloRMeasurement;
  };
  struct VeloLiteR : VeloLiteBase, VeloRBase {
    using MeasurementType = LHCb::VeloLiteRMeasurement;
  };
  struct VeloPhi : VeloFullBase, VeloPhiBase {
    using MeasurementType = LHCb::VeloPhiMeasurement;
  };
  struct VeloLitePhi : VeloLiteBase, VeloPhiBase {
    using MeasurementType = LHCb::VeloLitePhiMeasurement;
  };

  //////////// VP
  struct VP {
    using PositionToolType = IVPClusterPosition;
    using DetectorType = DeVP;
    using Cluster = LHCb::VPLightCluster;
    using ClusterContainerType = LHCb::VPLightClusters;
    using MeasurementType = LHCb::VPMeasurement;
    static std::string positionToolName() { return "VPClusterPosition"; }
    static const std::string& defaultDetectorLocation() { return DeVPLocation::Default ; }
    static LHCb::VPChannelID channelId( const LHCb::LHCbID& id ) { return id.vpID(); }
    static const std::string& defaultClusterLocation() { return LHCb::VPClusterLocation::Light; }
    static bool checkType(const LHCb::LHCbID& id) { return id.isVP(); }
    static double nominalZ(const DetectorType& det, const LHCb::LHCbID& id) { return det.sensorOfChannel( id.vpID() )->z() ; }
  };

  //////////// ST
  struct STBase {
    using PositionToolType = ISTClusterPosition       ;
    using DetectorType =     DeSTDetector             ;
    static LHCb::STChannelID channelId( const LHCb::LHCbID& id ) { return id.stID() ; }
    static double nominalZ(const DetectorType& det, const LHCb::LHCbID& id) {
        // extremely ugly. need more functionality in det elements to do this quicker.
        return det.findSector(id.stID())->globalCentre().z() ;
    }
  };

  struct STFullBase : STBase {
    using MeasurementType = LHCb::STMeasurement;
    using Cluster = LHCb::STCluster;
    using ClusterContainerType = LHCb::STClusters;
  };
  struct STLiteBase : STBase {
    using MeasurementType = LHCb::STLiteMeasurement;
    using Cluster = LHCb::STLiteCluster;
    using ClusterContainerType = LHCb::STLiteCluster::STLiteClusters;
  };

  struct TTBase {
    static const std::string& defaultDetectorLocation() { return DeSTDetLocation::TT ; }
    static bool checkType(const LHCb::LHCbID& id) { return id.isTT() ; }
  };
  struct ITBase {
    static const std::string& defaultDetectorLocation() { return DeSTDetLocation::IT ; }
    static bool checkType(const LHCb::LHCbID& id) { return id.isIT() ; }
  };

  struct TT : STFullBase, TTBase {
    static std::string positionToolName() { return "STOfflinePosition/TTClusterPosition" ; }
    static const std::string& defaultClusterLocation() { return LHCb::STClusterLocation::TTClusters ; }
  };
  struct TTLite :STLiteBase, TTBase {
    static std::string positionToolName() { return "STOnlinePosition/TTLiteClusterPosition" ; }
    static const std::string& defaultClusterLocation() { return LHCb::STLiteClusterLocation::TTClusters ; }
  };

  struct UTLite {
    using PositionToolType = IUTClusterPosition;
    using DetectorType = DeUTDetector;
    using MeasurementType = LHCb::UTLiteMeasurement;
    using Cluster = UT::Hit;
    using ClusterContainerType = UT::HitHandler;
    static LHCb::UTChannelID channelId( const LHCb::LHCbID& id ) { return id.utID() ; }
    static double nominalZ(const DetectorType& det, const LHCb::LHCbID& id) {
        // extremely ugly. need more functionality in det elements to do this quicker.
        return det.findSector(id.utID())->globalCentre().z() ;
    }
    static const std::string& defaultDetectorLocation() { return DeUTDetLocation::UT; }
    static bool checkType(const LHCb::LHCbID& id) { return id.isUT() ; }
    static std::string positionToolName() { return "UTOnlinePosition/UTLiteClusterPosition" ; }
    static const std::string& defaultClusterLocation() { return UT::Info::HitLocation ; }
  };

  struct IT : STFullBase, ITBase {
    static std::string positionToolName() { return "STOfflinePosition/ITClusterPosition" ; }
    static const std::string& defaultClusterLocation() { return LHCb::STClusterLocation::ITClusters ; }
  };
  struct ITLite : STLiteBase, ITBase {
    static std::string positionToolName() { return "STOnlinePosition/ITLiteClusterPosition" ; }
    static const std::string& defaultClusterLocation() { return LHCb::STLiteClusterLocation::ITClusters; }
  };

}
namespace {
  template <typename T>
  const typename T::Cluster*  id2cluster(LHCb::LHCbID id, typename T::ClusterContainerType const & clusters  ) {
      if constexpr( std::is_same_v< T, MeasurementProviderTypes::VP > ) {
         const auto clus = LIKELY(id.isVP()) ? std::find_if( clusters.begin(),  clusters.end(),
                                      [cid = id.vpID().channelID()](const LHCb::VPLightCluster& clus) {
                                        return clus.channelID() == cid;
                                      }) : clusters.end() ;
         return clus != clusters.end() ? &*clus : nullptr;
      } else if constexpr ( std::is_same_v<T, MeasurementProviderTypes::UTLite> ) {
        auto cid = id.utID();
        auto& hits = clusters.hits(cid.station(), cid.layer(),
                                   cid.detRegion(), cid.sector());
        auto hit = std::find_if( hits.begin(), hits.end(),
                                 [cid](const UT::Hit& hit) {
                                    return hit.chanID() == cid;
                                 });
        return hit!=hits.end() ? &*hit : nullptr;
      } else {
        return  LIKELY( T::checkType(id) ) ? clusters.object( T::channelId(id) ) : nullptr ;
      }
  }
}

//-----------------------------------------------------------------------------
/// Create a measurement
//-----------------------------------------------------------------------------

template <typename T>
std::unique_ptr<LHCb::Measurement> MeasurementProviderT<T>::measurement( const typename T::Cluster& clus, [[maybe_unused]] bool localY ) const
{
  if constexpr ( std::is_same_v<T,MeasurementProviderTypes::VP> ) {
      return std::make_unique<LHCb::VPMeasurement>(clus, m_positiontool->position(clus),
                                     localY ? LHCb::VPMeasurement::VPMeasurementType::Y
                                            : LHCb::VPMeasurement::VPMeasurementType::X);
  } else {
      return std::make_unique<typename T::MeasurementType>( clus, *m_det, *m_positiontool );
  }
}

//-----------------------------------------------------------------------------
/// Create a measurement with statevector. For now very inefficient.
//-----------------------------------------------------------------------------

template <typename T>
std::unique_ptr<LHCb::Measurement> MeasurementProviderT<T>::measurement( const typename T::Cluster& clus ,
                                                         const LHCb::ZTrajectory<double>& reftraj,
                                                         [[maybe_unused]] bool localY ) const
{
  if constexpr ( std::is_same_v<T,MeasurementProviderTypes::UTLite> ) { //UTLite ignores reference trajectory...
      return measurement(clus,localY);
  } else {
      if ( UNLIKELY(!m_useReference) ) return measurement( clus, localY ) ;
      if constexpr (std::is_same_v<T,MeasurementProviderTypes::VP> ) {
          LHCb::StateVector sv = reftraj.stateVector(T::nominalZ(*m_det,clus.channelID()));
          return std::make_unique<LHCb::VPMeasurement>(clus,
                                         m_positiontool->position(clus, sv.position(), sv.tx(), sv.ty()),
                                         localY ?  LHCb::VPMeasurement::VPMeasurementType::Y : LHCb::VPMeasurement::VPMeasurementType::X);
      } else {
          return std::make_unique<typename T::MeasurementType>( clus, *m_det, *m_positiontool,
                                 reftraj.stateVector( T::nominalZ(*m_det,clus.channelID()) ));
      }
  }
}

//-----------------------------------------------------------------------------
/// Create measurements for list of LHCbIDs
//-----------------------------------------------------------------------------

template <typename T>
void MeasurementProviderT<T>::addToMeasurements( LHCb::span<LHCb::LHCbID> ids,
                                                 std::vector<LHCb::Measurement*>& measurements,
                                                 const LHCb::ZTrajectory<double>& reftraj) const
{
  measurements.reserve(measurements.size()+ids.size());
  auto to_clus = [clusters = m_clustersDH.get()](LHCb::LHCbID id) { return id2cluster<T>(id,*clusters); };
  std::transform( ids.begin(), ids.end(),
                  std::back_inserter(measurements),
                  [&](const LHCb::LHCbID& id)
                  {
                      auto clus = to_clus(id);
                      return LIKELY(clus!=nullptr) ? this->measurement(*clus,reftraj,false).release() : nullptr;
                  } );
}

template <>
void MeasurementProviderT<MeasurementProviderTypes::VP>::addToMeasurements(LHCb::span<LHCb::LHCbID> ids,
                                                 std::vector<LHCb::Measurement*>& measurements,
                                                 const LHCb::ZTrajectory<double>& ref) const
{
  measurements.reserve(measurements.size()+2*ids.size());
  auto to_clus = [clusters = m_clustersDH.get()](LHCb::LHCbID id) { return id2cluster<MeasurementProviderTypes::VP>(id,*clusters); };
  std::for_each( ids.begin(), ids.end(),
                 [&](const LHCb::LHCbID& id) {
                    auto clus = to_clus(id);
                    if (UNLIKELY(!clus)) {
                      measurements.push_back(nullptr);
                      measurements.push_back(nullptr);
                    } else {
                      measurements.push_back(measurement(*clus, ref, false ).release());
                      measurements.push_back(measurement(*clus, ref, true ).release());
                    }
                 } );
}

using VPMeasurementProvider = MeasurementProviderT<MeasurementProviderTypes::VP>;
DECLARE_COMPONENT( VPMeasurementProvider )
using UTLiteMeasurementProvider = MeasurementProviderT<MeasurementProviderTypes::UTLite>;
DECLARE_COMPONENT( UTLiteMeasurementProvider )

using VeloRMeasurementProvider = MeasurementProviderT<MeasurementProviderTypes::VeloR>;
DECLARE_COMPONENT( VeloRMeasurementProvider )
using VeloLiteRMeasurementProvider = MeasurementProviderT<MeasurementProviderTypes::VeloLiteR>;
DECLARE_COMPONENT( VeloLiteRMeasurementProvider )
using VeloPhiMeasurementProvider = MeasurementProviderT<MeasurementProviderTypes::VeloPhi>;
DECLARE_COMPONENT( VeloPhiMeasurementProvider )
using VeloLitePhiMeasurementProvider = MeasurementProviderT<MeasurementProviderTypes::VeloLitePhi>;
DECLARE_COMPONENT( VeloLitePhiMeasurementProvider )
using TTMeasurementProvider = MeasurementProviderT<MeasurementProviderTypes::TT> ;
DECLARE_COMPONENT( TTMeasurementProvider )
using TTLiteMeasurementProvider = MeasurementProviderT<MeasurementProviderTypes::TTLite>;
DECLARE_COMPONENT( TTLiteMeasurementProvider )
using ITMeasurementProvider = MeasurementProviderT<MeasurementProviderTypes::IT>;
DECLARE_COMPONENT( ITMeasurementProvider )
using ITLiteMeasurementProvider = MeasurementProviderT<MeasurementProviderTypes::ITLite>;
DECLARE_COMPONENT( ITLiteMeasurementProvider )
