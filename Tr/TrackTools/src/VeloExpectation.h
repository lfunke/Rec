/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKTOOLS_VeloExpectation_H
#define TRACKTOOLS_VeloExpectation_H

//-----------------------------------------------------------------------------
/** @class VeloExpectation VeloExpectation.h
 *
 *  Tool to estimate how many hits are expected in the velo
 *
 *  @author M.Needham Matt.Needham@cern.ch
 *
 *  @date   30/12/2005
 */
//-----------------------------------------------------------------------------

#include "GaudiAlg/GaudiTool.h"
#include "TrackInterfaces/IVeloExpectation.h"
#include <string>

#include "Event/Track.h"
#include "TsaKernel/Line.h"

class DeVelo;
class DeVeloSensor;

class VeloExpectation : public extends<GaudiTool, IVeloExpectation>
{

public:

  /// constructer
  using extends::extends;

  /// Tool initialization
  StatusCode initialize() override;


  /** Returns number of hits expected, from zFirst to endVelo
   *
   *  @param aTrack Reference to the Track to test
   *
   *  @return number of hits expected
   */
  int nExpected ( const LHCb::Track& aTrack ) const override;


  /** Returns info on expected hits and fill bitmap for hit pattern
   *
   *  @param aTrack Reference to the Track to test
   *  @return Info
   */
  IVeloExpectation::Info expectedInfo ( const LHCb::Track& aTrack, VeloPattern velo[4] ) const override;


  /** Returns info on expected hits
   *
   *  @param aTrack Reference to the Track to test
   *  @return Info
   */
  IVeloExpectation::Info expectedInfo ( const LHCb::Track& aTrack ) const override;


  /** Returns number of hits expected, from zStart to zStop
   *
   *  @param aTrack Reference to the Track to test
   *  @param zStart --> start of scan range
   *  @param zStop --> end of scan range
   *  @return Info
   */
  int nExpected ( const LHCb::Track& aTrack , const double zStart, const double zStop) const override;


  /** Returns expected hits info, from zStart to zStop
   *
   *  @param aTrack Reference to the Track to test
   *  @param zStart --> start of scan range
   *  @param zStop --> end of scan range
   *  @return Info
   */
  IVeloExpectation::Info expectedInfo ( const LHCb::Track& aTrack , const double zStart, const double zStop) const override;


   /** Returns expected hits info, from zStart to zStop and hit pattern
   *
   *  @param aTrack Reference to the Track to test
   *  @param zStart --> start of scan range
   *  @param zStop --> end of scan range
   *  @return Info
   */
  IVeloExpectation::Info expectedInfo ( const LHCb::Track& aTrack , const double zStart, const double zStop,
						VeloPattern velo[4]) const override;


  /** Returns number of hits missed, from zBeamLine to firstHit
   *
   *  @param aTrack Reference to the Track to test
   *
   *  @return number of hits missed before first hit
   */
  int nMissed( const LHCb::Track& aTrack ) const override;


  /** Returns number of hits missed, from z to firstHit
   *
   *  @param aTrack Reference to the Track to test
   *  @param z --> z to start from
   *
   *  @return number of hits missed before first hit
   */
  int nMissed( const LHCb::Track& aTrack, const double z) const override;


  /** Returns true if track passses through a working strip in the sensor
   *
   *
   *  @param aTrack Reference to the Track to test
   *  @param sensorNum ---> sensor number
   *
   *  @return true if crosses a working strip
   */
  bool isInside(const LHCb::Track& aTrack,
                const unsigned int sensorNum) const override;



private:

  IVeloExpectation::Info scan(const LHCb::Track& aTrack,
                              const double zStart, const double zStop, VeloPattern velo[4] ) const;

  bool isInside(const DeVeloSensor* sensor, const Tf::Tsa::Line& xLine,
                const Tf::Tsa::Line& yLine, const double z) const;

  double zMin(const LHCb::Track& aTrack) const;

  double zMax(const LHCb::Track& aTrack) const;

  int nFound(const LHCb::Track& aTrack,
             const double zStart, const double zStop) const;

  void param(const LHCb::Track& aTrack, const double z,
             Tf::Tsa::Line& xLine, Tf::Tsa::Line& yLine) const;

  double zBeamLine(const LHCb::Track& aTrack) const;

  DeVelo* m_veloDet;

};

#endif // TRACKTOOLS_VeloExpectation_H
