/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <map>
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"
// from TrackInterfaces
#include "TrackInterfaces/IPatSeedFit.h"
#include "TrackInterfaces/ITrackStateInit.h"
#include "TrackInterfaces/ITrackFitter.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackInterfaces/ITrackManipulator.h"
#include "TrackInterfaces/IMaterialLocator.h"
#include "TrackInterfaces/IMeasurementProvider.h"
#include "TrackInterfaces/IPatVeloTTFit.h"

// from LHCb
#include "LHCbMath/LHCbMath.h"

// from TrackEvent
#include "Event/StateParameters.h"
#include "Event/Track.h"
#include "Event/State.h"
#include "TrackKernel/TrackTraj.h"

/** @class TrackStateInitTool TrackStateInitTool.h
 *
 * Tool to initialize the track starting from LHCbIDs
 *
 * @author Pavel Krokovny <krokovny@physi.uni-heidelberg.de>
 * @date   2009-03-02
 */

class DeSTDetector ;
struct ITrackMomentumEstimate;


class TrackStateInitTool final : public extends<GaudiTool, ITrackStateInit>
{
public:

  using extends::extends;
  StatusCode initialize() override;
  StatusCode fit(LHCb::Track& track, bool clearStates = true) const override;
  StatusCode initializeRefStates( LHCb::Track& track,
                                  const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const override;

private:
  StatusCode createTStationStates(LHCb::Track& track) const;
  StatusCode createTTState(LHCb::Track& track) const;
  StatusCode createVeloStates(LHCb::Track& track ) const ;
  StatusCode createVeloTTStates(LHCb::Track& track ) const ;

  IPatSeedFit* m_seedFit = nullptr;
  ITrackFitter* m_veloFitter = nullptr;
  IPatVeloTTFit* m_veloTTFit = nullptr;
  ITrackMomentumEstimate* m_fastMomentumTool= nullptr;
  const ITrackExtrapolator* m_extrapolator= nullptr;
  const DeSTDetector* m_ttdetector = nullptr;

  Gaudi::Property<std::string> m_veloFitterName { this, "VeloFitterName" , "Tf::PatVeloFitLHCbIDs/FitVelo"};
  Gaudi::Property<std::string> m_fastMomentumToolName { this,  "FastMomentumToolName", "FastMomentumEstimate"};
  Gaudi::Property<double> m_ptVelo { this,  "ptVelo", 400.*Gaudi::Units::MeV};
  Gaudi::Property<bool> m_useFastMomentumEstimate{ this,  "UseFastMomentumEstimate", false};
  Gaudi::Property<bool> m_reverseVeloCharge{ this,  "reverseVeloCharge", false }; ///< reverse the assigned "random" velo charge

} ;

template<class T>
inline bool LessThanFirst(const T& lhs, const T& rhs)
{
  return lhs.first < rhs.first ;
}
