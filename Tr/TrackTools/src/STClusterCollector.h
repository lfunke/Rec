/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _STClusterCollector_H
#define _STClusterCollector_H 1

#include <map>
#include <string>

// ST tool base class
#include "Kernel/STToolBase.h"

// LHCbKernel
#include "TrackInterfaces/ISTClusterCollector.h"

/** @class STClusterCollector STClusterCollector.h
 *
 *  Class for collecting STClusters around a track
 *  @author M.Needham
 *  @date   28/3/2009
 */

#include "GaudiKernel/IIncidentListener.h"

#include "Event/STCluster.h"
#include "Event/Track.h"

#include <memory>


namespace LHCb{
  class STChannelID;
}

struct ITrackExtrapolator;
struct ITrajPoca;
struct ISTChannelIDSelector;
class IMagneticFieldSvc;

class STClusterCollector final : public extends< ST::ToolBase, ISTClusterCollector, IIncidentListener>  {

 public:

  /// constructor
  STClusterCollector( const std::string& type,
                         const std::string& name,
                         const IInterface* parent );

  /// initialize method
  StatusCode initialize() override;

  /// collect the hits
  StatusCode execute(const LHCb::Track& track,
                     ISTClusterCollector::Hits& outputCont) const override;


  /** Implement the handle method for the Incident service.
  *  This is used to inform the tool of software incidents.
  *
  *  @param incident The incident identifier
  */
  void handle( const Incident& incident ) override;

  typedef std::vector< std::pair<LHCb::STCluster*, std::unique_ptr<LHCb::Trajectory<double>>> > STClusterTrajectories;

 private:

  void initEvent() const;

  bool select(const LHCb::STChannelID& chan) const;

  Gaudi::Property<double> m_xTol { this, "xTol", 20.0 * Gaudi::Units::mm };
  Gaudi::Property<double> m_yTol { this, "yTol", 10.0 * Gaudi::Units::mm };
  double m_refZ;
  Gaudi::Property<double> m_windowSize { this, "window", 20.0 *Gaudi::Units::mm };
  std::string m_dataLocation;
  mutable STClusterTrajectories  m_dataCont;
  mutable bool m_configured = false;
  Gaudi::Property<bool> m_ignoreHitsOnTrack { this, "ignoreHitsOnTrack", true };

  ITrackExtrapolator* m_extrapolator = nullptr;
  ISTChannelIDSelector* m_selector = nullptr;
  ITrajPoca * m_trajPoca = nullptr;
  IMagneticFieldSvc *m_magFieldSvc = nullptr;

  Gaudi::Property<std::string> m_extrapolatorName { this, "extrapolatorName", "TrackMasterExtrapolator" };
  Gaudi::Property<std::string> m_selectorType { this,  "SelectorType", "STSelectChannelIDByElement" };
  Gaudi::Property<std::string> m_selectorName { this,  "SelectorName", "ALL" };
  Gaudi::Property<bool> m_magneticField { this,  "MagFieldOn", true };
};

#endif // STClusterCollector_H
