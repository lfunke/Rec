/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiKernel/IIncidentSvc.h"


// Tsa
#include "TrackCaloMatch.h"
#include "Event/Track.h"
#include "CaloUtils/Calo2Track.h"

using namespace LHCb;

DECLARE_COMPONENT( TrackCaloMatch )


StatusCode TrackCaloMatch::initialize()
{

  StatusCode sc = GaudiTool::initialize();
  if (sc.isFailure()){
    return Error("Failed to initialize", sc);
  }

  m_ecalLocation = rootInTES() + CaloIdLocation::EcalE ;
  m_hcalLocation = rootInTES() + CaloIdLocation::HcalE;
  m_prsLocation = rootInTES()+ CaloIdLocation::PrsE;

  incSvc()->addListener( this, IncidentType::BeginEvent );

  return StatusCode::SUCCESS;
}

double TrackCaloMatch::energy(const Track& aTrack) const{

  // get the input - seeds
  if (!m_configured) initEvent();

  double eEcal = energy(aTrack, m_ecalE) ;
  double eHcal = energy(aTrack, m_hcalE) ;
  double ePrs = energy(aTrack, m_psE);

  // known bug - sometimes ps gives -ve energy
  if (ePrs < 0) {
    eEcal = 0;
    ePrs = 0;
  }

  return (m_alpha * ePrs) + (m_beta * eEcal) + (m_gamma * eHcal) ;

}

double TrackCaloMatch::energy(const Track& aTrack, const TrackCaloMatch::Table* table ) const{

  Table::Range aRange = table->relations( &aTrack ) ;
  return( !aRange.empty()  ? aRange.front().to() : 0);
}

void TrackCaloMatch::handle ( const Incident& incident )
{
  if ( IncidentType::BeginEvent == incident.type() ) { m_configured = false; }
}

void TrackCaloMatch::initEvent() const{

 m_configured = true;

 using namespace LHCb::CaloIdLocation;
 m_ecalE = get<Table>(m_ecalLocation) ;
 m_hcalE = get<Table>(m_hcalLocation) ;
 m_psE = get<Table>(m_prsLocation) ;

}
