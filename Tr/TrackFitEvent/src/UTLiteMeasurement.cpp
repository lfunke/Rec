/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from UTDet
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"
#include "UTDet/DeUTSensor.h"

// from Kernel
#include "TrackInterfaces/IUTClusterPosition.h"

// local
#include "Event/UTLiteMeasurement.h"

using namespace LHCb;

//-----------------------------------------------------------------------------
// Implementation file for class : UTLiteMeasurement
//
// 2005-04-07 : Jose Hernando, Eduardo Rodrigues
// Author: Rutger van der Eijk
// Created: 07-04-1999
//-----------------------------------------------------------------------------

/// Standard constructor, initializes variables
UTLiteMeasurement::UTLiteMeasurement( const UT::Hit& hit,
                                      const DeUTDetector& geom,
                                      const IUTClusterPosition& utClusPosTool)
  : Measurement(Measurement::Type::UTLite, hit.lhcbID(), nullptr)
{
  // Fill the data members
  const DeUTSector* utSector = geom.findSector( hit.chanID() );
  m_detectorElement = utSector ;

  m_errMeasure = utClusPosTool.error(hit.pseudoSize())*utSector->pitch();
  m_trajectory = utSector->trajectory( hit.chanID(), hit.fracStrip()) ;
  m_z = utSector->globalCentre().z();

  // get the best sensor to and go local
  // this is the only way to ensure we get inside a
  // sensor, and not into a bondgap
  m_measure = utSector->middleSensor()->localU( hit.strip() )
              + ( hit.fracStrip() * utSector -> pitch() );

}

