/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "Event/MuonMeasurement.h"
#include "MuonDet/DeMuonDetector.h"
#include "Kernel/LineTraj.h"
#include "LHCbMath/LHCbMath.h"

using namespace LHCb;

//-----------------------------------------------------------------------------
// Implementation file for class : MuonMeasurement
//
// Author: Jose Hernando, Miriam Gandelman, Erica Polycarpo, Silvia Pozzi
// Created: 27-04-2007
//-----------------------------------------------------------------------------

/// Standard constructor, initializes variables
MuonMeasurement::MuonMeasurement( const LHCbID& lhcbID,
				  const DeMuonDetector& muondet,
                                  const Gaudi::XYZPoint& position,
                                  double dx, double dy,
                                  MuonMeasurement::MuonMeasurementType& XY)
  : Measurement(Measurement::Type::Muon,lhcbID,nullptr)
{
  LHCb::MuonTileID muid = lhcbID.muonID();
  // they have promised to fix the const
  const DeMuonChamber* chamber = muondet.getChmbPtr(muid.station(),muid.region(),
						    const_cast<DeMuonDetector&>(muondet).Tile2Chamber(muid).front()->chamberNumber()) ;
  m_detectorElement = chamber ;
  setZ(position.z());
  if( XY == MuonMeasurementType::X) {
    setMeasure(position.x());
    setErrMeasure(2.*dx*LHCb::Math::inv_sqrt_12);
    Gaudi::XYZVector y_dir(0,1,0);
    m_trajectory = std::make_unique<LineTraj<double>>(position, y_dir, std::pair{-dy,dy},
                                                      Trajectory<double>::DirNormalized{true});

  } else {
    setMeasure(position.y());
    setErrMeasure(2.*dy*LHCb::Math::inv_sqrt_12);
    Gaudi::XYZVector x_dir(1,0,0);
    m_trajectory = std::make_unique<LineTraj<double>>(position, x_dir, std::pair{-dx,dx},
                                                      Trajectory<double>::DirNormalized{true});

  }
}

