/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from FTDet
#include "FTDet/DeFTDetector.h"

// from Event
#include "Event/FTCluster.h"

// local
#include "Event/FTMeasurement.h"

using namespace LHCb;

//-----------------------------------------------------------------------------
// Implementation file for class : FTMeasurement
//
//  2012-11-27 Olivier Callot, from STMeasurement
//-----------------------------------------------------------------------------

LHCb::FTMeasurement::FTMeasurement(const LHCb::FTLiteCluster& ftCluster,
                                   const DeFTDetector& det)
:  Measurement( Measurement::Type::FT, LHCbID(ftCluster.channelID()), 0)
{
  const DeFTMat* ftMat = det.findMat( ftCluster.channelID() );
  m_detectorElement = ftMat;
  m_errMeasure =  0.04  + 0.01 * ftCluster.pseudoSize() ; //need a better parametrization
  m_trajectory = ftMat->trajectory(ftCluster.channelID(), ftCluster.fraction());
  m_z = ftMat->globalZ();
  m_measure = 0. ; // JvT: I believe this is purely historical. Should remove it?
}

