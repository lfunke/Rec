/*****************************************************************************\
* (c) Copyright 2018 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from UTDet
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"
#include "UTDet/DeUTSensor.h"

// from Event
#include "Event/UTCluster.h"

// from Kernel
#include "TrackInterfaces/IUTClusterPosition.h"

// local
#include "Event/UTMeasurement.h"

using namespace LHCb;

//-----------------------------------------------------------------------------
/* Implementation file for class : UTMeasurement
 *
 * @author A. Beiter based on code by Jose Hernando,
 *          Eduardo Rodrigues, and Rutger van der Eijk
 * @date 2018-09-04
*/
//-----------------------------------------------------------------------------

/// Standard constructor, initializes variables
UTMeasurement::UTMeasurement( const UTCluster& utCluster,
                              const DeUTDetector& geom,
                              const IUTClusterPosition& utClusPosTool)
  : Measurement( Measurement::Type::UT, LHCbID(utCluster.channelID()), 0), m_cluster(&utCluster)
{
  // Fill the data members
  m_mtype = Measurement::Type::UT;

  // Get the corresponding sensor
  const DeUTSector* utSector = geom.findSector( m_cluster->channelID() );
  m_detectorElement = utSector ;

  // Get the centre of gravity and the measurement error
  IUTClusterPosition::Info measVal = utClusPosTool.estimate( m_cluster );

  m_errMeasure = measVal.fractionalError*utSector -> pitch();
  m_size = measVal.clusterSize;
  m_trajectory = utSector->trajectory( measVal.strip, measVal.fractionalPosition) ;
  m_z = utSector->globalCentre().z();

  // get the best sensor to and go local
  // this is the only way to ensure we get inside a
  // sensor, and not into a bondgap
  m_measure = utSector->middleSensor()->localU( m_cluster->strip() )
              + ( measVal.fractionalPosition* utSector -> pitch() );

}
