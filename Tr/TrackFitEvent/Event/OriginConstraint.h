/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKFITEVENT_ORIGINCONSTRAINT_H
#define TRACKFITEVENT_ORIGINCONSTRAINT_H

// Include files
#include "Event/Measurement.h"

// Forward declarations

namespace LHCb
{

  // Forward declarations
  class ChiSquare ;
  class State ;

  class OriginConstraint final : public LHCb::Measurement
  {
  public:
    OriginConstraint( const Gaudi::XYZPoint& point,
		      const Gaudi::SymMatrix3x3& covariance ) ;

    /// Clone the OTMeasurement
    LHCb::Measurement* clone() const override { return new OriginConstraint(*this) ; }

    /// filter this constraint. returns the chi2
    LHCb::ChiSquare filter( LHCb::State& state,
			    const Gaudi::TrackVector& reference) const ;

  private:
    Gaudi::XYZPoint m_origin ;
    Gaudi::SymMatrix3x3 m_weight ; // weight matrix (inverse covariance)
  };
}
#endif
