/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// track interfaces
#include "Event/Track.h"

#include "TrackInterfaces/ITrackManipulator.h"
#include "TrackComputeExpectedHits.h"
#include "GaudiKernel/ToStream.h"

#include "TrackInterfaces/IHitExpectation.h"
#include "TrackInterfaces/IVeloExpectation.h"
#include "Kernel/HitPattern.h"

using namespace LHCb;

DECLARE_COMPONENT( TrackComputeExpectedHits )

TrackComputeExpectedHits::TrackComputeExpectedHits(const std::string& name,
                                                   ISvcLocator* pSvcLocator)
: GaudiAlgorithm(name, pSvcLocator)
{
    declareProperty("inputLocation", m_inputLocation = TrackLocation::Default);
}


StatusCode TrackComputeExpectedHits::initialize() {

  m_veloExpectation = tool<IVeloExpectation>("VeloExpectation");
  m_ttExpectation = tool<IHitExpectation>("TTHitExpectation");
  m_itExpectation = tool<IHitExpectation>("ITHitExpectation");
  m_otExpectation = tool<IHitExpectation>("OTHitExpectation");

  return StatusCode::SUCCESS;
}

StatusCode TrackComputeExpectedHits::execute(){

  Tracks* inCont = get<Tracks>(m_inputLocation);

  // loop
  for (const auto& aTrack : *inCont) {

    std::vector<LHCb::LHCbID > ids;

    IVeloExpectation::VeloPattern velo[4];
    m_veloExpectation->expectedInfo(*aTrack,velo);
    m_ttExpectation->collect(*aTrack, ids);
    m_itExpectation->collect(*aTrack, ids);
    m_otExpectation->collect(*aTrack, ids);

    LHCb::HitPattern hitPattern = LHCb::HitPattern(ids);
    hitPattern.setVeloRA(velo[0]);
    hitPattern.setVeloRC(velo[1]);
    hitPattern.setVeloPhiA(velo[2]);
    hitPattern.setVeloPhiC(velo[3]);


    //    aTrack->setExpectedHitPattern(hitPattern);

  } // iterT

  return StatusCode::SUCCESS;
}
