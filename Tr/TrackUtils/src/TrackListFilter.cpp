/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class TrackListFilter TrackListFilter.h
 *
 *  Algorithm to filter events in which a track list is not empty
 *
 *  @author W. Hulsbergen
 *  @date   2008
 */

// Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include <string>

// track interfaces
#include "Event/Track.h"

class TrackListFilter: public GaudiAlgorithm
{
public:
  TrackListFilter(const std::string& name,
              ISvcLocator* pSvcLocator);
  StatusCode execute() override;

private:
  std::string m_inputLocation;
};


DECLARE_COMPONENT( TrackListFilter )

TrackListFilter::TrackListFilter(const std::string& name,
                       ISvcLocator* pSvcLocator):
  GaudiAlgorithm(name, pSvcLocator)
{
  // constructor
  declareProperty( "inputLocation",
		   m_inputLocation  = LHCb::TrackLocation::Default );
}

StatusCode TrackListFilter::execute()
{
  LHCb::Track::Range tracks = get<LHCb::Track::Range>(m_inputLocation);
  setFilterPassed( !tracks.empty() );
  return  StatusCode::SUCCESS ;
}
