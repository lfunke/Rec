/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// local
#include "TrajProjector.h"

//-----------------------------------------------------------------------------
/// Standard constructor, initializes variables
//-----------------------------------------------------------------------------
template <typename T>
TrajProjector<T>::TrajProjector( const std::string& type,
                                 const std::string& name,
                                 const IInterface* parent )
  : TrackProjector( type, name, parent )
{
  setProperty( "Tolerance", T::defaultTolerance() );
}

/// declare and instantiate ST and Velo projectors...
struct ST {
   static constexpr double defaultTolerance() { return 0.002*Gaudi::Units::mm; }
};

typedef TrajProjector<ST> TrajSTProjector;
DECLARE_COMPONENT( TrajSTProjector )

struct UT {
   static constexpr double defaultTolerance() { return 0.002*Gaudi::Units::mm; }
};

typedef TrajProjector<UT> TrajUTProjector;
DECLARE_COMPONENT( TrajUTProjector )

struct Velo {
  static constexpr double defaultTolerance() { return 0.0005*Gaudi::Units::mm; }
};

typedef TrajProjector<Velo> TrajVeloProjector;
DECLARE_COMPONENT( TrajVeloProjector )

struct VP {
  static constexpr double defaultTolerance() { return 0.0005*Gaudi::Units::mm; }
};

typedef TrajProjector<VP> TrajVPProjector;
DECLARE_COMPONENT( TrajVPProjector )

struct FT {
  static constexpr double defaultTolerance() { return 0.002*Gaudi::Units::mm; }
};

typedef TrajProjector<FT> TrajFTProjector;
DECLARE_COMPONENT( TrajFTProjector )

struct Muon {
  static constexpr double defaultTolerance() { return 0.002*Gaudi::Units::mm; }
};

typedef TrajProjector<Muon> TrajMuonProjector;
DECLARE_COMPONENT( TrajMuonProjector )
