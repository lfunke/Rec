/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from TrackFitEvent
#include "Event/OTMeasurement.h"
#include "Event/StateVector.h"

// local
#include "TrajOTProjector.h"

// Namespaces
using namespace Gaudi;
using namespace LHCb;
using ROOT::Math::SMatrix;

DECLARE_COMPONENT( TrajOTProjector )

//-----------------------------------------------------------------------------
/// Project a state onto a measurement
/// It returns the chi squared of the projection
//-----------------------------------------------------------------------------
TrackProjector::InternalProjectResult
TrajOTProjector::internal_project (const LHCb::StateVector& statevector,
                                   const Measurement& genMeas) const {
  // check that we ar dealing with an OTMeasurement
  if (!genMeas.checkType(LHCb::Measurement::Type::OT)) {
    throw StatusCode::FAILURE;
  }
  const LHCb::OTMeasurement& meas = static_cast<const LHCb::OTMeasurement&>(genMeas);
  OTMeasurement& nonconstmeas = const_cast<OTMeasurement&>(meas) ;

  // call the standard tracjectory-doca projector
  auto result = TrackProjector::internal_project(statevector, meas);

  result.errMeasure = meas.module().cellRadius()/std::sqrt(3.0) ;
  nonconstmeas.setDriftTimeStrategy(LHCb::OTMeasurement::DriftTimeStrategy::DriftTimeIgnored);

  // Is this a 'prefit'?
  bool prefit = m_prefitStrategy!=NoPrefit && meas.ambiguity() == 0 ;

  // is the ambiguity set by the LR sign tool?
  bool ambiguityIsFixed = std::abs( meas.ambiguity() ) == 2 ;

  // if not, set the ambiguity "on the fly"
  if( !ambiguityIsFixed && m_updateAmbiguity ) {
    nonconstmeas.setAmbiguity(result.doca > 0 ? 1 : -1);
  }

  // check that the drifttime is not out of range
  double measureddrifttime = meas.driftTime(result.sMeas) ;
  const OTDet::RtRelation& rtr = meas.module().rtRelation() ;
  bool gooddrifttime = useDriftTime() &&
    measureddrifttime > -m_outOfTimeTolerance &&
    measureddrifttime < rtr.tmax() + m_outOfTimeTolerance ;

  if ( gooddrifttime ) {
    // a zero ambiguity may indicate that we should not use the drifttime
    if(prefit) {
      if(m_prefitStrategy==TjeerdKetel) {
      nonconstmeas.setDriftTimeStrategy(LHCb::OTMeasurement::DriftTimeStrategy::PreFit);
      // This is the implementation of a proprosal by Tjeerd: assign
      // error based on the driftdistance
      OTDet::RadiusWithError radiusWithError = meas.driftRadiusWithError( result.sMeas );
      result.errMeasure = std::sqrt( radiusWithError.val * radiusWithError.val +
                                     radiusWithError.err * radiusWithError.err ) ;
      }
    } else if(meas.ambiguity()!=0) {
      // we have a large fraction of hits with very poor drifttime
      // measurements. if they are more than 3 sigma of the
      // prediction, we'll not use driftttime.

      if( m_maxDriftTimePull>0 ) {
        decltype(rtr.rmax()) predictedradius = std::min( rtr.rmax(), std::abs(static_cast<decltype(rtr.rmax())>(result.doca)) ) ;
        OTDet::DriftTimeWithError predictedtime = rtr.drifttimeWithError( predictedradius ) ;
        gooddrifttime = std::abs( measureddrifttime - predictedtime.val ) / predictedtime.err < m_maxDriftTimePull ;
      }

      if(gooddrifttime ) {
        int lrsign =  meas.ambiguity() > 0 ? 1 : -1 ;
        if(fitDriftTime()) {
          decltype(rtr.rmax()) radius = std::min( rtr.rmax(), std::abs(static_cast<decltype(rtr.rmax())>(result.doca)) ) ;
          OTDet::DriftTimeWithError predictedtime = rtr.drifttimeWithError( radius ) ;
          float dtdr                              = rtr.dtdr( radius ) ;
          result.residual   = measureddrifttime - predictedtime.val ;
          result.errMeasure = predictedtime.err ;
          result.H          *= ( lrsign * dtdr ) ;
          nonconstmeas.setDriftTimeStrategy(LHCb::OTMeasurement::DriftTimeStrategy::FitTime);
        } else {
          OTDet::RadiusWithError radiusWithError(meas.driftRadiusWithError(result.sMeas)) ;
          result.residual = -result.doca + lrsign * radiusWithError.val ;
          result.errMeasure = radiusWithError.err ;
          nonconstmeas.setDriftTimeStrategy(LHCb::OTMeasurement::DriftTimeStrategy::FitDistance);
        }
      }
    }
  }

  return result;
}
//-----------------------------------------------------------------------------
/// Initialize
//-----------------------------------------------------------------------------
StatusCode TrajOTProjector::initialize()
{
  StatusCode sc = TrackProjector::initialize();
  if(msgLevel(MSG::DEBUG)) debug() << "Use drifttime           = "
                                   << m_useDriftTime << endmsg
                                   << "Fit drifttime residuals = "
                                   << m_fitDriftTime << endmsg
                                   << "Prefit strategy = "
                                   << m_prefitStrategy << endmsg ;
  return sc;
}

//-----------------------------------------------------------------------------
/// Standard constructor, initializes variables
//-----------------------------------------------------------------------------
TrajOTProjector::TrajOTProjector( const std::string& type,
                                  const std::string& name,
                                  const IInterface* parent )
: TrackProjector( type, name, parent )
{
  m_tolerance = 0.001 ;
}
