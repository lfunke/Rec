/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "TrackSimpleExtraSelector.h"

// from TrackInterfaces
#include "TrackInterfaces/ITrackExtrapolator.h"

DECLARE_COMPONENT( TrackSimpleExtraSelector )


//=============================================================================
// Initialization
//=============================================================================
StatusCode TrackSimpleExtraSelector::initialize()
{
  // initialize
  StatusCode sc = GaudiTool::initialize();
  if (sc.isFailure()) return Error("Failed to initialize", sc);
  m_extrapolator= tool<ITrackExtrapolator>( m_extraName, m_extraName, this );
  return StatusCode::SUCCESS;
}

//=============================================================================
//
//=============================================================================
const ITrackExtrapolator*
TrackSimpleExtraSelector::select( double, double) const
{
  return m_extrapolator;
}

//=============================================================================
