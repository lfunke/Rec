/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKEXTRAPOLATORS_TRACKFASTMATERIALINTERSECTOR_H
#define TRACKEXTRAPOLATORS_TRACKFASTMATERIALINTERSECTOR_H

// Include files
// -------------
// from Gaudi
#include "GaudiAlg/GaudiTool.h"

// from TrackInterfaces
#include "MaterialLocatorBase.h"

// from std
#include <vector>

namespace MaterialLocatorUtils {
  class PVolumeWrapper ;
}

/** @class SimplifiedMaterialLocator SimplifiedMaterialLocator.h \
 *         "SimplifiedMaterialLocator.h"
 *
 * Implementation of a IMaterialLocator that uses a simplified geometry
 * finding materials on a trajectory.
 *
 *  @author Wouter Hulsbergen
 *  @date   21/05/2007
 */

class SimplifiedMaterialLocator : public MaterialLocatorBase
{
public:
  /// Constructor
  using MaterialLocatorBase::MaterialLocatorBase;

  /// intialize
  StatusCode initialize() override;
  StatusCode finalize() override;

  using MaterialLocatorBase::intersect;
  using MaterialLocatorBase::intersect_r;

  /// Intersect a line with volumes in the geometry
  size_t intersect( const Gaudi::XYZPoint& p,
                    const Gaudi::XYZVector& v,
                    ILVolume::Intersections& intersepts ) const override;

  /// Intersect a line with volumes in the geometry
  size_t intersect_r( const Gaudi::XYZPoint& p,
                      const Gaudi::XYZVector& v,
                      ILVolume::Intersections& intersepts,
                      std::any& ) const override
  {
    return intersect( p, v, intersepts );
  }

private:

  Gaudi::Property<std::string> m_tgvolname {this, "Geometry", "/dd/TrackfitGeometry/Geometry/lvLHCb" };   ///< path to the tracking geometry
  typedef std::vector<std::unique_ptr<MaterialLocatorUtils::PVolumeWrapper>> VolumeContainer ;
  VolumeContainer m_volumes ; ///< wrappers around geo volumes to speed things up

};

#endif // TRACKEXTRAPOLATORS_TRACKMATERIALINTERSECTORBASE_H
