/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKEXTRAPOLATORS_TRACKEXTRAPOLATOR_H
#define TRACKEXTRAPOLATORS_TRACKEXTRAPOLATOR_H 1

// Include files
// -------------
// from Gaudi
#include "GaudiAlg/GaudiTool.h"

// from TrackInterfaces
#include "TrackInterfaces/ITrackExtrapolator.h"

// from TrackEvent
#include "Event/Track.h"
#include "Event/State.h"
#include "Event/StateVector.h"


/** @class TrackExtrapolator TrackExtrapolator.h
 *
 *  A TrackExtrapolator is a base class implementing methods
 *  from the ITrackExtrapolator interface.
 *
 *  @author Edwin Bos (added extrapolation methods)
 *  @date   05/07/2005
 *
 *  @author Eduardo Rodrigues
 *  @date   2004-12-17
 */
class TrackExtrapolator : public extends<GaudiTool, ITrackExtrapolator> {
public:
  /// Standard constructor
  using base_class::base_class;

  using ITrackExtrapolator::propagate;

  /// Whether it uses grid interpolation for the magnetic field
  bool usesGridInterpolation() const override {
    return true;
  }

  /// Propagate a state vector from zOld to zNew
  StatusCode propagate( Gaudi::TrackVector& stateVec,
                        double zOld,
                        double zNew,
                        const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const override;

  /// Propagate a track to a given z-position
  StatusCode propagate( const LHCb::Track& track,
                        double z,
                        LHCb::State& state,
                        const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const override;

  /// Propagate a track to a given z-position
  StatusCode propagate(const LHCb::Track&,
		double z,
		LHCb::StateVector&,
		const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const override;

  /// Propagate a state to a given z-position
  StatusCode propagate( LHCb::State& state,
                        double z,
                        const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const override;

  /// Propagate a state to a given z-position
  /// Transport matrix is calulated when transMat pointer is not NULL
  StatusCode propagate( LHCb::State& state,
                        double z,
                        Gaudi::TrackMatrix* transMat,
                        const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const override;

  /// Propagate a statevector to a given z-position
  StatusCode propagate( LHCb::StateVector& state,
				double z,
				Gaudi::TrackMatrix* transportmatrix,
				const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const override;

  /// Propagate a track to the closest point to the specified point
  StatusCode propagate( const LHCb::Track& track,
                        const Gaudi::XYZPoint& point,
                        LHCb::State& state,
                        const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const override;

  /// Propagate a state to the closest point to the specified point
  StatusCode propagate( LHCb::State& state,
                        const Gaudi::XYZPoint& point,
                        const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const override;

  /// Propagate a track to within tolerance of a plane (default = 10 microns)
  StatusCode propagate( const LHCb::Track& track,
                        const Gaudi::Plane3D& plane,
                        LHCb::State& state,
                        double tolerance = 0.01,
                        const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const override;

  /// Propagate a state to within tolerance of a plane (default = 10 microns)
  StatusCode propagate( LHCb::State& state,
                        const Gaudi::Plane3D& plane,
                        double tolerance = 0.01,
                        const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const override;

  //--------- ACCESS METHOD ---------------------------------------

  /** Retrieve the position and momentum vectors and the corresponding
      6D covariance matrix (pos:1->3,mom:4-6) of a track at a given z-position
      @return status code
      @param[out] pos     the position of the track
      @param[out] mom     the momentum of the track
      @param[out] cov6D   the full position-momentum covariance matrix
      @param[in]  track   the input track
      @param[in]  z       the z-position at which to extrapolate
      @param[in]  pid     pid of the particle to extrapolate (default is pion)
  */
  StatusCode positionAndMomentum( const LHCb::Track& track,
                                  double z,
                                  Gaudi::XYZPoint& pos,
                                  Gaudi::XYZVector& mom,
                                  Gaudi::SymMatrix6x6& cov6D,
                                  const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const override;

  /// Retrieve the position and momentum vectors of a track at a given z-position
  StatusCode positionAndMomentum( const LHCb::Track& track,
                                  double z,
                                  Gaudi::XYZPoint& pos,
                                  Gaudi::XYZVector& mom,
                                  const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const override;

  /// Retrieve the 3D-position vector and error matrix of a track at a given z-position
  StatusCode position( const LHCb::Track& track,
                       double z,
                       Gaudi::XYZPoint& pos,
                       Gaudi::SymMatrix3x3& errPos,
                       const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const override;

  /// Retrieve the 3D-position vector of a track at a given z-position
  StatusCode position( const LHCb::Track& track,
                       double z,
                       Gaudi::XYZPoint& pos,
                       const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const override;

  /// Retrieve the slopes (dx/dz,dy/dz,1) and error matrix of a track at a given z-position
  StatusCode slopes( const LHCb::Track& track,
                     double z,
                     Gaudi::XYZVector& slopes,
                     Gaudi::SymMatrix3x3& errSlopes,
                     const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const override;

  /// Retrieve the slopes (dx/dz,dy/dz,1) of a track at a given z-position
  StatusCode slopes( const LHCb::Track& track,
                     double z,
                     Gaudi::XYZVector& slopes,
                     const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const override;

  /// Retrieve the momentum of a track at a given z-position
  StatusCode p( const LHCb::Track& track,
                double z,
                double& p,
                const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const override;

  /// Retrieve the transverse momentum of a track at a given z-position
  StatusCode pt( const LHCb::Track& track,
                 double z,
                 double& pt,
                 const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const override;

  /// Retrieve the momentum vector and error matrix of a track at a given z-position
  StatusCode momentum( const LHCb::Track& track,
                       double z,
                       Gaudi::XYZVector& mom,
                       Gaudi::SymMatrix3x3& errMom,
                       const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const override;

  /// Retrieve the momentum vector of a track at a given z-position
  StatusCode momentum( const LHCb::Track& track,
                       double z,
                       Gaudi::XYZVector& mom,
                       const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const override;


private :

  /// Maximum number of steps in propagation to a plane
  Gaudi::Property<int> m_maxIter { this,  "Iterations", 5 };

};
#endif // TRACKEXTRAPOLATORS_TRACKEXTRAPOLATOR_H
