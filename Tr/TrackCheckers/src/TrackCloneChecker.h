/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: TrackCloneChecker.h,v 1.3 2007-09-17 14:34:47 jonrob Exp $
#ifndef TRACKCLONECHECKER_H
#define TRACKCLONECHECKER_H 1

#include <map>

#include "TrackCheckerBase.h"

// from Event/LinkerEvent
#include "Linker/LinkedFrom.h"

// Boost
#include "boost/lexical_cast.hpp"
#include "boost/format.hpp"

/** @class TrackCloneChecker TrackCloneChecker.h
 *
 *  Produce some simple plots for the Clone linker information
 *
 *  @author Chris Jones
 *  @date   2007-09-13
 */

class TrackCloneChecker : public TrackCheckerBase
{

public:

  /// Standard constructor
  TrackCloneChecker( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~TrackCloneChecker( ); ///< Destructor

  StatusCode initialize() override;  ///< Algorithm initialize

  StatusCode execute() override;  ///< Algorithm execution

  StatusCode finalize() override;  ///< Algorithm finalize

private:

  /** @class TrackTally TrackCloneChecker.h
   *
   *  Counts track information for clones
   *
   *  @author Chris Jones
   *  @date   2007-09-13
   */
  class TrackTally
  {
  public:
    TrackTally()
      : totalClones(0), totalNonClones(0), totalGhosts(0),
        rejectedClones(0), rejectedNonClones(0), rejectedGhosts(0) { }
  public:
    unsigned long totalClones;
    unsigned long totalNonClones;
    unsigned long totalGhosts;
    unsigned long rejectedClones;
    unsigned long rejectedNonClones;
    unsigned long rejectedGhosts;
  public:
    /// Map for one tally object per track history type
    typedef std::map<LHCb::Track::History,TrackTally> Map;
  };

private:

  /// Does the given MCP have reconstructed clones
  bool hasMCClone( const LHCb::MCParticle * mcP );

  /// Get efficiency
  inline std::pair<double,double> getEff1( const double top,
                                           const double bot ) const
  {
    return std::pair<double,double>( ( bot>0 ? 100.0 * top/bot                          : 0 ),
                                     ( bot>0 ? 100.0 * sqrt((top/bot)*(1.-top/bot)/bot) : 0 ) );
  }

  /// Get efficiency
  inline std::pair<double,double> getEff2( const double top,
                                           const double bot ) const
  {
    return std::pair<double,double>( ( bot>0 ? top/bot       : 0 ),
                                     ( bot>0 ? sqrt(top)/bot : 0 ) );
  }

private:

  /// Clone linker information location in TES
  std::string m_cloneInfoTES;

  /// Summary map
  TrackTally::Map m_trackMap;

  /// KL distance cut
  double m_klCut;

  /// Event count
  unsigned long m_nEvts;

};

inline bool TrackCloneChecker::hasMCClone( const LHCb::MCParticle * mcP )
{
  return mcP ? reconstructedTracks(mcP).size() > 1 : false;
}

#endif // TRACKCLONECHECKER_H
