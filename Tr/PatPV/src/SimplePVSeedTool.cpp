/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "SimplePVSeedTool.h"

//-----------------------------------------------------------------------------
// Implementation file for class : SimplePVSeedTool
//
// 2005-11-19 : Mariusz Witek
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( SimplePVSeedTool )

//=============================================================================
// getSeeds
//=============================================================================
std::vector<Gaudi::XYZPoint>
SimplePVSeedTool::getSeeds(const std::vector<const LHCb::Track*>& inputTracks,
                           const Gaudi::XYZPoint& beamspot) const {

  std::vector<Gaudi::XYZPoint> seeds;
  if(inputTracks.size() < 3 ) return seeds;

  if(msgLevel(MSG::DEBUG))  {
     debug() << " Beam spot is ignored. BS: " << beamspot << endmsg;
  }

  seeds.emplace_back( 0., 0., 0.);
  return seeds;
}

//=============================================================================
