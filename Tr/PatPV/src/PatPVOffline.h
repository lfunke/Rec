/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PATPVOFFLINE_H
#define PATPVOFFLINE_H 1
// Include files:
// from Gaudi
#include "GaudiAlg/Transformer.h"
// Interfaces
#include "TrackInterfaces/IPVOfflineTool.h"
// Local
#include "Event/Track.h"
#include "Event/RecVertex.h"

/** @class PatPVOffline PatPVOffline.h
 *
 *
 *  @author Mariusz Witek
 *  @date   2010-10-05
 */
class PatPVOffline : public Gaudi::Functional::Transformer<std::vector<LHCb::RecVertex>(const std::vector<LHCb::Track>&)> {
public:
  // Standard constructor
  PatPVOffline(const std::string& name, ISvcLocator* pSvcLocator);

  /// Algorithm execution
  std::vector<LHCb::RecVertex> operator()(const std::vector<LHCb::Track>&) const override;

private:
  // Tools
  ToolHandle<IPVOfflineTool> m_pvsfit {"PVOfflineTool",this};
};
#endif // PATPVOFFLINE_H
