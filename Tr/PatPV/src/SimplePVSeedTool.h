/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PATPV_SIMPLEPVSEEDTOOL_H
#define PATPV_SIMPLEPVSEEDTOOL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "IPVSeeding.h"            // Interface
#include "Event/Track.h"


/** @class SimplePVSeedTool SimplePVSeedTool.h tmp/SimplePVSeedTool.h
 *
 *
 *  @author Mariusz Witek
 *  @date   2005-11-19
 */
class SimplePVSeedTool : public extends<GaudiTool, IPVSeeding> {
public:

  /// Standard constructor
  using extends::extends;

  std::vector<Gaudi::XYZPoint> getSeeds(const std::vector<const LHCb::Track*>& inputTracks,
		                                const Gaudi::XYZPoint& beamspot) const override;

};
#endif // PATPV_SIMPLEPVSEEDTOOL_H
