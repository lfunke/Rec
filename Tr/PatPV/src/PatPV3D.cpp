/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
// -------------

// From Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// Local
#include "PatPV3D.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PatPV3D
//
// 2004-02-17 : Eduardo Rodrigues
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( PatPV3D )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PatPV3D::PatPV3D(const std::string& name,
                 ISvcLocator* pSvcLocator) :
MultiTransformerFilter(name , pSvcLocator,
                       KeyValue{"InputTracks", LHCb::TrackLocation::Default},
                       KeyValue("OutputVerticesName", LHCb::RecVertexLocation::Velo3D)) {
}

//=============================================================================
// Main execution
//=============================================================================
std::tuple<bool, std::vector<LHCb::RecVertex>>
PatPV3D::operator()(const std::vector<LHCb::Track>& inputTracks) const {

  MsgStream msg(msgSvc(), name());
  bool isDebug = msg.level() <= MSG::DEBUG;

  if (isDebug) {
    debug() << "==> Execute" << endmsg;
  }

  std::vector<LHCb::RecVertex> rvts;
  rvts.reserve(20);
  bool filter = false;
  StatusCode scfit = m_pvsfit->reconstructMultiPV(inputTracks, rvts);
  if (scfit == StatusCode::SUCCESS) {
    filter = !rvts.empty();
  } else {
    Warning("reconstructMultiPV failed!",scfit).ignore();
  }

  m_nbPVsCounter += rvts.size();
  return std::make_tuple(filter, std::move(rvts));
}
