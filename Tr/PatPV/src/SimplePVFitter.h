/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SIMPLEPVFITTER_H
#define SIMPLEPVFITTER_H 1
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
// Interfaces
#include "IPVFitter.h"
#include "PVUtils.h"
// Track info
#include "Event/Track.h"
#include "Event/State.h"
#include "Event/RecVertex.h"
#include "TrackInterfaces/ITrackExtrapolator.h"

class SimplePVFitter : public extends<GaudiTool, IPVFitter> {

public:
  // Standard constructor
  using extends::extends;
  // Fitting
  StatusCode fitVertex(const Gaudi::XYZPoint& seedPoint,
                       const std::vector<const LHCb::Track*>& tracks,
                       LHCb::RecVertex& vtx,
                       std::vector<const LHCb::Track*>& tracks2remove) const override;
private:
  Gaudi::Property<int>    m_minTr { this, "MinTracks", 5 ,
    [=](Property&) {
        if (this->m_minTr<3) { // do not allow less than 3 tracks in a vertex
            this->m_minTr = 3;
            this->warning() << "MinTracks must be at least 3. Set to 3" << endmsg;
        }
    }, "Minimum number of tracks to make a vertex" };
  Gaudi::Property<int>    m_Iterations { this, "Iterations", 30, "Number of iterations for minimisation" };
  Gaudi::Property<double> m_maxChi2{ this, "maxChi2", 256.0, "Chi2 of completely wrong tracks" };
  Gaudi::Property<double> m_extrapRCut {this, "LinExtrapolation", 5.0 * Gaudi::Units::mm, "Radial limit for linear extrapolation" };
  Gaudi::Property<double> m_maxDeltaZ { this, "maxDeltaZ", 0.001 * Gaudi::Units::mm, "Fit convergence condition" };
  Gaudi::Property<double> m_acceptTrack { this, "acceptTrack", 0.00001, "Value of Tukey's weight to accept a track" };
  Gaudi::Property<double> m_trackMaxChi2Remove{ this, "trackMaxChi2Remove", 36., "Max chi2 tracks to be removed from next PV search"};

  // Extrapolators
  ToolHandle<ITrackExtrapolator> m_linExtrapolator { this, "LinearExtrapolator","TrackLinearExtrapolator"};
  ToolHandle<ITrackExtrapolator> m_fullExtrapolator { this, "FullExtrapolator","TrackMasterExtrapolator"};

  // Least square iterative PV fit
  StatusCode fit(LHCb::RecVertex& vtx,std::vector<PVTrack*>& pvTracks, std::vector<const LHCb::Track*>& tracks2remove) const;
  // Add track for PV
  StatusCode addTrackForPV(const LHCb::Track* str,std::vector<PVTrack>& pvTracks,
                           double zseed) const;

  void initVertex(PVTracks& pvTracks,
                  PVVertex& pvVertex,
                  const Gaudi::XYZPoint seedPoint) const;

  // Prepare hessian matrix and vectorD0
  void prepareVertex(LHCb::RecVertex& vtx,
                    PVTrackPtrs& pvTracks,
                    Gaudi::SymMatrix3x3& hess,
                    ROOT::Math::SVector<double,3>& d0vec,
                    int iter) const;
  // Extrapolation
  StatusCode trackExtrapolate(PVTrack* pvTrack,
                              const LHCb::RecVertex& vtx) const;
  // Add track to fit
  void addTrack(PVTrack* pTrack,
                Gaudi::SymMatrix3x3& hessian,
                ROOT::Math::SVector<double,3>& vectorD0) const;
  // Remove track from fit
  void removeTrack(PVTrack* pTrack,
                Gaudi::SymMatrix3x3& hessian,
                ROOT::Math::SVector<double,3>& vectorD0) const;
  // Add subtrack
  void addsubTrack(PVTrack* pTrack,
                   Gaudi::SymMatrix3x3& hessian,
                   ROOT::Math::SVector<double,3>& vectorD0,
                   double invs) const;
  // Update matrices and vectors
  StatusCode outVertex(LHCb::RecVertex& vtx,
                       PVTrackPtrs& pvTracks,
                       Gaudi::SymMatrix3x3& hess,
                       ROOT::Math::SVector<double,3>& d0vec) const;
  // Set current chi2
  void setChi2(LHCb::RecVertex& vtx,
               PVTrackPtrs& pvTracks) const;

  // Get Tukey's weight
  double getTukeyWeight(double trchi2, int iter) const;
};
#endif // SIMPLEPVFITTER_H
