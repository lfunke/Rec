/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Map.h"

const TrackIdealPR::CatMap& TrackIdealPR::catToType()
{
   static const TrackIdealPR::CatMap s_map = { { IMCReconstructible::ChargedVelo,LHCb::Track::Types::Velo },
                                               { IMCReconstructible::ChargedLong,LHCb::Track::Types::Long },
                                               { IMCReconstructible::ChargedUpstream,LHCb::Track::Types::Upstream },
                                               { IMCReconstructible::ChargedDownstream,LHCb::Track::Types::Downstream },
                                               { IMCReconstructible::ChargedTtrack,LHCb::Track::Types::Ttrack } };
   return s_map;
}
