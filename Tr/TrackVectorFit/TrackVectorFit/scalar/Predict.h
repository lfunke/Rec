/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "../Types.h"
#include "Math.h"

namespace Tr {

namespace TrackVectorFit {

namespace Scalar {

template<class T>
inline void initialize (
  Node& node,
  const std::array<TRACKVECTORFIT_PRECISION, 15>& covariance
) {
  node.get<T, Op::StateVector>().copy(node.get<Op::NodeParameters, Op::ReferenceVector>());
  node.get<T, Op::Covariance>().copy(covariance);
}

template<class T>
inline void predict (
  Node& node,
  const Node& prevnode
);

template<>
inline void predict<Op::Forward> (
  Node& node,
  const Node& prevnode
) {
  node.get<Op::Forward, Op::StateVector>().copy(node.get<Op::NodeParameters, Op::TransportVector>());
  for (int i=0; i<5; ++i) {
    for (int j=0; j<5; ++j) {
      node.get<Op::Forward, Op::StateVector>()[i] += 
        node.get<Op::Forward, Op::TransportMatrix>()[5*i + j] *
        prevnode.get<Op::Forward, Op::StateVector>()[j];
    }
  }

  Math::transportCovariance(
    node.get<Op::Forward, Op::TransportMatrix>(),
    prevnode.get<Op::Forward, Op::Covariance>(),
    node.get<Op::Forward, Op::Covariance>());
  node.get<Op::Forward, Op::Covariance>() += node.get<Op::NodeParameters, Op::NoiseMatrix>();
}

template<>
inline void predict<Op::Backward> (
  Node& node,
  const Node& prevnode
) {
  std::array<TRACKVECTORFIT_PRECISION, 5> temp_sub;
  for (int i=0; i<5; ++i) {
    temp_sub[i] = prevnode.get<Op::Backward, Op::StateVector>()[i] - prevnode.get<Op::NodeParameters, Op::TransportVector>()[i];
  };

  for (int i=0; i<5; ++i) {
    node.get<Op::Backward, Op::StateVector>()[i] = node.get<Op::Backward, Op::TransportMatrix>()[5*i] * temp_sub[0];
    for (int j=1; j<5; ++j) {
      node.get<Op::Backward, Op::StateVector>()[i] += node.get<Op::Backward, Op::TransportMatrix>()[5*i + j] * temp_sub[j];
    }
  }

  std::array<TRACKVECTORFIT_PRECISION, 15> temp_cov;
  for (int i=0; i<15; ++i) {
    temp_cov[i] = prevnode.get<Op::Backward, Op::Covariance>()[i] + prevnode.get<Op::NodeParameters, Op::NoiseMatrix>()[i];
  }

  Math::transportCovariance (
    node.get<Op::Backward, Op::TransportMatrix>(),
    temp_cov,
    node.get<Op::Backward, Op::Covariance>()
  );
}

}

}

}
