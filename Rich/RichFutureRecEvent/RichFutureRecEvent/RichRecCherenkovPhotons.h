/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <string>

#include "RichFutureUtils/RichGeomPhoton.h"
#include "RichFutureUtils/RichSIMDGeomPhoton.h"

namespace Rich::Future::Rec
{

  /** @class CherenkovPhoton RichFutureRecEvent/RichRecCherenkovPhotons.h
   *
   *  A scalar reconstructed photon object.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  using CherenkovPhoton = Rich::Future::RecoPhoton;

  /// TES locations
  namespace CherenkovPhotonLocation
  {
    /// Default Location in TES for the scalar photons
    inline const std::string Default = "Rec/RichFuture/CherenkovPhotons/Default";
  } // namespace CherenkovPhotonLocation

  /** @class SIMDCherenkovPhoton RichFutureRecEvent/RichRecCherenkovPhotons.h
   *
   *  A SIMD vectorised reconstructed photon object.
   *
   *  @author Chris Jones
   *  @date   2017-10-13
   */
  using SIMDCherenkovPhoton = Rich::SIMD::Future::RecoPhoton;

  /// TES locations
  namespace SIMDCherenkovPhotonLocation
  {
    /// Default Location in TES for the SIMD photons
    inline const std::string Default = "Rec/RichFuture/SIMDCherenkovPhotons/Default";
  } // namespace SIMDCherenkovPhotonLocation

} // namespace Rich::Future::Rec
