/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <string>

// Kernel
#include "Kernel/FastAllocVector.h"

// Utils
#include "RichFutureUtils/RichHypoData.h"

namespace Rich::Future::Rec
{

  /// Type for track PID values
  using TrackPIDHypos = LHCb::STL::Vector< Rich::ParticleIDType >;

  /// TES locations for track PID values
  namespace TrackPIDHyposLocation
  {
    /// Default TES location for track PID values
    inline const std::string Default = "Rec/RichFuture/TrackHypos/Default";
    /// Default TES location for track PID values produced by the GlobalPID
    inline const std::string Global = "Rec/RichFuture/TrackHypos/Global";
  } // namespace TrackPIDHyposLocation

  /// Type for track hypothesis DLL values
  using TrackDLLs = Rich::Future::HypoData< double >;

  /// TES locations for track PID values
  namespace TrackDLLsLocation
  {
    /// Default TES location for track PID values
    inline const std::string Default = "Rec/RichFuture/TrackDLLs/Default";
    /// Default TES location for track PID values produced by the GlobalPID
    inline const std::string Global = "Rec/RichFuture/TrackDLLs/Global";
  } // namespace TrackDLLsLocation

} // namespace Rich::Future::Rec
