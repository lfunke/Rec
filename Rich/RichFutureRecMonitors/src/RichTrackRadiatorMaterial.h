/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STD
#include <algorithm>

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Rich Utils
#include "RichUtils/RichTrackSegment.h"

// DetDesc
#include "DetDesc/ITransportSvc.h"
#include "DetDesc/TransportSvcException.h"

// Detector parameters
#include "RichRecUtils/RichDetParams.h"

namespace Rich::Future::Rec::Moni
{

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class TrackRadiatorMaterial RichTrackRadiatorMaterial.h
   *
   *  Monitors the reconstructed cherenkov angles.
   *
   *  @author Chris Jones
   *  @date   2016-12-12
   */

  class TrackRadiatorMaterial final
    : public Consumer< void( const LHCb::RichTrackSegment::Vector & ),
                       Traits::BaseClass_t< HistoAlgBase > >
  {

  public:

    /// Standard constructor
    TrackRadiatorMaterial( const std::string &name, ISvcLocator *pSvcLocator );

    /// Initialize
    StatusCode initialize() override;

  public:

    /// Functional operator
    void operator()( const LHCb::RichTrackSegment::Vector &segments ) const override;

  protected:

    /// Pre-Book all histograms
    StatusCode prebookHistograms() override;

  private:

    /// Which radiators to monitor
    Gaudi::Property< RadiatorArray< bool > > m_rads { this, "Radiators", { false, true, true } };

    /// Transport Service
    ITransportSvc *m_transSvc = nullptr;
  };

} // namespace Rich::Future::Rec::Moni
