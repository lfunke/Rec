###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: RichFutureRecTrackAlgorithms
################################################################################
gaudi_subdir(RichFutureRecTrackAlgorithms v1r0)

gaudi_depends_on_subdirs(Rich/RichFutureRecBase
                         Event/TrackEvent
                         Rich/RichUtils
                         Rich/RichFutureUtils
                         Tr/TrackKernel
                         Tr/TrackInterfaces)

find_package(ROOT)
find_package(Boost)
find_package(GSL)
find_package(VDT)
find_package(Vc)

include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS}
                           ${Vc_INCLUDE_DIR})

gaudi_add_module(RichFutureRecTrackAlgorithms
                 src/*.cpp
                 INCLUDE_DIRS Boost GSL VDT Rich/RichFutureRecBase Rich/RichUtils Rich/RichFutureUtils Tr/TrackInterfaces Tr/TrackKernel
                 LINK_LIBRARIES Boost GSL VDT RichFutureRecBase RichUtils RichFutureUtils )

# Fixes for GCC7.
if( BINARY_TAG_COMP_NAME STREQUAL "gcc" AND BINARY_TAG_COMP_VERSION VERSION_GREATER "6.99")
  set_property(TARGET RichFutureRecTrackAlgorithms APPEND PROPERTY COMPILE_FLAGS " -faligned-new ")
endif()
