/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <array>
#include <iostream>
#include <limits>
#include <memory>
#include <string>
#include <vector>

// base class
#include "RichBaseTrSegMaker.h"

// Gaudi
#include "GaudiKernel/IJobOptionsSvc.h"
#include "GaudiKernel/IMagneticFieldSvc.h"
#include "GaudiKernel/ToolHandle.h"

// Gaudi Functional
#include "GaudiAlg/Transformer.h"

// Event Model
#include "Event/Track.h"

// Rich Utils
#include "RichUtils/BoostArray.h"
#include "RichUtils/RichRayTracingUtils.h"
#include "RichUtils/RichTrackSegment.h"

// Rich Det
#include "RichDet/DeRichRadiator.h"

// Interfaces
#include "GaudiAlg/IGenericTool.h"
#include "RichInterfaces/IRichDetParameters.h"
#include "RichInterfaces/IRichRadiatorTool.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackInterfaces/ITrackStateProvider.h"

// Rec event model
#include "RichFutureRecEvent/RichRecRelations.h"

namespace Rich::Future::Rec
{

  // Use the functional framework
  using namespace Gaudi::Functional;

  namespace
  {
    /// The input track container type
    using InData = LHCb::Track::Selection;
    // using InData  = LHCb::Track::Range;
    /// The output data
    using OutData = std::tuple< LHCb::RichTrackSegment::Vector,
                                Relations::TrackToSegments::Vector,
                                Relations::SegmentToTrackVector >;
  } // namespace

  /** @class DetailedTrSegMakerFromTracks RichDetailedTrSegMakerFromTracks.h
   *
   *  Builds RichTrackSegments from LHCb::Tracks.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class DetailedTrSegMakerFromTracks final
    : public MultiTransformer< OutData( const InData & ), Traits::BaseClass_t< BaseTrSegMaker > >
  {

  public:

    /// Standard constructor
    DetailedTrSegMakerFromTracks( const std::string &name, ISvcLocator *pSvcLocator );

    /// Initialization after creation
    StatusCode initialize() override;

  public:

    /// Algorithm execution via transform
    OutData operator()( const InData &tracks ) const override;

  private:

    /// Construct the track segments for a given track
    void constructSegments( const LHCb::Track *                 track,
                            LHCb::RichTrackSegment::Vector &    segments,
                            const LHCb::Tracks::size_type       tkIndex,
                            Relations::TrackToSegments::Vector &tkToSegsRel,
                            Relations::SegmentToTrackVector &   segToTkRel ) const;

    /// Checks for 'strange' States
    void checkState( const LHCb::State *state, const Rich::RadiatorType rad ) const;

    /** Find all intersections with the given radiator volume(s)
     *  @return The number of radiator intersections
     */
    unsigned int getRadIntersections(
      const Gaudi::XYZPoint &        point,        ///< The start point
      const Gaudi::XYZVector &       direction,    ///< The direction from the start point
      const DeRichRadiator *         rad,          ///< The radiator
      Rich::RadIntersection::Vector &intersections ///< The intersections with the given radiator
      ) const;

    /** Get just the first intersection with the radiator volume
     *  @return boolean indicating if an intersection was found or not
     */
    bool
    getNextInterPoint( const Gaudi::XYZPoint & point,     ///< The start point
                       const Gaudi::XYZVector &direction, ///< The direction from the start point
                       const DeRichRadiator *  rad,       ///< The radiator
                       Gaudi::XYZPoint &       interP     ///< The first intersection point
                       ) const;

    /** Correct the exit state to the point where the track traverses the spherical mirror
     *
     *  @param radiator     Pointer to the apropriate radiator detector element
     *  @param track        The Track object
     *  @param state        State information to correct
     *  @param refState     Reference starting state.
     */
    void correctRadExitMirror( const DeRichRadiator *radiator,
                               const LHCb::Track &   track,
                               LHCb::State &         state,
                               const LHCb::State *   refState = nullptr ) const;

    /** Extrapolate a state to a new z position
     *
     * @param stateToMove  The state to extrapolate
     * @param track        The Track object
     * @param z            The z position to extrapolate the state to
     * @param refState     Reference starting state.
     *
     * @return The status of the extrapolation
     * @retval true  State was successfully extrapolated to the new z position
     * @retval false State could not be extrapolated to the z position.
     *         State remains unaltered.
     */
    bool moveState( LHCb::State &      stateToMove,
                    const LHCb::Track &track,
                    const double       z,
                    const LHCb::State *refState = nullptr ) const;

    /// Check if a track type should be skipped in a given radiator
    inline bool skipByType( const LHCb::Track *track, const Rich::RadiatorType rad ) const noexcept
    {
      // Skip RICH2 for Upstream tracks and RICH1 for T tracks
      return ( ( LHCb::Track::Types::Ttrack == track->type() && Rich::Rich1Gas == rad ) ||
               ( LHCb::Track::Types::Upstream == track->type() && Rich::Rich2Gas == rad ) );
    }

    /// Creates the middle point information
    bool createMiddleInfo( const LHCb::Track &                  track,
                           const Rich::RadiatorType             rad,
                           LHCb::State &                        fState,
                           const LHCb::State *                  fStateRef,
                           LHCb::State &                        lState,
                           const LHCb::State *                  lStateRef,
                           Gaudi::XYZPoint &                    midPoint,
                           Gaudi::XYZVector &                   midMomentum,
                           LHCb::RichTrackSegment::StateErrors &errors ) const;

    /// Access the magnetic field service
    const IMagneticFieldSvc *magFieldSvc() const noexcept { return m_magFieldSvc; }

  private:

    /// Rich1 and Rich2 detector elements
    DetectorArray< DeRich * > m_rich = { {} };

    /// Type for pointers to RICH radiator detector elements
    using Radiators = std::vector< const DeRichRadiator * >;
    /// Pointers to RICH radiator detector elements
    Radiators m_radiators;

    /// Allowable tolerance on state z positions
    Gaudi::Property< RadiatorArray< double > > m_zTolerance {
      this,
      "ZTolerances",
      { 10 * Gaudi::Units::mm, 10 * Gaudi::Units::mm, 10 * Gaudi::Units::mm }
    };

    /// Nominal z positions of states at RICHes
    Gaudi::Property< std::array< double, 2 * Rich::NRiches > > m_nomZstates {
      this,
      "NominalStateZ",
      {
        990 * Gaudi::Units::mm,  // Place to look for Rich1 entry state
        2165 * Gaudi::Units::mm, // Place to look for Rich1 exit state
        9450 * Gaudi::Units::mm, // Place to look for Rich2 entry state
        11900 * Gaudi::Units::mm // Place to look for Rich2 exit state
      }
    };

    /// shifts for mirror correction
    Gaudi::Property< DetectorArray< double > > m_mirrShift { this,
                                                             "MirrorShiftCorr",
                                                             { 35 * Gaudi::Units::cm,
                                                               150 * Gaudi::Units::cm } };

    /// sanity checks on state information
    Gaudi::Property< RadiatorArray< double > > m_minStateDiff {
      this,
      "ZSanityChecks",
      { 1 * Gaudi::Units::mm, 25 * Gaudi::Units::mm, 50 * Gaudi::Units::mm }
    };

    /// Flag to indicate if extrapolation should always be done from the reference states
    Gaudi::Property< bool > m_extrapFromRef { this, "ExtrapolateFromReference", false };

    /// Minimum state movement in z to bother with
    Gaudi::Property< double > m_minZmove { this, "MinimumZMove", 1 * Gaudi::Units::mm };

    /// Min radius at exit for each radiator
    Gaudi::Property< RadiatorArray< double > > m_minRadLength {
      this,
      "MinRadiatorPathLength",
      { 0 * Gaudi::Units::mm, 500 * Gaudi::Units::mm, 1500 * Gaudi::Units::mm }
    };

    /// Cache min radius^2 at exit
    RadiatorArray< double > m_minRadLengthSq = { {} };

    /// Flag to turn on/off the use of the TrackStateProvider to create missing states
    Gaudi::Property< bool > m_createMissingStates { this, "CreateMissingStates", false };

    /// Use the State provider instead of the extrapolator to move states
    Gaudi::Property< bool > m_useStateProvider { this, "UseStateProvider", false };

    /// Flag to turn on the final State sanity checks
    Gaudi::Property< bool > m_checkStates { this, "StateSanityCheck", false };

    /// Pointer to the Magnetic Field Service
    IMagneticFieldSvc *m_magFieldSvc = nullptr;

    /// Track state provider
    ToolHandle< const ITrackStateProvider > m_trStateP { this,
                                                         "TrackStateProvider",
                                                         "TrackStateProvider/StateProvider" };

    /// Track extrapolator
    ToolHandle< const ITrackExtrapolator > m_trExt { this,
                                                     "TrackExtrapolator",
                                                     "TrackRungeKuttaExtrapolator" };

    /// Pointer to the radiator intersections tool
    ToolHandle< const IRadiatorTool > m_radTool { this,
                                                  "RadiatorTool",
                                                  "Rich::Future::RadiatorTool/Radiators" };

    /// Pointer to detector radiator parameters
    ToolHandle< const IDetParameters > m_detParameters { this,
                                                         "DetParameters",
                                                         "Rich::Future::DetParameters" };

    /// Preload Geometry ?
    Gaudi::Property< bool > m_preload { this, "PreloadGeometry", false };

    /// Max number tracks GEC
    Gaudi::Property< unsigned int > m_maxTracks { this,
                                                  "MaxTracks",
                                                  std::numeric_limits< unsigned int >::max() };
  };

  //======================================================================================
  // Get radiator intersections
  inline unsigned int DetailedTrSegMakerFromTracks::getRadIntersections(
    const Gaudi::XYZPoint &        point,
    const Gaudi::XYZVector &       direction,
    const DeRichRadiator *         rad,
    Rich::RadIntersection::Vector &intersections ) const
  {
    // clear the intersections
    intersections.clear();
    // get the intersections
    m_radTool.get()->intersections( point, direction, rad->radiatorID(), intersections );
    // return the nuber of intersections found
    return intersections.size();
  }
  //======================================================================================

  //======================================================================================
  // Short cut method to get just first intersection point
  inline bool DetailedTrSegMakerFromTracks::getNextInterPoint( const Gaudi::XYZPoint & point,
                                                               const Gaudi::XYZVector &direction,
                                                               const DeRichRadiator *  rad,
                                                               Gaudi::XYZPoint &interP ) const
  {
    Rich::RadIntersection::Vector intersections;
    bool                          OK = true;
    if ( 0 < getRadIntersections( point, direction, rad, intersections ) )
    { interP = intersections.front().entryPoint(); }
    else
    {
      OK = false;
    }
    return OK;
  }
  //======================================================================================

} // namespace Rich::Future::Rec
