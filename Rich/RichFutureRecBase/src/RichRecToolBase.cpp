/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichRecToolBase.cpp
 *
 *  Implementation file for RICH reconstruction tool base class : RichRecToolBase
 *
 *  @author Chris Jones    Christopher.Rob.Jones@cern.ch
 *  @date   2002-07-26
 */
//-----------------------------------------------------------------------------

// local
#include "RichFutureRecBase/RichRecToolBase.h"

// ============================================================================
// Force creation of templated class
#include "RichRecBase.icpp"
template class Rich::Future::Rec::CommonBase< Rich::Future::ToolBase >;
// ============================================================================

// ============================================================================
// Standard constructor, initializes variables
// ============================================================================
Rich::Future::Rec::ToolBase::ToolBase( const std::string &type,
                                       const std::string &name,
                                       const IInterface * parent )
  : Rich::Future::ToolBase( type, name, parent )
  , Rich::Future::Rec::CommonBase< Rich::Future::ToolBase >( this )
{}
// ============================================================================

// ============================================================================
// Initialise
// ============================================================================
StatusCode
Rich::Future::Rec::ToolBase::initialize()
{
  // Initialise base class
  const auto sc = Rich::Future::ToolBase::initialize();
  if ( !sc ) return Error( "Failed to initialise Rich::ToolBase", sc );

  // Common initialisation
  return initialiseRichReco();
}
// ============================================================================

// ============================================================================
// Finalise
// ============================================================================
StatusCode
Rich::Future::Rec::ToolBase::finalize()
{
  // Common finalisation
  const auto sc = finaliseRichReco();
  if ( !sc ) return Error( "Failed to finalise Rich::RecBase", sc );

  // base class finalize
  return Rich::Future::ToolBase::finalize();
}
// ============================================================================
