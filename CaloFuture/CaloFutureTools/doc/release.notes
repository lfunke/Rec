!-------------------------------------------------------------------------------
! Package     : CaloFutureTools
! Responsible : Olivier Deschamps odescham@in2p3.fr
!-------------------------------------------------------------------------------

! 2016-06-07 - Olivier Deschamps
 - CaloFuture2MC tool : fix bug that affect the merged-pi0 matching based on CaloFutureHypo Linker table

! 2016-09-05 - Olivier Deschamps
  - propagate saturation & barycenter information 

! 2016-08-16 - Olivier Deschamps
  - new tool : FutureCounterLevel
  - implement counter() switch based on FutureCounterLevel tool

!========================= CaloFutureTools v6r13 2016-03-17 =========================
! 2016-02-29 - Olivier Deschamps
  - fix coverity warning

! 2016-02-22 - Marco Cattaneo
 - Remove unnecessary friend ToolFactory declaration

!========================= CaloFutureTools v6r12 2016-01-28 =========================
! 2015-12-19 - O. Deschamps
 - CaloFuture2MCTool : change default MC association to rely on CaloFutureHypo->MC Linker. In case the Linker doesn't exist
   the chain CaloFutureHypo->CaloFutureCluster->CaloFutureDigit->MC is used (assuming the reconstruction version is consistent with the original CaloFutureHypos)

! 2015-12-17 - Marco Cattaneo
 - CaloFutureHypoEstimator.cpp: reorganise an if clause to avoid an FPE with clang

!========================= CaloFutureTools v6r11 2015-11-23 =========================
! 2015-11-06 - O. Deschamps
 - fix clang compilation warnings

!========================= CaloFutureTools v6r10p1 2015-10-13 =========================
! 2015-08-12 - Gerhard Raven
 - remove #include of obsolete Gaudi headers

!========================= CaloFutureTools v6r10 2014-09-12 =========================
! 2014-09-12 - O. Deschamps
  - CaloFutureHypoEstimator.cpp : implement ClusterCode & ClusterFrac 

!========================= CaloFutureTools v6r9 2014-07-14 =========================
! 2014-06-30 - Olivier Deschamps
  - FutureGammaPi0SeparationTool : improve & speed-up the evaluation of Prs-related variables 
                             protect against unphysical value

! 2014-06-29 - Olivier Deschamps
  - fix FPE's, compilation warnings & verbosity excess

! 2014-06-27 - Olivier Deschamps
 - FutureNeutralIDTool.{cpp,h}       : (new) implementation for MLP-based neutralID
 - CaloFutureHypoEstimator.h, FutureGammaPi0SeparationTool.h : update against reco changes


!========================= CaloFutureTools v6r8 2014-06-10 =========================
! 2014-05-23 - Olivier Deschamps
 - FutureGammaPi0SeparationTool.cpp : fix FPE

!========================= CaloFutureTools v6r7 2013-10-24 =========================
! 2013-10-09 - Olivier Deschamps
 - CaloFutureGetterTool, CaloFuture2CaloFuture, FutureGammaPi0SeparationTool, CaloFutureHypo2CaloFuture : protect against the upgrade configuration with no Spd/Prs

! 2013-10-04 - Olivier Deschamps
 - Add CaloFutureRelationsGetter implementation
 - Instrument CaloFutureHypoEstimator with CaloFutureRelationsGetter to access relations table

!========================= CaloFutureTools v6r6 2013-07-18 =========================
! 2013-06-13 - Marco Cattaneo
 - Fix Coverity DIVIDE_BY_ZERO and FORWARD_NULL defects

!========================= CaloFutureTools v6r5p1 2013-06-03 =======================
! 2013-05-27 - Marco Cattaneo
 - Fix clang32 warning
 - Remove 'do nothing' finalize methods

!========================= CaloFutureTools v6r5 2012-11-28 =========================
! 2012-11-21 - Marco Clemencic
 - Added CMake configuration file.
 - Fixed a warning (-pedantic).

! 2012-10-19 - Marco Cattaneo
 - Fix gcc -pedantic warnings
 - Use getItExists where appropriate

!========================= CaloFutureTools v6r4 2012-06-25 =========================
! 2012-05-29 - Marco Cattaneo
 - Fix unused variable in previous commit
 - Fix UNINIT_CTOR defects

! 2012-05-22 - Olivier Deschamps for Miriam Calvo 
 - FutureGammaPi0SeparationTool  : new version of the tool (incl. Prs info)

! 2012-05-14 - Olivier Deschamps
 - new algorithm : CheckCaloFutureHypoRef - check CaloFutureHypo->CaloFutureCluster* smart 
                                      reference backward compatibility 

! 2012-05-10 - Olivier Deschamps
 - L0CaloFuture2CaloFuture tool : default clusterization level set to 2 
                      (5x5 area for merged pi0)

!========================= CaloFutureTools v6r3p1 2011-12-14 =========================
! 2011-11-23 - Marco Cattaneo
 - Fix trivial gcc warnings and icc remarks

!========================= CaloFutureTools v6r3 2011-07-27 =========================
! 2011-07-22 - Marco Cattaneo
 - Create debug() messages only when output level requires it,
   also using UNLIKELY macro
 - Replace all endreq by endmsg

! 2011-06-17 - Olivier Deschamps
  - fix window compilation warning

!========================= CaloFutureTools v6r2 2011-06-15 =========================
! 2011-06-14 - Olivier Deschamps
 - extend CaloFutureHypoEstimator : more Prs info + Gamma/pi0 separation info
 - new FutureGammaPi0SeparationTool + related MLP files

! 2011-05-16 - Olivier Deschamps
 - L0CaloFuture2CaloFuture tool : clusterize around right seedID

! 2011-05-13 - Olivier Deschamps
 - L0CaloFuture2CaloFuture tool : allow for managing the cluster on TES externally (new default)

!========================= CaloFutureTools v6r1 2011-02-01 =========================
! 2011-01-21 - Olivier Deschamps
 - Fix CaloFuture2MCTool initialization

! 2011-01-19 - Marco Cattaneo
 - Fix ICC warnings and remarks

!========================= CaloFutureTools v6r0 2010-10-25 =========================

! 2010-10-07 - Dmitry Golubkov
 - CaloFutureHypo2CaloFuture: change passing parameteres by value to const references
 - increment version to v6r0 due to the change in the interface

!========================== CaloFutureTools v5r17 2010-09-28 =========================
! 2010-09-01 - Olivier Deschamps
  - fix compilation error on windows
  - improve Track2CaloFuture & CaloFutureHypoEstimator

! 2010-08-31 - Olivier Deschamps
  - fix compilation warning on slc4

! 2010-08-27 - O. Deschamps
 - new tool CaloFutureHypoEstimator + update CaloFutureHypo2CaloFuture

!========================== CaloFutureTools v5r16p1 2010-06-21 =======================
! 2010-06-10 - Marco Cattaneo
 - Fix Windows compilation warnings

!========================== CaloFutureTools v5r16 2010-03-18 =========================
! 2010-03-08 - Olivier Deschamps
	- CaloFutureHypoToCaloFuture: Implement new virtual method _setProperty

!========================== CaloFutureTools v5r15 2010-02-15 =========================
! 2010-02-13 - Olivier Deschamps
	- Track2CaloFuture : switch HeraBExtrapolator to RungeKutta

!========================== CaloFutureTools v5r14 2009-11-27 =========================
! 2009-11-17 - Olivier Deschamps
 - CaloFuture2MCTool : 
  - new methods :
      isCaloFuture(particle) 
      isPureNeutralCaloFuture(particle) 
      findMCOrBest(string/ParticleID,threshold)
  - complete the code (still not fully completed)

!========================== CaloFutureTools v5r13 2009-11-13 =========================
! 2009-11-05 - Dmitry GOLUBKOV
 - L0CaloFuture2CaloFutureTool : bugfix - make storage of all created clusters on TES the
   only supported behaviour (therefore remove the 'DuplicateClustersOnTES' property),
   use CaloFutureFunctors::neighbours() instead of look_neig()
 - cmt/requirements version increment to v5r13

!========================== CaloFutureTools v5r12 2009-10-20 =========================
! 2009-12-21 - Olivier Deschamps
  -  adapt to change in CaloFutureDAQ
  -  improve CaloFuture2MCTool.{cpp,h} : not yet complete but usable 

! 2009-09-15 - Olivier Deschamps
  - new tool : CaloFuture2MCTool.{cpp,h} : handle CaloFuture object->MC relation
							 (interface CaloFutureInterfaces/ICaloFuture2MCTool.h)
      - preliminary (incomplete) version of the tool
  - use Relations in requirements

! 2009-09-14 - Olivier Deschamps
  - adapt to change in CaloFutureDAQ/CaloFutureReadoutTool 
    (->CaloFutureCosmicsTool & CaloFutureGetterTool & L0CaloFuture2CaloFutureTool)

! 2009-09-11 - Olivier Deschamps
 - CaloFuture2CaloFuture.cpp : 
   - make use of CaloFutureAlgUtils.h to define context-dependent TES I/O 
   - caloGetterTool : public -> private tool
   - use caloGetterTool in isLocMax(id) method


! 2009-09-11 - Olivier Deschamps
 - CalOGetterTool.cpp : make use of CaloFutureAlgUtils.h to define context-dependent TES I/O 

! 2009-09-04 - Dmitry Golubkov
 - cmt/requirements version increment to v5r12
 - release.notes : fix the order of the comments 2009-08-02
 - L0CaloFuture2CaloFutureTool.cpp: decrease error level for invalid ur/dr/ul cells

!========================== CaloFutureTools v5r11 2009-09-03 =========================
! 2009-09-03 - Marco Cattaneo
 - Remove obsolete file src/CaloFutureTools_dll.cpp

! 2009-08-05 - Stephanie Hansmann-Menzemer
 - remove filling of track-extra info, according to changes in Track.xml class

!========================== CaloFutureTools v5r10p1 2009-09-07 =========================
! 2009-09-04 - Dmitry Golubkov
 - L0CaloFuture2CaloFutureTool.cpp: decrease error level for invalid ur/dr/ul cells

!========================== CaloFutureTools v5r10 2009-08-31 =========================

! 2009-12-10 - Olivier Deschamps
 - adapt to change in CaloFutureDAQ

! 2009-08-10 - Vanya BELYAEV
 - CaloFuture2CaloFuture & CaloFutureGetter 
    fix the problem of mandatory "update" 

! 2009-08-05 - Vanya BELYAEV
 - fix the signature 

! 2009-08-02 - Dmitry GOLUBKOV
 - L0CaloFuture2CaloFutureTool.cpp: method to return CaloFutureClusters around the CaloFutureCellID

! 2009-07-31 - Dmitry Golubkov
 - L0CaloFuture2CaloFutureTool.cpp - fix compilation warnings on slc4

! 2009-07-29 - Dmitry GOLUBKOV
 - Add a new tool to get a list of CaloFutureClusters for the input L0CaloFutureCandidate(s)
  L0CaloFuture2CaloFutureTool.cpp, L0CaloFuture2CaloFutureTool.h
 - cmt/requirements verison increment to v5r10

!========================== CaloFutureTools v5r9 2009-06-03 =========================
! 2009-06-03 - Marco Cattaneo
 - Add missing CaloFutureTools_dll.cpp file

! 2009-05-19 - Marco Cattaneo
 - Add missing #include "Event/Particle.h" following removal from IPart2CaloFuture.h
 - Replace endreq by endmsg

! 2009-05-10 - Vanya Belyaev
 - a bit more statistical prinout  
 - cmt/requirements
     verison increment to v5r9

!========================== CaloFutureTools v5r8 2009-05-08 =========================
! 2008-04-17 - Olivier Deschamps
 - new tool CaloFutureGetterTool : helps to speed-up CaloFutureReco processing
 - CaloFuture2CaloFuture/CaloFutureHypo2CaloFuture : get digits data via CaloFutureGetterTool

! 2009-04-16 Olivier Deschamps
 - CaloFuture2CaloFuture/CaloFutureHypo2CaloFuture : speed-up processing + new property ('IdealGeometry')
   when perfect alignment is assumed

!========================== CaloFutureTools v5r7 2009-03-10 =========================
! 2009-03-09 Olivier Deschamps
 - Fix bad default setting for CaloFutureHypo2CaloFuture.cpp (Seed=false -> true)  + minor bug fix

! 2009-03-05 Olivier Deschamps
 - improve verbosity for CaloFutureCosmics stuff

!========================== CaloFutureTools v5r6 2009-01-12 =========================
! 2008-12-10 - Marco Cattaneo
 - Fix gcc 4.3 warnings

! 2008-12-05 - Olivier Deschamps
 - CaloFutureCosmicsTrackTool :: fix bug in Hcal timing 

!========================== CaloFutureTools v5r5 2008-10-01 =========================
! 2008-09-11 - Olivier Deschamps
 - CaloFuture2CaloFuture :: add multiplicity method
 - new tool : CaloFutureHypo2CaloFuture : specialized version of CaloFuture2CaloFuture

!========================== CaloFutureTools v5r4 2008-09-04 =========================
! 2008-08-21 - Olivier Deschamps
 - CaloFutureCosmicsTrackAlg : add monitoring histo for cosmics track (theta,phi) parameters
 - CaloFutureCosmicsTool : add cellId vector in the output ntuple

!========================== CaloFutureTools v5r3 2008-07-29 =========================
! 2008-07-21 - Marco Cattaneo
 - Adapt to new location of IEventTimeDecoder.h (needs Gaudi v20r2)

!========================== CaloFutureTools v5r2 2008-07-02 =========================
! 2008-06-30 - Olivier Deschamps
 - add asymmetry monitoring in CaloFutureCosmicsTrackAlg
 - add 'kernel' variable computation in CaloFutureCosmicsTool 

! 2008-06-26 - Juan PALACIOS
 - cmt/requirements
  . Increase version to v5r2
 - src/CaloFutureCosmicsTool.cpp
 - src/CaloFutureCosmicsTrackTool.cpp
 - src/Track2CaloFuture.cpp
  . Change all Gaudi::XYZLine and Gaudi::Line for Gaudi::Math::XYZLine and
    Gaudi::Math::XYZLine respectively (adapting to Kernel/LHCbMath v3)

!========================== CaloFutureTools v5r1 2008-06-05 =========================
! 2008-06-05 - Marco Cattaneo
  - Fix acos() for windows (cannot take an int argument)
  
! 2008-06-04 - Deschamps Olivier
  - tune the verbosity

! 2008-06-03 - Marco Cattaneo
 - In CaloFutureCosmicsTool.cpp: fix compiler warning, add missing include
 - In CaloFutureCosmicsTrackTool.cpp, CaloFutureCosmicsTrackAlg.cpp: add missing includes

! 2008-05-29 - Deschamps Olivier
  - use dedicated TrackEvent/Track flags

! 2008-05-29 - Deschamps Olivier
  - add more monitoring histo (split monitoring according to forward/backward cosmics tracks)

! 2008-05-27 - Deschamps Olivier
  - fix bug in CaloFutureCosmicsTrackTool.cpp (Hcal state replicated Ecal state)

! 2008-05-22 - Deschamps Olivier
  - Add new tools and alg for cosmics track reconstruction in calorimeter : 
	CaloFutureCosmicsTool, CaloFutureCosmicsTrackTool, CaloFutureCosmicsTrackAlg

!========================== CaloFutureTools v5r0 2008-05-08 =========================
! 2008-05-08 - Marco Cattaneo
 - Resurrect CaloFutureTools package (declared obsolete in 2002). It is now a
   component library for CaloFuturerimeter Tools, previously in CaloFutureUtils link library
