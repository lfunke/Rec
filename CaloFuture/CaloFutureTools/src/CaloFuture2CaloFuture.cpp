/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files 
// ============================================================================
#include "Kernel/CaloCellCode.h"
// ============================================================================
// CaloFutureUtils
// ============================================================================
#include "CaloFutureUtils/CaloFutureDataFunctor.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
// ============================================================================
// local
// ============================================================================
#include "CaloFuture2CaloFuture.h"
// ============================================================================
/** @file 
 *  Implementation file for class : CaloFuture2CaloFuture
 *  @date 2007-05-29 
 *  @author Olivier Deschamps
 */
// ============================================================================
// Declaration of the Tool Factory
DECLARE_COMPONENT( CaloFuture2CaloFuture )
// ============================================================================


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CaloFuture2CaloFuture::CaloFuture2CaloFuture( const std::string& type,
                      const std::string& name,
                      const IInterface* parent )
  : base_class ( type, name , parent )
{
  declareInterface<ICaloFuture2CaloFuture>(this);
}
//=============================================================================
StatusCode CaloFuture2CaloFuture::initialize()
{
  StatusCode sc = base_class::initialize();
  if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) 
    debug() << "Initialize CaloFuture2CaloFuture tool " << endmsg;
  
  // get getter tool : public tool
  m_getter = tool<ICaloFutureGetterTool>("CaloFutureGetterTool",m_getterName);

  // CaloDigit locations
  m_loc["Ecal"]=  LHCb::CaloFutureAlgUtils::CaloFutureDigitLocation( "Ecal" , context() );
  m_loc["Hcal"]=  LHCb::CaloFutureAlgUtils::CaloFutureDigitLocation( "Hcal" , context() );
  m_loc["Prs"] =  LHCb::CaloFutureAlgUtils::CaloFutureDigitLocation( "Prs"  , context() );
  m_loc["Spd"] =  LHCb::CaloFutureAlgUtils::CaloFutureDigitLocation( "Spd"  , context() );
  // DeCalorimeter* pointers
  int mask=m_getter->detectorsMask();
  m_det["Ecal"]=getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
  m_det["Hcal"]=getDet<DeCalorimeter>( DeCalorimeterLocation::Hcal );
  m_det["Prs"]=((mask&2)==0) ? NULL : getDetIfExists<DeCalorimeter>( DeCalorimeterLocation::Prs   );
  m_det["Spd"]=((mask&1)==0) ? NULL : getDetIfExists<DeCalorimeter>( DeCalorimeterLocation::Spd   );
  // CellSize reference (outer section)  Warning : factor 2 for Hcal
  m_refSize["Hcal"] = (*(m_det["Hcal"]->cellParams().begin())).size()/2.;
  m_refSize["Ecal"] = (*(m_det["Ecal"]->cellParams().begin())).size(); 
  if(NULL != m_det["Prs"])m_refSize["Prs"]  = (*(m_det["Prs"]->cellParams().begin())).size(); 
  if(NULL != m_det["Spd"])m_refSize["Spd"]  = (*(m_det["Spd"]->cellParams().begin())).size(); 
  // CaloPlanes
  m_plane["Hcal"] = m_det["Hcal"]->plane(CaloPlane::ShowerMax );
  m_plane["Ecal"] = m_det["Ecal"]->plane(CaloPlane::ShowerMax );
  if(NULL != m_det["Prs"])m_plane["Prs"]  = m_det["Prs"]->plane(CaloPlane::ShowerMax );
  if(NULL != m_det["Spd"])m_plane["Spd"]  = m_det["Spd"]->plane(CaloPlane::ShowerMax );  

  reset();

  if((m_fromCaloFuture == "Hcal" || m_fromCaloFuture == "Ecal" || m_fromCaloFuture == "Prs" || m_fromCaloFuture == "Spd" ) &&  
     (m_toCaloFuture   == "Hcal" || m_toCaloFuture   == "Ecal" || m_toCaloFuture   == "Prs" || m_toCaloFuture   == "Spd" ) )setCalos(m_fromCaloFuture,m_toCaloFuture);
  // 

  if( UNLIKELY( msgLevel(MSG::DEBUG) )){
    if( NULL == m_det["Prs"] && (m_fromCaloFuture=="Prs" || m_toCaloFuture=="Prs"))
      debug() << "Try to access non-existing Prs DetectorElement" << endmsg;
    if( NULL == m_det["Spd"] && (m_fromCaloFuture=="Spd" || m_toCaloFuture=="Spd"))
      debug() << "Try to access non-existing Spd DetectorElement" << endmsg;
  }
  return sc;
}

void CaloFuture2CaloFuture::reset(){
  m_digits.clear();
  m_cells.clear();
  m_energy = 0;
  m_count  = 0;
}

void CaloFuture2CaloFuture::setCalos
( const std::string& fromCaloFuture ,
  const std::string& toCaloFuture   )
{ 

  m_ok       = true;
  if( NULL == m_det["Prs"] && (fromCaloFuture=="Prs" || toCaloFuture=="Prs")){    
    if( UNLIKELY( msgLevel(MSG::DEBUG) ) )debug() <<"Try to access non-existing Prs DetectorElement"<<endmsg;    
    m_ok = false;
    return;
  }
  
  if( NULL == m_det["Spd"] && (fromCaloFuture=="Spd" || toCaloFuture=="Spd")){
    if( UNLIKELY( msgLevel(MSG::DEBUG) ) )debug() <<"Try to access non-existing Spd DetectorElement"<<endmsg;    
    m_ok=false;
    return;
  }

  m_fromCaloFuture = fromCaloFuture;
  m_toCaloFuture   = toCaloFuture;
  m_fromDet  = m_det[m_fromCaloFuture];
  m_fromSize = m_refSize[m_fromCaloFuture];
  m_toDet    = m_det[m_toCaloFuture];
  m_toPlane  = m_plane[m_toCaloFuture];
  m_toSize   = m_refSize[m_toCaloFuture];
  m_toLoc    = m_loc[ m_toCaloFuture ];


  

 }
//=======================================================================================================
const std::vector<LHCb::CaloCellID>& CaloFuture2CaloFuture::cellIDs ( const LHCb::CaloCluster& fromCluster , 
                                                          const std::string&       toCaloFuture      ){
  
  reset();
  LHCb::CaloCellID seedID = fromCluster.seed();
  std::string fromCaloFuture = CaloCellCode::CaloNameFromNum( seedID.calo() );
  if( toCaloFuture != m_toCaloFuture || fromCaloFuture != m_fromCaloFuture)setCalos(fromCaloFuture,toCaloFuture);
  if( !m_ok )return m_cells;

  for(const auto& ent : fromCluster.entries() ) {
    const LHCb::CaloDigit* digit = ent.digit();
    if ( digit ) cellIDs( digit->cellID() , toCaloFuture, false );
  }
  return m_cells;
}
//=======================================================================================================
const std::vector<LHCb::CaloCellID>& CaloFuture2CaloFuture::addCell( const LHCb::CaloCellID& id     , 
                                                         const std::string&      toCaloFuture ){

  if( !m_ok )return m_cells;
  // consistency check
  if( CaloCellCode::CaloNameFromNum(id.calo()) != m_toCaloFuture || toCaloFuture != m_toCaloFuture ){
    Warning("CellID is not consistent with CaloFuture setting").ignore();
    return m_cells;
  }
  // check duplicate
  if ( m_cells.end() != std::find ( m_cells.begin() , m_cells.end() , id ) ) { return m_cells ; }
  
  // add the cells
  m_cells.push_back( id );
  
  // added by VB.
  if ( 0 == m_digs ) 
  {
    Error ( "Digits* points to NULL") ;
    return m_cells ;
  }

  LHCb::CaloDigit* digit = m_digs->object( id );
  if( NULL != digit ) {
    m_digits.push_back( digit );
    m_energy += digit->e();
    m_count++;
  }
  return m_cells;
}


//=======================================================================================================
const std::vector<LHCb::CaloCellID>& 
CaloFuture2CaloFuture::cellIDs ( const LHCb::CaloCellID& fromId , 
                     const std::string&      toCaloFuture , bool init ){
  
  if( init )reset();
  std::string fromCaloFuture = CaloCellCode::CaloNameFromNum( fromId.calo() );
  if( toCaloFuture != m_toCaloFuture || fromCaloFuture != m_fromCaloFuture)setCalos(fromCaloFuture,toCaloFuture);
  if( !m_ok )return m_cells;

  LHCb::CaloCellID toId = fromId;
  // ---- Assume ideal geometry : trivial mapping for detectors having the same granularity (Prs/Spd/Ecal)
  if( ( m_geo && (m_fromCaloFuture == "Ecal" || m_fromCaloFuture == "Prs" || m_fromCaloFuture == "Spd") 
        && (m_toCaloFuture == "Ecal" || m_toCaloFuture == "Prs" || m_toCaloFuture == "Spd") ) 
      || m_fromCaloFuture.value() == m_toCaloFuture.value() ){
    toId.setCalo( CaloCellCode::CaloNumFromName( m_toCaloFuture ));
    if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) 
      debug() << "Add cell from trivial mapping" << endmsg;
    return addCell( toId , m_toCaloFuture);
  }
  
  // ---- Else use the actual geometry to connet detectors
  double scale = 1.;
  Gaudi::XYZPoint  center = m_fromDet->cellCenter( fromId );
  if( m_fromCaloFuture.value() != m_toCaloFuture.value() ){
    // z-scaling
    scale = m_toSize / m_fromSize ;
    center  = m_toPlane.ProjectOntoPlane( m_fromDet->cellCenter( fromId )*scale );
    // connect
    toId  = m_toDet->Cell( center );
  }
  double fromSize = m_fromDet->cellSize( fromId )*scale;
  //cell-center is outside 'toCaloFuture' - check corners  
  if( LHCb::CaloCellID() == toId){
    for( int i = 0 ; i != 2; ++i){
      for( int j = 0 ; j != 2; ++j){
        double x = m_fromDet->cellCenter( fromId ).X() + (i*2-1) * fromSize;        
        double y = m_fromDet->cellCenter( fromId ).Y() + (j*2-1) * fromSize;
        Gaudi::XYZPoint  corner = Gaudi::XYZPoint(x,y,center.Z());
        LHCb::CaloCellID cornerId  = m_toDet->Cell( corner );
        if( LHCb::CaloCellID() == cornerId)continue;
        toId=cornerId;
      }   
    }
  }
  if( LHCb::CaloCellID() == toId)return m_cells;
  int pad = 1;
  double x0  = center.X();
  double y0  = center.Y();
  if( m_fromDet != m_toDet ){
    double toSize   = m_toDet->cellSize( toId ) ;
    pad =  (int) floor( fromSize  / toSize + 0.25); // warning int precision
    if(pad < 1)pad=1;
    x0 = center.X() - (pad-1)*fromSize/2./pad;
    y0 = center.Y() - (pad-1)*fromSize/2./pad;  
  }
  for( int i = 0 ; i != pad; ++i){
    for( int j = 0 ; j != pad; ++j){
      double x = x0 + i * fromSize/pad;
      double y = y0 + j * fromSize/pad;    
      Gaudi::XYZPoint  pos(x,y,center.Z());
      if( m_fromDet != m_toDet ) toId  = m_toDet->Cell( pos ) ;
      if( LHCb::CaloCellID() == toId)continue;
      addCell( toId, m_toCaloFuture);
    }        
  }
  return m_cells;
}


// Digits
const std::vector<LHCb::CaloDigit*>& CaloFuture2CaloFuture::digits( const LHCb::CaloCellID& fromId ,
                                                        const std::string& toCaloFuture ){  
  m_digs     = m_getter->digits( m_toLoc );  
  cellIDs( fromId, toCaloFuture ) ;
  return m_digits;
}

const std::vector<LHCb::CaloDigit*>& CaloFuture2CaloFuture::digits( const LHCb::CaloCluster& fromCluster , 
                                                        const std::string& toCaloFuture ){  
  m_digs     = m_getter->digits( m_toLoc );  
  cellIDs( fromCluster, toCaloFuture);
  return m_digits;
}  

// Energy
double CaloFuture2CaloFuture::energy( const LHCb::CaloCellID& fromId , 
                          const std::string&      toCaloFuture ){
  m_digs     = m_getter->digits( m_toLoc );  
  cellIDs ( fromId , toCaloFuture );
  return m_energy;
}


int CaloFuture2CaloFuture::multiplicity( const LHCb::CaloCellID& fromId , 
                             const std::string&      toCaloFuture ){
  m_digs     = m_getter->digits( m_toLoc );  
  cellIDs(fromId, toCaloFuture);
  return m_count;
}

double CaloFuture2CaloFuture::energy( const LHCb::CaloCluster& fromCluster , 
                          const std::string&  toCaloFuture ){
  m_digs     = m_getter->digits( m_toLoc );  
  cellIDs(fromCluster, toCaloFuture);
  return m_energy;
}


int CaloFuture2CaloFuture::multiplicity( const LHCb::CaloCluster& fromCluster , 
                             const std::string& toCaloFuture ){
  m_digs     = m_getter->digits( m_toLoc );  
  cellIDs(fromCluster, toCaloFuture);
  return m_count;
}

// ============================================================================
// Additional method : isLocalMax
bool CaloFuture2CaloFuture::isLocalMax ( const LHCb::CaloDigit& digit){
  const LHCb::CaloCellID& id = digit.cellID();
  std::string    calo = CaloCellCode::CaloNameFromNum( id.calo() );
  DeCalorimeter* det  = m_det[ calo ];
  const LHCb::CaloDigits* digits = m_getter->digits( m_loc[ calo ] ); 
  //
  return LHCb::CaloFutureDataFunctor::isLocalMax ( &digit , det , digits ) ;
}
