/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

#include "CaloFutureUtils/CaloMomentum.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
// from Event
#include "Event/RecHeader.h"
#include "Event/CaloHypo.h"
#include "Event/CaloCluster.h"
#include <unordered_map>
#include <iostream>
#include <iomanip>
#include <string>
#include <cstdlib>

// local
#include "FutureGammaPi0XGBoostTool.h"
#include <fstream>

using namespace LHCb;
using namespace Gaudi::Units;
//-----------------------------------------------------------------------------
// Implementation file for class : FutureGammaPi0XGBoostTool
//
// 2018-03-24 : author @sayankotor
//-----------------------------------------------------------------------------

//Declare nessesary help functionality
namespace {

size_t getClusterType (const CaloCellID& id) {

  const int CaloFutureNCol[4] = {64, 32, 16, 16};
  const int CaloFutureNRow[4] = {52, 20, 12, 12};
  const unsigned Granularity[3] = {1, 2, 3};

  const unsigned ClusterSize = 5;
  int type (id.area());
  int xOffsetOut = std::min (int(id.col() - (32 - CaloFutureNCol[type]*Granularity[type]/2)), // left edge
                 int(31 + CaloFutureNCol[type]*Granularity[type]/2 - id.col())); // right edge
  int yOffsetOut = std::min (int(id.row() - (32 - CaloFutureNRow[type]*Granularity[type]/2)),
                 int(31 + CaloFutureNRow[type]*Granularity[type]/2 - id.row()));
  int innerWidth = CaloFutureNCol[type+1] * (type != 2 ? Granularity[type] : 1); // process inner hole specially
  int innerHeight = CaloFutureNRow[type+1] * (type != 2 ? Granularity[type] : 1); // process inner hole specially

  int xOffsetIn = std::min (int(id.col() - (31 - innerWidth/2)),
                int(32 + innerWidth/2 - id.col()));
  int yOffsetIn = std::min (int(id.row() - (31 - innerHeight/2)),
                int(32 + innerHeight/2 - id.row()));
  const int margin = (ClusterSize-1)/2;
  bool outerBorder = xOffsetOut < margin || yOffsetOut < margin;
  bool innerBorder = xOffsetIn > -margin && yOffsetIn > -margin;
  if (innerBorder) return type+3;
  else if (outerBorder) return type+6;
  return type;
}

}

// Declaration of the Tool Factory
DECLARE_COMPONENT( FutureGammaPi0XGBoostTool )

//=============================================================================
// Initialization
//=============================================================================
StatusCode FutureGammaPi0XGBoostTool::initialize() {

  StatusCode sc = base_class::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) debug() << "==> Initialize" << endmsg;

  /// Retrieve geometry of detector
  m_ecal = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );


  const std::string paramEnv = "PARAMFILESROOT";   
  std::string paramRoot = std::string(getenv(paramEnv.c_str()));    

  m_xgb0 = std::make_unique<FutureXGBClassifierPhPi0>(paramRoot + "/data/GammaPi0XgbTool_simpl0.model");
  m_xgb1 = std::make_unique<FutureXGBClassifierPhPi0>(paramRoot + "/data/GammaPi0XgbTool_simpl1.model");
  m_xgb2 = std::make_unique<FutureXGBClassifierPhPi0>(paramRoot + "/data/GammaPi0XgbTool_simpl2.model");
  m_xgb03_b = std::make_unique<FutureXGBClassifierPhPi0>(paramRoot + "/data/GammaPi0XgbTool_bound03.model");
  m_xgb06_b = std::make_unique<FutureXGBClassifierPhPi0>(paramRoot + "/data/GammaPi0XgbTool_bound06.model");
  m_xgb14_b = std::make_unique<FutureXGBClassifierPhPi0>(paramRoot + "/data/GammaPi0XgbTool_bound14.model");
  m_xgb17_b = std::make_unique<FutureXGBClassifierPhPi0>(paramRoot + "/data/GammaPi0XgbTool_bound17.model");
  m_xgb25_b = std::make_unique<FutureXGBClassifierPhPi0>(paramRoot + "/data/GammaPi0XgbTool_bound25.model");
  m_xgb28_b = std::make_unique<FutureXGBClassifierPhPi0>(paramRoot + "/data/GammaPi0XgbTool_bound28.model");

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode FutureGammaPi0XGBoostTool::finalize() {

  if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) debug() << "==> Finalize" << endmsg;

  m_xgb0.reset();
  m_xgb1.reset();
  m_xgb2.reset();
  m_xgb03_b.reset();
  m_xgb06_b.reset();
  m_xgb14_b.reset();
  m_xgb17_b.reset();
  m_xgb25_b.reset();
  m_xgb28_b.reset();

  return base_class::finalize(); // must be executed last
}

//=============================================================================
// Main execution
//=============================================================================


double FutureGammaPi0XGBoostTool::isPhoton(const LHCb::CaloHypo* hypo){

  if ( !m_ecal ) return m_def;
  if ( LHCb::CaloMomentum(hypo).pt() < m_minPt) return m_def;
  const LHCb::CaloCluster* cluster = LHCb::CaloFutureAlgUtils::ClusterFromHypo( hypo );
  if (!cluster) return m_def;

  std::vector<double> rawEnergyVector(25, 0.0);

  int cluster_type = getClusterType(cluster->seed());
  bool rawEnergy = GetRawEnergy(hypo, cluster_type, rawEnergyVector);

  if(!rawEnergy) return m_def;
  
  double prediction_xgb = XGBDiscriminant(cluster_type, rawEnergyVector);
  return prediction_xgb;
}


double FutureGammaPi0XGBoostTool::XGBDiscriminant(int cluster_type, std::vector<double>& row_energies)
{

        double value = -1e10;
        switch (cluster_type) {
            case 0: value = m_xgb0->getClassifierValues(row_energies);
                    break;
            case 1: value = m_xgb1->getClassifierValues(row_energies);
                    break;
            case 2: value = m_xgb2->getClassifierValues(row_energies);
                    break;
            case 3: value = m_xgb03_b->getClassifierValues(row_energies);
                    break;
            case 4: value = m_xgb14_b->getClassifierValues(row_energies);
                    break;
            case 5: value = m_xgb25_b->getClassifierValues(row_energies);
                    break;
            case 6: value = m_xgb06_b->getClassifierValues(row_energies);
                    break;
            case 7: value = m_xgb17_b->getClassifierValues(row_energies);
                    break;
            case 8: value = m_xgb28_b->getClassifierValues(row_energies);
                    break;
            default: warning() << "FutureGammaPi0XGBoostTool: Unsupperted cluster type" << endmsg;

        }
        return value;
}

bool FutureGammaPi0XGBoostTool::GetRawEnergy(const LHCb::CaloHypo* hypo, int& cluster_type, std::vector<double>& rowEnergy) const{
  if( nullptr == hypo)return false;

  const LHCb::CaloDigits * digits_full = getIfExists<LHCb::CaloDigits>(LHCb::CaloDigitLocation::Ecal);
  const LHCb::CaloCluster* cluster = LHCb::CaloFutureAlgUtils::ClusterFromHypo( hypo );   // OD 2014/05 - change to Split Or Main  cluster
  
  if( nullptr == digits_full || nullptr == cluster) return false;
  
  LHCb::CaloCellID centerID = cluster->seed();

  std::vector<std::vector<double>> vector_cells (5, std::vector<double>(5, 0.0));
  
  std::vector<int> col_numbers = {(int)centerID.col() - 2, (int)centerID.col() - 1, (int)centerID.col(), (int)centerID.col() + 1, (int)centerID.col() + 2};
  std::vector<int> row_numbers = {(int)centerID.row() - 2, (int)centerID.row() - 1, (int)centerID.row(), (int)centerID.row() + 1, (int)centerID.row() + 2};

  if (cluster_type > 2)
  {
    for (auto& col_number: col_numbers){
        for (auto& row_number: row_numbers){
            const auto id_ = LHCb::CaloCellID(centerID.calo(), centerID.area(), row_number, col_number);
            auto * test = digits_full->object(id_);
            if (test) {
                vector_cells[col_number - (int)centerID.col() + 2][row_number - (int)centerID.row() + 2] = test->e();
            } else {
                continue;
            }
        }
    }
  }

  else 
  { 
    for (auto& col_number: col_numbers){
        for (auto& row_number: row_numbers){
            const auto id_ = LHCb::CaloCellID(centerID.calo(), centerID.area(), row_number, col_number);
            auto * test = digits_full->object(id_);
            if (test) {
                vector_cells[col_number - (int)centerID.col() + 2][row_number - (int)centerID.row() + 2] = test->e();
            } else {
                continue;
            }
        }
    }
  }

  for (int i = 0; i < 5; i++){
      for (int j = 0; j < 5; j++){
      rowEnergy[i*5 + j] = vector_cells[i][j];
    }
  }
  return true;
}
