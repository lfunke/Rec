/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: CaloFutureHypo2CaloFuture.h,v 1.4 2010-03-08 01:58:39 odescham Exp $
#ifndef CALOFUTUREHYPO2CALOFUTURE_H
#define CALOFUTUREHYPO2CALOFUTURE_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "CaloFutureInterfaces/ICaloFutureHypo2CaloFuture.h"            // Interface
#include "CaloFuture2CaloFuture.h"
#include "CaloFutureUtils/CellNeighbour.h"

#ifdef __INTEL_COMPILER        // Disable ICC remark from ROOT
  #pragma warning(disable:654) // overloaded virtual function is only partially overridden
#endif

/** @class CaloFutureHypo2CaloFuture CaloFutureHypo2CaloFuture.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2008-09-11
 */
class CaloFutureHypo2CaloFuture : public CaloFuture2CaloFuture, virtual public ICaloFutureHypo2CaloFuture {
public:
  /// Standard constructor
  CaloFutureHypo2CaloFuture( const std::string& type,
                 const std::string& name,
                 const IInterface* parent);

  StatusCode initialize() override;
  // cellIDs
  using CaloFuture2CaloFuture::cellIDs;
  const std::vector<LHCb::CaloCellID>& cellIDs(const LHCb::CaloHypo    &fromHypo,    const std::string &toCaloFuture) override;
  const std::vector<LHCb::CaloCellID>& cellIDs(const LHCb::CaloCluster &fromCluster, const std::string &toCaloFuture) override;
  const std::vector<LHCb::CaloCellID>& cellIDs() override {return m_cells;};
  // digits
  using CaloFuture2CaloFuture::digits;
  const std::vector<LHCb::CaloDigit*>& digits(const LHCb::CaloCluster &fromCluster, const std::string &toCaloFuture) override;
  const std::vector<LHCb::CaloDigit*>& digits(const LHCb::CaloHypo    &fromHypo,    const std::string &toCaloFuture) override;
  const std::vector<LHCb::CaloDigit*>& digits() override {return m_digits;};
  // energy
  using CaloFuture2CaloFuture::energy;
  double energy(const LHCb::CaloCluster &fromCluster, const std::string &toCaloFuture) override;
  double energy(const LHCb::CaloHypo    &fromHypo,    const std::string &toCaloFuture) override;
  double energy() override {return m_energy;};
  // multiplicity
  using CaloFuture2CaloFuture::multiplicity;
  int multiplicity(const LHCb::CaloCluster &fromCluster, const std::string &toCaloFuture) override;
  int multiplicity(const LHCb::CaloHypo    &fromHypo,    const std::string &toCaloFuture) override;
  int multiplicity() override {return m_count;};
  void setCalos(const std::string &from, const std::string &to) override {CaloFuture2CaloFuture::setCalos(from,to); };

  // external setting
  StatusCode  _setProperty(const std::string& p,const std::string& v) override {return  setProperty(p,v);};


protected:

private:
  Gaudi::Property<bool> m_seed {this, "Seed", true};
  Gaudi::Property<bool> m_neighb {this, "AddNeighbors", true};
  Gaudi::Property<bool> m_line {this, "PhotonLine", true};
  Gaudi::Property<bool> m_whole {this, "WholeCluster", false};
  Gaudi::Property<int> m_status {this, "StatusMask", 0x0};
  Gaudi::Property<float> m_x {this, "xTolerance", 5.*Gaudi::Units::mm};
  Gaudi::Property<float> m_y {this, "yTolerance", 5.*Gaudi::Units::mm};
  CellNeighbour m_neighbour;
  LHCb::CaloCellID m_lineID ;
  Gaudi::XYZPoint  m_point;
};
#endif // CALOFUTUREHYPO2CALOFUTURE_H
