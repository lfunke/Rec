/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: Part2CaloFuture.cpp,v 1.2 2009-05-19 13:48:22 cattanem Exp $
// Include files 

// from LHCb
#include "Event/Particle.h"
#include "Kernel/TrackDefaultParticles.h"
// local
#include "Part2CaloFuture.h"

//-----------------------------------------------------------------------------
// Implementation file for class : Part2CaloFuture
//
// 2006-11-30 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( Part2CaloFuture )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Part2CaloFuture::Part2CaloFuture( const std::string& type,
                        const std::string& name,
                        const IInterface* parent )
  : Track2CaloFuture ( type, name , parent )
    , m_particle       ( NULL )
    , m_proto          ( NULL ){
  declareInterface<IPart2CaloFuture>(this);
}

//=============================================================================
StatusCode Part2CaloFuture::initialize(){
  StatusCode sc = Track2CaloFuture::initialize();
  if (sc.isFailure()) return Error("Failed to initialize", sc);
  return StatusCode::SUCCESS;  
}
//=============================================================================
bool Part2CaloFuture::match(const LHCb::Particle* part, std::string det,CaloPlane::Plane plane , double delta){
  m_status = setting( part );
  if ( m_status )
    return Track2CaloFuture::match( m_track, det, plane, delta, LHCb::Tr::PID( m_particle->particleID().abspid() ) );
  return m_status;
}
//=============================================================================
bool Part2CaloFuture::match(const LHCb::ProtoParticle* proto, std::string det,CaloPlane::Plane plane , double delta){
  m_status = setting( proto );
  if( m_status )return Track2CaloFuture::match(m_track, det, plane, delta);
  return m_status;
}
//=============================================================================
bool Part2CaloFuture::setting(const LHCb::Particle* particle){
  m_particle = particle;
  if( NULL == m_particle)return false;
  const LHCb::ProtoParticle* proto = m_particle -> proto();
  return setting(proto);
}
//=============================================================================
bool Part2CaloFuture::setting(const LHCb::ProtoParticle* proto){
  m_proto   = proto;
  if( NULL == m_proto)return false;
  return Track2CaloFuture::setting( m_proto->track() );
}
//=============================================================================

