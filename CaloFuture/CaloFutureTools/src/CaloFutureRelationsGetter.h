/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTURERELATIONSGETTER_H
#define CALOFUTURERELATIONSGETTER_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "CaloFutureInterfaces/ICaloFutureRelationsGetter.h"            // Interface
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/Incident.h"
// from LHCb
#include "Relations/Relation2D.h"
#include "Relations/IRelationWeighted.h"
#include "Relations/IRelationWeighted2D.h"
#include "Event/Track.h"
#include "Event/CaloHypo.h"
#include "CaloFutureUtils/CaloFuture2Track.h"


#include "Relations/RelationWeighted2D.h"


/** @class CaloFutureRelationsGetter CaloFutureRelationsGetter.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2013-10-04
 */
class CaloFutureRelationsGetter : public GaudiTool, virtual public ICaloFutureRelationsGetter, virtual public IIncidentListener   {
public:
  /// Standard constructor
  CaloFutureRelationsGetter( const std::string& type,
                       const std::string& name,
                       const IInterface* parent);

  StatusCode initialize() override;
  StatusCode finalize() override;
  void handle(const Incident&  ) override {
    if( UNLIKELY( msgLevel(MSG::DEBUG) ) )debug() << "IIncident Svc reset" << endmsg;
    clean();
  }

  // getters
  LHCb::CaloFuture2Track::ITrHypoTable2D* getTrHypoTable2D(std::string location) override;
  LHCb::CaloFuture2Track::IHypoEvalTable* getHypoEvalTable(std::string location) override;
  LHCb::CaloFuture2Track::IClusTrTable*   getClusTrTable  (std::string location) override;

private:

  LHCb::RelationWeighted2D<LHCb::Track, LHCb::CaloHypo, float> m_trHypo;
  std::map<std::string,LHCb::CaloFuture2Track::IHypoTrTable2D*> m_hypoTr;
  std::map<std::string,LHCb::CaloFuture2Track::IHypoEvalTable*> m_hypoEval;
  std::map<std::string,LHCb::CaloFuture2Track::IClusTrTable*>   m_clusTr;
  void clean();
};
#endif // CALOFUTURERELATIONSGETTER_H
