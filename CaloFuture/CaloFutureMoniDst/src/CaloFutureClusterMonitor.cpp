/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Includes
#include "GaudiAlg/Consumer.h"
#include "Event/CaloCluster.h"
#include "CaloFutureMoniAlg.h"

// =============================================================================

/** @class CaloFutureClusterMonitor CaloFutureClusterMonitor.cpp
 *
 *  The algorithm for trivial monitoring of "CaloFutureCluster" containers.
 *  The algorithm produces 8 histograms:
 *
 *  <ol>
 *  <li> @p CaloCluster multiplicity                    </li>
 *  <li> @p CaloCluster size (number of cells)          </li>
 *  <li> @p CaloCluster energy distribution             </li>
 *  <li> @p CaloCluster transverse energy distribution  </li>
 *  <li> @p CaloCluster x-distribution                  </li>
 *  <li> @p CaloCluster y-distribution                  </li>
 *  <li> @p CaloCluster x vs y-distribution             </li>
 *  </ol>
 *
 *  Histograms reside in the directory @p /stat/"Name" , where
 *  @ "Name" is the name of the algorithm
 *
 *  @see   CaloFutureMoniAlg
 *  @see GaudiHistoAlg
 *  @see GaudiAlgorithm
 *  @see      Algorithm
 *  @see     IAlgorithm
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   02/11/2001
 */

using Input = LHCb::CaloCluster::Container;

class CaloFutureClusterMonitor final
: public Gaudi::Functional::Consumer<void(const Input&),
    Gaudi::Functional::Traits::BaseClass_t<CaloFutureMoniAlg>>
{
public:
  /// standard algorithm initialization
  StatusCode initialize() override;
  void operator()(const Input&) const override;

  CaloFutureClusterMonitor( const std::string &name, ISvcLocator *pSvcLocator );
};

// =============================================================================

DECLARE_COMPONENT( CaloFutureClusterMonitor )

// =============================================================================

CaloFutureClusterMonitor::CaloFutureClusterMonitor( const std::string &name, ISvcLocator *pSvcLocator )
: Consumer( name, pSvcLocator, KeyValue{ "Input", {}}
){
  m_multMax = 200;
  m_multBin = 100;
  auto Input = LHCb::CaloFutureAlgUtils::CaloFutureClusterLocation( name, context() );
  updateHandleLocation( *this, "Input", Input );
}

// =============================================================================

/// standard algorithm initialization
StatusCode CaloFutureClusterMonitor::initialize(){
  StatusCode sc = Consumer::initialize();
  if ( sc.isFailure() ) return sc;
  hBook1( "1", "# of Clusters " + inputLocation()               , m_multMin   , m_multMax   , m_multBin );
  hBook1( "2", "Cluster digit multiplicity " + inputLocation()  , m_sizeMin   , m_sizeMax   , m_sizeBin );
  hBook1( "3", "Cluster Energy " + inputLocation()              , m_energyMin , m_energyMax , m_energyBin );
  hBook1( "4", "Cluster Et " + inputLocation()                  , m_etMin     , m_etMax     , m_etBin );
  hBook1( "5", "Cluster x " + inputLocation()                   , m_xMin      , m_xMax      , m_xBin );
  hBook1( "6", "Cluster y " + inputLocation()                   , m_yMin      , m_yMax      , m_yBin );
  hBook2( "7", "Cluster barycenter position x vs y " + inputLocation()                , m_xMin    , m_xMax    , m_xBin, m_yMin, m_yMax, m_yBin );
  hBook2( "8", "Energy-weighted cluster barycenter position x vs y " + inputLocation(), m_xMin    , m_xMax    , m_xBin, m_yMin, m_yMax, m_yBin );
  hBook1( "9", "Cluster digit used for Energy multiplicity " + inputLocation()        , m_sizeMin , m_sizeMax , m_sizeBin );
  return StatusCode::SUCCESS;
}

// =============================================================================
// standard execution method
// =============================================================================

// StatusCode CaloFutureClusterMonitor::execute(){
void CaloFutureClusterMonitor::operator()(const Input& clusters) const {

  if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
    debug() << " Producing histo " << produceHistos() << endmsg;
  // produce histos ?
  if ( !produceHistos() ) return;

  if ( clusters.empty() ){
    if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
      debug() << "Found empty cluster in " << inputLocation() << endmsg;
    return;
  }

  // fill multiplicity histogram
  initFutureCounters();

  for( const auto& cluster: clusters ){
    if ( 0 == cluster ) continue;
    const double e = cluster->position().e();
    const double x = cluster->position().x();
    const double y = cluster->position().y();
    const double z = cluster->position().z();
    const double et = e * sqrt( x*x + y*y ) / sqrt( x*x+y*y+z*z );
    if( e  < m_eFilter) continue;
    if( et < m_etFilter) continue;
    const auto id = cluster->seed();
    count( id );
    hFill1(id, "2", cluster->entries().size() );
    hFill1(id, "3", e  );
    hFill1(id, "4", et );
    hFill1(id, "5", x  );
    hFill1(id, "6", y  );
    hFill2(id, "7", x,y );
    hFill2(id, "8", x,y,e);

    int iuse = 0;
    for( const auto& entry: cluster->entries() ){
      if( 0 != (LHCb::CaloDigitStatus::UseForEnergy & entry.status()) ) iuse++;
    }
    hFill1(id,"9", iuse );
    if(doHisto("10"))fillCaloFuture2D("10", id, 1., "Cluster position 2Dview " + inputLocation() );
    if(doHisto("11"))fillCaloFuture2D("11", id, e , "Cluster Energy 2Dview " + inputLocation() );

  }
  // fill counter
  fillFutureCounters("1");

  return;
}
