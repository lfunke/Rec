/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTURECA_TAGGEDCELLFUNCTOR_H
#define CALOFUTURECA_TAGGEDCELLFUNCTOR_H 1
// ============================================================================
// Include files
// ============================================================================
// STL
// ============================================================================
#include <vector>
#include <iostream>
// ============================================================================
// local
// ============================================================================
#include "CelAutoTaggedCell.h"
// ============================================================================
/** @namespace TaggedCellFunctor TaggedCellFunctor.h CaloFutureCA/TaggedCellFunctor.h
 *
 * Algorithm of clustering
 * Contain functors for use with STL algorithms
 * for one CaloFutureTaggedCell*
 *
 *  @author  Nicole Brun
 *  @date    27/02/2001
 */
// ============================================================================
namespace TaggedCellFunctor
{
  struct isEdge;
  struct isClustered;
  struct isClusteredOrEdge;
  struct isSeed;
  class isWithSeed;
  struct setStatus;
}
// ============================================================================
struct TaggedCellFunctor::isEdge final {
  // Function
  inline bool operator() ( const CelAutoTaggedCell* taggedCell ) const {
    return ( ( 0 == taggedCell ) ? false : taggedCell->isEdge() );
  }


};

struct TaggedCellFunctor::isClustered final {
  // Function

  inline bool operator() ( const CelAutoTaggedCell* taggedCell ) const {
    return ( ( 0 == taggedCell ) ? false : taggedCell->isClustered() );
  }

};

struct TaggedCellFunctor::isClusteredOrEdge final {
  // Function

  inline bool operator() ( const CelAutoTaggedCell* taggedCell ) const {
    return ( ( 0 == taggedCell ) ? false : taggedCell->isClustered() || taggedCell->isEdge() );
  }

};


struct TaggedCellFunctor::isSeed final {
  // Function

  inline bool operator() ( const CelAutoTaggedCell* taggedCell ) const {
    return ( ( 0 == taggedCell ) ? false : taggedCell->isSeed() );
  }

};

class TaggedCellFunctor::isWithSeed final {

  public:
  // Constructor

  isWithSeed( const LHCb::CaloCellID& seed ) :
    m_seed ( seed ) {};

  // Function

  inline bool operator() ( CelAutoTaggedCell* taggedCell ) const {
    return ( ( 0 == taggedCell ) ? false : taggedCell->isWithSeed( m_seed )  );
  }

  private:

  LHCb::CaloCellID m_seed;

};

struct TaggedCellFunctor::setStatus final {

  // Function

  inline void operator() ( CelAutoTaggedCell* taggedCell ) const {
    taggedCell->setStatus();
  }

};

#endif // CALOFUTURECA_TAGGEDCELLFUNCTOR_H
