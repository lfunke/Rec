/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef CALOFUTURERECO_CALOFUTURESELECTCLUSTERWITHPRS_H
#define CALOFUTURERECO_CALOFUTURESELECTCLUSTERWITHPRS_H 1
// ============================================================================
#include <string>
#include "GaudiAlg/GaudiTool.h"
#include "CaloFutureInterfaces/ICaloFutureClusterSelector.h"
#include "CaloFutureInterfaces/ICaloFutureHypo2CaloFuture.h"
#include "CaloFutureInterfaces/IFutureCounterLevel.h"
// ============================================================================

class CaloFutureSelectClusterWithPrs :
  public virtual ICaloFutureClusterSelector ,
  public          GaudiTool
{
public:

  bool select( const LHCb::CaloCluster* cluster ) const override;
  bool operator()( const LHCb::CaloCluster* cluster ) const override;
  StatusCode initialize() override;

  CaloFutureSelectClusterWithPrs( const std::string& type   ,
                            const std::string& name   ,
                            const IInterface*  parent );

private:
  Gaudi::Property<float> m_cut  {this, "MinEnergy", -10. *Gaudi::Units::MeV};
  Gaudi::Property<float> m_mult {this, "MinMultiplicity", 0.};
  ICaloFutureHypo2CaloFuture* m_toPrs = nullptr;
  Gaudi::Property<std::string> m_det {this, "Detector", "Ecal"};
  IFutureCounterLevel* counterStat = nullptr;
};
#endif // CALOFUTURERECO_CALOFUTURESELECTCLUSTERWITHPRS_H
