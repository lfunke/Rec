/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// local
#include "CaloFutureSelectorNOT.h"
// ============================================================================
/** @file
 *
 *  Implementation file for class : CaloFutureSelectorNOT
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 27 Apr 2002
 */
// ============================================================================
DECLARE_COMPONENT( CaloFutureSelectorNOT )
// ============================================================================
/*  StNOTard constructor
 *  @see GaudiTool
 *  @see  AlgTool
 *  @see IAlgTool
 *  @param type   tool type (?)
 *  @param name   tool name
 *  @param parent tool parent
 */
// ============================================================================
CaloFutureSelectorNOT::CaloFutureSelectorNOT
( const std::string& type,
  const std::string& name,
  const IInterface* parent )
  : GaudiTool ( type, name, parent )
{
  declareInterface<ICaloFutureClusterSelector> (this);
}
// ============================================================================
/*  stNOTard initialization of the tool
 *  @see IAlgTool
 *  @see AlgTool
 *  @see GaudiTool
 *  @return status code
 */
// ============================================================================
StatusCode CaloFutureSelectorNOT::initialize ()
{
  // initialize the base class
  StatusCode sc = GaudiTool::initialize() ;
  if ( sc.isFailure() ) {
    return Error("Could not initialize the base class GaudiTool", sc);
  }
  // locate selectors
  for ( Names::const_iterator it = m_selectorsTypeNames.begin() ;
        m_selectorsTypeNames.end() != it ; ++it ) {
    ICaloFutureClusterSelector* selector = tool<ICaloFutureClusterSelector>( *it, this );
    m_selectors.push_back( selector );
  };
  ///
  return StatusCode::SUCCESS ;
}
// ============================================================================
/*  stNOTard finalization  of the tool
 *  @see IAlgTool
 *  @see AlgTool
 *  @see GaudiTool
 *  @return status code
 */
// ============================================================================
StatusCode CaloFutureSelectorNOT::finalize   ()
{
  // clear containers
  m_selectors          .clear() ;
  m_selectorsTypeNames .clear() ;
  // finalize the base class
  return GaudiTool::finalize () ;
}
// ============================================================================
/*  "select"/"preselect" method
 *  @see ICaloFutureClusterSelector
 *  @param  cluster pointer to calo cluster object to be selected
 *  @return true if cluster is selected
 */
// ============================================================================
bool CaloFutureSelectorNOT::select( const LHCb::CaloCluster* cluster ) const
{
  return (*this) (cluster);
}
// ============================================================================
/* "select"/"preselect" method (functor interface)
 *  @see ICaloFutureClusterSelector
 *  @param  cluster pointer to calo cluster object to be selected
 *  @return true if cluster is selected
 */
// ============================================================================
bool CaloFutureSelectorNOT::operator() ( const LHCb::CaloCluster* cluster ) const
{
  bool notSelected = false ;
  for ( Selectors::const_iterator selector = m_selectors.begin() ;
        !notSelected && m_selectors.end() != selector ; ++selector ) {
    notSelected = (**selector)( cluster );
  }
  if (!notSelected) { 
    ++m_counter;
  }
  return !notSelected;
}




