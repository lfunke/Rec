/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTURESHOWEROVERLAPTOOL_H
#define CALOFUTURESHOWEROVERLAPTOOL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "CaloDet/DeCalorimeter.h"
#include "Event/CaloCluster.h"
#include "CaloFutureCorrectionBase.h"
#include "CaloFutureInterfaces/ICaloFutureHypoTool.h"
#include "CaloFutureInterfaces/ICaloFutureShowerOverlapTool.h"            // Interface
#include "GaudiKernel/Counters.h"


/** @class CaloFutureShowerOverlapTool CaloFutureShowerOverlapTool.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2014-06-02
 */

// define type from instance name
namespace {
    std::string correctionType( const std::string& name ) {
        const std::string uName ( LHCb::CaloFutureAlgUtils::toUpper(name) );
        if ( uName.find( "MERGED" ) != std::string::npos || uName.find( "SPLITPHOTON" ) !=  std::string::npos )  return "SplitPhoton";
        return "Photon";
    }
}

class CaloFutureShowerOverlapTool : public extends<GaudiTool,ICaloFutureShowerOverlapTool> {

public:
  /// Extend ICaloFutureShowerOverlapTool
  using extends<GaudiTool,ICaloFutureShowerOverlapTool>::extends;

  StatusCode initialize() override;

  void process(LHCb::CaloCluster& c1, LHCb::CaloCluster& c2, const int niter=10,
               propagateInitialWeights=propagateInitialWeights{false}) const override;

protected:
  void storeInitialWeights(const LHCb::CaloCluster& cl1,
                           const LHCb::CaloCluster& cl2,
                           std::map<LHCb::CaloCellID,double>& weights) const;
  double getInitialWeight(const LHCb::CaloCellID id,
                          const std::map<LHCb::CaloCellID,double>& weights) const;
  double fraction(const LHCb::CaloCluster& c, 
                  const LHCb::CaloDigit* d,
                  const int& a1, const int& a2,
                  const int flag) const;
  void subtract(LHCb::CaloCluster& c1, LHCb::CaloCluster& c2,
                const propagateInitialWeights propagateInitialWeights,
                const std::map<LHCb::CaloCellID,double>& ini_weights,
                const int& a1, const int& a2) const;
  double showerFraction(const double d3d, const unsigned int area) const;
  void evaluate(LHCb::CaloCluster& c,bool hypoCorrection = true) const;
private:
  Gaudi::Property<std::string> m_detLoc {this, "Detector", DeCalorimeterLocation::Ecal};
  Gaudi::Property<std::string> m_pcond {this, "Profile", "Conditions/Reco/Calo/PhotonShowerProfile"};
  const DeCalorimeter* m_det = 0;
  ToolHandle<ICaloFutureHypoTool> m_stool{ this, "CaloFutureSCorrection",
      "CaloFutureSCorrection/"+correctionType(name())+"SCorrection" } ;
  ToolHandle<ICaloFutureHypoTool> m_ltool{ this, "CaloFutureLCorrection",
      "CaloFutureLCorrection/"+correctionType(name())+"LCorrection" } ;
  ToolHandle<CaloFutureCorrectionBase> m_shape{ this, "CaloFutureCorrectionBase",
      "CaloFutureCorrectionBase/ShowerProfile"};
  Gaudi::Property<unsigned int> m_minSize {this, "ClusterMinSize", 2};
  mutable Gaudi::Accumulators::Counter<> m_skippedSize{this, "Overlap correction skipped due to cluster size"};
  mutable Gaudi::Accumulators::Counter<> m_skippedEnergy{this, "Overlap correction skipped due to cluster energy"};  
  mutable Gaudi::Accumulators::Counter<> m_positionFailed{this, "Cluster position failed"};
};
#endif // CALOFUTURESHOWEROVERLAPTOOL_H
