/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTURESHOWEROVERLAP_H
#define CALOFUTURESHOWEROVERLAP_H 1

// Include files 
// from Gaudi
#include "GaudiAlg/Transformer.h"
#include "Event/CaloDataFunctor.h"
#include "Event/CellID.h"
#include "FutureSubClusterSelectorTool.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureInterfaces/ICaloFutureShowerOverlapTool.h"
#include "Event/CaloCluster.h"

/** @class CaloFutureShowerOverlap CaloFutureShowerOverlap.h
 *  
 *
 *  @author Olivier Deschamps
 *  @date   2014-06-02
 */
class CaloFutureShowerOverlap
: public Gaudi::Functional::Transformer<LHCb::CaloCluster::Container(const LHCb::CaloCluster::Container&)>

{

public: 

  /// Standard constructor
  CaloFutureShowerOverlap( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;    ///< Algorithm initialization

  LHCb::CaloCluster::Container operator()(const LHCb::CaloCluster::Container&) const override;    ///< Algorithm execution

private:

  Gaudi::Property<int>   m_dMin   {this, "DistanceThreshold", 4};
  Gaudi::Property<float> m_etMin  {this, "MinEtThreshold", 50., "( ET1 > x && ET2 > x)"};
  Gaudi::Property<float> m_etMin2 {this, "MaxEtThreshold", 150., "( ET2 > y || ET2 > y)"};
  Gaudi::Property<int>   m_iter   {this, "Iterations", 5};
  Gaudi::Property<std::string> m_det   {this, "Detector" , DeCalorimeterLocation::Ecal};
  
  // following properties are inherited by the selector tool when defined :
  Gaudi::Property<std::vector<std::string>> m_taggerP {this, "PositionTags"};
  Gaudi::Property<std::vector<std::string>> m_taggerE {this, "EnergyTags"};
  
  const DeCalorimeter*    m_detector = nullptr ;
  ICaloFutureShowerOverlapTool* m_oTool    = nullptr ;
  FutureSubClusterSelectorTool* m_tagger   = nullptr ;

};

#endif // CALOFUTURESHOWEROVERLAP_H
