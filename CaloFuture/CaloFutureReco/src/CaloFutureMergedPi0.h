/*****************************************************************************\
* (c) Copyright 2018 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef CALOFUTURERECO_CALOFUTUREMERGEDPI0_H
#define CALOFUTURERECO_CALOFUTUREMERGEDPI0_H 1
// ============================================================================
// Include files
// ============================================================================
// from STL
// ============================================================================
#include <string>
#include <vector>
// ============================================================================
#include "GaudiAlg/Transformer.h"
#include "CaloFutureInterfaces/ICaloFutureClusterTool.h"
#include "FutureSubClusterSelectorTool.h"
#include "CaloFutureInterfaces/ICaloFutureHypoTool.h"
#include "CaloFutureInterfaces/ICaloFutureShowerOverlapTool.h"
#include "CaloFutureInterfaces/IFutureCounterLevel.h"
#include "CaloDet/DeCalorimeter.h"
#include "Event/CaloCluster.h"
// ============================================================================

/** @class CaloFutureMergedPi0 CaloFutureMergedPi0.h
 *
 *  Merged pi0 reconstruction with iterativ Method
 *
 * NEW IMPLEMENTATION
 *
 *  @author Olivier Deschamps
 *  @date   05/05/2014
 */

class CaloFutureMergedPi0 : public Gaudi::Functional::MultiTransformer
  <std::tuple<LHCb::CaloClusters, LHCb::CaloHypos, LHCb::CaloHypos>(const LHCb::CaloClusters&)> {

public:
  CaloFutureMergedPi0(const std::string& name, ISvcLocator* svcloc);

  StatusCode initialize() override;
  std::tuple<LHCb::CaloClusters, LHCb::CaloHypos, LHCb::CaloHypos> operator()(const LHCb::CaloClusters &) const override;
  StatusCode finalize() override;

private:
  bool isNeighbor(LHCb::CaloCellID id0, LHCb::CaloCellID id1) const;


  Gaudi::Property<float>  m_etCut {this, "EtCut",            1500 * Gaudi::Units::MeV}; // minimal ET of clusters to consider
  Gaudi::Property<float>  m_minET {this, "SplitPhotonMinET", 0.};  // minimal ET of resulting split-clusters and photons
  Gaudi::Property<int>    m_iter  {this, "MaxIterations",    25};  // forwarded to CaloFutureShowerOverlapTool

  Gaudi::Property<bool>   m_createClusterOnly {this, "CreateSplitClustersOnly", false}; // temporarily deprecated & unsupported

  // tools name
  Gaudi::Property<std::vector<std::string>> m_photonTools {this, "PhotonTools"};
  Gaudi::Property<std::vector<std::string>> m_pi0Tools    {this, "Pi0Tools"};

  ToolHandleArray<ICaloFutureHypoTool> m_gTools {this};
  ToolHandleArray<ICaloFutureHypoTool> m_pTools {this};

  ToolHandle<ICaloFutureShowerOverlapTool> m_oTool  { "CaloFutureShowerOverlapTool/SplitPhotonShowerOverlap", this };
  ToolHandle<FutureSubClusterSelectorTool> m_tagger { "FutureSubClusterSelectorTool/EcalClusterTag", this };
  ToolHandle<ICaloFutureClusterTool>       m_spread { "FutureClusterSpreadTool/EcalSpread" , this };
  ToolHandle<ICaloFutureClusterTool>       m_cov    { "FutureClusterCovarianceMatrixTool/EcalCovariance", this };


  Gaudi::Property<std::string> m_det {this, "Detector", DeCalorimeterLocation::Ecal};
  const DeCalorimeter* m_detector = nullptr ;  // we use only const methods of the calo detector


  mutable Gaudi::Accumulators::AveragingCounter<unsigned int> m_cntNo2ndSeed            {this, "Cluster without 2nd seed found"};
  mutable Gaudi::Accumulators::AveragingCounter<unsigned int> m_cntFailsToTagECluster1  {this, "Fails to tag(E) cluster (1)"   };
  mutable Gaudi::Accumulators::AveragingCounter<unsigned int> m_cntFailsToTagECluster2  {this, "Fails to tag(E) cluster (2)"   };
  mutable Gaudi::Accumulators::AveragingCounter<unsigned int> m_cntFailsToSetCovariance1{this, "Fails to set covariance (1)"   };
  mutable Gaudi::Accumulators::AveragingCounter<unsigned int> m_cntFailsToSetCovariance2{this, "Fails to set covariance (2)"   };
  mutable Gaudi::Accumulators::AveragingCounter<unsigned int> m_cntFailsToSetSpread1    {this, "Fails to set spread (1)"       };
  mutable Gaudi::Accumulators::AveragingCounter<unsigned int> m_cntFailsToSetSpread2    {this, "Fails to set spread (2)"       };

  mutable Gaudi::Accumulators::AveragingCounter<unsigned int> m_cntPi0sSize             {this, "clusters => mergedPi0s"        };
  mutable Gaudi::Accumulators::AveragingCounter<unsigned int> m_cntSplitPhotonsSize     {this, "clusters => splitPhotons"      };
  mutable Gaudi::Accumulators::AveragingCounter<unsigned int> m_cntSplitClustersSize    {this, "clusters => splitClusters"     };
};
// ============================================================================
#endif // CALOFUTUREMERGEDPI0_H
