/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloFutureTrack2IDAlg.h"

// ============================================================================
/** @class FutureTrack2HcalEAlg FutureTrack2HcalEAlg.cpp
 *  preconfigured instance of class  CaloFutureTrack2IDAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-15
 */
struct FutureTrack2HcalEAlg final : public CaloFutureTrack2IDAlg {
  FutureTrack2HcalEAlg(const std::string& name, ISvcLocator* pSvc)
      : CaloFutureTrack2IDAlg(name, pSvc) {
    _setProperty("Digits", LHCb::CaloDigitLocation::Hcal);
    using LHCb::CaloFutureAlgUtils::CaloFutureIdLocation;
    _setProperty("Output", CaloFutureIdLocation("HcalE", context()));
    _setProperty("Filter", CaloFutureIdLocation("InHcal", context()));
    _setProperty("Tool", "FutureHcalEnergyForTrack/HcalEFuture");
  }
};

// ============================================================================

DECLARE_COMPONENT( FutureTrack2HcalEAlg )

// ============================================================================
