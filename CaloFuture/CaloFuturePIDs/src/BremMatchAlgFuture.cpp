/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloFutureTrackMatchAlg.h"

// =============================================================================
/** @class BremMatchAlgFuture
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-06-16
 */
// =============================================================================
using TABLE = LHCb::RelationWeighted2D<LHCb::CaloHypo, LHCb::Track, float>;
using CALOFUTURETYPES = LHCb::CaloHypos;

struct BremMatchAlgFuture final : CaloFutureTrackMatchAlg<TABLE,CALOFUTURETYPES> {
  static_assert(std::is_base_of<LHCb::CaloFuture2Track::IHypoTrTable2D, TABLE>::value,
                "TABLE must inherit from IHypoTrTable2D");
  
  BremMatchAlgFuture(const std::string& name, ISvcLocator* pSvc)
      : CaloFutureTrackMatchAlg<TABLE,CALOFUTURETYPES>(name, pSvc) {
    Gaudi::Functional::updateHandleLocation(*this, "Calos",  LHCb::CaloFutureAlgUtils::CaloFutureHypoLocation("Photons",context()));
    Gaudi::Functional::updateHandleLocation(*this, "Output", LHCb::CaloFutureAlgUtils::CaloFutureIdLocation("BremMatch",context()));
    Gaudi::Functional::updateHandleLocation(*this, "Filter", LHCb::CaloFutureAlgUtils::CaloFutureIdLocation("InBrem",context()));

    _setProperty("Tool", "CaloFutureBremMatch/BremMatchFuture");
    _setProperty("Threshold", "10000");
    // track types:
    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                     LHCb::Track::Types::Velo, LHCb::Track::Types::Long,
                                     LHCb::Track::Types::Upstream));
    _setProperty("TableSize", "1000");
  }

};

// =============================================================================

DECLARE_COMPONENT( BremMatchAlgFuture )
