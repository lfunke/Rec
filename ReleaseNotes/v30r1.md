

2018-08-06 Rec v30r1
===

This version uses Lbcom v30r1, LHCb v50r1, Gaudi v30r3 and LCG_93 with ROOT 6.12.06
<p>
This version is released on `master` branch.

Built relative to Rec v30r0, with the following changes:

### New features

- Propagate XGBoost-based IsPhoton separation variable to (Neutral)ProtoParticle::additionalInfo, !1118 (@deschamp)   
  Same as !1105 on `2018-patches`

- New pi0 gamma separation tool, !1114 (@vchekali)   
  Cherry-picked from lhcb/Rec!1087 on `2018-patches`

- Added LinearStateZTraj.h to Tr/TrackKernel, !1085 (@wouter)   
  Cherry-picked from lhcb/Rec!1082 on `2018-patches`  

- Add PrTrackerDumper tool to PrMCTools package [dump all VP,UT,FT hit] and the MCParticle linking information used in PrChecker, enable a wide range of offline studies and algorithm developments, !1078 (@rquaglia)  
  See MR description for detailed discussion of this change    
   
- Implement LoKi based generic track selector, !1028 (@graven)   
  


### Enhancements

- Fixed MuonMatch isolation computation & clusters return value, !1126 (@masantim)   
  - Renamed isolation and changed its computation to handle events with 2 matched stations only, as now implemented in lhcb/Rec!1076  
  - Prevented the return value to be NaN in the ncl=0 (i.e. no hits) case

- Add Append to ChargeProtoParticleMaker, !1119 (@lgarciam) [LHCBTRACK-21]  
  Add the possibility of appending instead of overwriting the new protoparticles if the directory already exists  

- Fix PrepareMuonHits and add 1D matrix inversion for isolation, !1076, !1011 (@rvazquez)   
  - Add the times to the muonCoords created which were dropped from CommonMuonHitManager.  
  - Add inversion of 1-D matrix which is needed by the isolation algorithm  

- Move CommonMuonHitManager files to 'MuonID/src/component'. Create interface ICommonMuonHitManager., !1099 (@mramospe)   
  Proposal of interface for the CommonMuonHitManager class following discussion in !1098

- Implement isolation MuonChi2MatchTool and add a fast clustering tool, !1018 (@masantim)   
  Isolation:  
  - computed within MuonChi2MatchTool with no timing degradation  
  - added a method to get the result and modified IMuonMatchTool accordingly MuonClusterRec2:  
  - New tool, negligible timing impact, not called at the moment

- Perform VPClustering inside MCLinking sequence using full VPCluster, !1117 (@rquaglia)   
  - Introduce back a VPClusFull algorithm which does the equivalent of the "Offline" Velo clustering [it also sorts the hits by phi] and produces the Offsets as done for tracking in TDR branch.  
  - Also modify the linking sequence to run instead of ```VPCluster2MCParticleLinker``` the new ```VPFullCluster2MCParticleLinker```.  Inside ```PrLHCbID2MCParticle``` try first to get if exist the ```vector<VPFullCluster>```, if it fails it tries to read ```vector<VPLightCluster>``` to produce the link of All LHCbID to MCParticle.   
  - MR linked to lhcb/LHCb!1396 and lhcb/Lbcom!263  

- ANNPID Add Upgrade tune, !1112 (@jonrob) [LHCBGAUSS-1242]  
  Adds a new `MCUpTuneV1` for the upgrade.   
  Based on the MC samples generated under https://its.cern.ch/jira/browse/LHCBGAUSS-1242  
  Performance information can be found under https://www.hep.phy.cam.ac.uk/~jonesc/lhcb/PID/ANN/MCUpTuneV1  

- Added template parameter for floating point type, as for Trajectory, !1110 (@wouter)   
  Needed for lhcb/Phys!376

- Add PT cut to PrFitFwdParams, !961 (@decianm)   


- Speed-up PrStoreFtHit, !1072 (@jvantilb)   
  Also, the MultiIndexedHitContainer is moved to LHCb project (lhcb/LHCb!1326).  

- Use bulk Rich PD cluster global positions method, !1075 (@jonrob)   
  Make use of the new more efficient bulk conversion method defined in the interface update in lhcb/LHCb!1330 and implemented in lhcb/Lbcom!248  
  - Due to changes in the way the pixel positions are calculated, the resulting positions are not bit-wise identical to before.   
  - A number of algorithms have been removed from the pixel processing sequence, as the calls to compute the global and local space points for each cluster are now incorporated into the SIMD pixel summary algorithm, for improved efficiency reasons.

- RichDAQ - Cleans up a number of HPD/PMT specific definitions, !1074 (@jonrob)   
  Requires lhcb/LHCb!1329

- Addition of reference nodes to TrackVectorFitter, !1055 (@dcampora)   
  See lhcb/Rec!1050 for the discussion leading to this MR.

- RichDetailedTrSegMakerFromTracks - Disable by default CreateMissingStates, !1050 (@jonrob)   
  Changes the default setting of the `RichDetailedTrSegMakerFromTracks` property `CreateMissingStates` from True to False.  
  Makes explicit the need to provide track states expected by the RICH reconstruction   
  See MR description for detailed discussion of this change.     

- Added option for a faster Fast sequence for VectorFitter, !1073 (@dcampora)   

- Rich CK Resolution Fitter : Add new signal forms, !1041 (@jonrob)   
  Cherry picked from lhcb/Rec!1040 on `run2-patches` branch    
  c1d9503e Add Asymmetric Normal signal form    
  c5ce7094 Add skewed normal signal form    
  aba1e730 Avoid some string comparisons and code duplication

- SIMDChi2Fit: update code optimizer settings for newer gcc versions, !953 (@mschille)   

- Rich improved upgrade backgrounds, !1030 (@jonrob)   
  Improves the PID performance for upgrade data by better tuning the pixel background estimates.

- Avoid pessimizing move, !1013 (@graven)   
  Do not use `std::move` when returning a local object -- as that forbids 'copy elision', which will actually ensure that the object is created 'in the right location' to begin with, thus eliminating a move...

- Add Deuteron ANNPID, !998 (@jonrob)   
  cherry picked from lhcb/Rec!997 on `2018-patches` branch 
  Adds support for `MC15TuneV1` versions of ANNPID networks for Deuterons.

- Fully qualify enums in Tr/TrackFitEvent and improve searching for TT Hits, !985 (@graven)   
  In addition, make less assumptions on the return type of `fitNodes()`  

- Propagate MR !942 changes related to HDKerasModel from 2018-patches to master, !991 (@kgizdov)   
  make private layers accessible by inheritance - fixes lhcb/Phys!301

- Improvements on PrStoreUTHit, !937 (@mhadji)   
  This MR needs lhcb/LHCb!1163  
  Gains around ~55% on PrStoreUTHit according to callgrind. See MR description for discussion of this change  

- Added linking to PartProp library, !975 (@philten)   
  Made necessary lhcb/LHCb!1189


### Bug fixes

- Protect against NaN in PrPixelTrack::zBeam() in the case that tx=ty=0, !1093 (@olupton) [LHCBTRACK-22]  
  Return the mean hit position in Z in this case  

- Protect from division by zero in PrCounter2, !1088 (@sstahl)   
  
- Fix multithread reproducibility in TMVA generated code used by PrLongLivedTracking, !1054 (@graven)   
  Closes #18

- PrLongLivedTracking: remove use of `static` in MLP, !1053 (@graven)   
  Prevents memory corruption in multithreaded running

- TrackCheckerNT: fix gcc7 warning, !1045 (@cattanem)   
  
- Fix lambda captures, !1014 (@graven)   
  
- Correctly set the RichPID container version, !1006 (@jobrob)   
  Cherry-picked from lhcb/Rec!1000 on `2018-patches`  

- Avoid memory leaks in ParamKalman, !973, !976 (@sstemmle)   
  Related to: lhcb/Rec#15  

### Code modernisations and cleanups

- Calo tools modernization - mainly changing counters, !1109 (@wkrzemie)   

- Remove unused typedefs (exposed by gaudi/Gaudi!408), !1132 (@cattanem)   
  
- Use std::invoke, !1131 (@graven)   
  
- Prefer std::optional over boost::optional, !1130 (@graven)   
  
- Prepare for const RawEvent, const RawBank, !1124 (@graven)   
  
- Porting CaloElectronAlg to GaudiFunctional, !1116 (@aszabels)   

- Modernize counters in CaloSinglePhotonAlg, !1108 (@wkrzemie)   
  Replace old counters by new ones in CaloSinglePhotonAlg. Also, remove empty calls to some tools.

- Add runtime dependency on BLAS, !1097 (@clemenci)   
  required by ROOT TMVA since 6.14

- Fully qualify enums, !1096, !1071, !1070, !1069, !1068, !1067, !1066, !1065, !1064, !1063, !1062, !1061, !1046, !1060, !1059, !1058, !1057, !1056, !993, !989, !988, !987, !986, !984, !983, !982, !980, !960 (@graven)   

- Make CaloClusterizationTool const, !1036 (@cmarinbe)   
  Changes the `CaloClusterizationTool` to make it `const` and propagates the changes to the users: `CellularAutomatonAlg` and `L0Calo2CaloTool`  
  The interface of the `CaloClusterizationTool` has been updated accordingly, see lhcb/LHCb!1295
  
- Follow lhcb/LHCb!1264, !1022 (@graven)   
  
- Remove AlignedAllocator.h from Rec and its include in  PrPixel, !1026 (@chasse)   
  This was a leftover from previous experiments, which has been cleaned up in TDR before but hasn't been removed here.

- Replace AnyDataHandle with DataObjectReadHandle, to prepare for gaudi/Gaudi!671, !1027 (@graven)   
  
- Small cleanup of MergeRichPIDs, !1012 (@jonrob)   
  - Prefer `#pragma once` to `#ifdef/#define`  
  - Reserve complete size for output PIDs once.  
  - Add a version check and warning if input version != output version.

- Prefer enums over strings to change behaviour, !1046 (@graven)

- Prefer inheriting constructors, !1046, !981, !980, !978 (@graven)  

- Prefer compiler-generated destructors, !1046, !981, !980, !978 (@graven)  

- Prefer Gaudi::Property over explicit calls to declareProperty, !1046, !981, !980, !978 (@graven)  

- Prefer ToolHandle over calls to tool<>, !1046, !980, !978 (@graven)  

- Prefer inheriting from `extends` instead of explicit virtual inheritance !981, !980 (@graven)  

- Prefer direct member initialization, !981, !980 (@graven)  

- Prefer range-based for loops, !981, !980 (@graven)  

- Prefer freestanding function in anonymous namespace over member function which does not access class data, !1046, !980 (@graven)  

- Modernize TrackMonitors, !980 (@graven)   
  - prefer static data in anonymous namespace in .cpp file over static data in local scope (which incurs an atomic check whether the data has (already) been initialized, as that must be done in a thread-safe manner, every time the data is accessed)  
  - prefer std::array::fill over std::fill

- Modernize PatPV, !978 (@graven)   
  - make IPVOfflineTool interface const-correct so it can be used with ToolHandles  
  - prefer STL algorithms over explicit loops  
  - generalize PVOfflineTool::removeTracks to avoid a temporary memory allocation  

- Modernize TrackFitter, !981 (@graven)   
  - do not hardwire the return type of `fitNodes()`  
  - prefer std::find_if on reverse range instead of explicit forward loop, remembering the 'last' valid item  
  - do not cache `msgLevel`, as `msgLevel` already is cached  
  - prefer lambda over stateless struct which provide call operator

- Use gaudi automated toolhandle retrieve, !980, !977 (@graven)   
  When a `ToolHandle` is constructed such that there is a corresponding property, Gaudi will implicitly retrieve the tool (unless explicitly disabled) after `::initialize()` is invoked

 


### Monitoring changes

- PVChecker for PR2, !1107 (@adudziak)   

- Rich Monitoring - Add various new plots, !1020 (@jonrob)   
  - Restores some MC performance plots not yet ported over to the new RichFuture framework.
  - Adds a plot showing the average CK theta resolution, for true photons, as a function of P.  


### Changes to tests

- Update reference for use with gcc7, !1049 (@cattanem)   

