2017-08-28 RecSys v21r6
---
This version uses Gaudi v28r2, LHCb v42r6 and Lbcom v20r6 (and LCG_88 with ROOT 6.08.06)

This version is released on `2017-patches` branch. 


## New features

**[MR !707] Add the MVATool to compute and store a BDT variable in the MuonPID**  

**[MR !724] RichCKResolutionFitter - Add hyperbolic background option**  
For use online during the refractive index calibration  

**[MR !691] Add standalone CK resolution fitter utility**  
Allows Cherenkov resolution fit used in Panoptes for the online refractive index calibration to be reused in other scenarios, like DQ monitoring or the alignment  

## Code improvements
**[MR !668, !674, !682] Refactor of TrackVectorFit**  
No changes to physics performance

**[MR !646] Fix code ndentation warnings in yPID**


## Bug fixes

**[MR !707] Fix bug in MuonID upgrade configuration, that was overestimating the efficiency**  

**[MR !700] Fix TrackStateProvider trackID method to only use track memory address**  
Improves the way the unique track ID is computed. The previous implementation did not always produce a unique ID  

## Monitoring changes

**[MR !693] Reduce CaloReco verbosity**  

**[MR !676] Add RICH per PD Cherenkov resolution plots**  
Disabled by default    


## Changes to tests

**[MR !703, !656] Increase lumi9mergesmallfiles VMEM test failure threshold to 1.5 GB, to avoid false alarms**  
